package com.fundcru.signin;

import static org.testng.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Login;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.objects.pagedefinitions.TextHints;
import com.fundcru.objects.pagedefinitions.TextLabels;
import com.fundcru.utilities.BaseTestCaseNoCredential;
import com.fundcru.utilities.DriverCreator;

public class ForgotPassword extends BaseTestCaseNoCredential {
	
	  @Test
	  public void testForgotPassword() {	  
		WebElement signInLink =(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.SIGN_IN_LINK_ID)));
		signInLink.click();
	    driver.findElement(By.id(Home.SIGN_IN_LINK_ID)).click();
	    WebElement signInHeader =(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Login.SIGNIN_HEADER_LABEL)));
	    signInHeader.isDisplayed();
	    driver.findElement(By.id(Login.FORGOT_PASSWORD)).click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Assert text labels
		String headerLabel = driver.findElement(By.cssSelector(Login.SIGNIN_HEADER_LABEL)).getText();
		Assert.assertTrue(headerLabel.equals(TextLabels.HEADER_RESET_PASSWORD_SCREEN), "Wrong header displays " + headerLabel);
		String emailLabel = driver.findElement(By.cssSelector(Login.EMAIL_TEXT_FIELD_LABLE)).getText();
		Assert.assertTrue(emailLabel.equals(TextLabels.EMAIL_FIELD_LABLE_SIGNIN), "Wrong label for email displays " + emailLabel);
		String dontHaveAccountText = driver.findElement(By.cssSelector(Login.DONT_HAVE_ACCOUNT)).getText();
		Assert.assertTrue(dontHaveAccountText.equals(TextLabels.DONT_HAVE_ACCOUNT), "Wrong text displays " + dontHaveAccountText);
		String verificationEmailLostText = driver.findElement(By.cssSelector(Login.VERIFICATION_EMAIL_LOST)).getText();
		Assert.assertTrue(verificationEmailLostText.equals(TextLabels.VERIFICATION_EMAIL_LOST), "Wrong text displays " + verificationEmailLostText);
		
		// Assert text hints
		String emailPlaceholder=driver.findElement(By.id(Login.EMAIL_TEXT_FIELD_ID)).getAttribute("placeholder");
		assertEquals(TextHints.EMAIL, emailPlaceholder);
		
		// Assert all buttons displayed
		driver.findElement(By.id(Login.SIGNIN_BUTTON_ID)).isDisplayed();
		driver.findElement(By.id(Login.CANCEL)).isDisplayed();
		
		// Assert all hyper links displayed
		driver.findElement(By.id(Login.REGISTER)).isDisplayed();
		driver.findElement(By.id(Login.SEND_AGAIN)).isDisplayed();
		 
		// Click button Reset Email Link but doesn't provide the email
		driver.findElement(By.id(Login.SIGNIN_BUTTON_ID)).click();
	    driver.findElement(By.cssSelector(Login.EMAIL_VALIDATION_MESSAGE)).getText().equalsIgnoreCase(Messages.REQUIRED_FIELD);
	    
	    // Enter an invalid email
	    driver.findElement(By.id(Login.EMAIL_TEXT_FIELD_ID)).sendKeys("tt.myantran@gmailtet");
	    driver.findElement(By.id(Login.SIGNIN_BUTTON_ID)).click();
	    driver.findElement(By.cssSelector(Login.EMAIL_VALIDATION_MESSAGE)).getText().equalsIgnoreCase(Messages.INVALID_EMAIL);
	    driver.findElement(By.id(Login.EMAIL_TEXT_FIELD_ID)).clear();
	    
	    // Enter an email that does not exist
	    driver.findElement(By.id(Login.EMAIL_TEXT_FIELD_ID)).sendKeys("audrey@abc.de");
	    driver.findElement(By.id(Login.SIGNIN_BUTTON_ID)).click();
	    try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    driver.findElement(By.cssSelector(Login.MESSAGE)).getText().equalsIgnoreCase(Messages.NOT_EXIST_EMAIL);
	    driver.findElement(By.id(Login.EMAIL_TEXT_FIELD_ID)).clear();
	    
	    // Click Cancel button, system will go to Sign In screen
	    driver.findElement(By.id(Login.CANCEL)).click();
		String headerLabel2 = driver.findElement(By.cssSelector(Login.SIGNIN_HEADER_LABEL)).getText();
		Assert.assertTrue(headerLabel2.equals(TextLabels.HEADER_SIGNIN), "Wrong header displays " + headerLabel2);

		// Click on forgot password link again
		driver.findElement(By.id(Login.FORGOT_PASSWORD)).click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Enter valid email to process resetting password
		driver.findElement(By.id(Login.EMAIL_TEXT_FIELD_ID)).sendKeys("fundcrutest@gmail.com");
	    driver.findElement(By.id(Login.SIGNIN_BUTTON_ID)).click();
	    WebElement successMessage =(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Login.RESET_PASSWORD_EMAIL_SUCCESS_MESSAGE)));
	    successMessage.getText().equalsIgnoreCase(Messages.SUCCESS_INFOR_EMAIL_SENT);
	    try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    WebElement Popular_campaigns =(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Home.Popular_campaigns)));
	    Popular_campaigns.isDisplayed();
	  }
	  
	  @Test(dependsOnMethods ="testForgotPassword" )
		public void checkEmail() throws Exception {
		//check email for the link to reset password 
		  DriverCreator driverCreator = new DriverCreator(getProperty(TEST_BROWSER),getProperty(TEST_SERVER_FLATFORM));
			driver = driverCreator.getWebDriver();
			driver.navigate().to("https://www.google.com.vn");
			WebElement signInButton =(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.id(Login.gmailSignInButton_id)));
			signInButton.click();
			WebElement gmailId =(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.id(Login.gmailId)));
			gmailId.sendKeys("fundcrutest@gmail.com");
			driver.findElement(By.cssSelector(Login.gmailNextButton)).click();
			WebElement gmailPassword =(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Login.gmailPassword)));
			gmailPassword.sendKeys("abc123456abc");
			driver.findElement(By.cssSelector(Login.gmailPasswordNextButton)).click();
			WebElement gmailLogedIcon =(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Login.gmailLogedIcon2)));
			gmailLogedIcon.isDisplayed();
			driver.findElement(By.cssSelector(Login.gmailButton)).click();
			WebElement gmailPrimaryTab =(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.id(Login.gmailPrimaryTabId)));
			gmailPrimaryTab.click();

			// talking un-read email form inbox into a list
			WebElement unreademail =(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@class='bog']")));
			List<WebElement> unreademailList = driver.findElements(By.xpath("//*[@class='bog']"));
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// Mailer name for which i want to check if have an email in my inbox 
			String MyMailer = "[Fundcru] Verify Your Email";
			
			// real logic starts here
			for(int i=0;i<unreademailList.size();i++){
			    if(unreademailList.get(i).isDisplayed()==true){
			        // now verify if you have got mail form a specific mailer (Note Un-read mails)
			        // for read mails xpath loactor will change but logic will remain same
			        if(unreademailList.get(i).getText().startsWith(MyMailer)){
			            // open Fundcru email, then click the link for reseting password
			            System.out.println("Yes we have got mail from Fundcru to " + MyMailer);
			            unreademailList.get(i).click();
			    		java.util.List<WebElement> links = driver.findElements(By.tagName("a"));
			    		for (int j = 1; j<=links.size(); j=j+1)
			    		{
			    			if(links.get(j).getText().startsWith("https://user12.fundcru.com/reset-password/")){
			    				try {
			    					Thread.sleep(1000);
			    				} catch (InterruptedException e) {
			    					// TODO Auto-generated catch block
			    					e.printStackTrace();
			    				}
			    				// Navigate to Reset Password screen
			    				driver.navigate().to(links.get(j).getText());
			    				
			    				// Assert text labels in Reset Password screen
			    				WebElement headerLabel =(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Login.HEADER_RESET_PW)));
			    				Assert.assertTrue(headerLabel.getText().equals(TextLabels.HEADER_RESET_PW), "Wrong header displays " + headerLabel);
			    				String firstPasswordLabel = driver.findElement(By.cssSelector(Login.FIRSTPASSWORD_TEXT_FIELD_LABLE_RESET_PW)).getText();
			    				Assert.assertTrue(firstPasswordLabel.equals(TextLabels.FIRST_PASSWORD_RESET_PW), "Wrong label for the first password field displays " + firstPasswordLabel);
			    				String secondPasswordLabel = driver.findElement(By.cssSelector(Login.SECONDPASSWORD_TEXT_FIELD_LABLE_RESET_PW)).getText();
			    				Assert.assertTrue(secondPasswordLabel.equals(TextLabels.SECOND_PASSWORD_RESET_PW), "Wrong label for the second password field displays " + secondPasswordLabel);
			    				
			    				// Assert text hints in Reset Password screen
			    				String firstPasswordPlaceholder=driver.findElement(By.id(Login.FIRSTPASSWORD_TEXT_FIELD_RESET_PW)).getAttribute("placeholder");
			    				assertEquals(TextHints.FIRST_PASSWORD_RESET_PW, firstPasswordPlaceholder);
			    				String secondPasswordPlaceholder=driver.findElement(By.id(Login.SECONDPASSWORD_TEXT_FIELD_RESET_PW)).getAttribute("placeholder");
			    				assertEquals(TextHints.SECOND_PASSWORD_RESET_PW, secondPasswordPlaceholder);
			    				
			    				// Assert all buttons displayed in Reset Password screen
			    				driver.findElement(By.id(Login.SETPASSWORD_BUTTON_RESET_PW)).isEnabled();
			    				driver.findElement(By.id(Login.CANCEL_BUTTON_RESET_PW)).isEnabled();
			    				
			    				// Check error when clicking Set Password Button without any fields entered
			    				driver.findElement(By.id(Login.SETPASSWORD_BUTTON_RESET_PW)).click();
			    				String errorPassword = driver.findElement(By.cssSelector(Login.FIRSTPASSWORD_ERROR_RESET_PW)).getText();
			    				Assert.assertTrue(errorPassword.equals(Messages.REQUIRED_FIELD), "Password - wrong error message" + errorPassword);
			    				String errorPasswordAgain = driver.findElement(By.cssSelector(Login.SECONDPASSWORD_ERROR_RESET_PW)).getText();
			    				Assert.assertTrue(errorPasswordAgain.equals(Messages.REQUIRED_FIELD), "Second Password - wrong error message" + errorPasswordAgain);
			    				
			    				// Enter invalid data in the password fields 
			    				driver.findElement(By.id(Login.FIRSTPASSWORD_TEXT_FIELD_RESET_PW)).sendKeys("123");
			    				driver.findElement(By.id(Login.SETPASSWORD_BUTTON_RESET_PW)).click();
			    				// Check error
			    				String minimumrequiredlengthError = driver.findElement(By.cssSelector(Login.FIRSTPASSWORD_ERROR_RESET_PW)).getText();
			    				Assert.assertTrue(minimumrequiredlengthError.equals(Messages.MINIMUM_REQUIRED_LEAGTH_RESET_PW), "Password - wrong error message" + minimumrequiredlengthError);
			    				String equiredPassError = driver.findElement(By.cssSelector(Login.SECONDPASSWORD_ERROR_RESET_PW)).getText();
			    				Assert.assertTrue(equiredPassError.equals(Messages.REQUIRED_FIELD), "Second Password - wrong error message" + equiredPassError);
			    				
			    				driver.findElement(By.id(Login.SECONDPASSWORD_TEXT_FIELD_RESET_PW)).sendKeys("123");
			    				driver.findElement(By.id(Login.SETPASSWORD_BUTTON_RESET_PW)).click();
			    				// Check error
			    				String minimumrequiredlengthError1 = driver.findElement(By.cssSelector(Login.FIRSTPASSWORD_ERROR_RESET_PW)).getText();
			    				Assert.assertTrue(minimumrequiredlengthError1.equals(Messages.MINIMUM_REQUIRED_LEAGTH_RESET_PW), "Password - wrong error message" + minimumrequiredlengthError1);
			    				String minimumrequiredlengthError2 = driver.findElement(By.cssSelector(Login.SECONDPASSWORD_ERROR_RESET_PW)).getText();
			    				Assert.assertTrue(minimumrequiredlengthError2.equals(Messages.MINIMUM_REQUIRED_LEAGTH_RESET_PW), "Second Password - wrong error message" + minimumrequiredlengthError2);
			    				
			    				// Enter two different passwords in two fields
			    				driver.findElement(By.id(Login.FIRSTPASSWORD_TEXT_FIELD_RESET_PW)).clear();
			    				driver.findElement(By.id(Login.SECONDPASSWORD_TEXT_FIELD_RESET_PW)).clear();
			    				driver.findElement(By.id(Login.FIRSTPASSWORD_TEXT_FIELD_RESET_PW)).sendKeys("123456");
			    				driver.findElement(By.id(Login.SECONDPASSWORD_TEXT_FIELD_RESET_PW)).sendKeys("1234567");
			    				driver.findElement(By.id(Login.SETPASSWORD_BUTTON_RESET_PW)).click();
			    				// Check error
			    				String pwNotMatchError = driver.findElement(By.cssSelector(Login.SECONDPASSWORD_ERROR_RESET_PW)).getText();
			    				Assert.assertTrue(pwNotMatchError.equals(Messages.PASSWORD_DONT_MATCH_RESET_PW), "Second Password - wrong error message" + pwNotMatchError);

			    				// Enter valid new passwords in two fields
			    				driver.findElement(By.id(Login.FIRSTPASSWORD_TEXT_FIELD_RESET_PW)).clear();
			    				driver.findElement(By.id(Login.SECONDPASSWORD_TEXT_FIELD_RESET_PW)).clear();
			    				driver.findElement(By.id(Login.FIRSTPASSWORD_TEXT_FIELD_RESET_PW)).sendKeys("123456789");
			    				driver.findElement(By.id(Login.SECONDPASSWORD_TEXT_FIELD_RESET_PW)).sendKeys("123456789");
			    				driver.findElement(By.id(Login.SETPASSWORD_BUTTON_RESET_PW)).click();
			    				// Check success message
			    			    WebElement successMessage =(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Login.RESET_PASSWORD_EMAIL_SUCCESS_MESSAGE)));
			    			    successMessage.getText().equalsIgnoreCase(Messages.SUCCESS_PASSWORD_RESET_PW);
			    			    try {
			    					Thread.sleep(1000);
			    				} catch (InterruptedException e) {
			    					// TODO Auto-generated catch block
			    					e.printStackTrace();
			    				}
			    			    WebElement Popular_campaigns =(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Home.Popular_campaigns)));
			    			    Popular_campaigns.isDisplayed();
			    				WebElement userLink =(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Home.LOGINED_USER_LINK)));
			    				userLink.isDisplayed();
					            break;
			    			}
			    		}
			        }else{
			            System.out.println("No mail from Fundcru to " + MyMailer);
			        }
			    }
			}

		}
	}