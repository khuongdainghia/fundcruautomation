package com.fundcru.signin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Login;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;

@Credentials(user="fundcrutest2@gmail.com", password="123456789")
public class SignOut extends BaseTestCase {
	@Test
	public void Login() throws Exception {
	driver.navigate().to(getServerURL());
	}
	
	@Test(dependsOnMethods ="Login" )
	public void TestSignOut() {
	WebElement loggedInIcon =(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Home.LOGINED_USER_LINK)));
	loggedInIcon.click();
	WebElement logOutButton =(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Home.LOGOUT_USER_DROPDOWN_ITEM)));
	logOutButton.click();
	try {
		Thread.sleep(1000);
	} catch (InterruptedException e) {
		e.printStackTrace();
	}
	driver.findElement(By.cssSelector(Home.CREATE_A_FUNDRAISER)).click();
	String errorMessage = driver.findElement(By.cssSelector(Login.MESSAGE)).getText();
	Assert.assertTrue(errorMessage.equals(Messages.WARNING_FOR_LOGIN),"Unexpected"+ errorMessage);
	}
}
