package com.fundcru.signin;

import static org.testng.Assert.assertEquals;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.campaigns.AddDraftCampaign;
import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Login;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.objects.pagedefinitions.SignUp;
import com.fundcru.objects.pagedefinitions.TextHints;
import com.fundcru.objects.pagedefinitions.TextLabels;
import com.fundcru.utilities.BaseTestCaseNoCredential;

public class SignUpAction extends BaseTestCaseNoCredential {	
	WebElement signUpHeader ;
	
	@Test
	public void ClickSignInLink() throws Exception {
		WebElement signInLink =(new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.SIGN_IN_LINK_ID)));
		signInLink.click();
		signInLink.isSelected();
	}
	
	@Test(dependsOnMethods ="ClickSignInLink")
	public void ClickSignUpLink() throws Exception {
		WebElement signUpLink =(new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.id(Login.REGISTER)));
		signUpLink.click();
	}
	
	@Test(dependsOnMethods ="ClickSignUpLink")
	public void SignUpVerification() {
		// Assert text labels
		signUpHeader = (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(SignUp.HEADER_LABEL)));
		String headerLabel = driver.findElement(By.cssSelector(SignUp.HEADER_LABEL)).getText();
		Assert.assertTrue(headerLabel.equals(TextLabels.HEADER_SIGNUP), "Wrong header displays" + headerLabel);
		String firstNameLabel = driver.findElement(By.cssSelector(SignUp.FIRST_NAME_LABEL)).getText();
		Assert.assertTrue(firstNameLabel.equals(TextLabels.FIRSTNAME_FIELD_LABLE_SIGNUP), "Wrong label for first name displays" + firstNameLabel);
		String lastNameLabel = driver.findElement(By.cssSelector(SignUp.LAST_NAME_LABEL)).getText();
		Assert.assertTrue(lastNameLabel.equals(TextLabels.LASTNAME_FIELD_LABLE_SIGNUP), "Wrong label for last name displays" + lastNameLabel);
		String emailLabel = driver.findElement(By.cssSelector(SignUp.EMAIL_LABEL)).getText();
		Assert.assertTrue(emailLabel.equals(TextLabels.EMAIL_FIELD_LABLE_SIGNUP), "Wrong label for email displays" + emailLabel);
		String password1Label = driver.findElement(By.cssSelector(SignUp.PASSWORD1_LABEL)).getText();
		Assert.assertTrue(password1Label.equals(TextLabels.FIRST_PASSWORD_FIELD_LABLE_SIGNUP), "Wrong label for the first password field displays" + password1Label);
		String password2Label = driver.findElement(By.cssSelector(SignUp.PASSWORD2_LABEL)).getText();
		Assert.assertTrue(password2Label.equals(TextLabels.SECOND_PASSWORD_FIELD_LABLE_SIGNUP), "Wrong label for the second password field displays" + password2Label);
		Assert.assertTrue(headerLabel.equals(TextLabels.HEADER_SIGNUP), "Wrong header displays" + headerLabel);
		Assert.assertTrue(lastNameLabel.equals(TextLabels.LASTNAME_FIELD_LABLE_SIGNUP), "Wrong label for last name displays" + lastNameLabel);
		Assert.assertTrue(emailLabel.equals(TextLabels.EMAIL_FIELD_LABLE_SIGNUP), "Wrong label for email displays" + emailLabel);
		Assert.assertTrue(password1Label.equals(TextLabels.FIRST_PASSWORD_FIELD_LABLE_SIGNUP), "Wrong label for the first password field displays" + password1Label);
		String orLabel = driver.findElement(By.cssSelector(SignUp.OR_LABEL)).getText();
		Assert.assertTrue(orLabel.equals(TextLabels.OR_TEXT_LABLE_SIGNUP), "Wrong header displays" + orLabel);
		
		// Assert text hints
		String firstNamePlaceholder=driver.findElement(By.id(SignUp.FIRST_NAME_FIELD)).getAttribute("placeholder");
		assertEquals(TextHints.FIRSTNAME, firstNamePlaceholder);
		String lastNamePlaceholder=driver.findElement(By.id(SignUp.LAST_NAME_FIELD)).getAttribute("placeholder");
		assertEquals(TextHints.LASTNAME, lastNamePlaceholder);
		String emailPlaceholder=driver.findElement(By.id(SignUp.EMAIL_FIELD)).getAttribute("placeholder");
		assertEquals(TextHints.EMAIL, emailPlaceholder);
		String password1Placeholder=driver.findElement(By.id(SignUp.PASSWORD_FIELD)).getAttribute("placeholder");
		assertEquals(TextHints.FIRST_PASSWORD, password1Placeholder);
		String password2Placeholder=driver.findElement(By.id(SignUp.PASSWORD_AGAIN_FIELD)).getAttribute("placeholder");
		assertEquals(TextHints.SECOND_PASSWORD, password2Placeholder);
		
		// Assert all buttons displayed
		driver.findElement(By.id(SignUp.REGISTER_BUTTON)).isEnabled();
		driver.findElement(By.id(SignUp.CANCEL_BUTTON)).isEnabled();
		driver.findElement(By.id(SignUp.REGISTER_WITH_FACEBOOK_BUTTON)).isEnabled();
		
		// Click Cancel button, Explore Fundraiser screen is displayed
		driver.findElement(By.id(SignUp.CANCEL_BUTTON)).click();
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement signInHeader =(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Login.SIGNIN_HEADER_LABEL)));
		
		// Step 5 Click Sign Up button again, the form for creating account should be appeared again
		WebElement signUpLink =(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.id(Login.REGISTER)));
		signUpLink.click();
		signUpHeader = (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(SignUp.HEADER_LABEL)));

		// Check error when clicking Sign Up without any fields entered
		driver.findElement(By.id(SignUp.REGISTER_BUTTON)).click();
		String errorFirstName = driver.findElement(By.cssSelector(SignUp.FIRSTNAME_ERROR)).getText();
		Assert.assertTrue(errorFirstName.equals(Messages.REQUIRED_FIELD), "First name - wrong error message" + errorFirstName);
		String errorLastName = driver.findElement(By.cssSelector(SignUp.LASTNAME_ERROR)).getText();
		Assert.assertTrue(errorLastName.equals(Messages.REQUIRED_FIELD), "Last name - wrong error message" + errorLastName);
		String errorEmail = driver.findElement(By.cssSelector(SignUp.EMAIL_ERROR)).getText();
		Assert.assertTrue(errorEmail.equals(Messages.REQUIRED_FIELD), "Email - wrong error message" + errorEmail);
		String errorPassword = driver.findElement(By.cssSelector(SignUp.PASSWORD_ERROR)).getText();
		Assert.assertTrue(errorPassword.equals(Messages.REQUIRED_FIELD), "Password - wrong error message" + errorPassword);
		String errorPasswordAgain = driver.findElement(By.cssSelector(SignUp.PASSWORD_ERROR)).getText();
		Assert.assertTrue(errorPasswordAgain.equals(Messages.REQUIRED_FIELD), "Second Password - wrong error message" + errorPasswordAgain);

		// Enter invalid data in the fields for signing up		
		driver.findElement(By.id(SignUp.FIRST_NAME_FIELD)).sendKeys("Audey");
		Assert.assertEquals("", driver.findElement(By.cssSelector(SignUp.FIRSTNAME_ERROR)).getText());

		driver.findElement(By.id(SignUp.LAST_NAME_FIELD)).sendKeys("Tran");
		Assert.assertEquals("", driver.findElement(By.cssSelector(SignUp.LASTNAME_ERROR)).getText());

		driver.findElement(By.id(SignUp.EMAIL_FIELD)).sendKeys("invalidEmail@");
		Assert.assertEquals("", driver.findElement(By.cssSelector(SignUp.EMAIL_ERROR)).getText());

		driver.findElement(By.id(SignUp.PASSWORD_FIELD)).sendKeys("pass123456@");
		Assert.assertEquals("", driver.findElement(By.cssSelector(SignUp.PASSWORD_ERROR)).getText());
		Assert.assertEquals(Messages.INVALID_EMAIL, driver.findElement(By.cssSelector(SignUp.EMAIL_ERROR)).getText());

		driver.findElement(By.id(SignUp.EMAIL_FIELD)).clear();
		driver.findElement(By.id(SignUp.EMAIL_FIELD)).sendKeys("ptvu.it@gmail.com");
		driver.findElement(By.id(SignUp.PASSWORD_AGAIN_FIELD)).sendKeys("pass123456");
		Assert.assertEquals("",driver.findElement(By.cssSelector(SignUp.PASSWORD_AGAIN_ERROR)).getText());
		driver.findElement(By.id(SignUp.PASSWORD_FIELD)).clear();
		driver.findElement(By.id(SignUp.PASSWORD_FIELD)).sendKeys("pass123456@");
		try {
			Thread.sleep(300);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		driver.findElement(By.id(SignUp.REGISTER_BUTTON)).click();
		Assert.assertEquals("", driver.findElement(By.cssSelector(SignUp.FIRSTNAME_ERROR)).getText());
		Assert.assertEquals("", driver.findElement(By.cssSelector(SignUp.LASTNAME_ERROR)).getText());
		Assert.assertEquals("", driver.findElement(By.cssSelector(SignUp.EMAIL_ERROR)).getText());
		Assert.assertEquals("", driver.findElement(By.cssSelector(SignUp.PASSWORD_ERROR)).getText());
		// Script will be failed at this step as the error message error.pwdsDontMatch display instead of correct error
//		Assert.assertEquals(Messages.PASSWORD_DONT_MATCH_SIGNUP, driver.findElement(By.cssSelector(SignUp.PASSWORD_AGAIN_ERROR)).getText());

		// Enter valid data in the fields but the email already used for signing up	
		driver.findElement(By.id(SignUp.PASSWORD_AGAIN_FIELD)).clear();
		driver.findElement(By.id(SignUp.PASSWORD_AGAIN_FIELD)).sendKeys("pass123456@");
		driver.findElement(By.id(SignUp.REGISTER_BUTTON)).click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String headerErrorMessage = driver.findElement(By.cssSelector(SignUp.HEADER_MESSAGE)).getText();
		Assert.assertEquals(Messages.ERROR_MESSAGE_SIGNUP, headerErrorMessage);
		
		// Enter the email that is new for signing up		
		Random rand = new Random(); 
		int randomNumber = rand.nextInt(2000000);
		driver.findElement(By.id(SignUp.EMAIL_FIELD)).clear();
		driver.findElement(By.id(SignUp.EMAIL_FIELD)).sendKeys("test"+randomNumber+"@gmail.com");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Scroll down the page
		JavascriptExecutor jsedown = (JavascriptExecutor)driver;
		jsedown.executeScript("window.scrollBy(0,200)", "");
		driver.findElement(By.id(SignUp.REGISTER_BUTTON)).click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Scroll up the page
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,-300)", "");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String successMessageSignUp = driver.findElement(By.cssSelector(SignUp.SIGNUP_SUCCESS_MESSAGE)).getText();
		Assert.assertEquals(Messages.SUCCESS_MESSAGE_SIGNUP, successMessageSignUp);
	}
}
