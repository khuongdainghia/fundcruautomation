package com.fundcru.signin;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Login;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.objects.pagedefinitions.TextHints;
import com.fundcru.objects.pagedefinitions.TextLabels;
import com.fundcru.utilities.BaseTestCaseNoCredential;

public class SignIn extends BaseTestCaseNoCredential {
	
	@Test
	public void ClickSignInLink() throws Exception {
		WebElement signInLink =(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.SIGN_IN_LINK_ID)));
		signInLink.click();
		signInLink.isSelected();
	}
	
	@Test(dependsOnMethods ="ClickSignInLink",alwaysRun = true )
	public void CheckLabel() {
		// Assert text labels
		String headerLabel = driver.findElement(By.cssSelector(Login.SIGNIN_HEADER_LABEL)).getText();
		Assert.assertTrue(headerLabel.equals(TextLabels.HEADER_SIGNIN), "Wrong header displays " + headerLabel);
		String emailLabel = driver.findElement(By.cssSelector(Login.EMAIL_TEXT_FIELD_LABLE)).getText();
		Assert.assertTrue(emailLabel.equals(TextLabels.EMAIL_FIELD_LABLE_SIGNIN), "Wrong label for email displays " + emailLabel);
		String passwordLabel = driver.findElement(By.cssSelector(Login.PASSWORD_TEXT_FIELD_LABLE)).getText();
		Assert.assertTrue(passwordLabel.equals(TextLabels.PASSWORD_FIELD_LABLE_SIGNIN), "Wrong label for the first password field displays " + passwordLabel);
		String orLabel = driver.findElement(By.cssSelector(Login.OR_LABLE)).getText();
		Assert.assertTrue(orLabel.equals(TextLabels.OR_TEXT_LABLE_SIGNIN), "Wrong text displays " + orLabel);
		String dontHaveAccountText = driver.findElement(By.cssSelector(Login.DONT_HAVE_ACCOUNT)).getText();
		Assert.assertTrue(dontHaveAccountText.equals(TextLabels.DONT_HAVE_ACCOUNT), "Wrong text displays " + dontHaveAccountText);
		String verificationEmailLostText = driver.findElement(By.cssSelector(Login.VERIFICATION_EMAIL_LOST)).getText();
		Assert.assertTrue(verificationEmailLostText.equals(TextLabels.VERIFICATION_EMAIL_LOST), "Wrong text displays " + verificationEmailLostText);
	}
	
	@Test(dependsOnMethods ="CheckLabel",alwaysRun = true )
	public void CheckHints() {
		// Assert text hints
		String emailPlaceholder=driver.findElement(By.id(Login.EMAIL_TEXT_FIELD_ID)).getAttribute("placeholder");
		assertEquals(TextHints.EMAIL, emailPlaceholder);
		String passwordPlaceholder=driver.findElement(By.id(Login.PASSWORD_TEXT_FIELD_ID)).getAttribute("placeholder");
		assertEquals(TextHints.FIRST_PASSWORD, passwordPlaceholder);
	}
	
	@Test(dependsOnMethods ="CheckHints",alwaysRun = true )
	public void CheckButtonDisplayed() {
		// Assert all buttons displayed
		driver.findElement(By.id(Login.SIGNIN_BUTTON_ID)).isEnabled();
		driver.findElement(By.id(Login.SIGNIN_WITH_FACEBOOK)).isEnabled();
		driver.findElement(By.id(Login.SIGNIN_WITH_GOOGLE)).isEnabled();
	}
	
	@Test(dependsOnMethods ="CheckButtonDisplayed",alwaysRun = true )
	public void CheckHyperLinks() {
		// Assert all hyper links displayed
		driver.findElement(By.id(Login.FORGOT_PASSWORD)).isEnabled();
		driver.findElement(By.id(Login.REGISTER)).isEnabled();
		driver.findElement(By.id(Login.SEND_AGAIN)).isEnabled();
	}
	
	@Test(dependsOnMethods ="CheckHyperLinks",alwaysRun = true )
	public void ClickSubmitWithoutAnyFieldsEntered() {
		// Check error after click Submit button without any fields entered
		driver.findElement(By.id(Login.SIGNIN_BUTTON_ID)).click();
	}
	
	@Test(dependsOnMethods ="ClickSubmitWithoutAnyFieldsEntered",alwaysRun = true )
	public void CheckErrorDisplayed() {
		WebElement errorMessage =(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Login.MESSAGE)));
		Assert.assertTrue(errorMessage.getText().equals(Messages.Login_failed), "Wrong message displays " + errorMessage);
	}
	
	@Test(dependsOnMethods ="CheckErrorDisplayed",alwaysRun = true )
	public void InputWrongPasswordToLogin() {
		// Check error when input wrong password for log in
		driver.findElement(By.id(Login.EMAIL_TEXT_FIELD_ID)).clear();	
		driver.findElement(By.id(Login.PASSWORD_TEXT_FIELD_ID)).clear();
		driver.findElement(By.id(Login.EMAIL_TEXT_FIELD_ID)).sendKeys("fundcrutest@gmail.com");
		driver.findElement(By.id(Login.PASSWORD_TEXT_FIELD_ID)).sendKeys("invalidpass");
	}
	
	@Test(dependsOnMethods ="InputWrongPasswordToLogin",alwaysRun = true )
	public void ClickSubmitII() {
		driver.findElement(By.id(Login.SIGNIN_BUTTON_ID)).click();
	}
	
	@Test(dependsOnMethods ="ClickSubmitII",alwaysRun = true )
	public void CheckErrorDisplayedAfterInputWrongPassword() {
		WebElement errorMessage =(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Login.MESSAGE)));
		Assert.assertTrue(errorMessage.getText().equals(Messages.Login_failed), "Wrong message displays " + errorMessage);
	}
	
	@Test(dependsOnMethods ="CheckErrorDisplayedAfterInputWrongPassword",alwaysRun = true )
	public void InputInvalidEmailForLogin() {
		// Enter an email that is not registered yet - error will appear
		driver.findElement(By.id(Login.EMAIL_TEXT_FIELD_ID)).clear();	
		driver.findElement(By.id(Login.PASSWORD_TEXT_FIELD_ID)).clear();
		driver.findElement(By.id(Login.EMAIL_TEXT_FIELD_ID)).sendKeys("ttt.myantran@gmail.com");
		driver.findElement(By.id(Login.PASSWORD_TEXT_FIELD_ID)).sendKeys("Chauyeuongbaba1");
	}
	
	@Test(dependsOnMethods ="InputInvalidEmailForLogin",alwaysRun = true )
	public void ClickSubmitIII() {
		driver.findElement(By.id(Login.SIGNIN_BUTTON_ID)).click();
	}
	
	@Test(dependsOnMethods ="ClickSubmitIII",alwaysRun = true )
	public void CheckErrorDisplayedAfterInputInvalidEmail() {
		WebElement errorMessage =(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Login.MESSAGE)));
		Assert.assertTrue(errorMessage.getText().equals(Messages.Login_failed), "Wrong message displays " + errorMessage);
	}
	
	@Test(dependsOnMethods ="CheckErrorDisplayedAfterInputInvalidEmail",alwaysRun = true )
	public void InputValidAccountForLogin() {
		// Enter valid account for log in - log in successfully
		driver.findElement(By.id(Login.EMAIL_TEXT_FIELD_ID)).clear();	
		driver.findElement(By.id(Login.PASSWORD_TEXT_FIELD_ID)).clear();	
		driver.findElement(By.id(Login.EMAIL_TEXT_FIELD_ID)).sendKeys("fundcrutest@gmail.com");
		driver.findElement(By.id(Login.PASSWORD_TEXT_FIELD_ID)).sendKeys("123456789");
	}
	
	@Test(dependsOnMethods ="InputValidAccountForLogin",alwaysRun = true )
	public void ClickSubmitIV() {
		driver.findElement(By.id(Login.SIGNIN_BUTTON_ID)).click();
	}
	
	@Test(dependsOnMethods ="ClickSubmitIV",alwaysRun = true )
	public void CheckUserLinkAfterLoginSuccessfully() {
		WebElement logInUserLink2 =(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Home.LOGINED_USER_LINK)));
		logInUserLink2.isDisplayed();
	}
}
