package com.fundcru.wepaygateway;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.DonateWepay;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;


@Credentials(user="khuongdainghia@gmail.com", password="loveyou")
public class DonateCampaignWithWePay extends BaseTestCase {
	int campaignRaised=0;
	int numberBeforeDonate;
	List<WebElement> numOfTestimonial;
	int timeout = 30;
	int numOfDonors;
	
	@Test
	public void NavigateToCampaignForDonation() {
		driver.navigate().to(getServerURL()+"/fundraisers/4PQxqA5xD7MKDYKs6/");
	}
	
	@Test(dependsOnMethods ="NavigateToCampaignForDonation",alwaysRun = true )
	public void GetCampaignRaised() {
		WebElement campaignRaisedElement= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CAMPAIGN_RAISED_WEPAY)));
		numOfTestimonial = driver.findElements(By.xpath(Campaign.TESTIMONIAL_LIST));
	    numberBeforeDonate = numOfTestimonial.size();
		campaignRaised= Integer.valueOf(campaignRaisedElement.getText().replace("$", "").replace(".00", ""));
		WebElement campaignDonors= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CAMPAIGN_DONORS)));
		numOfDonors = Integer.valueOf(campaignDonors.getText());
	}
	
	@Test(dependsOnMethods ="GetCampaignRaised",alwaysRun = true )
	public void ClickDonateButton() {
		WebElement donateButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Campaign.DONATE_BUTTON)));
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",donateButton);	
	}
	
	@Test(dependsOnMethods ="ClickDonateButton",alwaysRun = true )
	public void CheckErrorAfterSubmitInCardInformationScreenWithoutAnyFieldEntered() {
		WebElement submitButtonForCardInfor = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(DonateWepay.SUBMIT_CARD_INFORMATION_SCREEN)));
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",submitButtonForCardInfor);	
		String errorOnZipcode = driver.findElement(By.cssSelector(DonateWepay.ERROR_ON_ZIP_CODE)).getText();
		Assert.assertTrue(errorOnZipcode.equals(Messages.INVALID_ZIPCODE_WEPAY), "Zipcode - wrong error message" + errorOnZipcode);
		String errorOnCCV = driver.findElement(By.cssSelector(DonateWepay.ERROR_ON_CCV)).getText();
		Assert.assertTrue(errorOnCCV.equals(Messages.INVALID_CCV_WEPAY), "CCV - wrong error message" + errorOnCCV);
		String errorOnExpirationDate = driver.findElement(By.cssSelector(DonateWepay.ERROR_ON_EXPIRATION_DATE)).getText();
		Assert.assertTrue(errorOnExpirationDate.equals(Messages.INVALID_DATE_WEPAY), "Expiration Date - wrong error message" + errorOnExpirationDate);
		String errorOnCardNumberField = driver.findElement(By.cssSelector(DonateWepay.ERROR_ON_CARD_NUMBER)).getText();
		Assert.assertTrue(errorOnCardNumberField.equals(Messages.INVALID_CARD_NUMBER_WEPAY), "Card number - wrong error message" + errorOnCardNumberField);
		String errorOnEmailField = driver.findElement(By.cssSelector(DonateWepay.ERROR_ON_EMAIL)).getText();
		Assert.assertTrue(errorOnEmailField.equals(Messages.INVALID_EMAIL_WEPAY), "Email - wrong error message" + errorOnEmailField);
		String errorNameOnCard = driver.findElement(By.cssSelector(DonateWepay.ERROR_ON_NAME_ON_CARD)).getText();
		Assert.assertTrue(errorNameOnCard.equals(Messages.INVALID_NAME_ON_CARD_WEPAY), "Name on card - wrong error message" + errorNameOnCard);
		Assert.assertTrue(!driver.findElements(By.cssSelector(DonateWepay.ERROR_ON_AMOUNT)).isEmpty(),"There is no validation when don't input amount ");
		String errorAmount = driver.findElement(By.cssSelector(DonateWepay.ERROR_ON_AMOUNT)).getText();
		Assert.assertTrue(errorAmount.equals(Messages.INVALID_AMOUNT_WEPAY), "Amount - wrong error message" + errorAmount);    
	}
	@Test(dependsOnMethods ="CheckErrorAfterSubmitInCardInformationScreenWithoutAnyFieldEntered",alwaysRun = true )
	public void CheckErrorAfterInputInvalidEmail() {
		// Check error when input invalid card number
	    WebElement email = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.EMAIL)));
	    email.clear();
	    email.sendKeys("invalidEmail");
	    WebElement submitButtonForCardInfor = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(DonateWepay.SUBMIT_CARD_INFORMATION_SCREEN)));
	    JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",submitButtonForCardInfor);	
	    String errorInvalidEmail = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(DonateWepay.ERROR_ON_EMAIL))).getText();
		Assert.assertTrue(errorInvalidEmail.equals(Messages.INVALID_EMAIL_WEPAY), "Wrong error message for invalid email" + errorInvalidEmail);
	}
	
	@Test(dependsOnMethods ="CheckErrorAfterInputInvalidEmail",alwaysRun = true )
	public void CheckErrorAfterInputInvalidCardNumber() {
		// Check error when input invalid card number
	    WebElement cardNumber = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.CARD_NUMBER)));
	    cardNumber.clear();
	    cardNumber.sendKeys("1111112233");
	    WebElement submitButtonForCardInfor = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(DonateWepay.SUBMIT_CARD_INFORMATION_SCREEN)));
	    JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",submitButtonForCardInfor);	
	    String errorInvalidCardNumber = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(DonateWepay.ERROR_ON_CARD_NUMBER))).getText();
		Assert.assertTrue(errorInvalidCardNumber.equals(Messages.INVALID_CARD_NUMBER_WEPAY), "Wrong error message for invalid card number" + errorInvalidCardNumber);
	}
	
	@Test(dependsOnMethods ="CheckErrorAfterInputInvalidCardNumber",alwaysRun = true )
	public void CheckErrorAfterInputInvalidCCV() {
		// Check error when input invalid CCV
	    WebElement ccv = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.CVV)));
	    ccv.clear();
	    ccv.sendKeys("invalidCCV");
	    WebElement submitButtonForCardInfor = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(DonateWepay.SUBMIT_CARD_INFORMATION_SCREEN)));
	    JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",submitButtonForCardInfor);	
	    String errorInvalidCCV = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(DonateWepay.ERROR_ON_CCV))).getText();
		Assert.assertTrue(errorInvalidCCV.equals(Messages.INVALID_CCV_WEPAY), "Wrong error message for invalid CCV" + errorInvalidCCV);
	}
	
	@Test(dependsOnMethods ="CheckErrorAfterInputInvalidCCV",alwaysRun = true )
	public void CheckErrorAfterInputInvalidExpirationDate() {
		// Check error when input invalid EXPIRATION date
	    WebElement expiration = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.EXPIRATION)));
	    expiration.clear();
	    expiration.sendKeys("invalidExpiration");
	    WebElement submitButtonForCardInfor = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(DonateWepay.SUBMIT_CARD_INFORMATION_SCREEN)));
	    JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",submitButtonForCardInfor);	
	    String errorInvalidExpiration = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(DonateWepay.ERROR_ON_EXPIRATION_DATE))).getText();
		Assert.assertTrue(errorInvalidExpiration.equals(Messages.INVALID_DATE_WEPAY), "Wrong error message for invalid EXPIRATION date" + errorInvalidExpiration);
	}
	
	@Test(dependsOnMethods ="CheckErrorAfterInputInvalidExpirationDate",alwaysRun = true )
	public void CheckErrorAfterInputInvalidNameOnCard() {
		WebElement amount = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.AMOUNT)));
	    amount.clear();
	    amount.sendKeys("10");
		WebElement email = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.EMAIL)));
		email.clear();
		email.sendKeys("usr1001@fundcru.com");
		WebElement cardNumber = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.CARD_NUMBER)));
	    cardNumber.clear();
	    cardNumber.sendKeys("4003830171874018");
	    WebElement ccv = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.CVV)));
	    ccv.clear();
	    ccv.sendKeys("123");
	    WebElement expiration = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.EXPIRATION)));
	    expiration.clear();
	    expiration.sendKeys("11/22");
		WebElement zipcode = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.ZIPCODE)));
		zipcode.clear();
		zipcode.sendKeys("12345");
		
		// Check error when input invalid name on card
	    WebElement nameOnCard = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.DONATE_NAME)));
	    nameOnCard.clear();
	    nameOnCard.sendKeys("i");
	    WebElement submitButtonForCardInfor = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(DonateWepay.SUBMIT_CARD_INFORMATION_SCREEN)));
	    JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",submitButtonForCardInfor);	
	    String errorInvalidNameOnCard = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(DonateWepay.ERROR_ON_NAME_ON_CARD_FROM_WEPAY))).getText();
		Assert.assertTrue(errorInvalidNameOnCard.contains(Messages.INVALID_NAME_ON_CARD_WEPAY_GATEWAY), "Wrong error message for invalid name on card" + errorInvalidNameOnCard);
	}
	
	@Test(dependsOnMethods ="CheckErrorAfterInputInvalidNameOnCard",alwaysRun = true )
	public void ReInPutCardNumber() {
		WebElement cardNumber = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.CARD_NUMBER)));
	    cardNumber.clear();
	    cardNumber.sendKeys("4003830171874018");
	}
	
	@Test(dependsOnMethods ="ReInPutCardNumber",alwaysRun = true )
	public void ReInPutCCV() {
		WebElement ccv = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.CVV)));
	    ccv.clear();
	    ccv.sendKeys("123");
	}
	
	@Test(dependsOnMethods ="ReInPutCCV",alwaysRun = true )
	public void ReInPutExpirationDate() {
		WebElement expiration = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.EXPIRATION)));
	    expiration.clear();
	    expiration.sendKeys("11/22");
	}
	
	@Test(dependsOnMethods ="ReInPutExpirationDate",alwaysRun = true )
	public void ReInPutNameOnCard() {
		WebElement nameOnCard = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.DONATE_NAME)));
	    nameOnCard.clear();
	    nameOnCard.sendKeys("John Doe");
	}
	@Test(dependsOnMethods ="ReInPutNameOnCard",alwaysRun = true )
	public void ReInPutEmail() {
		WebElement email = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.EMAIL)));
		email.clear();
		email.sendKeys("usr1001@fundcru.com");
	}
	@Test(dependsOnMethods ="ReInPutEmail",alwaysRun = true )
	public void InputDonateAmount() {
		WebElement amount = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.AMOUNT)));
	    amount.clear();
	    amount.sendKeys("10");
	}
	@Test(dependsOnMethods ="InputDonateAmount",alwaysRun = true )
	public void InputZipcode() {
		WebElement zipcode = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.ZIPCODE)));
		zipcode.clear();
		zipcode.sendKeys("12345");
	}
	@Test(dependsOnMethods ="InputZipcode",alwaysRun = true )
	public void SubmitAfterInputCardInformation() {
		WebElement submitButtonForCardInfor = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(DonateWepay.SUBMIT_CARD_INFORMATION_SCREEN)));
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",submitButtonForCardInfor);	
	}

	@Test(dependsOnMethods ="SubmitAfterInputCardInformation")
	public void EnterDonateName() {
	    WebElement donateName = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.DONATE_NAME_ON_TESTIMONIAL )));
	    donateName.clear();
	    try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    donateName.sendKeys("Nicholas");
	}
	
	@Test(dependsOnMethods ="EnterDonateName")
	public void EnterTestimonial() {
		WebElement donateTestimonial = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.DONATE_MESSAGE_ON_TESTIMONIAL )));
		donateTestimonial.clear();
		donateTestimonial.sendKeys("Testimonial");
	}
	
	@Test(dependsOnMethods ="EnterTestimonial",alwaysRun = true)
	public void ClickShareButton() {
	    driver.findElement(By.cssSelector(DonateWepay.SHARE_BUTTON)).click();
	}
	
	@Test(dependsOnMethods ="ClickShareButton",alwaysRun = true )
	public void CheckCampaignTitleAfterDonation() {
		//check result here 
		WebElement campaignTitle = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.TITLE_SUCCESS_WEPAY)));
		Assert.assertEquals(campaignTitle.getText(),"College fund");
	}
	
	@Test(dependsOnMethods ="CheckCampaignTitleAfterDonation" )
	public void CheckGoalAfterDonation() {
		WebElement campaignGoal = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.GOAL_SUCCESS_WEPAY)));
		Assert.assertEquals(campaignGoal.getText(),"$1000.00");
	}
	
	@Test(dependsOnMethods ="CheckGoalAfterDonation" )
	public void CheckCampaignRaisedAfterDonation() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement campaignRaisedElement= driver.findElement(By.cssSelector(Campaign.CAMPAIGN_RAISED_WEPAY));
		Assert.assertEquals(campaignRaisedElement.getText(),String.format("$%s.00",(campaignRaised+10)));
	}
	
	@Test(dependsOnMethods ="CheckCampaignRaisedAfterDonation")
	public void CheckCampaignCategoryAfterDonation() {
		WebElement campaignSecondary_Category = driver.findElement(By.cssSelector(Campaign.PRIMARY_CATERGORY_SUCCESS_WEPAY));
		Assert.assertEquals(campaignSecondary_Category.getText(),"Education");
	}
	
	@Test(dependsOnMethods ="CheckCampaignCategoryAfterDonation" )
	public void CheckCampaignStoryAfterDonation() {
		// Scroll down the page
		JavascriptExecutor jsedown = (JavascriptExecutor)driver;
		jsedown.executeScript("window.scrollBy(0,300)", "");
		WebElement campaignStory = driver.findElement(By.cssSelector(Campaign.STORY_SUCCESS_WEPAY));
		Assert.assertEquals(campaignStory.getText(),"Help me go to college and change the world in the future.");
	}
	
	@Test(dependsOnMethods ="CheckCampaignStoryAfterDonation" )
	public void CheckCampaignTestimonialAfterDonation() {
		WebElement campaignCity = driver.findElement(By.cssSelector(Campaign.CITY_SUCCESS_WEPAY));
		WebElement testimonialName = driver.findElement(By.cssSelector(Campaign.TESTIMONIAL_NAME));
		WebElement testimonialText = driver.findElement(By.cssSelector(Campaign.TESTIMONIAL_TEXT));
		Assert.assertEquals(testimonialName.getText(),"Nicholas");
		Assert.assertEquals(testimonialText.getText(),"Testimonial");
		Assert.assertEquals(campaignCity.getText(),"Santa Clara, CA 95051");
		numOfTestimonial = driver.findElements(By.xpath(Campaign.TESTIMONIAL_LIST));
		Assert.assertEquals(numOfTestimonial.size(),numberBeforeDonate);
	}
	
	@Test(dependsOnMethods ="CheckCampaignTestimonialAfterDonation" )
	public void CheckCampaignDonorsAfterDonation() {
		WebElement campaignDonors= driver.findElement(By.cssSelector(Campaign.CAMPAIGN_DONORS));
		Assert.assertTrue(Integer.valueOf(campaignDonors.getText().trim())==(numOfDonors+1));
	}
}
