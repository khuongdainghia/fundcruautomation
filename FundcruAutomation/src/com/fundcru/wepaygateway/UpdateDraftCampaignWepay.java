package com.fundcru.wepaygateway;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;


@Credentials(user="khuongdainghia@gmail.com", password="loveyou")

public class UpdateDraftCampaignWepay extends BaseTestCase {
	int campaignRaised=0;
	WebElement campaignEditMode,saveAndRewiewButton,campaignName ;
	int timeout =60;
	String newCampaignURL = AddCampaignWepay.compaignID;
	
	@Test
	public void NavigateToCampaignForUpdating() {
		driver.navigate().to(getServerURL()+"/fundraisers/"+AddCampaignWepay.compaignID);
	}
	
	@Test(dependsOnMethods ="NavigateToCampaignForUpdating",alwaysRun = true )
	public void WaitForSwitchToEditModeDisplayed() {
		campaignEditMode = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.SWITCH_TO_EDIT_MODE)));
	}
	
	@Test(dependsOnMethods ="WaitForSwitchToEditModeDisplayed",alwaysRun = true )
	public void CheckCampaignTitle() {
		WebElement campaignTitle = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.TITLE_SUCCESS)));
		Assert.assertEquals(campaignTitle.getText(),"Draft Urban Xtreme Adventures");
		Assert.assertEquals((new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.DRAFT_WARNING))).getText(),"This fundraiser is private and is only visible to you. Switch to edit mode to edit or publish fundraiser.");
	}
	
	@Test(dependsOnMethods ="CheckCampaignTitle",alwaysRun = true )
	public void CheckCampaignGoalNumber() {
		WebElement campaignGoal = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.GOAL_SUCCESS)));
		Assert.assertEquals(campaignGoal.getText(),"$75000.00");
	}
	
	@Test(dependsOnMethods ="CheckCampaignGoalNumber",alwaysRun = true )
	public void CheckCampaignPrimaryCategory() {
		WebElement campaignPrimary_Category = driver.findElement(By.cssSelector(Campaign.SECONDARY_CATERGORY_SUCCESS));
		Assert.assertEquals(campaignPrimary_Category.getText(),"Sports");
	}
	
	@Test(dependsOnMethods ="CheckCampaignPrimaryCategory",alwaysRun = true )
	public void CheckCampaignSecondaryCategory() {
		WebElement campaignSecondary_Category = driver.findElement(By.cssSelector(Campaign.PRIMARY_CATERGORY_SUCCESS));
		Assert.assertEquals(campaignSecondary_Category.getText(),"Charity");
	}
	
	@Test(dependsOnMethods ="CheckCampaignSecondaryCategory",alwaysRun = true )
	public void CheckCampaignCity() {
		WebElement campaignCity = driver.findElement(By.cssSelector(Campaign.CITY_SUCCESS));
		Assert.assertEquals(campaignCity.getText(),"Los Angeles, CA");
	}
	
	@Test(dependsOnMethods ="CheckCampaignCity",alwaysRun = true )
	public void CheckCampaignStory() {
		JavascriptExecutor jsescrolldown = (JavascriptExecutor)driver;
		// Scroll down the page
		jsescrolldown.executeScript("window.scrollBy(0,300)", "");
		WebElement campaignStory = driver.findElement(By.cssSelector(Campaign.DRAFTSTORY_FOR_UPDATING));
		Assert.assertEquals(campaignStory.getText(),"BE PART OF BUILDING SOMETHING GREAT Would it be amazing if you could Ski, Snowboard, Bounce and Climb under one roof all year round in Brisbane? With your support we can! We are in the process of building Austrialia's FIRST Indoor Snow Sport and Adventure Centre! It will be right here in Brisbane within 15km of the CBD, opening mid 2017. In the lead up to our launch, we are now offering DISCOUNTED pre-sale passes for our ski and adventure activities. As a recognition of your support, all pre-sale passes will get limited time discounts as well as other awesome read on to find out more");
	}
	
	@Test(dependsOnMethods ="CheckCampaignStory",alwaysRun = true )
	public void SwitchToEditMode() {
		JavascriptExecutor jsescrollUp = (JavascriptExecutor)driver;
		jsescrollUp.executeScript("arguments[0].scrollIntoView(true);", campaignEditMode);
		jsescrollUp.executeScript("arguments[0].click();", campaignEditMode);
//		campaignEditMode.click();
	}
	
	@Test(dependsOnMethods ="SwitchToEditMode",alwaysRun = true )
	public void EditCompaignURL() {
		
		campaignName = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.name(Campaign.CAMPAIGN_URL_NAME)));
		campaignName.clear();
		Random r = new Random();
		int n = r.nextInt(10000);
		newCampaignURL =String.format("auto_%s",n);
		campaignName.sendKeys(newCampaignURL);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		saveAndRewiewButton=(new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.SAVE_AND_PREVIEW)));
		JavascriptExecutor jsescrollUp = (JavascriptExecutor)driver;
		jsescrollUp.executeScript("arguments[0].scrollIntoView(true);", saveAndRewiewButton);
		jsescrollUp.executeScript("arguments[0].click();", saveAndRewiewButton);
	}
	@Test(dependsOnMethods ="EditCompaignURL",alwaysRun = true )
	public void SwitchToEditModeAgain() {
		
		campaignEditMode=(new WebDriverWait(driver,30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.SWITCH_TO_EDIT_MODE)));
		JavascriptExecutor jsescrollUp = (JavascriptExecutor)driver;
		jsescrollUp.executeScript("arguments[0].scrollIntoView(true);", campaignEditMode);
		jsescrollUp.executeScript("arguments[0].click();", campaignEditMode);
		
		campaignName = driver.findElement(By.name(Campaign.CAMPAIGN_URL_NAME));
		Assert.assertEquals(campaignName.getAttribute("value"),newCampaignURL);
		WebElement campaignURLPrefix = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CAMPAIGN_URL_PREFIX)));
//		newCampaignURL=String.format("%s%s",campaignURLPrefix.getAttribute("innerHTML"),newCampaignURL);
		newCampaignURL = String.format("%s%s", getServerURL()+"/c/",newCampaignURL);
	}
	
	@Test(dependsOnMethods ="SwitchToEditModeAgain",alwaysRun = true )
	public void CheckErrorDisplayedWhenRemoveValueOnGoalField() {
		// Check error message displayed when leave the field Goal empty
		WebElement campaignGoalEditPage = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.name(Campaign.GOAL_ECP_NAME)));
		campaignGoalEditPage.clear();
		saveAndRewiewButton =driver.findElement(By.cssSelector(Campaign.SAVE_AND_PREVIEW));
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", saveAndRewiewButton);
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", saveAndRewiewButton);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement errorGoalFieldRequired = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.ERROR_MESSAGE_FAILED_TO_UPDATE_CAMPAIGN)));
		String errorMessage = errorGoalFieldRequired.getText();
		Assert.assertTrue(errorMessage.contains(Messages.FAILED_TO_UPDATE_CAMPAIGN));
	}
	
	@Test(dependsOnMethods ="CheckErrorDisplayedWhenRemoveValueOnGoalField",alwaysRun = true )
	public void ReUpdateCampaignGoal() {
		WebElement campaignGoalEditPage = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.name(Campaign.GOAL_ECP_NAME)));
		campaignGoalEditPage.clear();
		// Update Goal number to 80000
		campaignGoalEditPage.sendKeys("80000");
	}
	
	@Test(dependsOnMethods ="ReUpdateCampaignGoal",alwaysRun = true )
	public void UpdateCampaignTitle() {
		driver.findElement(By.cssSelector(Campaign.CAMPAIGN_TITLE_ECP)).clear();
		driver.findElement(By.cssSelector(Campaign.CAMPAIGN_TITLE_ECP)).sendKeys("Draft Urban Xtreme Adventures Modified");	
	}
	
	@Test(dependsOnMethods ="UpdateCampaignTitle",alwaysRun = true )
	public void UpdateCampaignCategory() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JavascriptExecutor jsescrolldown2 = (JavascriptExecutor)driver;
		jsescrolldown2.executeScript("window.scrollBy(0,200)", "");
		Select campaignPrimaryCategory = new Select( driver.findElement(By.cssSelector(Campaign.PRIMARY_CATEGORY_ECP)));
		campaignPrimaryCategory.selectByVisibleText("Animals");
		Select campaignSecondaryCategory = new Select(driver.findElement(By.cssSelector(Campaign.SECONDARY_CATEGORY_ECP)));
		campaignSecondaryCategory.selectByVisibleText("Veteran");
	}
	
	@Test(dependsOnMethods ="UpdateCampaignCategory",alwaysRun = true )
	public void UpdateCampaignSummary() {
		WebElement campaignSummary = driver.findElement(By.cssSelector(Campaign.CAMPAIGN_SUMMARY));
		campaignSummary.clear();
		campaignSummary.sendKeys("Draft Summary Modified");
	}
	
	@Test(dependsOnMethods ="UpdateCampaignSummary",alwaysRun = true )
	public void UpdateCampaignStory() {
		// Scroll down the page
		JavascriptExecutor jsescroll = (JavascriptExecutor)driver;
		jsescroll.executeScript("window.scrollBy(0,300)", "");
	    WebElement campaignStoryTextArea = driver.findElement(By.cssSelector(Campaign.CAMPAIGN_STORY));
	    campaignStoryTextArea.clear();
	    campaignStoryTextArea.sendKeys("From the origin of a flower's name to its distinctive characteristics and rich mythology, flowers are infused with symbolism and meaning. Our Flower Meaning Guide is designed to unravel these hidden mysteries! ");
	}
	
	@Test(dependsOnMethods ="UpdateCampaignStory",alwaysRun = true )
	public void ClickSaveAndPreviewButton() {
		JavascriptExecutor jsescroll2 = (JavascriptExecutor)driver;
		jsescroll2.executeScript("arguments[0].scrollIntoView(true);", saveAndRewiewButton);
		jsescroll2.executeScript("arguments[0].click();", saveAndRewiewButton);
	}
	
	@Test(dependsOnMethods ="ClickSaveAndPreviewButton",alwaysRun = true )
	public void CheckCampaignURL() {
		campaignEditMode = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.SWITCH_TO_EDIT_MODE)));
		driver.getCurrentUrl().substring(driver.getCurrentUrl().lastIndexOf("/")+1,driver.getCurrentUrl().length());
		System.out.println("Campaign ID: " +AddCampaignWepay.compaignID);
		Assert.assertEquals(driver.getCurrentUrl(), String.format(getServerURL()+"/fundraisers/%s",AddCampaignWepay.compaignID));
	}

	
	@Test(dependsOnMethods ="CheckCampaignURL",alwaysRun = true )
	public void CheckCampaignTitleAfterUpdating() {
		WebElement campaignTitle = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.TITLE_SUCCESS)));
		Assert.assertEquals(campaignTitle.getText(),"Draft Urban Xtreme Adventures Modified");
	}
	
	@Test(dependsOnMethods ="CheckCampaignTitleAfterUpdating",alwaysRun = true )
	public void CheckCampaignSummaryAfterUpdating() {
		WebElement campaignSummary = driver.findElement(By.cssSelector(Campaign.CAMPAIGN_SUMMARY_AFTER_EDIT));
		Assert.assertEquals(campaignSummary.getText(),"Draft Summary Modified");
	}
	
	@Test(dependsOnMethods ="CheckCampaignSummaryAfterUpdating",alwaysRun = true )
	public void CheckCampaignGoalAfterUpdating() {
		WebElement campaignGoal = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.GOAL_SUCCESS)));
		Assert.assertEquals(campaignGoal.getText(),"$80000.00");
	}
	
	@Test(dependsOnMethods ="CheckCampaignGoalAfterUpdating",alwaysRun = true )
	public void CheckCampaignPrimaryCategoryAfterUpdating() {
		WebElement campaignPrimary_Category = driver.findElement(By.cssSelector(Campaign.SECONDARY_CATERGORY_SUCCESS));
		Assert.assertEquals(campaignPrimary_Category.getText(),"Animals");
	}
	
	@Test(dependsOnMethods ="CheckCampaignPrimaryCategoryAfterUpdating",alwaysRun = true )
	public void CheckCampaignSecondaryCategoryAfterUpdating() {
		WebElement campaignSecondary_Category = driver.findElement(By.cssSelector(Campaign.PRIMARY_CATERGORY_SUCCESS));
		Assert.assertEquals(campaignSecondary_Category.getText(),"Veteran");
	}
	
	@Test(dependsOnMethods ="CheckCampaignSecondaryCategoryAfterUpdating",alwaysRun = true )
	public void CheckCampaignStoryAfterUpdating() {
		// Scroll down the page
		JavascriptExecutor jsescroll = (JavascriptExecutor)driver;
		jsescroll.executeScript("window.scrollBy(0,300)", "");
		WebElement campaignStory = driver.findElement(By.cssSelector(Campaign.DRAFTSTORY_FOR_UPDATING));
		Assert.assertEquals(campaignStory.getText(),"From the origin of a flower's name to its distinctive characteristics and rich mythology, flowers are infused with symbolism and meaning. Our Flower Meaning Guide is designed to unravel these hidden mysteries!");
	}

	@Test(dependsOnMethods ="CheckCampaignStoryAfterUpdating",alwaysRun = true )
	public void UpdateCampaignCityCode() {
		JavascriptExecutor jsescroll2 = (JavascriptExecutor)driver;
		jsescroll2.executeScript("arguments[0].scrollIntoView(true);", campaignEditMode);
		jsescroll2.executeScript("arguments[0].click();", campaignEditMode);
		WebElement campaignCityCode = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.City_Zip_Code)));
		campaignCityCode.clear();
		campaignCityCode.sendKeys("Arizona, United States");
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CITY_LIST_1)));
		campaignCityCode.sendKeys(Keys.DOWN);
		campaignCityCode.sendKeys(Keys.ENTER);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		saveAndRewiewButton = driver.findElement(By.cssSelector(Campaign.SAVE_AND_PREVIEW));
		jsescroll2.executeScript("arguments[0].scrollIntoView(true);", saveAndRewiewButton);
		jsescroll2.executeScript("arguments[0].click();", saveAndRewiewButton);
		
	}

	@Test(dependsOnMethods ="UpdateCampaignCityCode",alwaysRun = true )
	public void CheckCampaignCityAfterUpdating() {
		WebElement campaignCity = driver.findElement(By.cssSelector(Campaign.CITY_SUCCESS));
		Assert.assertEquals(campaignCity.getText(),"Arizona");
	}
	
	@Test(dependsOnMethods ="CheckCampaignCityAfterUpdating",alwaysRun = true )
	public void publishCampaignAferUpdated() {
		
		WebElement publish = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.PUBLISH_FROM_DRAFT)));
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", publish);
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", publish);
	}
	@Test(dependsOnMethods ="publishCampaignAferUpdated",alwaysRun = true )
	public void LogOut() {
		WebElement userLink =(new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		userLink.click();
		WebElement logOutButton =(new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.SIGN_OUT)));
		logOutButton.click();		
	}
	
	@Test(dependsOnMethods ="LogOut",alwaysRun = true )
	public void NavigateToNewCampaignAfterLogout() {
		driver.get(getServerURL() + "/fundraisers/" +AddCampaignWepay.compaignID);
	}
	@Test(dependsOnMethods ="NavigateToNewCampaignAfterLogout",alwaysRun = true )
	public void checkToSeeIfCanAccessWithoutLoginNow() {
		WebElement campaignTitle = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.TITLE_SUCCESS)));
		Assert.assertEquals(campaignTitle.getText(),"Draft Urban Xtreme Adventures Modified");
	}
	@Test(dependsOnMethods ="checkToSeeIfCanAccessWithoutLoginNow",alwaysRun = true )
	public void NavigateToNewCampaignURL() {
		driver.navigate().to(newCampaignURL);
	}
	@Test(dependsOnMethods ="NavigateToNewCampaignURL",alwaysRun = true )
	public void checkToSeeIfCanAccessNewCampaignURL() {
		WebElement campaignTitle = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.TITLE_SUCCESS)));
		Assert.assertEquals(campaignTitle.getText(),"Draft Urban Xtreme Adventures Modified");
	}
}
