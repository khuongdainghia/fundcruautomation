package com.fundcru.campaigns;

import static org.testng.AssertJUnit.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.Donate;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;


@Credentials(user="khuongdainghia@gmail.com", password="loveyou")
public class DonateCampaign extends BaseTestCase {
	int campaignRaised=0;
	int numberBeforeDonate;
	List<WebElement> numOfTestimonial;
	int timeout = 30;
	
	@Test
	public void NavigateToCampaignForDonation() {
		driver.get(getServerURL() + "/fundraisers/zzRpeYyuQ95DuGEJD");
	}
	
	@Test(dependsOnMethods ="NavigateToCampaignForDonation",alwaysRun = true )
	public void GetCampaignRaised() {
		WebElement campaignRaisedElement= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CAMPAIGN_RAISED)));
		numOfTestimonial = driver.findElements(By.xpath(Campaign.TESTIMONIAL_LIST));
	    numberBeforeDonate = numOfTestimonial.size();
		campaignRaised= Integer.valueOf(campaignRaisedElement.getText().replace("$", "").replace(".00", ""));	
	}
	
	@Test(dependsOnMethods ="GetCampaignRaised",alwaysRun = true )
	public void ClickDonateButton() {
		WebElement donateButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Campaign.DONATE_BUTTON)));
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",donateButton);	
	}
	
	@Test(dependsOnMethods ="ClickDonateButton",alwaysRun = true )
	public void ClickDonateByCreditCard() {
		WebElement donateByCreditCard = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Donate.CREDIT_CARD_DONATE)));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();",donateByCreditCard);
	}
	
	@Test(dependsOnMethods ="ClickDonateByCreditCard",alwaysRun = true )
	public void EnterAmountForDonation() {
	    WebElement amount = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Donate.AMOUNT)));
	    amount.clear();
	    amount.sendKeys("10");
	}
	
	@Test(dependsOnMethods ="EnterAmountForDonation",alwaysRun = true )
	public void ClickSubmitToEnterCardInformationScreen() {
	    driver.findElement(By.cssSelector(Donate.SUBMIT)).click();
	}
	
	@Test(dependsOnMethods ="ClickSubmitToEnterCardInformationScreen",alwaysRun = true )
	public void CheckErrorAfterSubmitInCardInformationScreenWithoutAnyFieldEntered() {
		// Check error when clicking Submit button without any fields entered in "Secure transaction with Paypal" screen
		WebElement submitButtonForCardInfor = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Donate.SUBMIT_CARD_INFORMATION_SCREEN)));
		submitButtonForCardInfor.click();
		String errorOnCardNumberField = driver.findElement(By.cssSelector(Donate.ERROR_ON_CARD_NUMBER)).getText();
		Assert.assertTrue(errorOnCardNumberField.equals(Messages.REQUIRED_FIELD_DONATE), "Card number - wrong error message" + errorOnCardNumberField);
		String errorOnCCV = driver.findElement(By.cssSelector(Donate.ERROR_ON_CCV)).getText();
		Assert.assertTrue(errorOnCCV.equals(Messages.REQUIRED_FIELD_DONATE), "CCV - wrong error message" + errorOnCCV);
		String errorOnExpirationDate = driver.findElement(By.cssSelector(Donate.ERROR_ON_EXPIRATION_DATE)).getText();
		Assert.assertTrue(errorOnExpirationDate.equals(Messages.REQUIRED_FIELD_DONATE), "Expiration Date - wrong error message" + errorOnExpirationDate);
		String errorNameOnCard = driver.findElement(By.cssSelector(Donate.ERROR_ON_NAME_ON_CARD)).getText();
		Assert.assertTrue(errorNameOnCard.equals(Messages.REQUIRED_FIELD_DONATE), "Name on card - wrong error message" + errorNameOnCard);    
	}
	
	@Test(dependsOnMethods ="CheckErrorAfterSubmitInCardInformationScreenWithoutAnyFieldEntered",alwaysRun = true )
	public void CheckErrorAfterInputInvalidCardNumber() {
		// Check error when input invalid card number
	    WebElement cardNumber = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.name(Donate.CARD_NUMBER)));
	    cardNumber.clear();
	    cardNumber.sendKeys("1111112233");
	    String errorInvalidCardNumber = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Donate.ERROR_ON_CARD_NUMBER))).getText();
		Assert.assertTrue(errorInvalidCardNumber.equals(Messages.INVALID_CARD_NUMBER), "Wrong error message for invalid card number" + errorInvalidCardNumber);
	}
	
	@Test(dependsOnMethods ="CheckErrorAfterInputInvalidCardNumber",alwaysRun = true )
	public void CheckErrorAfterInputInvalidCCV() {
		// Check error when input invalid CCV
	    WebElement ccv = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.name(Donate.CCV)));
	    ccv.clear();
	    ccv.sendKeys("invalidCCV");
	    String errorInvalidCCV = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Donate.ERROR_ON_CCV))).getText();
		Assert.assertTrue(errorInvalidCCV.equals(Messages.INVALID_CCV), "Wrong error message for invalid CCV" + errorInvalidCCV);
	}
	
	@Test(dependsOnMethods ="CheckErrorAfterInputInvalidCCV",alwaysRun = true )
	public void CheckErrorAfterInputInvalidExpirationDate() {
		// Check error when input invalid EXPIRATION date
	    WebElement expiration = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.name(Donate.EXPIRATION)));
	    expiration.clear();
	    expiration.sendKeys("invalidExpiration");
	    String errorInvalidExpiration = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Donate.ERROR_ON_EXPIRATION_DATE))).getText();
		Assert.assertTrue(errorInvalidExpiration.equals(Messages.INVALID_DATE), "Wrong error message for invalid EXPIRATION date" + errorInvalidExpiration);
	}
	
	@Test(dependsOnMethods ="CheckErrorAfterInputInvalidExpirationDate",alwaysRun = true )
	public void CheckErrorAfterInputInvalidNameOnCard() {
		// Check error when input invalid name on card
	    WebElement nameOnCard = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.name(Donate.DONATE_NAME_ON_CARD)));
	    nameOnCard.clear();
	    nameOnCard.sendKeys("invalidname123");
	    String errorInvalidNameOnCard = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Donate.ERROR_ON_NAME_ON_CARD))).getText();
		Assert.assertTrue(errorInvalidNameOnCard.equals(Messages.INVALID_NAME_ON_CARD), "Wrong error message for invalid name on card" + errorInvalidNameOnCard);
	}
	
	@Test(dependsOnMethods ="CheckErrorAfterInputInvalidNameOnCard",alwaysRun = true )
	public void ReInPutCardNumber() {
		driver.findElement(By.name("card_number")).clear();
		driver.findElement(By.name("card_number")).clear();
		driver.findElement(By.name("card_number")).sendKeys("4032038120259944");
	}
	
	@Test(dependsOnMethods ="ReInPutCardNumber",alwaysRun = true )
	public void ReInPutCCV() {
	    driver.findElement(By.name("ccv")).clear();
	    driver.findElement(By.name("ccv")).clear();
	    driver.findElement(By.name("ccv")).sendKeys("123");
	}
	
	@Test(dependsOnMethods ="ReInPutCCV",alwaysRun = true )
	public void ReInPutExpirationDate() {
	    driver.findElement(By.name("expiration")).clear();
	    driver.findElement(By.name("expiration")).clear();
	    driver.findElement(By.name("expiration")).sendKeys("11/22");
	}
	
	@Test(dependsOnMethods ="ReInPutExpirationDate",alwaysRun = true )
	public void ReInPutNameOnCard() {
	    driver.findElement(By.name("fullname")).clear();
	    driver.findElement(By.name("fullname")).clear();
	    driver.findElement(By.name("fullname")).sendKeys("Charlie Angles");
	}
	
	@Test(dependsOnMethods ="ReInPutNameOnCard",alwaysRun = true )
	public void SubmitAfterInputCardInformation() {
		WebElement submitButtonForCardInfor = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Donate.SUBMIT_CARD_INFORMATION_SCREEN)));
	    submitButtonForCardInfor.click();
	}
	
	@Test(dependsOnMethods ="SubmitAfterInputCardInformation",alwaysRun = true )
	public void CheckAmountForConfirmation() {
	    WebElement amount = (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Donate.DONATE_AMOUNT_CONFIRM)));
	    amount = (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Donate.AMOUNT_FOR_CONFIRMATION)));
	    assertEquals(amount.getText(), "$10.00");
	}
	
	@Test(dependsOnMethods ="CheckAmountForConfirmation",alwaysRun = true )
	public void CheckNameOnCardForConfirmation() {
	    assertEquals(driver.findElement(By.cssSelector(Donate.NAME_ON_CARD_CONFIRMATION)).getText(), "Charlie Angles");
	}
	
	@Test(dependsOnMethods ="CheckNameOnCardForConfirmation",alwaysRun = true )
	public void CheckCardEndingdForConfirmation() {
	    assertEquals(driver.findElement(By.cssSelector(Donate.CARD_ENDING_CONFIRMATION)).getText(), "9944");
	}
	
	@Test(dependsOnMethods ="CheckCardEndingdForConfirmation",alwaysRun = true )
	public void ClickConfirm() {
	    driver.findElement(By.cssSelector(Donate.CONFIRM)).click();
	}
	
	@Test(dependsOnMethods ="ClickConfirm",alwaysRun = true )
	public void EnterDonateName() {
	    WebElement donateName = (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.name(Donate.DONATE_NAME)));
	    donateName.clear();
	    donateName.sendKeys("Nicholas");
	}
	
	@Test(dependsOnMethods ="EnterDonateName",alwaysRun = true )
	public void EnterTestimonial() {
	    driver.findElement(By.id("testimonial")).clear();
	    driver.findElement(By.id("testimonial")).sendKeys("Test");
	}
	
	@Test(dependsOnMethods ="EnterTestimonial",alwaysRun = true )
	public void ClickShareButton() {
	    driver.findElement(By.cssSelector(Donate.SHARE_BUTTON)).click();
	}
	
	@Test(dependsOnMethods ="ClickShareButton",alwaysRun = true )
	public void CheckCampaignTitleAfterDonation() {
		//check result here 
		WebElement campaignTitle = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.TITLE_SUCCESS)));
		Assert.assertEquals(campaignTitle.getText(),"Draft Urban Xtreme Adventures");
	}
	
	@Test(dependsOnMethods ="CheckCampaignTitleAfterDonation",alwaysRun = true )
	public void CheckGoalAfterDonation() {
		WebElement campaignGoal = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.GOAL_SUCCESS)));
		Assert.assertEquals(campaignGoal.getText(),"$7500.00");
	}
	
	@Test(dependsOnMethods ="CheckGoalAfterDonation",alwaysRun = true )
	public void CheckCampaignRaisedAfterDonation() {
		WebElement campaignRaisedElement= driver.findElement(By.cssSelector(Campaign.CAMPAIGN_RAISED));
		campaignRaised +=10;
		Assert.assertEquals(campaignRaisedElement.getText(),String.format("$%s.00",campaignRaised));
	}
	
	@Test(dependsOnMethods ="CheckCampaignRaisedAfterDonation",alwaysRun = true )
	public void CheckCampaignCategoryAfterDonation() {
		WebElement campaignSecondary_Category = driver.findElement(By.cssSelector(Campaign.PRIMARY_CATERGORY_SUCCESS));
		Assert.assertEquals(campaignSecondary_Category.getText(),"Travel");
	}
	
	@Test(dependsOnMethods ="CheckCampaignCategoryAfterDonation",alwaysRun = true )
	public void CheckCampaignStoryAfterDonation() {
		// Scroll down the page
		JavascriptExecutor jsedown = (JavascriptExecutor)driver;
		jsedown.executeScript("window.scrollBy(0,300)", "");
//		WebElement campaignStory = driver.findElement(By.cssSelector(Campaign.STORY_SUCCESS));
//		Assert.assertEquals(campaignStory.getText(),"BE PART OF BUILDING SOMETHING GREAT Wouldn t it be amazing if you could Ski, Snowboard, Bounce and Climb under one roof all year round in Brisbane? With your support we can! We are in the process of building Austrialia s FIRST Indoor Snow Sport and Adventure Centre! It will be right here in Brisbane within 15km of the CBD, opening mid 2017. In the lead up to our launch, we are now offering DISCOUNTED pre-sale passes for our ski and adventure activities. As a recognition of your support, all pre-sale passes will get limited time discounts as well as other awesome perks read on to find out more!");
	}
	
	@Test(dependsOnMethods ="CheckCampaignStoryAfterDonation",alwaysRun = true )
	public void CheckCampaignTestimonialAfterDonation() {
		WebElement campaignCity = driver.findElement(By.cssSelector(Campaign.CITY_SUCCESS));
		WebElement testimonialName = driver.findElement(By.cssSelector(Campaign.TESTIMONIAL_NAME));
		WebElement testimonialText = driver.findElement(By.cssSelector(Campaign.TESTIMONIAL_TEXT));
		Assert.assertEquals(testimonialName.getText(),"Nicholas");
		Assert.assertEquals(testimonialText.getText(),"Test");
		Assert.assertEquals(campaignCity.getText(),"Los Angeles, CA");
		numOfTestimonial = driver.findElements(By.xpath(Campaign.TESTIMONIAL_LIST));
		Assert.assertEquals(numberBeforeDonate+2, numOfTestimonial.size());
	}
	
	@Test(dependsOnMethods ="CheckCampaignTestimonialAfterDonation", alwaysRun=true )
	public void DonateCompaignWithoutTestimonial() {
		driver.get(getServerURL() + "/fundraisers/zzRpeYyuQ95DuGEJD");
		WebElement donateButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Campaign.DONATE_BUTTON)));
		WebElement campaignRaisedElement= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CAMPAIGN_RAISED)));
	    numOfTestimonial = driver.findElements(By.xpath(Campaign.TESTIMONIAL_LIST));
	    numberBeforeDonate = numOfTestimonial.size();
		campaignRaised= Integer.valueOf(campaignRaisedElement.getText().replace("$", "").replace(".00", ""));
		donateButton.click();
	    driver.findElement(By.id(Donate.AMOUNT)).clear();
	    driver.findElement(By.id(Donate.AMOUNT)).sendKeys("10");
	    driver.findElement(By.cssSelector(Donate.SUBMIT)).click();
	    WebElement cardNumber = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.name(Donate.CARD_NUMBER)));
	    cardNumber.clear();
	    cardNumber.sendKeys("4032038120259944");
	    driver.findElement(By.name("ccv")).clear();
	    driver.findElement(By.name("ccv")).sendKeys("123");
	    driver.findElement(By.name("expiration")).clear();
	    driver.findElement(By.name("expiration")).sendKeys("11/22");
	    driver.findElement(By.name("fullname")).clear();
	    driver.findElement(By.name("fullname")).sendKeys("Charlies");
	    driver.findElement(By.name("fullname")).clear();
	    driver.findElement(By.name("fullname")).sendKeys("Charlie");
	    driver.findElement(By.name("fullname")).clear();
	    driver.findElement(By.name("fullname")).sendKeys("Charlie Angles");
	    driver.findElement(By.xpath(Donate.SUBMIT_BUTTON_CARD_FORM)).click();
	    WebElement amount = (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Donate.DONATE_AMOUNT_CONFIRM)));
	    assertEquals(amount.getText(), "$10.00");
	    assertEquals(driver.findElement(By.cssSelector(Donate.CARD_NAME)).getText(), "Charlie Angles");
	    assertEquals(driver.findElement(By.cssSelector(Donate.CARD_NUMBER_LAST_4)).getText(), "9944");
	    driver.findElement(By.cssSelector(Donate.SUBMIT_BUTTON_CONFIRM_FORM)).click();
	    WebElement donateName = (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.name(Donate.DONATE_NAME)));
	    donateName.clear();
	    donateName.sendKeys("Nicholas");
	    driver.findElement(By.cssSelector(Donate.SHARE_BUTTON)).click();
	}
	
	
	@Test(dependsOnMethods ="DonateCompaignWithoutTestimonial",alwaysRun = true )
	public void CheckDonatedSuccessfulWithoutTestimonial() {
			//check result here 
		WebElement campaignTitle = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.TITLE_SUCCESS)));
		Assert.assertEquals(campaignTitle.getText(),"Draft Urban Xtreme Adventures");
		WebElement campaignGoal = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.GOAL_SUCCESS)));
		Assert.assertEquals(campaignGoal.getText(),"$7500.00");
		WebElement campaignRaisedElement= driver.findElement(By.cssSelector(Campaign.CAMPAIGN_RAISED));
		campaignRaised +=20;
		Assert.assertEquals(campaignRaisedElement.getText(),String.format("$%s.00",campaignRaised));
		WebElement campaignStory = driver.findElement(By.cssSelector(Campaign.STORY_SUCCESS));
		Assert.assertEquals(campaignStory.getText(),"BE PART OF BUILDING SOMETHING GREAT Wouldn t it be amazing if you could Ski, Snowboard, Bounce and Climb under one roof all year round in Brisbane? With your support we can! We are in the process of building Austrialia s FIRST Indoor Snow Sport and Adventure Centre! It will be right here in Brisbane within 15km of the CBD, opening mid 2017. In the lead up to our launch, we are now offering DISCOUNTED pre-sale passes for our ski and adventure activities. As a recognition of your support, all pre-sale passes will get limited time discounts as well as other awesome perks read on to find out more!");
		WebElement campaignPrimary_Category = driver.findElement(By.cssSelector(Campaign.SECONDARY_CATERGORY_SUCCESS));
		Assert.assertEquals(campaignPrimary_Category.getText(),"Charity");
		WebElement campaignSecondary_Category = driver.findElement(By.cssSelector(Campaign.PRIMARY_CATERGORY_SUCCESS));
		Assert.assertEquals(campaignSecondary_Category.getText(),"Charity");
		WebElement campaignCity = driver.findElement(By.cssSelector(Campaign.CITY_SUCCESS));
		Assert.assertEquals(campaignCity.getText(),"Los Angeles, CA");
		numOfTestimonial = driver.findElements(By.xpath(Campaign.TESTIMONIAL_LIST));
		Assert.assertEquals(numberBeforeDonate, numOfTestimonial.size());
		}
}
