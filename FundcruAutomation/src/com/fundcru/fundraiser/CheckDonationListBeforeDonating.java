package com.fundcru.fundraiser;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.objects.pages.TableFunction;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;


@Credentials(user="khuongdainghia@gmail.com", password="loveyou")
public class CheckDonationListBeforeDonating extends BaseTestCase {
	int timeout =30,numberDraftFundraiser;
	ScreenAction action;
	TableFunction table;
	JavascriptExecutor executor;
	WebElement fundraiser;
	WebElement parentElement;
	List<WebElement> actionList,noPage;
	public static int noOfDonation;
	public static int noOfMyDonation;
	int numberPage;
	@Test
	public void AccessToMyFundraiser() {
		action = new ScreenAction(driver);
		table = new TableFunction(driver);
		executor = (JavascriptExecutor)driver;
		WebElement userLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		userLink.click();
		WebElement myFundraiser =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.MY_CAMPAIGN_LINK)));
		myFundraiser.click();
	}
	
	@Test(dependsOnMethods ="AccessToMyFundraiser",alwaysRun = true )
	public void CheckAction() {
		fundraiser =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(String.format(Campaign.FUNDRAISER_ITEM_ID,DonateFundraiser.fundraiserID))));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", fundraiser);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", fundraiser);
		
		parentElement = (WebElement)executor.executeScript("return arguments[0].parentNode;", fundraiser);
		actionList = parentElement.findElements(By.className(Campaign.ACTION_ITEM_CLASS));
		Assert.assertTrue(actionList.get(0).getText().trim().contains("Edit"),String.format("Wrong expected: Edit but Actual: %s",actionList.get(0).getText().trim()));
		Assert.assertTrue(actionList.get(1).getText().trim().contains("Publish"),String.format("Wrong expected: Publish but Actual: %s",actionList.get(1).getText().trim()));
		Assert.assertTrue(actionList.get(2).getText().trim().contains("Show donations"),String.format("Wrong expected: Show donations but Actual: %s",actionList.get(2).getText().trim()));
	}
	@Test(dependsOnMethods ="CheckAction",alwaysRun = true )
	public void ClickShowDonationsAction() {
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", fundraiser);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", actionList.get(2));
	}
	@Test(dependsOnMethods ="ClickShowDonationsAction",alwaysRun = true )
	public void NavigateToDonationListScreen() {
		 WebElement fundraiserDonation =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.DONATION_LIST_LABLE)));
		 Assert.assertEquals(fundraiserDonation.getText().trim(),"Donation list");
		 noOfDonation = table.countRow();
	}
	
	@Test(dependsOnMethods ="NavigateToDonationListScreen",alwaysRun = true )
	public void getPaginationItems () {
		 WebElement pageList= driver.findElement(By.cssSelector(Campaign.DONATION_PAGINATION));
		 noPage = pageList.findElements(By.tagName("li"));
		 numberPage = noPage.size();
	}
	
	@Test(dependsOnMethods ="getPaginationItems",alwaysRun = true )
	public void getTotalOfDonations () {
		for(int i=2; i<numberPage-1;i++) {
 			noPage.get(i).click();
			noOfDonation = noOfDonation + table.countRow();
		}
	}
	
	@Test(dependsOnMethods ="getTotalOfDonations",alwaysRun = true )
	public void NavigateToMyDonation() {
		WebElement userLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		userLink.click();
		WebElement myDonation =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.MY_DONATIONS_LINK)));
		myDonation.click();
		WebElement fundraiserDonation =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.MY_DONATION_LIST)));
		Assert.assertEquals(fundraiserDonation.getText().trim(),"My donations");
		noOfMyDonation = table.countRow();
	}
}
