package com.fundcru.fundraiser;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.objects.pagedefinitions.TextLabels;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;

@Credentials(user="khuongdainghia@gmail.com", password="loveyou")
public class AddFundraiserFieldValidation extends BaseTestCase {
	
	int timeout = 30;
	ScreenAction action;
	
	@Test	
	public void ClickCreateNewCampaign() {
		WebElement createNewCampaign =(new WebDriverWait(driver, 120)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CREATE_NEW_COMPAIGN)));
		createNewCampaign.click();
	}
	
	@Test(dependsOnMethods ="ClickCreateNewCampaign",alwaysRun = true )
	public void waitForCampaignCreateFromVisible() {
		action = new ScreenAction(driver);
		action.waitObjVisible(By.cssSelector(Campaign.CAMPAIGN_CREATE_FORM_TITLE));
	}
	
	@Test(dependsOnMethods ="waitForCampaignCreateFromVisible",alwaysRun = true )
	public void checkCreateCampaignFromHeader() {
		action.assertTextEqual(By.cssSelector(Campaign.CAMPAIGN_CREATE_FORM_TITLE), TextLabels.CAMPAIGN_CREATE_FORM_TITLE);
	}
	
	@Test(dependsOnMethods ="checkCreateCampaignFromHeader",alwaysRun = true )
	public void ClickSaveButtonI() {
	    ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.cssSelector(Campaign.SAVE)));
	}
	
	@Test(dependsOnMethods ="ClickSaveButtonI",alwaysRun = true )
	public void checkValidationErrorOnRequiredGoalField() {
		action.waitObjVisible(By.cssSelector(Campaign.GOAL_VALIDATION_ERROR));
		action.assertTextEqual(By.cssSelector(Campaign.GOAL_VALIDATION_ERROR), Messages.REQUIRED_FIELD_GOAL_ERROR);
	}
	
	@Test(dependsOnMethods ="checkValidationErrorOnRequiredGoalField",alwaysRun = true )
	public void checkValidationErrorOnRequiredTitleField() {
		action.waitObjVisible(By.cssSelector(Campaign.FUNDRAISER_TITLE_VALIDATION_ERROR));
		action.assertTextEqual(By.cssSelector(Campaign.FUNDRAISER_TITLE_VALIDATION_ERROR), Messages.REQUIRED_FIELD_FUNDRAISER_TITLE_ERROR);
	}
	
	@Test(dependsOnMethods ="checkValidationErrorOnRequiredTitleField",alwaysRun = true )
	public void checkValidationErrorOnRequiredCityCodeField() {
		action.waitObjVisible(By.cssSelector(Campaign.CITY_CODE_VALIDATION_ERROR));
		action.assertTextEqual(By.cssSelector(Campaign.CITY_CODE_VALIDATION_ERROR), Messages.REQUIRED_FIELD_CITY_CODE_ERROR);
	}
	
	@Test(dependsOnMethods ="checkValidationErrorOnRequiredCityCodeField",alwaysRun = true )
	public void checkValidationErrorOnRequiredPrimaryCategoryField() {
		action.waitObjVisible(By.cssSelector(Campaign.PRIMARY_CATEGORY_VALIDATION_ERROR));
		action.assertTextEqual(By.cssSelector(Campaign.PRIMARY_CATEGORY_VALIDATION_ERROR), Messages.REQUIRED_FIELD_PRIMARY_CATEGORY_ERROR);
	}
	
	@Test(dependsOnMethods ="checkValidationErrorOnRequiredPrimaryCategoryField",alwaysRun = true )
	public void scrollDownToSummaryField() {
		WebElement summaryField = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.SUMMARY_VALIDATION_ERROR)));
		action.clickHorizontalScrollBarToElement(summaryField);
	}
	
	@Test(dependsOnMethods ="scrollDownToSummaryField",alwaysRun = true )
	public void checkValidationErrorOnRequiredSummaryField() {
		action.waitObjVisible(By.cssSelector(Campaign.SUMMARY_VALIDATION_ERROR));
		action.assertTextEqual(By.cssSelector(Campaign.SUMMARY_VALIDATION_ERROR), Messages.REQUIRED_FIELD_SUMMARY_ERROR);
	}
	
	@Test(dependsOnMethods ="checkValidationErrorOnRequiredSummaryField",alwaysRun = true )
	public void checkValidationErrorOnRequiredImageField() {
		action.waitObjVisible(By.cssSelector(Campaign.IMAGE_VALIDATION_ERROR));
		action.assertTextEqual(By.cssSelector(Campaign.IMAGE_VALIDATION_ERROR), Messages.REQUIRED_FIELD_IMAGE_ERROR);
	}
	
	@Test(dependsOnMethods ="checkValidationErrorOnRequiredImageField",alwaysRun = true )
	public void scrollDownToStoryField() {
		WebElement storyField = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.STORY_VALIDATION_ERROR)));
		action.clickHorizontalScrollBarToElement(storyField);
	}
	
	@Test(dependsOnMethods ="scrollDownToStoryField",alwaysRun = true )
	public void checkValidationErrorOnRequiredStoryField() {
		action.waitObjVisible(By.cssSelector(Campaign.STORY_VALIDATION_ERROR));
		action.assertTextEqual(By.cssSelector(Campaign.STORY_VALIDATION_ERROR), Messages.REQUIRED_FIELD_STORY_ERROR);
	}
	
	@Test(dependsOnMethods ="checkValidationErrorOnRequiredStoryField",alwaysRun = true )
	public void ClickSaveButtonII() {
	    ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.cssSelector(Campaign.SAVE)));
	}
	
	@Test(dependsOnMethods ="ClickSaveButtonII",alwaysRun = true )
	public void checkFormHeaderErrorMessage() {
		action.waitObjVisible(By.cssSelector(Campaign.FUND_DATA_VALIDATION_ERROR_MESSAGE));
		driver.findElement(By.cssSelector(Campaign.FUND_DATA_VALIDATION_ERROR_MESSAGE)).getText().contains(Messages.FUND_DATA_VALIDATION_ERROR_MESSAGE);
	}
	
	@Test(dependsOnMethods ="checkFormHeaderErrorMessage",alwaysRun = true )
	public void EnterValueGreaterThanMaximumCampaignGoal() {
		action.inputTextFieldBy(By.cssSelector(Campaign.GOAL), "1000000000000000000000");
	}
	
	@Test(dependsOnMethods ="EnterValueGreaterThanMaximumCampaignGoal",alwaysRun = true )
	public void ClickSaveButtonIII() {
	    ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.cssSelector(Campaign.SAVE)));
	}
	
	@Test(dependsOnMethods ="ClickSaveButtonIII",alwaysRun = true )
	public void checMaximumValidationError() {
		action.waitObjVisible(By.cssSelector(Campaign.GOAL_VALIDATION_ERROR));
		action.assertTextEqual(By.cssSelector(Campaign.GOAL_VALIDATION_ERROR), Messages.GOAL_MAXIMUM_VALIDATION_ERROR);
	}
	
	@Test(dependsOnMethods ="checMaximumValidationError",alwaysRun = true )
	public void EnterValueLessThanMinimumCampaignGoal() {
		action.inputTextFieldBy(By.cssSelector(Campaign.GOAL), "0");
	}
	
	@Test(dependsOnMethods ="EnterValueLessThanMinimumCampaignGoal",alwaysRun = true )
	public void ClickSaveButtonIV() {
	    ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.cssSelector(Campaign.SAVE)));
	}
	
	@Test(dependsOnMethods ="ClickSaveButtonIV",alwaysRun = true )
	public void checMinimumlidationErrorOnGoalField() {
		action.waitObjVisible(By.cssSelector(Campaign.GOAL_VALIDATION_ERROR));
		action.assertTextEqual(By.cssSelector(Campaign.GOAL_VALIDATION_ERROR), Messages.GOAL_MINIMUM_VALIDATION_ERROR);
	}
	
	@Test(dependsOnMethods ="checMinimumlidationErrorOnGoalField",alwaysRun = true )
	public void EnterValueLessThanMinimumCampaignStory() {
		action.inputTextFieldBy(By.cssSelector(Campaign.STORY_TEXTAREA), "What are you raising money for?");
	}
	
	@Test(dependsOnMethods ="EnterValueLessThanMinimumCampaignStory",alwaysRun = true )
	public void ClickSaveButtonV() {
	    ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.cssSelector(Campaign.SAVE)));
	}
	
	@Test(dependsOnMethods ="ClickSaveButtonV",alwaysRun = true )
	public void checMinimumlidationErrorOnStoryField() {
		action.waitObjVisible(By.cssSelector(Campaign.STORY_VALIDATION_ERROR));
		action.assertTextEqual(By.cssSelector(Campaign.STORY_VALIDATION_ERROR), Messages.STORY_MINIMUM_VALIDATION_ERROR);
	}
}
