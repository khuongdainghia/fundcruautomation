package com.fundcru.fundraiser;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Login;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.objects.pagedefinitions.Offer;
import com.fundcru.objects.pagedefinitions.TextLabels;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.objects.pages.TableFunction;
import com.fundcru.utilities.BaseTestCaseNoCredential;

public class AddFundraiserToFavorites extends BaseTestCaseNoCredential {
	int timeout =30;
	String emailStr ="khuongdainghia@gmail.com",passwordStr="loveyou",titleStr="ChefsGiving: chefs, restaurants, wineries & YOU in support of those devastated by recent wildfires";
	ScreenAction action;
	TableFunction table;
	JavascriptExecutor js;	
	@Test
	public void NavigateToFundraiser() {
		action = new ScreenAction(driver);
		table = new TableFunction(driver);
		js = (JavascriptExecutor) driver;
		driver.get(getServerURL() + "/fundraisers/SqXRSK9FoZGRipWrv");
		action.waitObjVisible(By.cssSelector(Campaign.CAMPAIGN_TITLES));
		WebElement campaignTitles = driver.findElement(By.cssSelector(Campaign.CAMPAIGN_TITLES));
		titleStr = campaignTitles.getText();
	}
	
	@Test(dependsOnMethods ="NavigateToFundraiser" )
	public void ClickOnAddFundraiserToFavorites() {
		WebElement addToFavoritesButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Campaign.BOOKMARK_CAMPAIGN_BUTTON_LG)));
	    js.executeScript("arguments[0].click();",addToFavoritesButton);
	}
	
	@Test(dependsOnMethods ="ClickOnAddFundraiserToFavorites" )
	public void RequiredLoginErrorDisplayedAfterSignOut() {
		WebElement errorMessage =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Login.ERROR_MESSAGE)));
		Assert.assertEquals(errorMessage.getText(),Messages.ERROR_LOGIN_REQUIRED);
	}
	
	@Test(dependsOnMethods ="RequiredLoginErrorDisplayedAfterSignOut" )
	public void InputValidEmailAndPasswordToLogin() {
		action.inputTextFieldByID(Login.EMAIL_TEXT_FIELD_ID, emailStr);
		action.inputTextFieldByID(Login.PASSWORD_TEXT_FIELD_ID, passwordStr);
	}
	
	@Test(dependsOnMethods ="InputValidEmailAndPasswordToLogin",alwaysRun = true )
	public void ClickSubmit() {
		driver.findElement(By.id(Login.SIGNIN_BUTTON_ID)).click();
		action.waitObjVisible(By.cssSelector(Campaign.CAMPAIGN_TITLES));
	}
	
	@Test(dependsOnMethods ="ClickSubmit" )
	public void CheckFundraiserAfterAddToFavorites() {
		WebElement removeFromFavoritesButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Campaign.UNBOOKMARK_CAMPAIGN_BUTTON_LG)));
		Assert.assertTrue(removeFromFavoritesButton.getAttribute("innerHTML").contains("Remove from favorites"),"Text is wrong!");
	}
	
	@Test(dependsOnMethods ="CheckFundraiserAfterAddToFavorites" )
	public void clickUserLink() {
		WebElement userLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		userLink.click();
	}
		
	@Test(dependsOnMethods ="clickUserLink",alwaysRun = true)
	public void clickMyfavouriteOffer() {
		WebElement my_Favorite_Fundraiser_Link =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.MY_FAVARITE_FUNDRAISERS_LINK)));
		my_Favorite_Fundraiser_Link.click();
	}
		
	@Test(dependsOnMethods ="clickMyfavouriteOffer",alwaysRun = true)
	public void CheckToSeeFundraiserInTheList() {
		table.assertValueRow(2, 1, titleStr);
	}
	
	@Test(dependsOnMethods ="CheckToSeeFundraiserInTheList" )
	public void RemoveFundraiserFromFavorites() {
		driver.get(getServerURL() + "/fundraisers/SqXRSK9FoZGRipWrv");
		WebElement removeFromFavoritesButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Campaign.UNBOOKMARK_CAMPAIGN_BUTTON_LG)));
		js.executeScript("arguments[0].click();",removeFromFavoritesButton);
	}
	@Test(dependsOnMethods ="RemoveFundraiserFromFavorites" )
	public void CheckFundraiserAfterRemoveFromFavorites() {
		action.pause(3000);
		WebElement addToFavoritesButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Campaign.BOOKMARK_CAMPAIGN_BUTTON_LG)));
		Assert.assertTrue(addToFavoritesButton.getAttribute("innerHTML").contains("Add to favorites"),"Text is wrong!");
	}
	@Test(dependsOnMethods ="CheckFundraiserAfterRemoveFromFavorites" )
	public void clickUserLinkAgain() {
		WebElement userLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		userLink.click();
	}
		
	@Test(dependsOnMethods ="clickUserLinkAgain",alwaysRun = true)
	public void clickMyfavouriteOfferAgain() {
		WebElement my_Favorite_Fundraiser_Link =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.MY_FAVARITE_FUNDRAISERS_LINK)));
		my_Favorite_Fundraiser_Link.click();
	}
	@Test(dependsOnMethods ="clickMyfavouriteOfferAgain" )
	public void CheckToSeeNoFavoriteAfterRemoved() {
		WebElement noFavoriteMessage = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Home.NO_MY_FAVORITE_MESSAGE)));
		Assert.assertEquals(noFavoriteMessage.getText().trim(), Messages.ALERT_MESSAGE_NO_FUNDRAISER_EXIST);
		driver.findElement(By.cssSelector(Offer.RECOMMENDED_FUNDRAISER_LIST)).getText().startsWith(TextLabels.RECOMMENDED_FUNDRAISER);
	}
	@Test(dependsOnMethods ="CheckToSeeNoFavoriteAfterRemoved" )
	public void ClickOnAddFundraiserToFavoritesAgain() {

		driver.get(getServerURL() + "/fundraisers/SqXRSK9FoZGRipWrv");
		WebElement addToFavoritesButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Campaign.BOOKMARK_CAMPAIGN_BUTTON_LG)));
		js.executeScript("arguments[0].click();",addToFavoritesButton);
		(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Campaign.UNBOOKMARK_CAMPAIGN_BUTTON_LG)));
	}
	@Test(dependsOnMethods ="ClickOnAddFundraiserToFavoritesAgain",alwaysRun = true)
	public void AccessToFavoriteByURL() {
		driver.get(getServerURL() + "/dashboard/favorite-fundraisers");
	}
		
	@Test(dependsOnMethods ="AccessToFavoriteByURL",alwaysRun = true)
	public void CheckToSeeFundraiserInTheListAgain() {
		(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(Home.TABLE_BODY_XPATH)));
		WebElement cell= table.getCellObject(1, 2);
		Assert.assertTrue(cell.getAttribute("innerHTML").contains("YOU in support of those devastated by recent wildfires"),"Wrong expected Fundraiser! Expected: YOU in support of those devastated by recent wildfires Actual: " +cell.getAttribute("innerHTML"));
		Assert.assertEquals(table.countRow(Home.TABLE_BODY_XPATH,true),1,"Row count number is wrong!");
		
	}
	@Test(dependsOnMethods ="CheckToSeeFundraiserInTheListAgain" )
	public void RemoveByClickingIconOnGrid() {
		table.clickDeleteIcon(1);
	}
	@Test(dependsOnMethods ="RemoveByClickingIconOnGrid" )
	public void CheckToSeeNoFavoriteAfterRemovedAgain() {
		WebElement noFavoriteMessage = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Home.NO_MY_FAVORITE_MESSAGE)));
		Assert.assertEquals(noFavoriteMessage.getText().trim(), Messages.ALERT_MESSAGE_NO_FUNDRAISER_EXIST);
		driver.findElement(By.cssSelector(Offer.RECOMMENDED_FUNDRAISER_LIST)).getText().startsWith(TextLabels.RECOMMENDED_FUNDRAISER);
	}
}
