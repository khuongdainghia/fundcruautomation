package com.fundcru.fundraiser;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.objects.pages.TableFunction;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;


@Credentials(user="khuongdainghia@gmail.com", password="loveyou")
public class ShowDonationListAfterDonatedAFundraiser extends BaseTestCase {
	int timeout =30,numberDraftFundraiser;
	ScreenAction action;
	TableFunction table;
	JavascriptExecutor executor;
	WebElement fundraiser;
	WebElement parentElement;
	List<WebElement> actionList,noPage;
	int numberPage, noOfDonationAfterDonated, itemsOnCurrentPage;

	@Test
	public void ClickUserLink() {
		action = new ScreenAction(driver);
		table = new TableFunction(driver);
		executor = (JavascriptExecutor)driver;
		action.clickBtn(By.className(Home.USER_LINK_CLASS));
	}
	
	@Test(dependsOnMethods ="ClickUserLink",alwaysRun = true )
	public void clickMyFundraiser () {
		action.clickBtn(By.id(Home.MY_CAMPAIGN_LINK));
	}
	
	@Test(dependsOnMethods ="clickMyFundraiser",alwaysRun = true )
	public void CheckAction() {
		fundraiser =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(String.format(Campaign.FUNDRAISER_ITEM_ID,DonateFundraiser.fundraiserID))));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", fundraiser);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", fundraiser);
		parentElement = (WebElement)executor.executeScript("return arguments[0].parentNode;", fundraiser);
	}
	
	@Test(dependsOnMethods ="CheckAction",alwaysRun = true )
	public void CheckActionDropdownDisplaysForFundraiser () {
		actionList = parentElement.findElements(By.className(Campaign.ACTION_ITEM_CLASS));
	}
	
	@Test(dependsOnMethods ="CheckActionDropdownDisplaysForFundraiser",alwaysRun = true )
	public void CheckActionEditPresented() {
		Assert.assertTrue(actionList.get(0).getText().trim().contains("Edit"),String.format("Wrong expected: Edit but Actual: %s",actionList.get(0).getText().trim()));
	}
	
	@Test(dependsOnMethods ="CheckActionEditPresented",alwaysRun = true )
	public void CheckActionPublishPresented() {
		Assert.assertTrue(actionList.get(1).getText().trim().contains("Publish"),String.format("Wrong expected: Publish but Actual: %s",actionList.get(1).getText().trim()));
	}
	
	@Test(dependsOnMethods ="CheckActionPublishPresented",alwaysRun = true )
	public void CheckActionShowDonationsPresented() {
		Assert.assertTrue(actionList.get(2).getText().trim().contains("Show donations"),String.format("Wrong expected: Show donations but Actual: %s",actionList.get(2).getText().trim()));
	}
	
	@Test(dependsOnMethods ="CheckActionShowDonationsPresented",alwaysRun = true )
	public void ClickShowDonationsAction() {
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", fundraiser);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", actionList.get(2));
	}
	
	@Test(dependsOnMethods ="ClickShowDonationsAction",alwaysRun = true )
	public void NavigateToDonationListScreen() {
		 WebElement fundraiserDonation =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.DONATION_LIST_LABLE)));
		 Assert.assertEquals(fundraiserDonation.getText().trim(),"Donation list");
	}
	
	@Test(dependsOnMethods ="NavigateToDonationListScreen",alwaysRun = true )
	public void getPaginationItems () {
		 WebElement pageList= driver.findElement(By.cssSelector(Campaign.DONATION_PAGINATION));
		 noPage = pageList.findElements(By.tagName("li"));
		 numberPage = noPage.size();
		 noOfDonationAfterDonated = table.countRow();
	}
	
	@Test(dependsOnMethods ="getPaginationItems",alwaysRun = true )
	public void getTotalOfDonations () {
		for(int i=2; i<numberPage-1;i++) {
			noPage.get(i).click();
			noOfDonationAfterDonated = noOfDonationAfterDonated + table.countRow();
		}
	}
	
	@Test(dependsOnMethods ="getTotalOfDonations",alwaysRun = true )
	public void CheckTotalOfDonationsAfterDonated() {
		 Assert.assertEquals(noOfDonationAfterDonated,CheckDonationListBeforeDonating.noOfDonation+1);
	}
	
	@Test(dependsOnMethods ="CheckTotalOfDonationsAfterDonated",alwaysRun = true )
	public void AssertDonationName() {
		 itemsOnCurrentPage = table.countRow();
		 table.assertValueRow(2, itemsOnCurrentPage, "Nicholas");
	}
	
	@Test(dependsOnMethods ="AssertDonationName",alwaysRun = true )
	public void AssertDonationEmail() {
		 table.assertValueRow(3, itemsOnCurrentPage, "johndoe@gmail.com");
	}
	
	@Test(dependsOnMethods ="AssertDonationEmail",alwaysRun = true )
	public void AssertDonationMessage() {
		 table.assertValueRow(4, itemsOnCurrentPage, "Good job!");
	}
	
	@Test(dependsOnMethods ="AssertDonationMessage",alwaysRun = true )
	public void AssertDonationAmount() {
		 table.assertValueRow(5, itemsOnCurrentPage, "$10.00");
	}
	
	@Test(dependsOnMethods ="AssertDonationAmount",alwaysRun = true )
	public void ClickUserLinkII () {
		action.clickBtn(By.className(Home.USER_LINK_CLASS));
	}
	
	@Test(dependsOnMethods ="ClickUserLinkII",alwaysRun = true )
	public void ClickMyDonation() {
		action.clickBtn(By.id(Home.MY_DONATIONS_LINK));
	}
	
	@Test(dependsOnMethods ="ClickMyDonation",alwaysRun = true )
	public void CheckMyDonationPageDisplays() {
		WebElement fundraiserDonation =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.MY_DONATION_LIST)));
		Assert.assertEquals(fundraiserDonation.getText().trim(),"My donations");
	}
	
	@Test(dependsOnMethods ="CheckMyDonationPageDisplays",alwaysRun = true )
	public void CheckDonationListAfterDonating () {
	 	Assert.assertEquals(table.countRow(),CheckDonationListBeforeDonating.noOfMyDonation+1);
	}
	
	@Test(dependsOnMethods ="CheckDonationListAfterDonating",alwaysRun = true )
	public void CheckFundraiserTitleDonation() {
		table.assertValueRow(3, 1, "Draft Urban Xtreme Adventures Modified");
	}
	
	@Test(dependsOnMethods ="CheckFundraiserTitleDonation",alwaysRun = true )
	public void CheckFundraiserAmountDonation() {
		table.assertValueRow(4, 1, "$10.00");
	}
}
