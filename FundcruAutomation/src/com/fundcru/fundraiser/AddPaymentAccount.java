package com.fundcru.fundraiser;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;

@Credentials(user="khuongdainghia@gmail.com", password="loveyou")
public class AddPaymentAccount extends BaseTestCase {
	int timeout =30,numberOfAccount;
	@Test
	public void ClickOnUserLink() {
		WebElement userLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		userLink.click();
		driver.findElement(By.id(Home.PAYMENT_ACCOUNTS)).click();
		WebElement linkAddPaymentAccount= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.linkText(Home.ADD_PAYMENT_ACOUNT_LINK_TEXT)));
		Assert.assertEquals(linkAddPaymentAccount.getText(),Home.ADD_PAYMENT_ACOUNT_LINK_TEXT);
	}
	@Test(dependsOnMethods="ClickOnUserLink",alwaysRun = true)
	public void AccessToPaymentAccount() {
		driver.get(getServerURL() + "/payment/accounts");
		
	}
	@Test(dependsOnMethods="AccessToPaymentAccount",alwaysRun = true)
	public void ClickOnAddPaymentButton() {
		WebElement linkAddPaymentAccount= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.linkText(Home.ADD_PAYMENT_ACOUNT_LINK_TEXT)));
		Assert.assertEquals(linkAddPaymentAccount.getText(),Home.ADD_PAYMENT_ACOUNT_LINK_TEXT);
		WebElement tablePayment= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(Home.TABLE_BODY_CSS)));
		List<WebElement> tablePaymentRow = tablePayment.findElements(By.tagName("tr"));
		numberOfAccount =tablePaymentRow.size();
		linkAddPaymentAccount.click();
	}
	@Test(dependsOnMethods="ClickOnAddPaymentButton",alwaysRun = true)
	public void InputNameAndEmail() {
	    driver.findElement(By.id(Home.FIRST_NAME)).clear();
	    driver.findElement(By.id(Home.FIRST_NAME)).sendKeys("2nd");
	    driver.findElement(By.id(Home.LAST_NAME)).clear();
	    driver.findElement(By.id(Home.LAST_NAME)).sendKeys("Account");
	    driver.findElement(By.id(Home.EMAIL)).clear();
	    driver.findElement(By.id(Home.EMAIL)).sendKeys("khuongdainghia02t5@gmail.com");
	    driver.findElement(By.cssSelector(Home.CONFIRM_ADD_PAYMENT_ACCOUNT_BUTTON)).click();
	}
	@Test(dependsOnMethods="InputNameAndEmail",alwaysRun = true)
	public void CheckConfirmScreen() {
	    driver.findElement(By.linkText(Home.RETURN_TO_PAYMENT_ACCOUNT_LINK_TEXT)).click();
	}
	@Test(dependsOnMethods="InputNameAndEmail",alwaysRun = true)
	public void CheckInformationAfterAdded() {
		WebElement tablePayment= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(Home.TABLE_BODY_CSS)));
		List<WebElement> tablePaymentRow = tablePayment.findElements(By.tagName("tr"));
		Assert.assertEquals(tablePaymentRow.size(), numberOfAccount+1);
	    Assert.assertEquals(driver.findElement(By.xpath(Home.NAME_PAYMENT_ACCOUNT_ROW2_XPATH)).getText(), "2nd Account");
	    Assert.assertEquals(driver.findElement(By.xpath(Home.EMAIL_PAYMENT_ACCOUNT_ROW2_XPATH)).getText(), "khuongdainghia02t5@gmail.com");
	    Assert.assertEquals(driver.findElement(By.xpath(Home.NAME_PAYMENT_ACCOUNT_ROW1_XPATH)).getText(), "Nick Khuong");
	    Assert.assertEquals(driver.findElement(By.cssSelector(Home.EMAIL_PAYMENT_ACCOUNT_ROW1)).getText(), "khuongdainghia@gmail.com");
	}
	@Test(dependsOnMethods="CheckInformationAfterAdded",alwaysRun = true)
	public void ClickOnAddPaymentButtonAgain() {
		WebElement linkAddPaymentAccount= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.linkText(Home.ADD_PAYMENT_ACOUNT_LINK_TEXT)));
		Assert.assertEquals(linkAddPaymentAccount.getText(),Home.ADD_PAYMENT_ACOUNT_LINK_TEXT);
		linkAddPaymentAccount.click();
	}
	@Test(dependsOnMethods="ClickOnAddPaymentButtonAgain",alwaysRun = true)
	public void InputNameAndEmailToCheckIfEmailExist() {
	    driver.findElement(By.id(Home.FIRST_NAME)).clear();
	    driver.findElement(By.id(Home.FIRST_NAME)).sendKeys("2nd");
	    driver.findElement(By.id(Home.LAST_NAME)).clear();
	    driver.findElement(By.id(Home.LAST_NAME)).sendKeys("Account");
	    driver.findElement(By.id(Home.EMAIL)).clear();
	    driver.findElement(By.id(Home.EMAIL)).sendKeys("khuongdainghia02t5@gmail.com");
	    driver.findElement(By.cssSelector(Home.CONFIRM_ADD_PAYMENT_ACCOUNT_BUTTON)).click();
	    WebElement errorMessage= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(Home.ERROR_EXIST_PAYMENT_ACCOUNT)));
		Assert.assertTrue(errorMessage.getText().contains(Messages.PAYMENT_ACCOUNT_EXIST),String.format("Expected message: '%s' but actual: '%s'",Messages.PAYMENT_ACCOUNT_EXIST,errorMessage.getText()));
	    driver.findElement(By.linkText(Home.RETURN_PAYMENT_ACCOUNT_BUTTON_TEXT)).click();
	}
	@Test(dependsOnMethods="InputNameAndEmailToCheckIfEmailExist",alwaysRun = true)
	public void CancelDelete() {
		(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(Home.DELETE_PAYMENT_ACCOUNT_ROW2_XPATH))).click();
		WebElement cancelButton =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(Home.CANCEL_BUTTON_DIALOGUE)));
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",cancelButton);
	}
	@Test(dependsOnMethods="CancelDelete",alwaysRun = true)
	public void DelelePaymentAccount() {

		driver.get(getServerURL() + "/payment/accounts");
		(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(Home.DELETE_PAYMENT_ACCOUNT_ROW2_XPATH))).click();
		WebElement deleteButton =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(Home.DELETE_BUTTON_DIALOGUE)));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();",deleteButton);
	}
	@Test(dependsOnMethods="DelelePaymentAccount",alwaysRun = true)
	public void RecheckPaymentAfterDeleted() {
		WebElement tablePayment= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(Home.TABLE_BODY_CSS)));
		List<WebElement> tablePaymentRow = tablePayment.findElements(By.tagName("tr"));
		Assert.assertEquals(tablePaymentRow.size(), numberOfAccount);
	}
}
