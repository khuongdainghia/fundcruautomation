package com.fundcru.fundraiser;

import static org.testng.Assert.assertEquals;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.Linkedin;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.utilities.BaseTestCaseNoCredential;

public class ShareFundraiserToLinkedin extends BaseTestCaseNoCredential {
	int timeout =30;
	String email="fundcrutesting@gmail.com",pass="abc123456abc", campaignTitleValue;
	ScreenAction action;
	String mwh, popupHandle;
	String numberOfSharingBefore, numberOfSharingAfter;
			
	@Test
	public void ClickSignInLink() throws Exception {
		action = new ScreenAction(driver);
		driver.navigate().to(getServerURL()+"/fundraisers/SqXRSK9FoZGRipWrv");
		WebElement campaignTitles = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CAMPAIGN_TITLES)));
		campaignTitleValue = campaignTitles.getText();
	}
	
	@Test(dependsOnMethods ="ClickSignInLink",alwaysRun = true )
	public void GetNumberOfSharingBefore () {
		WebElement sharedNumber = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.TOTAL_NUMBER_OF_SHARING)));
		numberOfSharingBefore = sharedNumber.getText().substring(0, sharedNumber.getText().indexOf(" "));
	}
	
	@Test(dependsOnMethods ="GetNumberOfSharingBefore",alwaysRun = true )
	public void CheckButtonDisplayed() {
		WebElement btn = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.SHARE_LINKEDIN)));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn);
	}
	
	@Test(dependsOnMethods ="CheckButtonDisplayed",alwaysRun = true )
	public void LinkedinLogin () {
		mwh=driver.getWindowHandle();
		Set s=driver.getWindowHandles(); //this method will gives the handles of all opened windows
		Iterator ite=s.iterator();
		while(ite.hasNext())
		{
		    popupHandle=ite.next().toString();
		    if(!popupHandle.contains(mwh))
		    {
		        driver.switchTo().window(popupHandle);
		        ((JavascriptExecutor) driver).executeScript("window.showModalDialog = window.open;");
		        driver.getCurrentUrl();
		        boolean LinkedinWindow = false;
		        if(driver.getCurrentUrl().startsWith("https://www.linkedin.com")){
		        	LinkedinWindow = true;
		        }
		        assertEquals(LinkedinWindow, true);
		        driver.findElement(By.cssSelector(Linkedin.SIGNIN)).click();
		        action.waitObjVisible(By.cssSelector(Linkedin.LOGO));
		        driver.findElement(By.id(Linkedin.USERNAME)).sendKeys(email);
		        driver.findElement(By.id(Linkedin.PASSWORD)).sendKeys(pass);
		        driver.findElement(By.id(Linkedin.LOGIN_ID)).click();	
		    }
		}
	}
	
	@Test(dependsOnMethods ="LinkedinLogin",alwaysRun = true )
	public void CheckDataBeforeShareToLinkedin () {
        action.waitObjVisible(By.id(Linkedin.SHARE_OWNER));
		assertEquals(driver.findElement(By.id(Linkedin.SHARE_OWNER)).getText(), "FundCru");
		action.inputTextFieldBy(By.id(Linkedin.SHARE_TEXT), "Test"+Integer.parseInt(numberOfSharingBefore)+1);
	}
	
	@Test(dependsOnMethods ="CheckDataBeforeShareToLinkedin",alwaysRun = true )
	public void PostToLinkedin() {
		action.clickBtn(By.cssSelector(Linkedin.SHARE_BUTTON));
	}
	
	@Test(dependsOnMethods ="PostToLinkedin",alwaysRun = true )
	public void CheckMessageDisplayedAfterShare() {
		action.waitObjVisible(By.id(Linkedin.SHARE_ALERT_MESSAGE));
		assertEquals(driver.findElement(By.id(Linkedin.SHARE_ALERT_MESSAGE)).getText(), Messages.LINKEDIN_SHARE_ALERT_MESSAGE);
	}
	
	@Test(dependsOnMethods ="CheckMessageDisplayedAfterShare",alwaysRun = true )
	public void CloseLinkedinPopupWindowAfterShare () {
		action.clickBtn(By.cssSelector(Linkedin.WINDOW_CLOSURE_BUTTON));
		try {
	    	 driver.switchTo().window(mwh);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	@Test(dependsOnMethods ="CloseLinkedinPopupWindowAfterShare",alwaysRun = true )
	public void CheckBackToDefaultScreen() {
		action.assertTextEqual(By.cssSelector(Campaign.CAMPAIGN_TITLES), campaignTitleValue);
	}
	
	@Test(dependsOnMethods ="CheckBackToDefaultScreen",alwaysRun = true )
	public void RefreshPage() {
		driver.navigate().refresh();
		driver.navigate().refresh();
	}
	
	@Test(dependsOnMethods ="RefreshPage",alwaysRun = true )
	public void CheckNumberOfSharingIncreasingAfterShared () {
		WebElement sharedNumber = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.TOTAL_NUMBER_OF_SHARING)));
		numberOfSharingAfter = sharedNumber.getText().substring(0, sharedNumber.getText().indexOf(" "));
		assertEquals(Integer.parseInt(numberOfSharingAfter), Integer.parseInt(numberOfSharingBefore)+1);
	}
}
