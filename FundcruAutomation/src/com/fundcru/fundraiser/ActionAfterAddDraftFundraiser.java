package com.fundcru.fundraiser;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;


@Credentials(user="khuongdainghia@gmail.com", password="loveyou")
public class ActionAfterAddDraftFundraiser extends BaseTestCase {
	int timeout =30,numberDraftFundraiser;
	ScreenAction action;
	JavascriptExecutor executor;
	WebElement fundraiser;
	WebElement parentElement;
	List<WebElement> actionList;
	@Test
	public void CheckIfPrivateListBeforeCreateFundraiser() {
		executor = (JavascriptExecutor)driver;
		WebElement userLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		userLink.click();
		WebElement myFundraiser =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.MY_CAMPAIGN_LINK)));
		myFundraiser.click();
	}
	
	@Test(dependsOnMethods ="CheckIfPrivateListBeforeCreateFundraiser",alwaysRun = true )
	public void CheckNoOfPrivateFundraiserBefore() {
		WebElement listPrivateCampaign =(new WebDriverWait(driver, timeout*5)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.PUBLISHED_FUNDRAISER_LIST)));
		List<WebElement> publishList= listPrivateCampaign.findElements(By.className(Campaign.FUNDRAISER_ITEM_CLASS));
		numberDraftFundraiser = publishList.size();
	}
	@Test(dependsOnMethods ="CheckNoOfPrivateFundraiserBefore",alwaysRun = true )
	public void CheckAction() {
		fundraiser =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(String.format(Campaign.FUNDRAISER_ITEM_ID,AddDraftFundraiser.compaignID))));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", fundraiser);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", fundraiser);
		
		parentElement = (WebElement)executor.executeScript("return arguments[0].parentNode;", fundraiser);
		actionList = parentElement.findElements(By.className(Campaign.ACTION_ITEM_CLASS));
		Assert.assertTrue(actionList.get(0).getText().trim().contains("Edit"),String.format("Wrong expected: Edit but Actual: %s",actionList.get(0).getText().trim()));
		Assert.assertTrue(actionList.get(1).getText().trim().contains("Publish"),String.format("Wrong expected: Publish but Actual: %s",actionList.get(1).getText().trim()));
		Assert.assertTrue(actionList.get(2).getText().trim().contains("Show donations"),String.format("Wrong expected: Show donations but Actual: %s",actionList.get(2).getText().trim()));
	}
	@Test(dependsOnMethods ="CheckAction",alwaysRun = true )
	public void ClickEditAction() {
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", fundraiser);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", actionList.get(0));
	}
	@Test(dependsOnMethods ="ClickEditAction",alwaysRun = true )
	public void CheckToSeIfNavigateToRightPage() {
		WebElement campaignName = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.name(Campaign.CAMPAIGN_URL_NAME)));
		Assert.assertEquals(campaignName.getAttribute("value"), UpdateDraftFundraiser.updateURL);
	}
}
