package com.fundcru.fundraiser;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.Donate;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;


@Credentials(user="khuongdainghia@gmail.com", password="loveyou")
public class DonateFundraiserWithInvalidData extends BaseTestCase {
	int timeout = 30;
	
	@Test
	public void NavigateToCampaignForDonation() {
		driver.navigate().to(getServerURL() + "/raise/kl9vJoD2M");
	}
	
	@Test(dependsOnMethods ="NavigateToCampaignForDonation",alwaysRun = true )
	public void ClickDonateButton() {
		WebElement donateButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Campaign.DONATE_BUTTON)));
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",donateButton);	
	}
	
	@Test(dependsOnMethods ="ClickDonateButton",alwaysRun = true )
	public void CheckErrorAfterSubmitInCardInformationScreenWithoutAnyFieldEntered() {
		WebElement submitButtonForCardInfor = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Donate.SUBMIT_CARD_INFORMATION_SCREEN)));
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",submitButtonForCardInfor);	
		String errorOnZipcode = driver.findElement(By.cssSelector(Donate.ERROR_ON_ZIP_CODE)).getText();
		Assert.assertTrue(errorOnZipcode.equals(Messages.INVALID_ZIPCODE_WEPAY), "Zipcode - wrong error message:" + errorOnZipcode);
		String errorOnCCV = driver.findElement(By.cssSelector(Donate.ERROR_ON_CCV)).getText();
		Assert.assertTrue(errorOnCCV.equals(Messages.INVALID_CCV_WEPAY), "CCV - wrong error message" + errorOnCCV);
		String errorOnExpirationDate = driver.findElement(By.cssSelector(Donate.ERROR_ON_EXPIRATION_DATE)).getText();
		Assert.assertTrue(errorOnExpirationDate.equals(Messages.INVALID_DATE_WEPAY), "Expiration Date - wrong error message:" + errorOnExpirationDate);
		String errorOnCardNumberField = driver.findElement(By.cssSelector(Donate.ERROR_ON_CARD_NUMBER)).getText();
		Assert.assertTrue(errorOnCardNumberField.equals(Messages.INVALID_CARD_NUMBER_WEPAY), "Card number - wrong error message:" + errorOnCardNumberField);
		String errorOnEmailField = driver.findElement(By.cssSelector(Donate.ERROR_ON_EMAIL)).getText();
		Assert.assertTrue(errorOnEmailField.equals(Messages.INVALID_EMAIL_WEPAY), "Email - wrong error message:" + errorOnEmailField);
		String errorNameOnCard = driver.findElement(By.cssSelector(Donate.ERROR_ON_NAME_ON_CARD)).getText();
		Assert.assertTrue(errorNameOnCard.equals(Messages.INVALID_NAME_ON_CARD_WEPAY), "Name on card - wrong error message:" + errorNameOnCard);
		Assert.assertTrue(!driver.findElements(By.cssSelector(Donate.ERROR_ON_AMOUNT)).isEmpty(),"There is no validation when don't input amount ");
		String errorAmount = driver.findElement(By.cssSelector(Donate.ERROR_ON_AMOUNT)).getText();
		Assert.assertTrue(errorAmount.equals(Messages.INVALID_AMOUNT_WEPAY), "Amount - wrong error message:" + errorAmount);    
	}
	@Test(dependsOnMethods ="CheckErrorAfterSubmitInCardInformationScreenWithoutAnyFieldEntered",alwaysRun = true )
	public void CheckErrorAfterInputInvalidEmail() {
		// Check error when input invalid card number
	    WebElement email = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Donate.EMAIL)));
	    email.clear();
	    email.sendKeys("invalidEmail");
	    WebElement submitButtonForCardInfor = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Donate.SUBMIT_CARD_INFORMATION_SCREEN)));
	    JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",submitButtonForCardInfor);	
	    String errorInvalidEmail = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Donate.ERROR_ON_EMAIL))).getText();
		Assert.assertTrue(errorInvalidEmail.equals(Messages.INVALID_EMAIL_WEPAY), "Wrong error message for invalid email:" + errorInvalidEmail);
	}
	
	@Test(dependsOnMethods ="CheckErrorAfterInputInvalidEmail",alwaysRun = true )
	public void CheckErrorAfterInputInvalidCardNumber() {
		// Check error when input invalid card number
	    WebElement cardNumber = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Donate.CARD_NUMBER)));
	    cardNumber.clear();
	    cardNumber.sendKeys("1111112233");
	    WebElement submitButtonForCardInfor = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Donate.SUBMIT_CARD_INFORMATION_SCREEN)));
	    JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",submitButtonForCardInfor);	
	    String errorInvalidCardNumber = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Donate.ERROR_ON_CARD_NUMBER))).getText();
		Assert.assertTrue(errorInvalidCardNumber.equals(Messages.INVALID_CARD_NUMBER_WEPAY), "Wrong error message for invalid card number:" + errorInvalidCardNumber);
	}
	
	@Test(dependsOnMethods ="CheckErrorAfterInputInvalidCardNumber",alwaysRun = true )
	public void CheckErrorAfterInputInvalidCCV() {
		// Check error when input invalid CCV
	    WebElement ccv = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Donate.CVV)));
	    ccv.clear();
	    ccv.sendKeys("invalidCCV");
	    WebElement submitButtonForCardInfor = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Donate.SUBMIT_CARD_INFORMATION_SCREEN)));
	    JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",submitButtonForCardInfor);	
	    String errorInvalidCCV = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Donate.ERROR_ON_CCV))).getText();
		Assert.assertTrue(errorInvalidCCV.equals(Messages.INVALID_CCV_WEPAY), "Wrong error message for invalid CCV:" + errorInvalidCCV);
	}
	
	@Test(dependsOnMethods ="CheckErrorAfterInputInvalidCCV",alwaysRun = true )
	public void CheckErrorAfterInputInvalidExpirationDate() {
		// Check error when input invalid EXPIRATION date
	    WebElement expiration = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Donate.EXPIRATION)));
	    expiration.clear();
	    expiration.sendKeys("invalidExpiration");
	    WebElement submitButtonForCardInfor = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Donate.SUBMIT_CARD_INFORMATION_SCREEN)));
	    JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",submitButtonForCardInfor);	
	    String errorInvalidExpiration = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Donate.ERROR_ON_EXPIRATION_DATE))).getText();
		Assert.assertTrue(errorInvalidExpiration.equals(Messages.INVALID_DATE_WEPAY), "Wrong error message for invalid EXPIRATION date:" + errorInvalidExpiration);
	}
	
	@Test(dependsOnMethods ="CheckErrorAfterInputInvalidExpirationDate",alwaysRun = true )
	public void CheckErrorAfterInputInvalidNameOnCard() {
		WebElement amount = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Donate.AMOUNT)));
	    amount.clear();
	    amount.sendKeys("10");
		WebElement email = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Donate.EMAIL)));
		email.clear();
		email.sendKeys("usr1001@fundcru.com");
		WebElement cardNumber = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Donate.CARD_NUMBER)));
	    cardNumber.clear();
	    cardNumber.sendKeys("4003830171874018");
	    WebElement ccv = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Donate.CVV)));
	    ccv.clear();
	    ccv.sendKeys("123");
	    WebElement expiration = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Donate.EXPIRATION)));
	    expiration.clear();
	    expiration.sendKeys("11/22");
		WebElement zipcode = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Donate.ZIPCODE)));
		zipcode.clear();
		zipcode.sendKeys("12345");
		
		// Check error when input invalid name on card
	    WebElement nameOnCard = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Donate.DONATE_NAME)));
	    nameOnCard.clear();
	    nameOnCard.sendKeys("i");
	    WebElement submitButtonForCardInfor = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Donate.SUBMIT_CARD_INFORMATION_SCREEN)));
	    JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",submitButtonForCardInfor);	
	    String errorInvalidNameOnCard = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Donate.ERROR_ON_NAME_ON_CARD_FROM_WEPAY))).getText();
		Assert.assertTrue(errorInvalidNameOnCard.contains(Messages.INVALID_NAME_ON_CARD_WEPAY_GATEWAY), "Wrong error message for invalid name on card:" + errorInvalidNameOnCard);
	}
}
