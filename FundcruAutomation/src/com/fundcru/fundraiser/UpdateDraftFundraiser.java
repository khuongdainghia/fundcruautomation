package com.fundcru.fundraiser;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;


@Credentials(user="khuongdainghia@gmail.com", password="loveyou")

public class UpdateDraftFundraiser extends BaseTestCase {
	private static final String existingCampaign = "kl9vJoD2M";
	int campaignRaised=0;
	WebElement campaignEditMode,saveAndRewiewButton,campaignName ;
	int timeout =30;
	public static String newCampaignURL = "https://user122.fundcru.com/raise/auto_1000";
	JavascriptExecutor jsescrollUp;
	String oldTitle = "Draft Urban Xtreme Adventures";
	String oldPrimaryCategory = "Sports";
	String oldSecondaryCategory = "Charity";
	String oldCity = "Los Angeles, CA";
	String oldGoal = "$75,000.00";
	String oldStory = "BE PART OF BUILDING SOMETHING GREAT Would it be amazing if you could Ski, Snowboard, Bounce and Climb under one roof all year round in Brisbane? With your support we can! We are in the process of building Austrialia's FIRST Indoor Snow Sport and Adventure Centre! It will be right here in Brisbane within 15km of the CBD, opening mid 2017. In the lead up to our launch, we are now offering DISCOUNTED pre-sale passes for our ski and adventure activities. As a recognition of your support, all pre-sale passes will get limited time discounts as well as other awesome read on to find out more";
	
	String updatedPrimaryCategory = "Animals";
	String updatedSecondaryCategory = "Education";
	String updatedGoal = "1200";
	String checkUpdatedGoal = "$1,200.00";
	String updatedStory = "From the origin of a flower's name to its distinctive characteristics and rich mythology, flowers are infused with symbolism and meaning. Our Flower Meaning Guide is designed to unravel these hidden mysteries! ";
	String updatedSummary = "Draft Summary Modified";
	String updatedCampaignTitle = "Draft Urban Xtreme Adventures Modified";
	String updatedCampaignTitleMaxLength = "Max leng Urban Xtreme Adventures11111111111111111111111111111111111111111111111111111111111111111111111111111";
	String updatedCity = "Arizona";
	public static String updateURL = "auto_9117";
	ScreenAction action;
	
	@Test
	public void NavigateToCampaignForUpdating() {
		action = new ScreenAction(driver);
		jsescrollUp = (JavascriptExecutor)driver;
		driver.get(getServerURL() + "/fundraisers/" + AddDraftFundraiser.compaignID);
	}
	
	@Test(dependsOnMethods ="NavigateToCampaignForUpdating",alwaysRun = true )
	public void WaitForSwitchToEditModeDisplayed() {
		campaignEditMode = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.SWITCH_TO_EDIT_MODE)));
		WebElement campaignTitles = driver.findElement(By.cssSelector(Campaign.CAMPAIGN_TITLES));
		oldTitle = campaignTitles.getText();
		WebElement campaignPrimaryCategory = driver.findElement(By.cssSelector(Campaign.PRIMARY_CATERGORY_SUCCESS_1));
		oldPrimaryCategory = campaignPrimaryCategory.getText();
		WebElement campaignSecondartCategory = driver.findElement(By.cssSelector(Campaign.SECONDARY_CATERGORY_SUCCESS));
		oldSecondaryCategory = campaignSecondartCategory.getText();
		WebElement campaignCity = driver.findElement(By.cssSelector(Campaign.CITY_SUCCESS));
		oldCity = campaignCity.getText();
		WebElement campaignGoal = driver.findElement(By.cssSelector(Campaign.GOAL_SUCCESS));
		oldGoal = campaignGoal.getText();
		WebElement campaignStory = driver.findElement(By.cssSelector(Campaign.STORY_SUCCESS));
		oldStory = campaignStory.getText();
	}
	
	@Test(dependsOnMethods ="WaitForSwitchToEditModeDisplayed",alwaysRun = true )
	public void CheckCampaignTitle() {
		WebElement campaignTitle = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.TITLE_SUCCESS)));
		Assert.assertEquals(campaignTitle.getText(),oldTitle);
	}
	
	@Test(dependsOnMethods ="CheckCampaignTitle",alwaysRun = true )
	public void CheckCampaignGoalNumber() {
		WebElement campaignGoal = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.GOAL_SUCCESS)));
		Assert.assertEquals(campaignGoal.getText(), oldGoal);
	}
	
	@Test(dependsOnMethods ="CheckCampaignGoalNumber",alwaysRun = true )
	public void CheckCampaignPrimaryCategory() {
		WebElement campaignPrimary_Category = driver.findElement(By.cssSelector(Campaign.PRIMARY_CATERGORY_SUCCESS));
		Assert.assertEquals(campaignPrimary_Category.getText(), oldPrimaryCategory);
	}
	
	@Test(dependsOnMethods ="CheckCampaignPrimaryCategory",alwaysRun = true )
	public void CheckCampaignSecondaryCategory() {
		WebElement campaignSecondary_Category = driver.findElement(By.cssSelector(Campaign.SECONDARY_CATERGORY_SUCCESS));
		Assert.assertEquals(campaignSecondary_Category.getText(), oldSecondaryCategory);
	}
	
	@Test(dependsOnMethods ="CheckCampaignSecondaryCategory",alwaysRun = true )
	public void CheckCampaignCity() {
		WebElement campaignCity = driver.findElement(By.cssSelector(Campaign.CITY_SUCCESS));
		Assert.assertEquals(campaignCity.getText(), oldCity);
	}
	
	@Test(dependsOnMethods ="CheckCampaignCity",alwaysRun = true )
	public void CheckCampaignStory() {
		JavascriptExecutor jsescrolldown = (JavascriptExecutor)driver;
		// Scroll down the page
		jsescrolldown.executeScript("window.scrollBy(0,300)", "");
		WebElement campaignStory = driver.findElement(By.cssSelector(Campaign.DRAFTSTORY_FOR_UPDATING));
		Assert.assertEquals(campaignStory.getText(), oldStory);
	}
	
	@Test(dependsOnMethods ="CheckCampaignStory",alwaysRun = true )
	public void SwitchToEditMode() {
		JavascriptExecutor jsescrollUp = (JavascriptExecutor)driver;
		jsescrollUp.executeScript("arguments[0].scrollIntoView(true);", campaignEditMode);
		jsescrollUp.executeScript("arguments[0].click();", campaignEditMode);
	}
	
	@Test(dependsOnMethods ="SwitchToEditMode",alwaysRun = true )
	public void EditCompaignURL() {
		
		campaignName = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.name(Campaign.CAMPAIGN_URL_NAME)));
		campaignName.clear();
		Random r = new Random();
		int n = r.nextInt(10000);
		newCampaignURL =String.format("auto_%s",n);
		campaignName.sendKeys(newCampaignURL);
		action.pause(5000);
		saveAndRewiewButton=(new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.SAVE_AND_PREVIEW)));
		JavascriptExecutor jsescrollUp = (JavascriptExecutor)driver;
		jsescrollUp.executeScript("arguments[0].scrollIntoView(true);", saveAndRewiewButton);
		jsescrollUp.executeScript("arguments[0].click();", saveAndRewiewButton);
	}
	@Test(dependsOnMethods ="EditCompaignURL",alwaysRun = true )
	public void SwitchToEditModeAgain() {
		
		campaignEditMode=(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.SWITCH_TO_EDIT_MODE)));
		JavascriptExecutor jsescrollUp = (JavascriptExecutor)driver;
		jsescrollUp.executeScript("arguments[0].scrollIntoView(true);", campaignEditMode);
		jsescrollUp.executeScript("arguments[0].click();", campaignEditMode);
		
		campaignName = driver.findElement(By.name(Campaign.CAMPAIGN_URL_NAME));
		Assert.assertEquals(campaignName.getAttribute("value"),newCampaignURL);
		WebElement campaignURLPrefix = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CAMPAIGN_URL_PREFIX)));
		System.out.println(String.format("New URL from system: %s",String.format("%s%s",campaignURLPrefix.getAttribute("innerHTML"),newCampaignURL)));
		newCampaignURL = String.format("%s/raise/%s", getServerURL(),newCampaignURL);
		System.out.println(String.format("New URL from system: %s",newCampaignURL));
	}
	
	@Test(dependsOnMethods ="SwitchToEditModeAgain",alwaysRun = true )
	public void CheckDuplicateFundraiserURL() {
		
		campaignName = driver.findElement(By.name(Campaign.CAMPAIGN_URL_NAME));
		updateURL = campaignName.getAttribute("value");
		campaignName.clear();
		campaignName.sendKeys(existingCampaign);
		saveAndRewiewButton =driver.findElement(By.cssSelector(Campaign.SAVE_AND_PREVIEW));
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", saveAndRewiewButton);
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", saveAndRewiewButton);
		WebElement errorGoalFieldRequired = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.ERROR_MESSAGE_FAILED_TO_UPDATE_CAMPAIGN)));
		String errorMessage = errorGoalFieldRequired.getText();
		Assert.assertTrue(errorMessage.contains(Messages.FAILED_TO_UPDATE_CAMPAIGN_URL_EXIST));
	}
	
	@Test(dependsOnMethods ="CheckDuplicateFundraiserURL",alwaysRun = true )
	public void InputURLBack() {
		campaignName.clear();
		campaignName.sendKeys(updateURL);
		action.pause(3000);
	}
	
	@Test(dependsOnMethods ="InputURLBack",alwaysRun = true )
	public void CheckErrorDisplayedWhenRemoveValueOnGoalField() {
		// Check error message displayed when leave the field Goal empty
		WebElement campaignGoalEditPage = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.name(Campaign.GOAL_ECP_NAME)));
		campaignGoalEditPage.clear();
		saveAndRewiewButton =driver.findElement(By.cssSelector(Campaign.SAVE_AND_PREVIEW));
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", saveAndRewiewButton);
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", saveAndRewiewButton);
		action.pause(3000);
		WebElement errorGoalFieldRequired = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.ERROR_MESSAGE_FAILED_TO_UPDATE_CAMPAIGN)));
		String errorMessage = errorGoalFieldRequired.getText();
		Assert.assertTrue(errorMessage.contains(Messages.FAILED_TO_UPDATE_CAMPAIGN));
	}
	
	@Test(dependsOnMethods ="CheckErrorDisplayedWhenRemoveValueOnGoalField",alwaysRun = true )
	public void ReUpdateCampaignGoal() {
		WebElement campaignGoalEditPage = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.name(Campaign.GOAL_ECP_NAME)));
		campaignGoalEditPage.clear();
		// Update Goal number to 80000
		campaignGoalEditPage.sendKeys(updatedGoal);
	}
	
	@Test(dependsOnMethods ="ReUpdateCampaignGoal",alwaysRun = true )
	public void UpdateCampaignTitle() {
		driver.findElement(By.cssSelector(Campaign.CAMPAIGN_TITLE_ECP)).clear();
		driver.findElement(By.cssSelector(Campaign.CAMPAIGN_TITLE_ECP)).sendKeys(updatedCampaignTitle);	
	}
	
	@Test(dependsOnMethods ="UpdateCampaignTitle",alwaysRun = true )
	public void UpdateCampaignCityCode() {
		WebElement campaignCityCode = driver.findElement(By.cssSelector(Campaign.City_Zip_Code));
		campaignCityCode.clear();
		campaignCityCode.sendKeys(updatedCity);
		action.pause(3000);
		(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CITY_LIST_1)));
		campaignCityCode.sendKeys(Keys.DOWN);
		campaignCityCode.sendKeys(Keys.ENTER);
	}
	
	@Test(dependsOnMethods ="UpdateCampaignCityCode",alwaysRun = true )
	public void UpdateCampaignCategory() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JavascriptExecutor jsescrolldown2 = (JavascriptExecutor)driver;
		jsescrolldown2.executeScript("window.scrollBy(0,200)", "");
		Select campaignPrimaryCategory = new Select( driver.findElement(By.cssSelector(Campaign.PRIMARY_CATEGORY_ECP)));
		campaignPrimaryCategory.selectByVisibleText(updatedPrimaryCategory);
		Select campaignSecondaryCategory = new Select(driver.findElement(By.cssSelector(Campaign.SECONDARY_CATEGORY_ECP)));
		campaignSecondaryCategory.selectByVisibleText(updatedSecondaryCategory);
	}
	
	@Test(dependsOnMethods ="UpdateCampaignCategory",alwaysRun = true )
	public void UpdateCampaignSummary() {
		WebElement campaignSummary = driver.findElement(By.cssSelector(Campaign.CAMPAIGN_SUMMARY));
		campaignSummary.clear();
		campaignSummary.sendKeys(updatedSummary);
	}
	
	@Test(dependsOnMethods ="UpdateCampaignSummary",alwaysRun = true )
	public void UpdateCampaignStory() {
		// Scroll down the page
		JavascriptExecutor jsescroll = (JavascriptExecutor)driver;
		jsescroll.executeScript("window.scrollBy(0,300)", "");
	    WebElement campaignStoryTextArea = driver.findElement(By.cssSelector(Campaign.CAMPAIGN_STORY));
	    campaignStoryTextArea.clear();
	    campaignStoryTextArea.sendKeys(updatedStory);
	}
	
	@Test(dependsOnMethods ="UpdateCampaignStory",alwaysRun = true )
	public void ClickSaveAndPreviewButton() {
		saveAndRewiewButton =driver.findElement(By.cssSelector(Campaign.SAVE_AND_PREVIEW));
		jsescrollUp.executeScript("arguments[0].scrollIntoView(true);", saveAndRewiewButton);
		jsescrollUp.executeScript("arguments[0].click();", saveAndRewiewButton);
	}
	
	@Test(dependsOnMethods ="ClickSaveAndPreviewButton",alwaysRun = true )
	public void CheckEmptyForFundraiserTitle() {
		campaignEditMode=(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.SWITCH_TO_EDIT_MODE)));
		
		jsescrollUp.executeScript("arguments[0].scrollIntoView(true);", campaignEditMode);
		jsescrollUp.executeScript("arguments[0].click();", campaignEditMode);
		driver.findElement(By.cssSelector(Campaign.CAMPAIGN_TITLE_ECP)).clear();
		driver.findElement(By.cssSelector(Campaign.CAMPAIGN_TITLE_ECP)).sendKeys("");
		saveAndRewiewButton =driver.findElement(By.cssSelector(Campaign.SAVE_AND_PREVIEW));
		jsescrollUp.executeScript("arguments[0].scrollIntoView(true);", saveAndRewiewButton);
		jsescrollUp.executeScript("arguments[0].click();", saveAndRewiewButton);
		WebElement errorGoalFieldRequired = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.ERROR_MESSAGE_TITLE_REQUIRED_UPDATE_CAMPAIGN)));
		String errorMessage = errorGoalFieldRequired.getText();
		Assert.assertTrue(errorMessage.contains(Messages.FAILED_TO_UPDATE_CAMPAIGN_TITLE_REQUIRED_IN_LINE));
	}
	
	@Test(dependsOnMethods ="CheckEmptyForFundraiserTitle",alwaysRun = true )
	public void CheckMaxlengthAndEmptyForFundraiserTitle() {
		WebElement title= driver.findElement(By.cssSelector(Campaign.CAMPAIGN_TITLE_ECP));
		title.sendKeys(updatedCampaignTitleMaxLength);
		Assert.assertEquals(title.getAttribute("value").length(), 100, "Maximum length is incorrect!");
	}
	
	@Test(dependsOnMethods ="CheckMaxlengthAndEmptyForFundraiserTitle",alwaysRun = true )
	public void BackToPreviousScreen() {
		WebElement cancelEdit = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.linkText(Campaign.CANCEL_CHANGES_AND_PREVIEW)));
		jsescrollUp.executeScript("arguments[0].click();", cancelEdit);
	}
	
	@Test(dependsOnMethods ="BackToPreviousScreen",alwaysRun = true )
	public void CheckCampaignURL() {
		campaignEditMode = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.SWITCH_TO_EDIT_MODE)));
		driver.getCurrentUrl().substring(driver.getCurrentUrl().lastIndexOf("/")+1,driver.getCurrentUrl().length());
		System.out.println("Campaign ID: " +AddDraftFundraiser.compaignID);
		Assert.assertEquals(driver.getCurrentUrl(), String.format("%s/fundraisers/%s",getServerURL(),AddDraftFundraiser.compaignID));
	}

	
	@Test(dependsOnMethods ="CheckCampaignURL",alwaysRun = true )
	public void CheckCampaignTitleAfterUpdating() {
		WebElement campaignTitle = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.TITLE_SUCCESS)));
		Assert.assertEquals(campaignTitle.getText(), updatedCampaignTitle);
	}
	
	@Test(dependsOnMethods ="CheckCampaignTitleAfterUpdating",alwaysRun = true )
	public void CheckCampaignSummaryAfterUpdating() {
		WebElement campaignSummary = driver.findElement(By.cssSelector(Campaign.CAMPAIGN_SUMMARY_AFTER_EDIT));
		Assert.assertEquals(campaignSummary.getText(), updatedSummary);
	}
	
	@Test(dependsOnMethods ="CheckCampaignSummaryAfterUpdating",alwaysRun = true )
	public void CheckCampaignGoalAfterUpdating() {
		WebElement campaignGoal = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.GOAL_SUCCESS)));
		Assert.assertEquals(campaignGoal.getText(), checkUpdatedGoal);
	}
	
	@Test(dependsOnMethods ="CheckCampaignGoalAfterUpdating",alwaysRun = true )
	public void CheckCampaignPrimaryCategoryAfterUpdating() {
		WebElement campaignPrimary_Category = driver.findElement(By.cssSelector(Campaign.PRIMARY_CATERGORY_SUCCESS));
		Assert.assertEquals(campaignPrimary_Category.getText(), updatedPrimaryCategory);
	}
	
	@Test(dependsOnMethods ="CheckCampaignPrimaryCategoryAfterUpdating",alwaysRun = true )
	public void CheckCampaignSecondaryCategoryAfterUpdating() {
		WebElement campaignSecondary_Category = driver.findElement(By.cssSelector(Campaign.SECONDARY_CATERGORY_SUCCESS));
		Assert.assertEquals(campaignSecondary_Category.getText(), updatedSecondaryCategory);
	}
	
	@Test(dependsOnMethods ="CheckCampaignSecondaryCategoryAfterUpdating",alwaysRun = true )
	public void CheckCampaignStoryAfterUpdating() {
		// Scroll down the page
		jsescrollUp.executeScript("window.scrollBy(0,300)", "");
		WebElement campaignStory = driver.findElement(By.cssSelector(Campaign.DRAFTSTORY_FOR_UPDATING));
		Assert.assertEquals(campaignStory.getText().trim(), updatedStory.trim());
	}

	@Test(dependsOnMethods ="CheckCampaignStoryAfterUpdating",alwaysRun = true )
	public void CheckCampaignCityAfterUpdating() {
		WebElement campaignCity = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CITY_SUCCESS_AFTER_UPDATE)));
		Assert.assertEquals(campaignCity.getText(), updatedCity);
	}
	
}
