package com.fundcru.fundraiser;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Login;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.utilities.BaseTestCaseNoCredential;

public class AddFundraiserWithoutLogin extends BaseTestCaseNoCredential {
	int timeout =10;
	String emailStr ="khuongdainghia@gmail.com",passwordStr="loveyou";
	ScreenAction action;
	@Test
	public void ClickOnCreateFundraiser() {
		action = new ScreenAction(driver);
		action.waitObjVisibleAndClick(By.linkText(Home.CREATE_A_FUNDRAISER_LINK_TEXT));
	}
	
	@Test(dependsOnMethods ="ClickOnCreateFundraiser",alwaysRun = true )
	public void RequiredLoginErrorDisplayedAfterSignOut() {
		action.waitObjInvisible((By.cssSelector(Login.ERROR_MESSAGE)));
	}
	
	@Test(dependsOnMethods ="RequiredLoginErrorDisplayedAfterSignOut" ,alwaysRun = true)
	public void InputValidEmailAndPasswordToLogin() {
		action.inputTextFieldByID(Login.EMAIL_TEXT_FIELD_ID, emailStr);
		action.inputTextFieldByID(Login.PASSWORD_TEXT_FIELD_ID, passwordStr);
	}
	
	@Test(dependsOnMethods ="InputValidEmailAndPasswordToLogin",alwaysRun = true )
	public void ClickSubmit() {
		driver.findElement(By.id(Login.SIGNIN_BUTTON_ID)).click();
	}
	
	@Test(dependsOnMethods ="ClickSubmit",alwaysRun = true )
	public void EnterCampaignGoal() {
		action.inputTextFieldBy(By.cssSelector(Campaign.GOAL), "75000");
	}
}
