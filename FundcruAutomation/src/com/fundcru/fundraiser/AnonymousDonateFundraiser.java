package com.fundcru.fundraiser;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.DonateWepay;
import com.fundcru.utilities.BaseTestCaseNoCredential;

public class AnonymousDonateFundraiser extends BaseTestCaseNoCredential {
	float campaignRaised=0;
	int numberBeforeDonate;
	int numOfDonors;
	List<WebElement> numOfTestimonial;
	int timeout = 45;
	String campaignTitleValue;
	String campaignCategoryValue;
	String campaignCityValue;
	String campaignGoalValue;
	
	String supporterName = "Nicholas";
	String testimonialMessage = "Good job!";
	
	@Test
	public void NavigateToCampaignForDonation() {
		driver.get(getServerURL() + "/fundraisers/SqXRSK9FoZGRipWrv");
	}
	@Test(dependsOnMethods ="NavigateToCampaignForDonation",alwaysRun = true )
	public void CheckNumnerOfDonors() {
		WebElement campaignDonors= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CAMPAIGN_DONORS)));
		numOfDonors = Integer.valueOf(campaignDonors.getText());
		WebElement campaignTitles = driver.findElement(By.cssSelector(Campaign.CAMPAIGN_TITLES));
		campaignTitleValue = campaignTitles.getText();
		WebElement campaignPrimaryCategory = driver.findElement(By.cssSelector(Campaign.PRIMARY_CATERGORY_SUCCESS_1));
		campaignCategoryValue = campaignPrimaryCategory.getText();
		WebElement campaignCity = driver.findElement(By.cssSelector(Campaign.CITY_SUCCESS));
		campaignCityValue = campaignCity.getText();
		WebElement campaignGoal = driver.findElement(By.cssSelector(Campaign.GOAL_SUCCESS));
		campaignGoalValue = campaignGoal.getText();
	}
	@Test(dependsOnMethods ="CheckNumnerOfDonors",alwaysRun = true )
	public void GetNumberOfTestimonial() {
		numOfTestimonial = driver.findElements(By.xpath(Campaign.TESTIMONIAL_LIST));
	    numberBeforeDonate = numOfTestimonial.size();
	}
	@Test(dependsOnMethods ="GetNumberOfTestimonial",alwaysRun = true )
	public void GetCampaignRaised() {
		WebElement campaignRaisedElement= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CAMPAIGN_RAISED)));
		campaignRaised= Float.valueOf(campaignRaisedElement.getText().replace("$", "").replace(",", ""));	
	}
	@Test(dependsOnMethods ="GetCampaignRaised",alwaysRun = true )
	public void ClickDonateButton() {
		WebElement donateButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Campaign.DONATE_BUTTON)));
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",donateButton);	
	}

	@Test(dependsOnMethods ="ClickDonateButton",alwaysRun = true )
	public void ReInPutCardNumber() {
		WebElement cardNumber = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.CARD_NUMBER)));
	    cardNumber.clear();
	    cardNumber.sendKeys("4003830171874018");
	}
	
	@Test(dependsOnMethods ="ReInPutCardNumber",alwaysRun = true )
	public void ReInPutCCV() {
		WebElement ccv = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.CVV)));
	    ccv.clear();
	    ccv.sendKeys("123");
	}
	
	@Test(dependsOnMethods ="ReInPutCCV",alwaysRun = true )
	public void ReInPutExpirationDate() {
		WebElement expiration = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.EXPIRATION)));
	    expiration.clear();
	    expiration.sendKeys("11/22");
	}
	
	@Test(dependsOnMethods ="ReInPutExpirationDate",alwaysRun = true )
	public void ReInPutNameOnCard() {
		WebElement nameOnCard = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.DONATE_NAME)));
	    nameOnCard.clear();
	    nameOnCard.sendKeys("John Doe");
	}
	@Test(dependsOnMethods ="ReInPutNameOnCard",alwaysRun = true )
	public void ReInPutEmail() {
		WebElement email = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.EMAIL)));
		email.clear();
		email.sendKeys("johndoe@gmail.com");
	}
	@Test(dependsOnMethods ="ReInPutEmail",alwaysRun = true )
	public void InputDonateAmount() {
		WebElement amount = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.AMOUNT)));
	    amount.clear();
	    amount.sendKeys("10");
	}
	@Test(dependsOnMethods ="InputDonateAmount",alwaysRun = true )
	public void InputZipcode() {
		WebElement zipcode = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.ZIPCODE)));
		zipcode.clear();
		zipcode.sendKeys("12345");
	}
	@Test(dependsOnMethods ="InputZipcode",alwaysRun = true )
	public void SubmitAfterInputCardInformation() {
		WebElement submitButtonForCardInfor = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(DonateWepay.SUBMIT_CARD_INFORMATION_SCREEN)));
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",submitButtonForCardInfor);	
	}

	@Test(dependsOnMethods ="SubmitAfterInputCardInformation")
	public void EnterDonateName() {
	    WebElement donateName = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.DONATE_NAME_ON_TESTIMONIAL )));
	    donateName.clear();
	    try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    donateName.sendKeys(supporterName);
	}
	
	@Test(dependsOnMethods ="EnterDonateName")
	public void EnterTestimonial() {
		WebElement donateTestimonial = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.DONATE_MESSAGE_ON_TESTIMONIAL )));
		donateTestimonial.clear();
		donateTestimonial.sendKeys(testimonialMessage);
	}
	
	@Test(dependsOnMethods ="EnterTestimonial")
	public void ClickShareButton() {
		(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(DonateWepay.SHARE_BUTTON))).click();
	}
	
	@Test(dependsOnMethods ="ClickShareButton" )
	public void CheckCampaignTitleAfterDonation() {
		//check result here 
		WebElement campaignTitle = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.TITLE_SUCCESS)));
		Assert.assertEquals(campaignTitle.getText(), campaignTitleValue);
	}
	
	@Test(dependsOnMethods ="CheckCampaignTitleAfterDonation")
	public void CheckGoalAfterDonation() {
		WebElement campaignGoal = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.GOAL_SUCCESS)));
		Assert.assertEquals(campaignGoal.getText(), campaignGoalValue);
	}
	
	@Test(dependsOnMethods ="CheckGoalAfterDonation", alwaysRun = true)
	public void CheckCampaignRaisedAfterDonation() {
		WebElement campaignRaisedElement= driver.findElement(By.cssSelector(Campaign.CAMPAIGN_RAISED));
		campaignRaised += 10.00;
		Float campaignCurrentRaised = Float.valueOf(campaignRaisedElement.getText().replace("$", "").replaceAll(",", ""));
		Assert.assertEquals(campaignCurrentRaised, campaignRaised);
	}
	
	@Test(dependsOnMethods ="CheckCampaignRaisedAfterDonation", alwaysRun = true)
	public void CheckCampaignCategoryAfterDonation() {
		WebElement campaignSecondary_Category = driver.findElement(By.cssSelector(Campaign.PRIMARY_CATERGORY_SUCCESS));
		Assert.assertEquals(campaignSecondary_Category.getText(), campaignCategoryValue);
	}
	
	@Test(dependsOnMethods ="CheckCampaignCategoryAfterDonation", alwaysRun = true)
	public void CheckCampaignStoryAfterDonation() {
		// Scroll down the page
		JavascriptExecutor jsedown = (JavascriptExecutor)driver;
		jsedown.executeScript("window.scrollBy(0,300)", "");
//		WebElement campaignStory = driver.findElement(By.cssSelector(Campaign.STORY_SUCCESS));
//		Assert.assertEquals(campaignStory.getText(),"BE PART OF BUILDING SOMETHING GREAT Wouldn t it be amazing if you could Ski, Snowboard, Bounce and Climb under one roof all year round in Brisbane? With your support we can! We are in the process of building Austrialia s FIRST Indoor Snow Sport and Adventure Centre! It will be right here in Brisbane within 15km of the CBD, opening mid 2017. In the lead up to our launch, we are now offering DISCOUNTED pre-sale passes for our ski and adventure activities. As a recognition of your support, all pre-sale passes will get limited time discounts as well as other awesome perks read on to find out more!");
	}
	
	@Test(dependsOnMethods ="CheckCampaignStoryAfterDonation", alwaysRun = true)
	public void CheckCampaignDonorsAfterDonation() {
		WebElement campaignDonors= driver.findElement(By.cssSelector(Campaign.CAMPAIGN_DONORS));
		Assert.assertTrue(Integer.valueOf(campaignDonors.getText().trim())==(numOfDonors+1));
	}
	
	@Test(dependsOnMethods ="CheckCampaignDonorsAfterDonation", alwaysRun = true)
	public void CheckCampaignTestimonialAfterDonation() {
		WebElement campaignCity = driver.findElement(By.cssSelector(Campaign.CITY_SUCCESS));
		WebElement testimonialName = driver.findElement(By.cssSelector(Campaign.TESTIMONIAL_NAME));
		WebElement testimonialText = driver.findElement(By.cssSelector(Campaign.TESTIMONIAL_TEXT));
		Assert.assertEquals(testimonialName.getText(), supporterName);
		Assert.assertEquals(testimonialText.getText(), testimonialMessage);
		Assert.assertEquals(campaignCity.getText(), campaignCityValue);
	}
	
	@Test(dependsOnMethods ="CheckCampaignTestimonialAfterDonation", alwaysRun = true)
	public void CheckDonatedSuccessfulTestimonial() {
		numOfTestimonial = driver.findElements(By.xpath(Campaign.TESTIMONIAL_LIST));
		Assert.assertEquals(numberBeforeDonate, numOfTestimonial.size());
		}
}
