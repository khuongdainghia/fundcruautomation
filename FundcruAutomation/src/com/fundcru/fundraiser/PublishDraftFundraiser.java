package com.fundcru.fundraiser;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;


@Credentials(user="khuongdainghia@gmail.com", password="loveyou")
public class PublishDraftFundraiser extends BaseTestCase {
	int campaignRaised=0;
	WebElement campaignEditMode,saveAndRewiewButton,campaignName ;
	int timeout =30,numberDraftFundraiser;
	String updatedPrimaryCategory = "Animals";
	String updatedSecondaryCategory = "Education";
	String updatedGoal = "1200";
	String checkUpdatedGoal = "$1,200.00";
	String updatedStory = "From the origin of a flower's name to its distinctive characteristics and rich mythology, flowers are infused with symbolism and meaning. Our Flower Meaning Guide is designed to unravel these hidden mysteries! ";
	String updatedSummary = "Draft Summary Modified";
	String updatedCampaignTitle = "Draft Urban Xtreme Adventures Modified";
	String updatedCity = "Arizona";
	String oldURL;
	String emailStr="khuongdainghia@gmail.com";
	String nameStr="Nick Khuong";
	ScreenAction action;
	@Test
	public void CheckIfPrivateListBeforeCreateFundraiser() {
		WebElement userLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		userLink.click();
		WebElement myFundraiser =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.MY_CAMPAIGN_LINK)));
		myFundraiser.click();
	}
	
	@Test(dependsOnMethods ="CheckIfPrivateListBeforeCreateFundraiser",alwaysRun = true )
	public void CheckNoOfPrivateFundraiserBefore() {
		WebElement listPrivateCampaign =(new WebDriverWait(driver, timeout*5)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.PUBLISHED_FUNDRAISER_LIST)));
		List<WebElement> privateList= listPrivateCampaign.findElements(By.className(Campaign.FUNDRAISER_ITEM_CLASS));
		numberDraftFundraiser = privateList.size();
	}
	@Test(dependsOnMethods ="CheckNoOfPrivateFundraiserBefore",alwaysRun = true )
	public void NavigateToCampaignForUpdating() {
		action = new ScreenAction(driver);
		driver.get(getServerURL() + "/fundraisers/" + AddDraftFundraiser.compaignID);
	}
	@Test(dependsOnMethods ="NavigateToCampaignForUpdating",alwaysRun = true )
	public void selectPaymentAccountBeforePublishing() {
		WebElement selectPayment =null;
		WebElement selectPaymentText = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.SELECT_PAYMENT_ACCOUNT_TEXT_CONTENT)));
		if (selectPaymentText.getText().contains(Messages.SELECT_PAYMENT_ACCOUNT_TEXT_CONTENT))
		{
			selectPayment= (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.linkText(Campaign.SELECT_PAYMENT_ACCOUNT_TEXT)));
		}
		else
		{
			selectPayment= (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.linkText(Campaign.CHANGE_PAYMENT_ACCOUNT_TEXT)));
		}
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", selectPayment);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", selectPayment);
		WebElement paymentAccount1 = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Campaign.PAYMENT_ACCOUNT_1)));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", paymentAccount1);
	}
	
	@Test(dependsOnMethods ="selectPaymentAccountBeforePublishing",alwaysRun = true )
	public void publishCampaignAferUpdated() {
		
		WebElement publish = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.PUBLISH_FROM_DRAFT)));
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", publish);
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", publish);
		action.waitObjInvisible(By.cssSelector(Campaign.PUBLISH_FROM_DRAFT));
	}
	@Test(dependsOnMethods ="publishCampaignAferUpdated",alwaysRun = true )
	public void checkAccountAfterPublished() {
		
		WebElement paymentAccountAfterPublised = driver.findElement(By.cssSelector(Campaign.PAYMENT_ACCOUNT_FOR_FUNRAISER));
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", paymentAccountAfterPublised);
		Assert.assertEquals(paymentAccountAfterPublised.getText(), String.format(Messages.FUNDRAISER_PAYMENT_ACCOUNT_TEXT,nameStr,emailStr));
		
		
	}
	@Test(dependsOnMethods ="checkAccountAfterPublished",alwaysRun = true )
	public void CheckIfFundraiserListIncreased() {
		WebElement userLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", userLink);
		action.pause(500);
		WebElement myFundraiser =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.MY_CAMPAIGN_LINK)));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", myFundraiser);
		action.pause(500);	
	}
	
	@Test(dependsOnMethods ="CheckIfFundraiserListIncreased",alwaysRun = true )
	public void CheckNoOfFundraiser() {
		WebElement listPrivateCampaign =(new WebDriverWait(driver, timeout*5)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.PUBLISHED_FUNDRAISER_LIST)));
		List<WebElement> privateList= listPrivateCampaign.findElements(By.className(Campaign.FUNDRAISER_ITEM_CLASS));
		Assert.assertEquals(privateList.size(),numberDraftFundraiser+1,String.format("Wrong expected number of Fundraiser Expected: %s Actual: %s",numberDraftFundraiser+1,privateList.size()));
	}
	@Test(dependsOnMethods ="CheckNoOfFundraiser",alwaysRun = true )
	public void LogOut() {
		WebElement userLink =(new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", userLink);
		WebElement logOutButton =(new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.SIGN_OUT)));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", logOutButton);
	}
	
	@Test(dependsOnMethods ="LogOut",alwaysRun = true )
	public void NavigateToNewCampaignAfterLogout() {
		driver.get(getServerURL() + "/fundraisers/" +AddDraftFundraiser.compaignID);
	}
	@Test(dependsOnMethods ="NavigateToNewCampaignAfterLogout",alwaysRun = true )
	public void checkToSeeIfCanAccessWithoutLoginNow() {
		WebElement campaignTitle = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.TITLE_SUCCESS)));
		Assert.assertEquals(campaignTitle.getText(), updatedCampaignTitle);
	}
	@Test(dependsOnMethods ="checkToSeeIfCanAccessWithoutLoginNow",alwaysRun = true )
	public void NavigateToNewCampaignURL() {
		driver.navigate().to(UpdateDraftFundraiser.newCampaignURL);
	}
	@Test(dependsOnMethods ="NavigateToNewCampaignURL",alwaysRun = true )
	public void checkToSeeIfCanAccessNewCampaignURL() {
		WebElement campaignTitle = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.TITLE_SUCCESS)));
		Assert.assertEquals(campaignTitle.getText(), updatedCampaignTitle);
	}
}
