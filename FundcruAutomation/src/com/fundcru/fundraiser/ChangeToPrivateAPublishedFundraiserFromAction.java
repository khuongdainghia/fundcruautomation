package com.fundcru.fundraiser;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;


@Credentials(user="khuongdainghia@gmail.com", password="loveyou")
public class ChangeToPrivateAPublishedFundraiserFromAction extends BaseTestCase {
	int timeout =30,numberPublishedFundraiser;
	ScreenAction action;
	JavascriptExecutor jsescrollUp;
	@Test
	public void CheckIfPrivateListBeforeCreateFundraiser() {
		action = new ScreenAction(driver);
		jsescrollUp = (JavascriptExecutor)driver;
		WebElement userLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		userLink.click();
		WebElement myFundraiser =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.MY_CAMPAIGN_LINK)));
		myFundraiser.click();
	}
	
	@Test(dependsOnMethods ="CheckIfPrivateListBeforeCreateFundraiser",alwaysRun = true )
	public void CheckNoOfPrivateFundraiserBefore() {
		WebElement listPrivateCampaign =(new WebDriverWait(driver, timeout*5)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.PUBLISHED_FUNDRAISER_LIST)));
		List<WebElement> publishList= listPrivateCampaign.findElements(By.className(Campaign.FUNDRAISER_ITEM_CLASS));
		numberPublishedFundraiser = publishList.size();
	}
	@Test(dependsOnMethods ="CheckNoOfPrivateFundraiserBefore",alwaysRun = true )
	public void ClickActionMakePrivate() {
		WebElement fundraiser =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(String.format(Campaign.FUNDRAISER_ITEM_ID,AddDraftFundraiser.compaignID))));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", fundraiser);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", fundraiser);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		WebElement parentElement = (WebElement)executor.executeScript("return arguments[0].parentNode;", fundraiser);
		List<WebElement> actionList = parentElement.findElements(By.className(Campaign.ACTION_ITEM_CLASS));
		Assert.assertEquals(actionList.size(), 3, "Wrong action number");
		Assert.assertTrue(actionList.get(0).getText().trim().contains("Edit"),String.format("Wrong expected: Edit but Actual: %s",actionList.get(0).getText().trim()));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",actionList.get(1));
		action.clickOkOnDilogueBox();
	}
	@Test(dependsOnMethods ="ClickActionMakePrivate",alwaysRun = true )
	public void NavigateToCampaignForUpdating() {
		action = new ScreenAction(driver);
		jsescrollUp = (JavascriptExecutor)driver;
		driver.get(getServerURL() + "/fundraisers/" + AddDraftFundraiser.compaignID);
	}
	
	@Test(dependsOnMethods ="NavigateToCampaignForUpdating",alwaysRun = true )
	public void WaitForSwitchToEditModeDisplayed() {
		(new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.SWITCH_TO_EDIT_MODE)));
	}
	@Test(dependsOnMethods ="WaitForSwitchToEditModeDisplayed",alwaysRun = true )
	public void AccessToMyFundraiserList() {
		jsescrollUp = (JavascriptExecutor)driver;
		WebElement userLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		userLink.click();
		WebElement myFundraiser =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.MY_CAMPAIGN_LINK)));
		myFundraiser.click();
	}
	@Test(dependsOnMethods ="AccessToMyFundraiserList",alwaysRun = true )
	public void CheckNoOfFundraiser() {
		WebElement listPrivateCampaign =(new WebDriverWait(driver, timeout*5)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.PUBLISHED_FUNDRAISER_LIST)));
		List<WebElement> publishedList= listPrivateCampaign.findElements(By.className(Campaign.FUNDRAISER_ITEM_CLASS));
		Assert.assertEquals(publishedList.size(),numberPublishedFundraiser-1,String.format("Wrong expected number of Fundraiser Expected: %s Actual: %s",numberPublishedFundraiser+1,publishedList.size()));
	}
	@Test(dependsOnMethods ="CheckNoOfFundraiser",alwaysRun = true )
	public void CheckActionAfterPublished() {
		WebElement fundraiser =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(String.format(Campaign.FUNDRAISER_ITEM_ID,AddDraftFundraiser.compaignID))));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", fundraiser);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", fundraiser);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		WebElement parentElement = (WebElement)executor.executeScript("return arguments[0].parentNode;", fundraiser);
		List<WebElement> actionList = parentElement.findElements(By.className(Campaign.ACTION_ITEM_CLASS));
		Assert.assertEquals(actionList.size(), 3, "Wrong action number");
		((JavascriptExecutor) driver).executeScript("arguments[0].click();",actionList.get(2));
	}
	@Test(dependsOnMethods ="CheckNoOfFundraiser",alwaysRun = true )
	public void CheckIfNavigateToRightScreen() {
	 WebElement fundraiserDonation =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.DONATION_LIST)));
	 Assert.assertEquals(fundraiserDonation.getText().trim(),"Donation list");
	 WebElement fundraiserDonationList =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.DONATION_LIST_NO_ITEM)));
	 Assert.assertEquals(fundraiserDonationList.getText().trim(),"This fundraiser doesn't have any donations yet.");
		
	}
}
