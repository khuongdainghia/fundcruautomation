package com.fundcru.fundraiser;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.DonateWepay;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;


@Credentials(user="khuongdainghia@gmail.com", password="loveyou")
public class DonateFundraiserWithoutTestImonial extends BaseTestCase {
	float campaignRaised=0;
	int numberBeforeDonate;
	int numOfDonors;
	List<WebElement> numOfTestimonial;
	int timeout = 30;
	String campaignTitleValue;
	String campaignCategoryValue;
	String campaignCityValue;
	String campaignGoalValue;
	String campaignStoryValue;
	
	@Test
	public void NavigateToCampaignForDonation() {
		driver.get(getServerURL() + "/raise/kl9vJoD2M");
	}
	@Test(dependsOnMethods ="NavigateToCampaignForDonation" )
	public void checkTestimonialBeforeDonating() {
		WebElement campaignDonors= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CAMPAIGN_DONORS)));
		numOfDonors = Integer.valueOf(campaignDonors.getText());
		WebElement campaignRaisedElement= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CAMPAIGN_RAISED)));
	    numOfTestimonial = driver.findElements(By.xpath(Campaign.TESTIMONIAL_LIST));
	    numberBeforeDonate = numOfTestimonial.size();
	    campaignRaised= Float.valueOf(campaignRaisedElement.getText().replace("$", "").replace(",", ""));
	    
		WebElement campaignTitles = driver.findElement(By.cssSelector(Campaign.CAMPAIGN_TITLES));
		campaignTitleValue = campaignTitles.getText();
		WebElement campaignPrimaryCategory = driver.findElement(By.cssSelector(Campaign.PRIMARY_CATERGORY_SUCCESS_1));
		campaignCategoryValue = campaignPrimaryCategory.getText();
		WebElement campaignCity = driver.findElement(By.cssSelector(Campaign.CITY_SUCCESS));
		campaignCityValue = campaignCity.getText();
		WebElement campaignGoal = driver.findElement(By.cssSelector(Campaign.GOAL_SUCCESS));
		campaignGoalValue = campaignGoal.getText();
		WebElement campaignStory = driver.findElement(By.cssSelector(Campaign.STORY_SUCCESS));
		campaignStoryValue = campaignStory.getText();
	}
	@Test(dependsOnMethods ="checkTestimonialBeforeDonating" )
	public void clickOnSupportButton() {
		WebElement donateButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Campaign.DONATE_BUTTON)));
		JavascriptExecutor jsescrollUp = (JavascriptExecutor)driver;
		jsescrollUp.executeScript("arguments[0].scrollIntoView(true);", donateButton);
		jsescrollUp.executeScript("arguments[0].click();", donateButton);
	}

	@Test(dependsOnMethods ="clickOnSupportButton",alwaysRun = true )
	public void ReInPutCardNumber() {
		WebElement cardNumber = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.CARD_NUMBER)));
	    cardNumber.clear();
	    cardNumber.sendKeys("4003830171874018");
	}
	
	@Test(dependsOnMethods ="ReInPutCardNumber",alwaysRun = true )
	public void ReInPutCCV() {
		WebElement ccv = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.CVV)));
	    ccv.clear();
	    ccv.sendKeys("123");
	}
	
	@Test(dependsOnMethods ="ReInPutCCV",alwaysRun = true )
	public void ReInPutExpirationDate() {
		WebElement expiration = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.EXPIRATION)));
	    expiration.clear();
	    expiration.sendKeys("11/22");
	}
	
	@Test(dependsOnMethods ="ReInPutExpirationDate",alwaysRun = true )
	public void ReInPutNameOnCard() {
		WebElement nameOnCard = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.DONATE_NAME)));
	    nameOnCard.clear();
	    nameOnCard.sendKeys("John Doe");
	}
	@Test(dependsOnMethods ="ReInPutNameOnCard",alwaysRun = true )
	public void ReInPutEmail() {
		WebElement email = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.EMAIL)));
		email.clear();
		email.sendKeys("usr1001@fundcru.com");
	}
	
	@Test(dependsOnMethods ="ReInPutEmail",alwaysRun = true )
	public void InputDonateAmount() {
		WebElement amount = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.AMOUNT)));
	    amount.clear();
	    amount.sendKeys("10");
	}
	
	@Test(dependsOnMethods ="InputDonateAmount",alwaysRun = true )
	public void InputZipcode() {
		WebElement zipcode = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(DonateWepay.ZIPCODE)));
		zipcode.clear();
		zipcode.sendKeys("12345");
	}
	
	@Test(dependsOnMethods ="InputZipcode",alwaysRun = true )
	public void SubmitAfterInputCardInformation() {
		WebElement submitButtonForCardInfor = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(DonateWepay.SUBMIT_CARD_INFORMATION_SCREEN)));
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",submitButtonForCardInfor);	
	}
	
	@Test(dependsOnMethods ="SubmitAfterInputCardInformation",alwaysRun = true)
	public void ClickShareButton() {
		(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(DonateWepay.SHARE_BUTTON))).click();
	}
	

	@Test(dependsOnMethods ="ClickShareButton")
	public void CheckGoalAfterDonation() {
		WebElement campaignGoal = (new WebDriverWait(driver, 90)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.GOAL_SUCCESS)));
		Assert.assertEquals(campaignGoal.getText(), campaignGoalValue);
	}
	
	@Test(dependsOnMethods ="CheckGoalAfterDonation", alwaysRun = true)
	public void CheckCampaignRaisedAfterDonation() {
		WebElement campaignRaisedElement= driver.findElement(By.cssSelector(Campaign.CAMPAIGN_RAISED));
		campaignRaised += 10.00;
		Float campaignCurrentRaised = Float.valueOf(campaignRaisedElement.getText().replace("$", "").replaceAll(",", ""));
		Assert.assertEquals(campaignCurrentRaised, campaignRaised);
	}
	
	@Test(dependsOnMethods ="CheckCampaignRaisedAfterDonation", alwaysRun = true)
	public void CheckCampaignCategoryAfterDonation() {
		WebElement campaignSecondary_Category = driver.findElement(By.cssSelector(Campaign.PRIMARY_CATERGORY_SUCCESS));
		Assert.assertEquals(campaignSecondary_Category.getText(), campaignCategoryValue);
	}
	
	@Test(dependsOnMethods ="CheckCampaignCategoryAfterDonation", alwaysRun = true)
	public void CheckCampaignStoryAfterDonation() {
		// Scroll down the page
		JavascriptExecutor jsedown = (JavascriptExecutor)driver;
		jsedown.executeScript("window.scrollBy(0,300)", "");
		WebElement campaignStory = driver.findElement(By.cssSelector(Campaign.STORY_SUCCESS));
		Assert.assertEquals(campaignStory.getText(), campaignStoryValue);
	}
	
	@Test(dependsOnMethods ="CheckCampaignStoryAfterDonation", alwaysRun = true)
	public void CheckCampaignDonorsAfterDonation() {
		WebElement campaignDonors= driver.findElement(By.cssSelector(Campaign.CAMPAIGN_DONORS));
		Assert.assertTrue(Integer.valueOf(campaignDonors.getText().trim())==(numOfDonors+1));
	}
	
	@Test(dependsOnMethods ="CheckCampaignDonorsAfterDonation", alwaysRun = true)
	public void CheckDonatedSuccessfulTestimonial() {
		numOfTestimonial = driver.findElements(By.xpath(Campaign.TESTIMONIAL_LIST));
		Assert.assertEquals(numberBeforeDonate, numOfTestimonial.size());
	}
}
