package com.fundcru.fundraiser;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;

@Credentials(user="khuongdainghia@gmail.com", password="loveyou")
public class AddFundraiser extends BaseTestCase {
	int timeout =5;
	@Test
	public void ClickOnUserLink() {
		WebElement userLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		userLink.click();
	}
	
	@Test(dependsOnMethods ="ClickOnUserLink",alwaysRun = true )
	public void ClickOnMyCampaignLink() {
		WebElement myCampaignLink =(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.MY_CAMPAIGN_LINK)));
		myCampaignLink.click();
	}
	
	@Test(dependsOnMethods ="ClickOnMyCampaignLink",alwaysRun = true )
	public void ClickOnCreateNewCampaign() {
		WebElement createNewCampaign =(new WebDriverWait(driver, 120)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CREATE_NEW_COMPAIGN)));
		createNewCampaign.click();
	}
	
	@Test(dependsOnMethods ="ClickOnCreateNewCampaign",alwaysRun = true )
	public void EnterCampaignGoal() {
		WebElement campaignGoal = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.GOAL)));
		campaignGoal.clear();
		campaignGoal.sendKeys("75000");
	}
	
	@Test(dependsOnMethods ="EnterCampaignGoal",alwaysRun = true )
	public void EnterCampaignTitle() {
		WebElement campaignTitle = driver.findElement(By.cssSelector(Campaign.TITLE));
		campaignTitle.clear();
		campaignTitle.sendKeys("Urban Xtreme Adventures");
	}
	
	@Test(dependsOnMethods ="EnterCampaignTitle",alwaysRun = true )
	public void EnterCampaignCityCode() {
		WebElement campaignCityCode = driver.findElement(By.cssSelector(Campaign.CITY_CODE));
		campaignCityCode.clear();
		campaignCityCode.sendKeys("Los Angeles, CA");
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CITY_LIST_1)));
		campaignCityCode.sendKeys(Keys.DOWN);
		campaignCityCode.sendKeys(Keys.ENTER);
	}
	
	@Test(dependsOnMethods ="EnterCampaignCityCode",alwaysRun = true )
	public void EnterCampaignCategory() {
		Select campaignPrimaryCategory = new Select( driver.findElement(By.name(Campaign.PRIMARY_CATERGORY)));
		Select campaignSecondaryCategory = new Select(driver.findElement(By.name(Campaign.SECONDARY_CATERGORY)));
		campaignPrimaryCategory.selectByVisibleText("Charity");
		campaignSecondaryCategory.selectByVisibleText("Sports");
	}
	
	@Test(dependsOnMethods ="EnterCampaignCategory",alwaysRun = true )
	public void EnterCampaignSummary() {
		WebElement campaignSummary = driver.findElement(By.name(Campaign.SUMMARY));
		campaignSummary.clear();
		campaignSummary.sendKeys("Summary");
	}
	
	@Test(dependsOnMethods ="EnterCampaignSummary",alwaysRun = true )
	public void AddPhotoForCampaign() {
	    WebElement uploadPhoto = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.name(Campaign.UPLOAD_PHOTO)));
	    JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",uploadPhoto);
	    WebElement campaignChooseImage = driver.findElement(By.xpath(Campaign.CHOOSE_IMAGE_XPATH));
	    campaignChooseImage.clear();
	    Path path = Paths.get(System.getProperty("user.dir"));
	    campaignChooseImage.sendKeys(path.resolve(System.getProperty("user.dir")+File.separator +"automation_testing.png").toAbsolutePath().toString());
//	    campaignChooseImage.sendKeys(System.getProperty("user.dir")+ File.separator +"automation_testing.png");
	    WebElement uploadButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.UPLOAD_BUTTON)));
	    js.executeScript("arguments[0].click();",uploadButton);
	    WebElement campaignImage =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.IMAGE)));
	    Assert.assertEquals(campaignImage.getAttribute("class"),"thumbnail");
	}
	
	@Test(dependsOnMethods ="AddPhotoForCampaign",alwaysRun = true )
	public void EnterCampaignTextArea() {
	    WebElement campaignStoryTextArea = driver.findElement(By.cssSelector(Campaign.STORY_TEXTAREA));
	    campaignStoryTextArea.clear();
	    campaignStoryTextArea.sendKeys("BE PART OF BUILDING SOMETHING GREAT Wouldn't it be amazing if you could Ski, Snowboard, Bounce and Climb under one roof all year round in Brisbane? With your support we can! We are in the process of building Austrialia's FIRST Indoor Snow Sport and Adventure Centre! It will be right here in Brisbane within 15km of the CBD, opening mid 2017. In the lead up to our launch, we are now offering DISCOUNTED pre-sale passes for our ski and adventure activities. As a recognition of your support, all pre-sale passes will get limited time discounts as well as other awesome perks read on to find out more! ");
	}
	
	@Test(dependsOnMethods ="EnterCampaignTextArea",alwaysRun = true )
	public void ClickSaveButton() {
	    ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.cssSelector(Campaign.SAVE)));
	}
	
	@Test(dependsOnMethods ="ClickSaveButton",alwaysRun = true )
	public void CheckTitleAfterSuccessfullyCreateCampaign() {
		//check result here 
		WebElement campaignTitle = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.TITLE_SUCCESS)));
		Assert.assertEquals(campaignTitle.getText(),"Urban Xtreme Adventures");	
	}
	
	@Test(dependsOnMethods ="CheckTitleAfterSuccessfullyCreateCampaign",alwaysRun = true )
	public void CheckGoalNumberAfterSuccessfullyCreateCampaign() {
		String compaignID = driver.getCurrentUrl().substring(driver.getCurrentUrl().lastIndexOf("/")+1,driver.getCurrentUrl().length());
		System.out.println("Campaign ID: " +compaignID);
		WebElement campaignGoal = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.GOAL_SUCCESS)));
		Assert.assertEquals("$75,000.00",campaignGoal.getText());	
	}
	
	@Test(dependsOnMethods ="CheckGoalNumberAfterSuccessfullyCreateCampaign",alwaysRun = true )
	public void CheckStoryAfterSuccessfullyCreateCampaign() {
		// Scroll down the page
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		WebElement campaignStory = driver.findElement(By.cssSelector(Campaign.STORY_SUCCESS));	
		Assert.assertEquals(campaignStory.getText(),"BE PART OF BUILDING SOMETHING GREAT Wouldn't it be amazing if you could Ski, Snowboard, Bounce and Climb under one roof all year round in Brisbane? With your support we can! We are in the process of building Austrialia's FIRST Indoor Snow Sport and Adventure Centre! It will be right here in Brisbane within 15km of the CBD, opening mid 2017. In the lead up to our launch, we are now offering DISCOUNTED pre-sale passes for our ski and adventure activities. As a recognition of your support, all pre-sale passes will get limited time discounts as well as other awesome perks read on to find out more!");
	}
	
	@Test(dependsOnMethods ="CheckStoryAfterSuccessfullyCreateCampaign",alwaysRun = true )
	public void CheckPrimaryCategoryAfterSuccessfullyCreateCampaign() {
		WebElement campaignPrimary_Category = driver.findElement(By.cssSelector(Campaign.SECONDARY_CATERGORY_SUCCESS));
		Assert.assertEquals(campaignPrimary_Category.getText(),"Sports");
	}
	
	@Test(dependsOnMethods ="CheckPrimaryCategoryAfterSuccessfullyCreateCampaign",alwaysRun = true )
	public void CheckSecondaryCategoryAfterSuccessfullyCreateCampaign() {
		WebElement campaignSecondary_Category = driver.findElement(By.cssSelector(Campaign.PRIMARY_CATERGORY_SUCCESS));
		Assert.assertEquals(campaignSecondary_Category.getText(),"Charity");
	}
	
	@Test(dependsOnMethods ="CheckSecondaryCategoryAfterSuccessfullyCreateCampaign",alwaysRun = true )
	public void CheckCityAfterSuccessfullyCreateCampaign() {
		WebElement campaignCity = driver.findElement(By.cssSelector(Campaign.CITY_SUCCESS));
		Assert.assertEquals(campaignCity.getText(),"Los Angeles, CA");
	}
	@Test(dependsOnMethods ="CheckCityAfterSuccessfullyCreateCampaign",alwaysRun = true )
	public void CheckSummaryAfterSuccessfullyCreateCampaign() {
		WebElement campaignSummary = driver.findElement(By.cssSelector(Campaign.SUMMARY_SUCCESS));
		Assert.assertEquals(campaignSummary.getText(),"Summary");
	}
}
