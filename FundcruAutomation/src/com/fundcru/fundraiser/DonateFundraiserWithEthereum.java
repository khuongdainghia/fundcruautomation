package com.fundcru.fundraiser;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.DonateWepay;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.objects.pagedefinitions.SendEther;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;


@Credentials(user="khuongdainghia@gmail.com", password="loveyou")
public class DonateFundraiserWithEthereum extends BaseTestCase {
	float campaignRaised=0;
	int numberBeforeDonate;
	int numOfDonors;
	List<WebElement> numOfTestimonial;
	int timeout = 45;
	String campaignTitleValue,campaignCategoryValue,campaignCityValue,campaignGoalValue,ethereumCode;
	ScreenAction action;
	String supporterName = "Nicholas";
	String testimonialMessage = "Good job!";
	
	@Test
	public void NavigateToCampaignForDonation() {
		driver.get(getServerURL() + "/raise/kl9vJoD2M");
		action = new ScreenAction(driver);
	}
	@Test(dependsOnMethods ="NavigateToCampaignForDonation",alwaysRun = true )
	public void CheckNumnerOfDonors() {
		WebElement campaignDonors= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CAMPAIGN_DONORS)));
		numOfDonors = Integer.valueOf(campaignDonors.getText());
		campaignTitleValue = action.getTextFieldByCssSelector(Campaign.CAMPAIGN_TITLES);
		campaignCategoryValue = action.getTextFieldByCssSelector(Campaign.PRIMARY_CATERGORY_SUCCESS_1);
		campaignCityValue = action.getTextFieldByCssSelector(Campaign.CITY_SUCCESS);
		campaignGoalValue = action.getTextFieldByCssSelector(Campaign.GOAL_SUCCESS);
	}
	@Test(dependsOnMethods ="CheckNumnerOfDonors",alwaysRun = true )
	public void GetNumberOfTestimonial() {
		numOfTestimonial = driver.findElements(By.xpath(Campaign.TESTIMONIAL_LIST));
	    numberBeforeDonate = numOfTestimonial.size();
	}
	@Test(dependsOnMethods ="GetNumberOfTestimonial",alwaysRun = true )
	public void GetCampaignRaised() {
		campaignRaised= Float.valueOf(action.getTextFieldByCssSelector(Campaign.CAMPAIGN_RAISED).replace("$", "").replace(",", ""));	
	}
	@Test(dependsOnMethods ="GetCampaignRaised",alwaysRun = true )
	public void ClickDonateButton() {
		WebElement donateButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Campaign.DONATE_BUTTON)));
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",donateButton);	
	}

	@Test(dependsOnMethods ="ClickDonateButton",alwaysRun = true )
	public void ClickOnEthereumDonation() {
		WebElement ethereumLink = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(DonateWepay.ETHEREUM_DONATION_LINK)));
		ethereumLink.click();
	}

	@Test(dependsOnMethods ="ClickOnEthereumDonation",alwaysRun = true )
	public void getEtherAccount() {
		ethereumCode= action.getTextFieldByID(DonateWepay.ETHEREUM_CODE);
	}
	@Test(dependsOnMethods ="getEtherAccount",alwaysRun = true )
	public void InputDonateAccount() {
		action.inputTextFieldByID(DonateWepay.ETHEREUM_ACCOUNT, "0x8652ccade248c435b9546935edc8ca4bf67fa069");
	}
	@Test(dependsOnMethods ="InputDonateAccount",alwaysRun = true )
	public void SubmitAfterInputEtherAccount() {
		WebElement submitButtonForCardInfor = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(DonateWepay.SUBMIT_ETHEREUM_INFORMATION_SCREEN)));
		submitButtonForCardInfor.click();	
	}
//	@Test(dependsOnMethods ="SubmitAfterInputEtherAccount", alwaysRun = true)
//	public void CheckMessageAfterSubmit() {
//		Assert.assertEquals(action.getTextFieldByCssSelector(DonateWepay.ETHEREUM_SUCCESS_MESSAGE), Messages.ETHEREUM_SUCCESS_MESSAGE);
//	}
//	@Test(dependsOnMethods ="CheckMessageAfterSubmit",alwaysRun = true )
//	public void InputDonateAccountAgain() {
//		driver.get(getServerURL() + "/fundraisers/YB2oCspPAfsP4xSZt/donate");
//		action.inputTextFieldByID(DonateWepay.ETHEREUM_ACCOUNT, "0x8652ccade248c435b9546935edc8ca4bf67fa069");
//	}
//	@Test(dependsOnMethods ="InputDonateAccountAgain",alwaysRun = true )
//	public void SubmitAfterInputEtherAccountAgain() {
//		WebElement submitButtonForCardInfor = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(DonateWepay.SUBMIT_ETHEREUM_INFORMATION_SCREEN)));
//		submitButtonForCardInfor.click();	
//	}
//	@Test(dependsOnMethods ="SubmitAfterInputEtherAccountAgain", alwaysRun = true)
	@Test(dependsOnMethods ="SubmitAfterInputEtherAccount", alwaysRun = true)
	public void CheckWarningMessageAfterSubmitAgain() {
		Assert.assertEquals(action.getTextFieldByCssSelector(DonateWepay.ETHEREUM_WARNING_MESSAGE), Messages.ETHEREUM_WARNING_MESSAGE);
	}
		@Test(dependsOnMethods ="CheckWarningMessageAfterSubmitAgain", alwaysRun = true)
	public void NavigateToSendEther() {
		driver.get("https://corp.fundcru.com/send-ether");
	}
	@Test(dependsOnMethods ="NavigateToSendEther", alwaysRun = true)
	public void InputInformationAndSubmit() {

		action.inputTextFieldByID(SendEther.ETHEREUM_EMAIL, "khuongdainghia@gmail.com");
		action.inputTextFieldBy(By.xpath(SendEther.ETHEREUM_PASS), "loveyou");
		action.inputTextFieldByID(SendEther.ETHEREUM_TO, ethereumCode);
		action.inputTextFieldByID(SendEther.ETHEREUM_AMOUNT, "1");
		WebElement submitButtonForCardInfor = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(SendEther.SUBMIT_ETHEREUM_INFORMATION_SCREEN)));
		submitButtonForCardInfor.click();	
		Assert.assertEquals(action.getTextFieldByCssSelector(SendEther.ETHEREUM_SUCCESS_MESSAGE), Messages.ETHEREUM_SUCCESS_MESSAGE_SEND_ETHER);
	}
		
}
