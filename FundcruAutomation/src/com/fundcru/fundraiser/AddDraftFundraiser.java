package com.fundcru.fundraiser;

import java.io.File;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Login;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;

@Credentials(user="khuongdainghia@gmail.com", password="loveyou")
public class AddDraftFundraiser extends BaseTestCase {
	public static String compaignID ="hhcjP9rgWqT5sAkQs";
	int timeout =60,numberDraftFundraiser;
	@Test
	public void CheckIfPrivateListBeforeCreateFundraiser() {
		WebElement userLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		userLink.click();
		WebElement myFundraiser =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.MY_CAMPAIGN_LINK)));
		myFundraiser.click();
	}
	
	@Test(dependsOnMethods ="CheckIfPrivateListBeforeCreateFundraiser",alwaysRun = true )
	public void CheckNoOfPrivateFundraiserBefore() {
		WebElement listPrivateCampaign =(new WebDriverWait(driver, timeout*5)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.PRIVATE_FUNDRAISER_LIST)));
		List<WebElement> privateList= listPrivateCampaign.findElements(By.className(Campaign.FUNDRAISER_ITEM_CLASS));
		numberDraftFundraiser = privateList.size();
	}
	@Test(dependsOnMethods ="CheckNoOfPrivateFundraiserBefore",alwaysRun = true )
	public void ClickOnUserLink() {
		WebElement userLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		userLink.click();
	}
	
	@Test(dependsOnMethods ="ClickOnUserLink")
	public void ClickOnMyCampaignLink() {
		WebElement myCampaignLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.MY_CAMPAIGN_LINK)));
		myCampaignLink.click();
	}
	
	@Test(dependsOnMethods ="ClickOnMyCampaignLink" )
	public void ClickOnCreateNewCampaign() {
		WebElement createNewCampaign =(new WebDriverWait(driver, timeout*2)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CREATE_NEW_COMPAIGN)));
		createNewCampaign.click();
	}
	
	@Test(dependsOnMethods ="ClickOnCreateNewCampaign",alwaysRun = true )
	public void EnterCampaignGoal() {
		WebElement campaignGoal = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.GOAL)));
		campaignGoal.clear();
		campaignGoal.sendKeys("75000");
	}
	
	@Test(dependsOnMethods ="EnterCampaignGoal",alwaysRun = true)
	public void EnterCampaignTitle() {
		WebElement campaignTitle = driver.findElement(By.cssSelector(Campaign.TITLE));
		campaignTitle.clear();
		campaignTitle.sendKeys("Draft Urban Xtreme Adventures");
	}
	
	@Test(dependsOnMethods ="EnterCampaignTitle",alwaysRun = true )
	public void EnterCampaignCityCode() {
		WebElement campaignCityCode = driver.findElement(By.cssSelector(Campaign.CITY_CODE));
		campaignCityCode.clear();
		campaignCityCode.sendKeys("Los Angeles, CA");
		(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CITY_LIST_1)));
		campaignCityCode.sendKeys(Keys.DOWN);
		campaignCityCode.sendKeys(Keys.ENTER);
	}
	
	@Test(dependsOnMethods ="EnterCampaignCityCode",alwaysRun = true )
	public void EnterCampaignCategory() {
		Select campaignPrimaryCategory = new Select( driver.findElement(By.name(Campaign.PRIMARY_CATERGORY)));
		Select campaignSecondaryCategory = new Select(driver.findElement(By.name(Campaign.SECONDARY_CATERGORY)));
		campaignPrimaryCategory.selectByVisibleText("Charity");
		campaignSecondaryCategory.selectByVisibleText("Sports");
	}
	
	@Test(dependsOnMethods ="EnterCampaignCategory",alwaysRun = true )
	public void EnterCampaignSummary() {
		WebElement campaignSummary = driver.findElement(By.name(Campaign.SUMMARY));
		campaignSummary.clear();
		campaignSummary.sendKeys("Summary");
	}
	
	@Test(dependsOnMethods ="EnterCampaignSummary",alwaysRun = true )
	public void AddPhotoForCampaign() {
		// Scroll down the page
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
//	    driver.findElement(By.cssSelector(Campaign.UPLOAD_PHOTO)).click();
	    WebElement campaignChooseImage = driver.findElement(By.xpath(Campaign.CHOOSE_IMAGE_XPATH));
	    campaignChooseImage.clear();
	    campaignChooseImage.sendKeys(System.getProperty("user.dir")+ File.separator +"automation_testing.png");
	    WebElement uploadButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.UPLOAD_BUTTON)));
	    JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",uploadButton);
	    WebElement campaignImage =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.IMAGE)));
	    Assert.assertEquals(campaignImage.getAttribute("class"),"thumbnail");
	}
	@Test(dependsOnMethods ="AddPhotoForCampaign",alwaysRun = true )
	public void Add2ndPhotoForCampaign() {
		// Scroll down the page
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
	    WebElement campaignChooseImage = driver.findElement(By.xpath(Campaign.CHOOSE_2ND_IMAGE_XPATH));
	    campaignChooseImage.click();
	    WebElement inputImage = driver.findElement(By.name(Campaign.UPLOAD_PHOTO));
	    inputImage.clear();
	    inputImage.sendKeys(System.getProperty("user.dir")+ File.separator +"automation_testing01.jpg");
	    WebElement uploadButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.UPLOAD_BUTTON)));
	    JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",uploadButton);
	    WebElement campaignImage =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.IMAGE)));
	    Assert.assertEquals(campaignImage.getAttribute("class"),"thumbnail");
	    List<WebElement> campaignImageList =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.className(Campaign.IMAGE_LIST_CLASS)));
	    Assert.assertEquals(campaignImageList.size(), 2, "Imnage can't upload");
	}
	
	@Test(dependsOnMethods ="Add2ndPhotoForCampaign",alwaysRun = true )
	public void EnterCampaignTextArea() {
		JavascriptExecutor jsescroll = (JavascriptExecutor)driver;
		jsescroll.executeScript("window.scrollBy(0,300)", "");
	    WebElement campaignStoryTextArea = driver.findElement(By.cssSelector(Campaign.STORY));
	    campaignStoryTextArea.clear();
	    campaignStoryTextArea.sendKeys("BE PART OF BUILDING SOMETHING GREAT Would it be amazing if you could Ski, Snowboard, Bounce and Climb under one roof all year round in Brisbane? With your support we can! We are in the process of building Austrialia's FIRST Indoor Snow Sport and Adventure Centre! It will be right here in Brisbane within 15km of the CBD, opening mid 2017. In the lead up to our launch, we are now offering DISCOUNTED pre-sale passes for our ski and adventure activities. As a recognition of your support, all pre-sale passes will get limited time discounts as well as other awesome read on to find out more");
	}
	
	@Test(dependsOnMethods ="EnterCampaignTextArea",alwaysRun = true )
	public void ClickSaveButton() {
		
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.cssSelector(Campaign.SAVE)));
	}
	
	@Test(dependsOnMethods ="ClickSaveButton",alwaysRun = true)
	public void GetCampaignIdAfterSuccessfullyCreateCampaign() {
		// check result here 
		(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.SWITCH_TO_EDIT_MODE)));
		compaignID = driver.getCurrentUrl().substring(driver.getCurrentUrl().lastIndexOf("/")+1,driver.getCurrentUrl().length());
		System.out.println("Campaign ID: " +compaignID);
		Assert.assertEquals(driver.getCurrentUrl(), String.format("%s/fundraisers/%s",getServerURL(),compaignID));	
	}
	
	@Test(dependsOnMethods ="GetCampaignIdAfterSuccessfullyCreateCampaign",alwaysRun = true )
	public void CheckTitleAfterSuccessfullyCreateCampaign() {
		//check result here 
		WebElement campaignTitle = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.TITLE_SUCCESS)));
		Assert.assertEquals(campaignTitle.getText(),"Draft Urban Xtreme Adventures");
	}
	
	@Test(dependsOnMethods ="CheckTitleAfterSuccessfullyCreateCampaign",alwaysRun = true)
	public void CheckGoalNumberAfterSuccessfullyCreateCampaign() {
		WebElement campaignGoal = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.GOAL_SUCCESS)));
		Assert.assertEquals(campaignGoal.getText(),"$75,000.00");	
	}
	
	@Test(dependsOnMethods ="CheckGoalNumberAfterSuccessfullyCreateCampaign",alwaysRun = true )
	public void CheckPrimaryCategoryAfterSuccessfullyCreateCampaign() {
		WebElement campaignPrimary_Category = driver.findElement(By.cssSelector(Campaign.SECONDARY_CATERGORY_SUCCESS));
		Assert.assertEquals(campaignPrimary_Category.getText(),"Sports");
	}
	
	@Test(dependsOnMethods ="CheckPrimaryCategoryAfterSuccessfullyCreateCampaign",alwaysRun = true )
	public void CheckSecondaryCategoryAfterSuccessfullyCreateCampaign() {
		WebElement campaignSecondary_Category = driver.findElement(By.cssSelector(Campaign.PRIMARY_CATERGORY_SUCCESS));
		Assert.assertEquals(campaignSecondary_Category.getText(),"Charity");
	}
	
	@Test(dependsOnMethods ="CheckSecondaryCategoryAfterSuccessfullyCreateCampaign",alwaysRun = true )
	public void CheckCityAfterSuccessfullyCreateCampaign() {
		WebElement campaignCity = driver.findElement(By.cssSelector(Campaign.CITY_SUCCESS));
		Assert.assertEquals(campaignCity.getText(),"Los Angeles, CA");
	}
	
	@Test(dependsOnMethods ="CheckCityAfterSuccessfullyCreateCampaign",alwaysRun = true )
	public void CheckStoryAfterSuccessfullyCreateCampaign() {
		// Scroll down the page
		JavascriptExecutor jsescroll = (JavascriptExecutor)driver;
		jsescroll.executeScript("window.scrollBy(0,300)", "");
		WebElement campaignStory = driver.findElement(By.cssSelector(Campaign.STORY_SUCCESS));
		Assert.assertEquals(campaignStory.getText(),"BE PART OF BUILDING SOMETHING GREAT Would it be amazing if you could Ski, Snowboard, Bounce and Climb under one roof all year round in Brisbane? With your support we can! We are in the process of building Austrialia's FIRST Indoor Snow Sport and Adventure Centre! It will be right here in Brisbane within 15km of the CBD, opening mid 2017. In the lead up to our launch, we are now offering DISCOUNTED pre-sale passes for our ski and adventure activities. As a recognition of your support, all pre-sale passes will get limited time discounts as well as other awesome read on to find out more");	
	}
	
	@Test(dependsOnMethods ="CheckStoryAfterSuccessfullyCreateCampaign",alwaysRun = true )
	public void CheckIfPrivateListIncreased() {
		WebElement userLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		userLink.click();
		WebElement myFundraiser =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.MY_CAMPAIGN_LINK)));
		myFundraiser.click();
	}
	
	@Test(dependsOnMethods ="CheckIfPrivateListIncreased",alwaysRun = true )
	public void CheckNoOfPrivateFundraiser() {
		WebElement listPrivateCampaign =(new WebDriverWait(driver, timeout*5)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.PRIVATE_FUNDRAISER_LIST)));
		List<WebElement> privateList= listPrivateCampaign.findElements(By.className(Campaign.FUNDRAISER_ITEM_CLASS));
		Assert.assertEquals(privateList.size(),numberDraftFundraiser+1,String.format("Wrong expected number of Fundraiser Expected: %s Actual: %s",numberDraftFundraiser+1,privateList.size()));
	}
	
	@Test(dependsOnMethods ="CheckNoOfPrivateFundraiser",alwaysRun = true )
	public void LogOut() {
		WebElement userLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		userLink.click();
		WebElement logOutButton =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.SIGN_OUT)));
		logOutButton.click();
		WebElement signInLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Login.SIGNIN_LINK )));
		Assert.assertEquals(signInLink.getText(), "Sign in");
	}
	@Test(dependsOnMethods ="LogOut",alwaysRun = true )
	public void NavigateToNewCampaignAfterLogout() {
		driver.navigate().to(getServerURL() + "/fundraisers/" + compaignID);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement campaignNotFound = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CAMPAIGN_NOT_FOUND)));
		Assert.assertEquals(campaignNotFound.getText(),"Fundraiser not found");
	}
}
