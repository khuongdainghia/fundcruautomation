package com.fundcru.fundraiser;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.Login;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.utilities.BaseTestCaseNoCredential;

public class AskQuestionOnFundraiser extends BaseTestCaseNoCredential {
	int timeout =10;
	String emailStr ="khuongdainghia@gmail.com",passwordStr="loveyou",invalidEmailStr ="khuongdainghia@";
	String messageSubjectd ="Message subject",messageContent="Message content";
	ScreenAction action;
	@Test
	public void NavigateToCampaignForAskQuestion() {
		driver.get(getServerURL() + "/raise/auto_1000");
		action = new ScreenAction(driver);
	}
	@Test(dependsOnMethods ="NavigateToCampaignForAskQuestion" )
	public void ClickOnAskQuestionButton() {
		action.waitObjVisibleAndClick(By.linkText(Campaign.ASK_QUESTION));
	}
	
	@Test(dependsOnMethods ="ClickOnAskQuestionButton" )
	public void InputMessageContent() {
		action.waitObjVisible(By.cssSelector(Campaign.MESSAGE_CONTENT));
		action.inputTextFieldBy(By.cssSelector(Campaign.MESSAGE_EMAIL), emailStr);
		action.inputTextFieldBy(By.cssSelector(Campaign.MESSAGE_SUBJECT), messageSubjectd);
		action.inputTextFieldBy(By.cssSelector(Campaign.MESSAGE_CONTENT), messageContent);
		
	}
	@Test(dependsOnMethods ="InputMessageContent" )
	public void ClickSendButton() {
		action.clickBtn(By.cssSelector(Campaign.MESSAGE_SEND_BUTTON));
	}
	@Test(dependsOnMethods ="ClickSendButton" )
	public void CheckMessage() {
		action.assertMessge(Campaign.MESSAGE_ALERT_SUCCESSFUL, Messages.MESSAGE_SENT);
	}
	@Test(dependsOnMethods ="CheckMessage", alwaysRun = true )
	public void InputInvalidData() {
		action.pause(1000);
		action.waitObjVisibleAndClick(By.linkText(Campaign.ASK_QUESTION));
		action.pause(1000);
		action.waitObjVisible(By.cssSelector(Campaign.MESSAGE_EMAIL));
		action.inputTextFieldBy(By.cssSelector(Campaign.MESSAGE_EMAIL), invalidEmailStr);
		action.inputTextFieldBy(By.cssSelector(Campaign.MESSAGE_SUBJECT), messageSubjectd);
		action.inputTextFieldBy(By.cssSelector(Campaign.MESSAGE_CONTENT), messageContent);
	}
	@Test(dependsOnMethods ="InputInvalidData" )
	public void ClickSendButtonWithInvalidData() {
		action.clickBtn(By.cssSelector(Campaign.MESSAGE_SEND_BUTTON));
		action.checkObjVisible(By.cssSelector(Campaign.MESSAGE_EMAIL));
		
	}
	@Test(dependsOnMethods ="ClickSendButtonWithInvalidData", alwaysRun = true)
	public void ClickCancel() {

		action.clickBtn(By.cssSelector(Campaign.MESSAGE_CANCEL_BUTTON));
	}
	@Test(dependsOnMethods ="ClickCancel", alwaysRun = true)
	public void InputValidEmailAndPasswordToLogin() {
		WebElement signInLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Login.SIGNIN_LINK )));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", signInLink);
		action.inputTextFieldByID(Login.EMAIL_TEXT_FIELD_ID, emailStr);
		action.inputTextFieldByID(Login.PASSWORD_TEXT_FIELD_ID, passwordStr);
	}
	
	@Test(dependsOnMethods ="InputValidEmailAndPasswordToLogin",alwaysRun = true )
	public void ClickSubmitToLogin() {
		action.waitObjVisibleAndClick(By.id(Login.SIGNIN_BUTTON_ID));
	}
	
	@Test(dependsOnMethods ="ClickSubmitToLogin",alwaysRun = true )
	public void ClickOnMessageIncon() {
		action.waitObjVisibleAndClick(By.className(Login.ENVELOPE_ICON_CLASS));
		
	}
	@Test(dependsOnMethods ="ClickOnMessageIncon",alwaysRun = true )
	public void CheckMessageContent() {
		action.assertTextEqual(By.cssSelector(Campaign.MESSAGE_CENTER_TEXT), "Message Center");
		action.assertTextEqual(By.cssSelector(Campaign.MESSAGE_SUBJECT_DETAIL), String.format("Subject: %s",messageSubjectd));
		action.assertTextEqual(By.cssSelector(Campaign.MESSAGE_EMAIL_DETAIL), emailStr);
		action.assertTextEqual(By.cssSelector(Campaign.MESSAGE_CONTENT_DETAIL), messageContent);
	}
	@Test(dependsOnMethods ="CheckMessageContent",alwaysRun = true )
	public void AskQuestionWithLogin() {
		driver.get(getServerURL() + "/raise/kl9vJoD2M");
		action = new ScreenAction(driver);
	}
	@Test(dependsOnMethods ="AskQuestionWithLogin" )
	public void ClickOnAskQuestionButtonAgain() {
		action.waitObjVisibleAndClick(By.linkText(Campaign.ASK_QUESTION));
	}
	@Test(dependsOnMethods ="ClickOnAskQuestionButtonAgain" )
	public void InputMessageContentAgain() {
		action.waitObjVisible(By.cssSelector(Campaign.MESSAGE_CONTENT));
		action.inputTextFieldBy(By.cssSelector(Campaign.MESSAGE_SUBJECT), messageSubjectd);
		action.inputTextFieldBy(By.cssSelector(Campaign.MESSAGE_CONTENT), messageContent);
	}
	@Test(dependsOnMethods ="InputMessageContentAgain" )
	public void ClickSendButtonAgain() {
		action.clickBtn(By.cssSelector(Campaign.MESSAGE_SEND_BUTTON));
	}
	@Test(dependsOnMethods ="ClickSendButtonAgain" )
	public void CheckMessageAgain() {
		action.assertMessge(Campaign.MESSAGE_ALERT_SUCCESSFUL, Messages.MESSAGE_SENT);
	}
}
