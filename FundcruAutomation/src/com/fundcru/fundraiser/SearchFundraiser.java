package com.fundcru.fundraiser;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.utilities.BaseTestCaseNoCredential;

public class SearchFundraiser extends BaseTestCaseNoCredential {
	int timeout =60;
	String searchValue ="Family";
	ScreenAction action;
	@Test
	public void InputDataOnSearchBox() {
		action = new ScreenAction(driver);
		WebElement searchInput = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.SEARCH_TEXT_BOX)));
		JavascriptExecutor js = (JavascriptExecutor)driver;
//		js.executeScript("arguments[0].click();", searchInput);
//		searchInput.sendKeys(searchValue);
//		js.executeScript("arguments[0].value='[1]';", searchInput,searchValue);
//		
//		action.inputTextFieldBy(By.cssSelector(Home.SEARCH_TEXT_BOX_FULL), searchValue);
		WebElement searchInputFull = (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Home.SEARCH_TEXT_BOX_FULL)));
		js.executeScript("arguments[0].value='[1]';", searchInputFull,searchValue);
		action.clickBtn(By.cssSelector(Home.SEARCH_BUTTON));
	}
	@Test(dependsOnMethods ="InputDataOnSearchBox" )
	public void CheckTheReturnResult() {
		List<WebElement> listCampaign =  (new WebDriverWait(driver,timeout)).until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.className(Home.SEARCH_CAMPAGIN_LIST_CLASSNAME)));
		for (WebElement webElement : listCampaign) {
			WebElement title = webElement.findElement(By.cssSelector(Home.SEARCH_CAMPAGIN_ITEM_TITLE));
			Assert.assertTrue(title.getText().toLowerCase().contains(searchValue.toLowerCase()), String.format("Wrong expected: %s Actual: %s",searchValue,title.getText()));
		}
	}
	
	
}
