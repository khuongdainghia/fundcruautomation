package com.fundcru.admin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Admin;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.objects.pages.TableFunction;
import com.fundcru.utilities.BaseTestCaseAdmin;
import com.fundcru.utilities.Credentials;

@Credentials(user="khuongdainghia@gmail.com", password="1@#456")
public class AdminFundcru extends BaseTestCaseAdmin {
	int timeout =30, numberOfAll, numberOfNeedsDelivery, numberOfDeliveried;
	ScreenAction action;
	TableFunction table;
	String firstName="Nick";
	String lastName="Khuong";
	@Test
	public void ClickOnUserLink() {
		action = new ScreenAction(driver);
		table = new TableFunction(driver);
		WebElement loginUser = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Admin.ADMIN_LOGGED_LINK)));
		loginUser.click();
	}
	@Test(dependsOnMethods="ClickOnUserLink")
	public void ClickOnBasicInformationLink() {
		WebElement basicInfo = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.linkText(Admin.BASIC_INFORMATION)));
		basicInfo.click();
		action.pause(1000);
		action.assertTextEqual(By.name(Admin.FIRST_NAME), firstName);
		action.assertTextEqual(By.name(Admin.LAST_NAME), lastName);
	}
	@Test(dependsOnMethods="ClickOnBasicInformationLink",alwaysRun = true)
	public void ClickOnGriftCards() {
		WebElement giftCard = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.linkText(Admin.GIFT_CARDS)));
		giftCard.click();
	}
	@Test(dependsOnMethods="ClickOnGriftCards")
	public void ClickShowAllGiftCards() {
		WebElement giftCardAll = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Admin.SHOW_ALL_GIFTCARDS)));
		giftCardAll.click();
		action.pause(2000);
		numberOfAll= table.countRow(Admin.TABLE_BODY_XPATH,true);
	}
	@Test(dependsOnMethods="ClickShowAllGiftCards")
	public void ClickShowDeliveriedGiftCards() {
		WebElement giftCardNeedsGiftCard = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Admin.SHOW_DELIVERIED_GIFTCARDS)));
		giftCardNeedsGiftCard.click();
		action.pause(2000);
		numberOfDeliveried= table.countRow(Admin.TABLE_BODY_XPATH,true);
	}
	@Test(dependsOnMethods="ClickShowDeliveriedGiftCards")
	public void ClickShowNeedsDeliveryGiftCards() {
		WebElement giftCardNeedsGiftCard = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Admin.SHOW_NEEDS_DELIVERY_GIFTCARDS)));
		giftCardNeedsGiftCard.click();
		action.pause(1000);
		numberOfNeedsDelivery= table.countRow(Admin.TABLE_BODY_XPATH,true);
	}
	@Test(dependsOnMethods="ClickShowNeedsDeliveryGiftCards")
	public void ClickActionOnOrder() {
		WebElement cell = table.getCellObject(Admin.TABLE_BODY_XPATH, 1, 6);
		WebElement action = cell.findElement(By.id(Admin.TABLE_ACTION_ID));
		action.click();
		WebElement actionUpdateCode = cell.findElement(By.linkText(Admin.UPDATE_CODE_ACTION));
		actionUpdateCode.click();
	}
	@Test(dependsOnMethods="ClickActionOnOrder")
	public void ClickShowNeedsDeliveryGiftCards_01() {
		action.inputTextFieldBy(By.cssSelector(Admin.GIFT_CODE), "123");
	}
	@Test(dependsOnMethods="ClickShowNeedsDeliveryGiftCards_01")
	public void SaveNewData() {
		action.clickBtn(By.cssSelector(Admin.SAVE_BUTTON));
		WebElement message = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Admin.SUCCESSFUL_MESSAGE)));
		Assert.assertTrue(message.getText().contains(Messages.GIFT_CARD_CODE_UPDATED), String.format("Wrong expected: %s Actua: %s",Messages.GIFT_CARD_CODE_UPDATED,message.getText()));
	}
	@Test(dependsOnMethods="SaveNewData")
	public void BackToMenu() {
		action.clickBtn(By.cssSelector(Admin.GOBACK_BUTTON));
		WebElement giftCardNeedsGiftCard = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Admin.SHOW_NEEDS_DELIVERY_GIFTCARDS)));
		giftCardNeedsGiftCard.click();
		action.pause(2000);
		Assert.assertEquals(table.countRow(Admin.TABLE_BODY_XPATH,true), numberOfNeedsDelivery-1);
	}
	@Test(dependsOnMethods="BackToMenu")
	public void ClickShowDeliveriedGiftCardsAgain() {
		WebElement giftCardNeedsGiftCard = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Admin.SHOW_DELIVERIED_GIFTCARDS)));
		giftCardNeedsGiftCard.click();
		action.pause(1000);
		Assert.assertEquals(table.countRow(Admin.TABLE_BODY_XPATH,true), numberOfDeliveried+1);
	}
}
