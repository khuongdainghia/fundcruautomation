package com.fundcru.accountsettings;

import static org.testng.Assert.assertEquals;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Login;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.objects.pagedefinitions.SignUp;
import com.fundcru.objects.pagedefinitions.TextHints;
import com.fundcru.objects.pagedefinitions.TextLabels;
import com.fundcru.utilities.BaseTestCaseNoCredential;

public class SignUpAction extends BaseTestCaseNoCredential {
	int timeout =30;
	@Test
	public void ClickSignInLink() throws Exception {
		WebElement signInLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Login.SIGNIN_LINK )));
		signInLink.click();
		signInLink.isSelected();
	}
	
	@Test(dependsOnMethods ="ClickSignInLink",alwaysRun = true )
	public void ClickSignUpLink() throws Exception {
		WebElement registerLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Login.REGISTER)));
		registerLink.click();
	}
	
	@Test(dependsOnMethods ="ClickSignUpLink",alwaysRun = true )
	public void CheckPageHeader() {
		// Check page header
		String headerLabel = driver.findElement(By.cssSelector(SignUp.HEADER_LABEL)).getText();
		Assert.assertTrue(headerLabel.equals(TextLabels.HEADER_SIGNUP), "Wrong header displays" + headerLabel);
	}
	
	@Test(dependsOnMethods ="CheckPageHeader",alwaysRun = true )
	public void CheckLabel() {
		// Assert text labels
		String firstNameLabel = driver.findElement(By.cssSelector(SignUp.FIRST_NAME_LABEL)).getText();
		Assert.assertTrue(firstNameLabel.equals(TextLabels.FIRSTNAME_FIELD_LABLE_SIGNUP), "Wrong label for first name displays" + firstNameLabel);
		String lastNameLabel = driver.findElement(By.cssSelector(SignUp.LAST_NAME_LABEL)).getText();
		Assert.assertTrue(lastNameLabel.equals(TextLabels.LASTNAME_FIELD_LABLE_SIGNUP), "Wrong label for last name displays" + lastNameLabel);
		String emailLabel = driver.findElement(By.cssSelector(SignUp.EMAIL_LABEL)).getText();
		Assert.assertTrue(emailLabel.equals(TextLabels.EMAIL_FIELD_LABLE_SIGNUP), "Wrong label for email displays" + emailLabel);
		String password1Label = driver.findElement(By.cssSelector(SignUp.PASSWORD1_LABEL)).getText();
		Assert.assertTrue(password1Label.equals(TextLabels.FIRST_PASSWORD_FIELD_LABLE_SIGNUP), "Wrong label for the first password field displays" + password1Label);
		String password2Label = driver.findElement(By.cssSelector(SignUp.PASSWORD2_LABEL)).getText();
		Assert.assertTrue(password2Label.equals(TextLabels.SECOND_PASSWORD_FIELD_LABLE_SIGNUP), "Wrong label for the second password field displays" + password2Label);
		Assert.assertTrue(lastNameLabel.equals(TextLabels.LASTNAME_FIELD_LABLE_SIGNUP), "Wrong label for last name displays" + lastNameLabel);
		Assert.assertTrue(emailLabel.equals(TextLabels.EMAIL_FIELD_LABLE_SIGNUP), "Wrong label for email displays" + emailLabel);
		Assert.assertTrue(password1Label.equals(TextLabels.FIRST_PASSWORD_FIELD_LABLE_SIGNUP), "Wrong label for the first password field displays" + password1Label);
		String orLabel = driver.findElement(By.cssSelector(SignUp.OR_LABEL)).getText();
		Assert.assertTrue(orLabel.equals(TextLabels.OR_TEXT_LABLE_SIGNUP), "Wrong header displays" + orLabel);
	}
	
	@Test(dependsOnMethods ="CheckLabel",alwaysRun = true )
	public void CheckHints() {
		// Assert text hints
		String firstNamePlaceholder=driver.findElement(By.id(SignUp.FIRST_NAME_FIELD)).getAttribute("placeholder");
		assertEquals(TextHints.FIRSTNAME, firstNamePlaceholder);
		String lastNamePlaceholder=driver.findElement(By.id(SignUp.LAST_NAME_FIELD)).getAttribute("placeholder");
		assertEquals(TextHints.LASTNAME, lastNamePlaceholder);
		String emailPlaceholder=driver.findElement(By.id(SignUp.EMAIL_FIELD)).getAttribute("placeholder");
		assertEquals(TextHints.EMAIL, emailPlaceholder);
		String password1Placeholder=driver.findElement(By.id(SignUp.PASSWORD_FIELD)).getAttribute("placeholder");
		assertEquals(TextHints.FIRST_PASSWORD, password1Placeholder);
		String password2Placeholder=driver.findElement(By.id(SignUp.PASSWORD_AGAIN_FIELD)).getAttribute("placeholder");
		assertEquals(TextHints.SECOND_PASSWORD, password2Placeholder);
	}

	@Test(dependsOnMethods ="CheckHints",alwaysRun = true )
	public void CheckButtonDisplayed() {
		// Scroll down the page
		JavascriptExecutor jsedown = (JavascriptExecutor)driver;
		jsedown.executeScript("window.scrollBy(0,300)", "");
		// Assert all buttons displayed
		driver.findElement(By.id(SignUp.REGISTER_BUTTON)).isEnabled();
		driver.findElement(By.id(SignUp.CANCEL_BUTTON)).isEnabled();
		driver.findElement(By.id(SignUp.REGISTER_WITH_FACEBOOK_BUTTON)).isEnabled();
//		driver.findElement(By.id(SignUp.REGISTER_WITH_GOOGLE_BUTTON)).isEnabled();
	}
	
	@Test(dependsOnMethods ="CheckButtonDisplayed",alwaysRun = true )
	public void ClickRegisterButtonWithoutDataEntered() {
		// Check error when clicking Sign Up without any fields entered
		driver.findElement(By.id(SignUp.REGISTER_BUTTON)).click();
	}
	
	@Test(dependsOnMethods ="ClickRegisterButtonWithoutDataEntered",alwaysRun = true )
	public void CheckErrorForRequiredFields() {
		// Scroll up the page
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,-300)", "");
		// Check error when clicking Sign Up without any fields entered
		WebElement errorFirstName =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(SignUp.FIRSTNAME_ERROR)));
		Assert.assertTrue(errorFirstName.getText().equals(Messages.REQUIRED_FIELD), "First name - wrong error message" + errorFirstName);
		String errorLastName = driver.findElement(By.cssSelector(SignUp.LASTNAME_ERROR)).getText();
		Assert.assertTrue(errorLastName.equals(Messages.REQUIRED_FIELD), "Last name - wrong error message" + errorLastName);
		String errorEmail = driver.findElement(By.cssSelector(SignUp.EMAIL_ERROR)).getText();
		Assert.assertTrue(errorEmail.equals(Messages.REQUIRED_FIELD), "Email - wrong error message" + errorEmail);
		String errorPassword = driver.findElement(By.cssSelector(SignUp.PASSWORD_ERROR)).getText();
		Assert.assertTrue(errorPassword.equals(Messages.REQUIRED_FIELD), "Password - wrong error message" + errorPassword);
		String errorPasswordAgain = driver.findElement(By.cssSelector(SignUp.PASSWORD_ERROR)).getText();
		Assert.assertTrue(errorPasswordAgain.equals(Messages.REQUIRED_FIELD), "Second Password - wrong error message" + errorPasswordAgain);
	}
	
	@Test(dependsOnMethods ="CheckErrorForRequiredFields",alwaysRun = true )
	public void InputFirstName() {
		// Input first name
		driver.findElement(By.id(SignUp.FIRST_NAME_FIELD)).sendKeys("Audey");
		driver.findElement(By.id(SignUp.LAST_NAME_FIELD)).clear();
	}
	
	@Test(dependsOnMethods ="InputFirstName",alwaysRun = true )
	public void CheckErrorDisappearedAfterInputFirstNameField() {
		// Check Error Disappeared After Input First Name Field
		WebElement errorFirstName =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(SignUp.FIRSTNAME_ERROR)));
		Assert.assertEquals("", errorFirstName.getText());
	}
	
	@Test(dependsOnMethods ="CheckErrorDisappearedAfterInputFirstNameField",alwaysRun = true )
	public void InputLastName() {
		// Input last name
		driver.findElement(By.id(SignUp.LAST_NAME_FIELD)).sendKeys("Tran");
		driver.findElement(By.id(SignUp.EMAIL_FIELD)).clear();
	}
	
	@Test(dependsOnMethods ="InputLastName",alwaysRun = true )
	public void CheckErrorDisappearedAfterInputLastNameField() {
		// Check Error Disappeared After Input last Name Field
		WebElement errorLastName =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(SignUp.LASTNAME_ERROR)));
		Assert.assertEquals("", errorLastName.getText());
	}
	
	@Test(dependsOnMethods ="CheckErrorDisappearedAfterInputLastNameField",alwaysRun = true )
	public void InputInvalidEmail() {
		// input invalid email for sign up
		driver.findElement(By.id(SignUp.EMAIL_FIELD)).sendKeys("invalidEmail@");
		driver.findElement(By.id(SignUp.PASSWORD_FIELD)).clear();
	}
	
	@Test(dependsOnMethods ="InputInvalidEmail",alwaysRun = true )
	public void CheckErrorAfterInputInvalidEmail() {
		// Check error 
		WebElement errorEmail =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(SignUp.EMAIL_ERROR)));
		Assert.assertEquals(Messages.INVALID_EMAIL, errorEmail.getText());
	}
	
	@Test(dependsOnMethods ="CheckErrorAfterInputInvalidEmail",alwaysRun = true )
	public void InputValidEmail() {
		// input valid email
		driver.findElement(By.id(SignUp.EMAIL_FIELD)).clear();
		driver.findElement(By.id(SignUp.EMAIL_FIELD)).sendKeys("text.myantran@gmail.com");
		driver.findElement(By.id(SignUp.PASSWORD_FIELD)).clear();
	}
	
	@Test(dependsOnMethods ="InputValidEmail",alwaysRun = true )
	public void CheckErrorDisappearedAfterInputValidEmail() {
		// Check error disappeared
		WebElement errorEmail =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(SignUp.EMAIL_ERROR)));
		Assert.assertEquals("", errorEmail.getText());
	}
	
	@Test(dependsOnMethods ="CheckErrorDisappearedAfterInputValidEmail",alwaysRun = true )
	public void EnterInvalidPasswordToCheckMinimumCharacter() {
		// Input invalid password
		driver.findElement(By.id(SignUp.PASSWORD_FIELD)).sendKeys("123");
		driver.findElement(By.id(SignUp.PASSWORD_AGAIN_FIELD)).clear();
	}
	
	@Test(dependsOnMethods ="EnterInvalidPasswordToCheckMinimumCharacter",alwaysRun = true )
	public void CheckErrorForMinimumCharacterPW1() {
		// Check error
		WebElement errorPassword =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(SignUp.PASSWORD_ERROR)));
		Assert.assertEquals(Messages.MINIMUM_CHARACTER_REQUIRED_LENGTH_ERROR, errorPassword.getText());
	}
	
	@Test(dependsOnMethods ="CheckErrorForMinimumCharacterPW1",alwaysRun = true )
	public void EnterInvalidPasswordAgainToCheckMinimumCharacter() {
		// Input invalid password again
		driver.findElement(By.id(SignUp.PASSWORD_AGAIN_FIELD)).sendKeys("123");
		driver.findElement(By.id(SignUp.REGISTER_BUTTON)).click();
	}
	
	@Test(dependsOnMethods ="EnterInvalidPasswordAgainToCheckMinimumCharacter",alwaysRun = true )
	public void CheckErrorForMinimumCharacterPWAgain() {
		// Check error
		WebElement errorPasswordAgain =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(SignUp.PASSWORD_AGAIN_ERROR)));
		Assert.assertEquals(Messages.MINIMUM_CHARACTER_REQUIRED_LENGTH_ERROR,errorPasswordAgain.getText());
	}
	
	@Test(dependsOnMethods ="CheckErrorForMinimumCharacterPWAgain",alwaysRun = true )
	public void InputUnmatchedPasswordFields() {
		// input different passwords for sign up
		driver.findElement(By.id(SignUp.PASSWORD_FIELD)).clear();
		driver.findElement(By.id(SignUp.PASSWORD_AGAIN_FIELD)).clear();
		driver.findElement(By.id(SignUp.PASSWORD_FIELD)).sendKeys("1233456");
		driver.findElement(By.id(SignUp.PASSWORD_AGAIN_FIELD)).sendKeys("1234567");
	}
	
	@Test(dependsOnMethods ="InputUnmatchedPasswordFields",alwaysRun = true )
	public void ClickRegisterButton() {
		// click register button
		driver.findElement(By.id(SignUp.REGISTER_BUTTON)).click();
	}
	
	@Test(dependsOnMethods ="ClickRegisterButton",alwaysRun = true )
	public void CheckErrorUnmatchedPasswordFields() {
		// Check error for unmatched passwords
		WebElement errorPasswordAgain =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(SignUp.PASSWORD_AGAIN_ERROR)));
		Assert.assertEquals(Messages.PASSWORD_DONT_MATCH_SIGNUP,errorPasswordAgain.getText());
		Assert.assertEquals("", driver.findElement(By.cssSelector(SignUp.FIRSTNAME_ERROR)).getText());
		Assert.assertEquals("", driver.findElement(By.cssSelector(SignUp.LASTNAME_ERROR)).getText());
		Assert.assertEquals("", driver.findElement(By.cssSelector(SignUp.EMAIL_ERROR)).getText());
		Assert.assertEquals("", driver.findElement(By.cssSelector(SignUp.PASSWORD_ERROR)).getText());
	}
	
	@Test(dependsOnMethods ="CheckErrorUnmatchedPasswordFields",alwaysRun = true )
	public void InputMatchedPasswordsForSignUp() {
		// input valid password 
		driver.findElement(By.id(SignUp.PASSWORD_FIELD)).clear();
		driver.findElement(By.id(SignUp.PASSWORD_AGAIN_FIELD)).clear();
		driver.findElement(By.id(SignUp.PASSWORD_FIELD)).sendKeys("1233456");
		driver.findElement(By.id(SignUp.PASSWORD_AGAIN_FIELD)).sendKeys("1233456");
	}
	
	@Test(dependsOnMethods ="InputMatchedPasswordsForSignUp",alwaysRun = true )
	public void EnterExistingEmailForSignUp() {
		// enter an existing email for sign up
		driver.findElement(By.id(SignUp.EMAIL_FIELD)).clear();
		driver.findElement(By.id(SignUp.EMAIL_FIELD)).sendKeys("tt.myantran@gmail.com");
	}
	
	@Test(dependsOnMethods ="EnterExistingEmailForSignUp",alwaysRun = true )
	public void ClickRegisterButtonI() {
		driver.findElement(By.id(SignUp.REGISTER_BUTTON)).click();
	}
	
	@Test(dependsOnMethods ="ClickRegisterButtonI",alwaysRun = true )
	public void CheckErrorWhenSignUpWithExistingEmail() {
		// Check error
		WebElement existingEmailForSignUpError =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(SignUp.HEADER_MESSAGE)));
		Assert.assertEquals(Messages.ERROR_MESSAGE_SIGNUP, existingEmailForSignUpError.getText());
		Assert.assertEquals("", driver.findElement(By.cssSelector(SignUp.FIRSTNAME_ERROR)).getText());
		Assert.assertEquals("", driver.findElement(By.cssSelector(SignUp.LASTNAME_ERROR)).getText());
		Assert.assertEquals("", driver.findElement(By.cssSelector(SignUp.EMAIL_ERROR)).getText());
		Assert.assertEquals("", driver.findElement(By.cssSelector(SignUp.PASSWORD_ERROR)).getText());
		Assert.assertEquals("", driver.findElement(By.cssSelector(SignUp.PASSWORD_AGAIN_ERROR)).getText());
	}
	
	@Test(dependsOnMethods ="CheckErrorWhenSignUpWithExistingEmail",alwaysRun = true )
	public void EnterValidEmailForSignUp() {
		// Enter the email that is new for signing up		
		Random rand = new Random(); 
		int randomNumber = rand.nextInt(200000000);
		driver.findElement(By.id(SignUp.EMAIL_FIELD)).clear();
		driver.findElement(By.id(SignUp.EMAIL_FIELD)).sendKeys("test"+randomNumber+"@gmail.com");
	}
	
	@Test(dependsOnMethods ="EnterValidEmailForSignUp",alwaysRun = true )
	public void ClickRegisterButtonII() {
		// Scroll down the page
		JavascriptExecutor jsedown = (JavascriptExecutor)driver;
		jsedown.executeScript("window.scrollBy(0,200)", "");
		// Click register button
		driver.findElement(By.id(SignUp.REGISTER_BUTTON)).click();
	}
	
	@Test(dependsOnMethods ="ClickRegisterButtonII",alwaysRun = true )
	public void CheckSuccessMessage() {
		// Scroll up the page
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,-300)", "");
		// Check success message after sign up
		WebElement successMessageSignUp =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(SignUp.SIGNUP_SUCCESS_MESSAGE)));
		Assert.assertEquals(successMessageSignUp.getText(),Messages.SUCCESS_MESSAGE_SIGNUP);
	}
}
