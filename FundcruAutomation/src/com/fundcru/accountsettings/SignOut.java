package com.fundcru.accountsettings;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Login;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;

@Credentials(user="fundcrutest@gmail.com", password="123456789")
public class SignOut extends BaseTestCase {
	int timeout =30;
	@Test
	public void ClickUserIcon() {
		// ClickUserIcon
		WebElement loggedInIcon =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		loggedInIcon.click();
	}
	
	@Test(dependsOnMethods ="ClickUserIcon",alwaysRun = true )
	public void ClickLogOutButton() {
		// Click Log Out Button
		WebElement logOutButton =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.SIGN_OUT)));
		logOutButton.click();
	}
	
	@Test(dependsOnMethods ="ClickLogOutButton",alwaysRun = true )
	public void CheckSignInButtonDisplayedAfterLogOutSuccessfully() {
		WebElement SigninButton =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Login.SIGNIN_LINK )));
		SigninButton.isDisplayed();
		Assert.assertEquals(SigninButton.getText(), "Sign in", "Wrong text displays");
	}
	
	@Test(dependsOnMethods ="CheckSignInButtonDisplayedAfterLogOutSuccessfully",alwaysRun = true )
	public void ClickCreateFundraiserButton() {
		// Click Create fundraiser button
		WebElement createFundraiser =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.CREATE_A_FUNDRAISER)));
		createFundraiser.click();
	}
	
	@Test(dependsOnMethods ="ClickCreateFundraiserButton" )
	public void RequiredLoginErrorDisplayedAfterSignOut() {
		// Check error as user need to log in to be able to create fundraiser
		WebElement email =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Login.EMAIL_TEXT_FIELD_ID)));
		Assert.assertTrue(email.isDisplayed(),"Login field doesnt display");
	}
}
