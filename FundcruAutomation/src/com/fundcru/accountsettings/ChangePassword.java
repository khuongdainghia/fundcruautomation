package com.fundcru.accountsettings;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.AccountSettings;
import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.objects.pagedefinitions.TextHints;
import com.fundcru.objects.pagedefinitions.TextLabels;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;

@Credentials(user="fundcrutest@gmail.com", password="123456789")
public class ChangePassword extends BaseTestCase {
	int timeout = 30;
	
	@Test
	public void ClickUserLink() {
		WebElement userLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		userLink.click();
	}
	
	@Test(dependsOnMethods ="ClickUserLink",alwaysRun = true )
	public void ClickAccountSettings() {
		WebElement accountSettings =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.ACCOUNT_SETTINGS_LINK)));
		accountSettings.click();
	}
	
	@Test(dependsOnMethods ="ClickAccountSettings",alwaysRun = true )
	public void ClickSignInSecurityLink() {
		WebElement SIGNIN_SECURITY_LINK =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(AccountSettings.SIGNIN_SECURITY_LINK)));
		SIGNIN_SECURITY_LINK.click();
	}
	
	@Test(dependsOnMethods ="ClickSignInSecurityLink",alwaysRun = true )
	public void ClickChangePasswordLink() {
		WebElement CHANGE_PASSWORK_LINK =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(AccountSettings.CHANGE_PASSWORK_LINK)));
		CHANGE_PASSWORK_LINK.click();
	}
	
	@Test(dependsOnMethods ="ClickChangePasswordLink",alwaysRun = true )
	public void CheckLabels() {
		// Assert text labels
		WebElement header =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(AccountSettings.HEADER)));
		Assert.assertTrue(header.getText().equalsIgnoreCase(TextLabels.HEADER_CHANGE_PW), "Wrong header displays " + header);
		String currentPW = driver.findElement(By.cssSelector(AccountSettings.CURRENT_PW_LABEL)).getText();
		Assert.assertTrue(currentPW.equalsIgnoreCase(TextLabels.CURRENT_PASSWORD_CHANGE_PW), "Wrong label for email displays " + currentPW);
		String passwordLabel = driver.findElement(By.cssSelector(AccountSettings.NEW_PW_FIRST_FIELD_LABLE)).getText();
		Assert.assertTrue(passwordLabel.equalsIgnoreCase(TextLabels.FIRST_PASSWORD_CHANGE_PW), "Wrong label for the first password field displays " + passwordLabel);
		String passwordAgainLabel = driver.findElement(By.cssSelector(AccountSettings.NEW_PW_SECOND_FIELD_LABLE)).getText();
		Assert.assertTrue(passwordAgainLabel.equalsIgnoreCase(TextLabels.SECOND_PASSWORD_CHANGE_PW), "Wrong label for the second password field displays " + passwordAgainLabel);
	}
	
	@Test(dependsOnMethods ="CheckLabels",alwaysRun = true )
	public void CheckHints() {
		// Assert text hints
		String currentPWHint=driver.findElement(By.id(AccountSettings.CURRENT_PW_FIELD_ID)).getAttribute("placeholder");
		assertEquals(TextHints.CURRENT_PASSWORD_CHANGE_PW, currentPWHint);
		String passwordPlaceholder=driver.findElement(By.id(AccountSettings.NEW_PW_FIRST_FIELD_ID)).getAttribute("placeholder");
		assertEquals(TextHints.FIRST_PASSWORD_CHANGE_PW, passwordPlaceholder);
		String passwordAgainPlaceholder=driver.findElement(By.id(AccountSettings.NEW_PW_SECOND_FIELD_ID)).getAttribute("placeholder");
		assertEquals(TextHints.SECOND_PASSWORD_CHANGE_PW, passwordAgainPlaceholder);
	}
	
	@Test(dependsOnMethods ="CheckHints",alwaysRun = true )
	public void CheckButtonDisplayed() {
		// Assert all buttons displayed
		driver.findElement(By.id(AccountSettings.UPDATE_PW_BUTTON)).isEnabled();
		driver.findElement(By.id(AccountSettings.CANCEL_BUTTON)).isEnabled();
	}
	
	@Test(dependsOnMethods ="CheckButtonDisplayed",alwaysRun = true )
	public void ClickCancelButton() {
		// Click Cancel button, Sign in and security screen is displayed
		driver.findElement(By.id(AccountSettings.CANCEL_BUTTON)).click();
	}
	
	@Test(dependsOnMethods ="ClickCancelButton",alwaysRun = true )
	public void CheckSignInAndSecurityPageDisplayed() {
		WebElement SIGNIN_SECURITY_HEADER =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(AccountSettings.SIGNIN_SECURITY_HEADER)));
		SIGNIN_SECURITY_HEADER.isDisplayed();
		WebElement CHANGE_PASSWORK_LINK =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(AccountSettings.CHANGE_PASSWORK_LINK)));
		CHANGE_PASSWORK_LINK.isDisplayed();
	}
	
	@Test(dependsOnMethods ="CheckSignInAndSecurityPageDisplayed",alwaysRun = true )
	public void ClickChangePasswordLinkAgain() {
		// Click on Change Password link again 
		driver.findElement(By.cssSelector(AccountSettings.CHANGE_PASSWORK_LINK)).click();
	}
	
	@Test(dependsOnMethods ="ClickChangePasswordLinkAgain",alwaysRun = true )
	public void CheckMadatoryFields() {
		// Check error when clicking Update your password button without any fields entered
		driver.findElement(By.id(AccountSettings.UPDATE_PW_BUTTON)).click();
		WebElement errorCurrentPWRequired =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(AccountSettings.CURRENT_PW_VALIDATION_ERROR)));
		WebElement errorNewPasswordRequired =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(AccountSettings.NEW_PW_FIRST_FIELD_VALIDATION_ERROR)));
		WebElement errorPasswordAgainRequired =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(AccountSettings.NEW_PW_SECOND_FIELD_VALIDATION_ERROR)));
		Assert.assertTrue(errorCurrentPWRequired.getText().equals(Messages.REQUIRED_FIELD), "Current password - wrong error message for required field" + errorCurrentPWRequired);
		Assert.assertTrue(errorNewPasswordRequired.getText().equals(Messages.REQUIRED_FIELD), "Password - wrong error message for required field" + errorNewPasswordRequired);
		Assert.assertTrue(errorPasswordAgainRequired.getText().equals(Messages.REQUIRED_FIELD), "Second Password - wrong error message for required field" + errorPasswordAgainRequired);
	}
	
	@Test(dependsOnMethods ="CheckMadatoryFields",alwaysRun = true )
	public void EnterIncorrectCurrentPassword() {
		// Enter Incorrect Current password for changing pass
		driver.findElement(By.id(AccountSettings.CURRENT_PW_FIELD_ID)).sendKeys("1234567");
		driver.findElement(By.id(AccountSettings.NEW_PW_FIRST_FIELD_ID)).sendKeys("123456789");
		driver.findElement(By.id(AccountSettings.NEW_PW_SECOND_FIELD_ID)).sendKeys("123456789");
	}
	
	@Test(dependsOnMethods ="EnterIncorrectCurrentPassword",alwaysRun = true )
	public void ClickUpdatePasswordButtonI() {
		driver.findElement(By.id(AccountSettings.UPDATE_PW_BUTTON)).click();
	}
	
	@Test(dependsOnMethods ="ClickUpdatePasswordButtonI",alwaysRun = true )
	public void CheckErrorIncorrectCurrentPassword() {
		WebElement INCORRECT_PASSWORD_ERROR =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(AccountSettings.INCORRECT_PASSWORD_ERROR)));
		Assert.assertEquals(INCORRECT_PASSWORD_ERROR.getText().trim(), Messages.INCORRECT_PASSWORD.trim());
	}
	
	@Test(dependsOnMethods ="CheckErrorIncorrectCurrentPassword",alwaysRun = true )
	public void CheckMinimumCharacterValidationForNewPasswordFields() {
		driver.findElement(By.id(AccountSettings.CURRENT_PW_FIELD_ID)).clear();
		driver.findElement(By.id(AccountSettings.NEW_PW_FIRST_FIELD_ID)).clear();
		driver.findElement(By.id(AccountSettings.NEW_PW_SECOND_FIELD_ID)).clear();
		driver.findElement(By.id(AccountSettings.CURRENT_PW_FIELD_ID)).sendKeys("123456789");
		driver.findElement(By.id(AccountSettings.NEW_PW_FIRST_FIELD_ID)).sendKeys("123");
		driver.findElement(By.id(AccountSettings.NEW_PW_SECOND_FIELD_ID)).sendKeys("123");
	}
	
	@Test(dependsOnMethods ="CheckMinimumCharacterValidationForNewPasswordFields",alwaysRun = true )
	public void ClickUpdatePasswordButtonII() {
		driver.findElement(By.id(AccountSettings.UPDATE_PW_BUTTON)).click();
	}
	
	@Test(dependsOnMethods ="ClickUpdatePasswordButtonII",alwaysRun = true )
	public void CheckErrorValidationForMinimumCharacter() {
		WebElement errorMinimumRequired1 =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(AccountSettings.NEW_PW_FIRST_FIELD_VALIDATION_ERROR)));
		Assert.assertTrue(errorMinimumRequired1.getText().equals(Messages.MINIMUM_REQUIRED_LEAGTH_RESET_PW), "Password - Wrong error message for minimum required character" + errorMinimumRequired1);
		WebElement errorMinimumRequired2 =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(AccountSettings.NEW_PW_SECOND_FIELD_VALIDATION_ERROR)));
		Assert.assertTrue(errorMinimumRequired2.getText().equals(Messages.MINIMUM_REQUIRED_LEAGTH_RESET_PW), "Second Password - Wrong error message for minimum required character" + errorMinimumRequired2);
	}
	
	@Test(dependsOnMethods ="CheckErrorValidationForMinimumCharacter",alwaysRun = true )
	public void EnterValidPasswordsForUpdating() {
		// Enter valid password in three fields for changing pass
		driver.findElement(By.id(AccountSettings.CURRENT_PW_FIELD_ID)).clear();
		driver.findElement(By.id(AccountSettings.NEW_PW_FIRST_FIELD_ID)).clear();
		driver.findElement(By.id(AccountSettings.NEW_PW_SECOND_FIELD_ID)).clear();
		driver.findElement(By.id(AccountSettings.CURRENT_PW_FIELD_ID)).sendKeys("123456789");
		driver.findElement(By.id(AccountSettings.NEW_PW_FIRST_FIELD_ID)).sendKeys("123456789");
		driver.findElement(By.id(AccountSettings.NEW_PW_SECOND_FIELD_ID)).sendKeys("123456789");
	}
	
	@Test(dependsOnMethods ="EnterValidPasswordsForUpdating",alwaysRun = true )
	public void ClickUpdatePasswordButtonIV() {
		driver.findElement(By.id(AccountSettings.UPDATE_PW_BUTTON)).click();
	}
	
	@Test(dependsOnMethods ="ClickUpdatePasswordButtonIV",alwaysRun = true )
	public void CheckSuccessMessage() {
		WebElement changePWlink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(AccountSettings.CHANGE_PASSWORK_LINK)));
		changePWlink.click();
		WebElement accountSettingsHeader =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(AccountSettings.HEADER_EUP)));
		Assert.assertEquals(accountSettingsHeader.isDisplayed(), true);
	}
	
	@Test(dependsOnMethods ="CheckSuccessMessage",alwaysRun = true )
	public void CheckMaximumCharacterValidationForNewPasswordFields() {
		driver.findElement(By.id(AccountSettings.CURRENT_PW_FIELD_ID)).clear();
		driver.findElement(By.id(AccountSettings.NEW_PW_FIRST_FIELD_ID)).clear();
		driver.findElement(By.id(AccountSettings.NEW_PW_SECOND_FIELD_ID)).clear();
		// Enter 129 characters - maximum should be 128
		driver.findElement(By.id(AccountSettings.CURRENT_PW_FIELD_ID)).sendKeys("123456789");
		driver.findElement(By.id(AccountSettings.NEW_PW_FIRST_FIELD_ID)).sendKeys("0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789");
	}
	
	@Test(dependsOnMethods ="CheckMaximumCharacterValidationForNewPasswordFields",alwaysRun = true )
	public void ClickUpdatePasswordButtonIII() {
		driver.findElement(By.id(AccountSettings.UPDATE_PW_BUTTON)).click();
	}
	
	@Test(dependsOnMethods ="ClickUpdatePasswordButtonIII",alwaysRun = true )
	public void CheckErrorValidationForMaximumCharacter() {
		WebElement errorMaximunLength1 =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(AccountSettings.NEW_PW_FIRST_FIELD_VALIDATION_ERROR)));
		Assert.assertTrue(errorMaximunLength1.getText().equals(Messages.MAXIMUM_LEAGTH_RESET_PW), "Password - failed validation for maximum character" + errorMaximunLength1);
	}
}
