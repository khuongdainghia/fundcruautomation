package com.fundcru.accountsettings;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.AccountSettings;
import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.PersonalInfomation;
import com.fundcru.objects.pagedefinitions.TextLabels;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;

@Credentials(user="fundcrutest@gmail.com", password="123456789")
public class UpdatePersonalInfomation extends BaseTestCase {
	
	int timeout =30;
	ScreenAction action;
	
	@Test
	public void ClickUserLink() {
		action = new ScreenAction(driver);
		action.clickBtn(By.className(Home.USER_LINK_CLASS));
	}
	
	@Test(dependsOnMethods ="ClickUserLink",alwaysRun = true )
	public void ClickAccountSettings() {
		action.clickBtn(By.id(Home.ACCOUNT_SETTINGS_LINK));
	}
	
	@Test(dependsOnMethods ="ClickAccountSettings",alwaysRun = true )
	public void ClickPersonalInfomationLink() {
		action.clickBtn(By.cssSelector(AccountSettings.PERSONAL_INF_LINK));
	}
	
	@Test(dependsOnMethods ="ClickPersonalInfomationLink",alwaysRun = true )
	public void waitForPersonalInfomationScreenVisible() {
		action.waitObjVisible(By.cssSelector(PersonalInfomation.HEADER));
	}
	
	@Test(dependsOnMethods ="waitForPersonalInfomationScreenVisible",alwaysRun = true )
	public void checkEmailTitle() {
		action.assertTextEqual(By.xpath(PersonalInfomation.EMAIL_TITLE), TextLabels.EMAIL_TITLE);
	}
	
	@Test(dependsOnMethods ="checkEmailTitle",alwaysRun = true )
	public void checkProfileTitle() {
		action.assertTextEqual(By.xpath(PersonalInfomation.PROFILE_TITLE), TextLabels.PROFILE_TITLE);
	}
	
	@Test(dependsOnMethods ="checkProfileTitle",alwaysRun = true )
	public void checkFullnameTitle() {
		action.assertTextEqual(By.xpath(PersonalInfomation.FULLNAME_TITLE), TextLabels.FULLNAME_TITLE);
	}
	
	@Test(dependsOnMethods ="checkFullnameTitle",alwaysRun = true )
	public void checkAboutTitle() {
		action.assertTextEqual(By.xpath(PersonalInfomation.ABOUT_TITLE), TextLabels.ABOUT_TITLE);
	}
	
	@Test(dependsOnMethods ="checkAboutTitle",alwaysRun = true )
	public void checkEmail() {
		action.assertTextEqual(By.xpath(PersonalInfomation.EMAIL), "fundcrutest@gmail.com");
	}
	
	@Test(dependsOnMethods ="checkEmail",alwaysRun = true )
	public void checkFullnameBeforeModify() {
		action.assertTextEqual(By.xpath(PersonalInfomation.FULLNAME), "Audrey Tran");
	}
	
	@Test(dependsOnMethods ="checkFullnameBeforeModify",alwaysRun = true )
	public void checkAbout() {
		action.assertTextEqual(By.xpath(PersonalInfomation.ABOUT), "About Testing");
	}
	
	@Test(dependsOnMethods ="checkAbout",alwaysRun = true )
	public void checkBackButtonVisible() {
		action.checkObjVisible(By.cssSelector(PersonalInfomation.BACK_BUTTON));
	}
	
	@Test(dependsOnMethods ="checkBackButtonVisible",alwaysRun = true )
	public void editProfile() {
		action.uploadPhoto(By.xpath(PersonalInfomation.PROFILE_ACTION), 
				"automation_testing.png", 
				By.cssSelector(PersonalInfomation.IMAGE_MODIFY_DIALOG_SAVE_IMAGE_BUTTON), 
				By.xpath(PersonalInfomation.PROFILE));
	}
	
	@Test(dependsOnMethods ="editProfile",alwaysRun = true )
	public void clickEditFullname() {
	    JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",driver.findElement(By.xpath(PersonalInfomation.FULLNAME_ACTION)));
	}
	
	@Test(dependsOnMethods ="clickEditFullname",alwaysRun = true )
	public void checkFullnameDialogHeader() {
	    action.assertTextEqual((By.cssSelector(PersonalInfomation.MODIFY_DIALOG_HEADER)), TextLabels.FULLNAME_MODIFY_DIALOG_HEADER);
	}
	
	@Test(dependsOnMethods ="checkFullnameDialogHeader",alwaysRun = true )
	public void checkDialogFirstnameLabel() {
	    action.assertTextEqual((By.xpath(PersonalInfomation.FULLNAME_MODIFY_DIALOG_FIRSTNAME_LABEL)), TextLabels.FULLNAME_MODIFY_DIALOG_FIRSTNAME_LABEL);
	}
	
	@Test(dependsOnMethods ="checkDialogFirstnameLabel",alwaysRun = true )
	public void checkDialogLastnameLabel() {
	    action.assertTextEqual((By.xpath(PersonalInfomation.FULLNAME_MODIFY_DIALOG_LASTNAME_LABEL)), TextLabels.FULLNAME_MODIFY_DIALOG_LASTNAME_LABEL);
	}
	
	@Test(dependsOnMethods ="checkDialogLastnameLabel",alwaysRun = true )
	public void checkDialogFirstnameField() {
	    action.assertTextEqual((By.xpath(PersonalInfomation.FULLNAME_MODIFY_DIALOG_FIRSTNAME)), "Audrey");
	}
	
	@Test(dependsOnMethods ="checkDialogFirstnameField",alwaysRun = true )
	public void checkDialogLastnameField() {
	    action.assertTextEqual((By.xpath(PersonalInfomation.FULLNAME_MODIFY_DIALOG_LASTNAME)), "Tran");
	}
	
	@Test(dependsOnMethods ="checkDialogLastnameField",alwaysRun = true )
	public void checkDialogButtonsDisplay() {
	    action.checkObjVisible((By.xpath(PersonalInfomation.FULLNAME_MODIFY_DIALOG_SUBMIT_BUTTON)));
	    action.checkObjVisible((By.xpath(PersonalInfomation.FULLNAME_MODIFY_DIALOG_CANCEL_BUTTON)));
	}
	
	@Test(dependsOnMethods ="checkDialogButtonsDisplay",alwaysRun = true )
	public void updateFirstnameField() {
		action.inputTextFieldBy(By.xpath(PersonalInfomation.FULLNAME_MODIFY_DIALOG_FIRSTNAME), "Jolie");
	}
	
	@Test(dependsOnMethods ="updateFirstnameField",alwaysRun = true )
	public void updateLastnameField() {
		action.inputTextFieldBy(By.xpath(PersonalInfomation.FULLNAME_MODIFY_DIALOG_LASTNAME), "Nguyen");
	}
	
	@Test(dependsOnMethods ="updateLastnameField",alwaysRun = true )
	public void clickSubmitButtonFullnameDialog() {
		action.clickBtn(By.xpath(PersonalInfomation.FULLNAME_MODIFY_DIALOG_SUBMIT_BUTTON));
	}
	
	@Test(dependsOnMethods ="clickSubmitButtonFullnameDialog",alwaysRun = true )
	public void checkFullnameAfterModify() {
		action.assertTextEqual(By.xpath(PersonalInfomation.FULLNAME), "Jolie Nguyen");
	}
	
	@Test(dependsOnMethods ="checkFullnameAfterModify",alwaysRun = true )
	public void clickEditAboutLink() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",driver.findElement(By.xpath(PersonalInfomation.ABOUT_ACTION)));
	}
	
	@Test(dependsOnMethods ="clickEditAboutLink",alwaysRun = true )
	public void checkAboutDialogHeader() {
	    action.assertTextEqual((By.cssSelector(PersonalInfomation.MODIFY_DIALOG_HEADER)), TextLabels.ABOUT_MODIFY_DIALOG_HEADER);
	}
	
	@Test(dependsOnMethods ="checkAboutDialogHeader",alwaysRun = true )
	public void checkDialogAboutLabel() {
	    action.assertTextEqual((By.xpath(PersonalInfomation.ABOUT_MODIFY_DIALOG_ABOUT_LABEL)), TextLabels.ABOUT_MODIFY_DIALOG_LABEL);
	}
	
	@Test(dependsOnMethods ="checkDialogAboutLabel",alwaysRun = true )
	public void checkAboutBeforeModify() {
	    action.assertTextEqual((By.xpath(PersonalInfomation.ABOUT_MODIFY_DIALOG_ABOUT_TEXT_AREA)), "About Testing");
	}
	
	@Test(dependsOnMethods ="checkAboutBeforeModify",alwaysRun = true )
	public void checkAboutDialogButtonsDisplay() {
	    action.checkObjVisible((By.xpath(PersonalInfomation.ABOUT_MODIFY_DIALOG_SUBMIT_BUTTON)));
	    action.checkObjVisible((By.xpath(PersonalInfomation.ABOUT_MODIFY_DIALOG_CANCEL_BUTTON)));
	}
	
	@Test(dependsOnMethods ="checkAboutDialogButtonsDisplay",alwaysRun = true )
	public void updateAboutValue() {
		action.inputTextFieldBy(By.xpath(PersonalInfomation.ABOUT_MODIFY_DIALOG_ABOUT_TEXT_AREA), "About Modified");
	}
	
	@Test(dependsOnMethods ="updateAboutValue",alwaysRun = true )
	public void clickSubmitButtonAboutDialog() {
		action.clickBtn(By.xpath(PersonalInfomation.ABOUT_MODIFY_DIALOG_SUBMIT_BUTTON));
	}
	
	@Test(dependsOnMethods ="clickSubmitButtonAboutDialog",alwaysRun = true )
	public void checkAboutAfterModify() {
		action.assertTextEqual(By.xpath(PersonalInfomation.ABOUT), "About Modified");
	}
	
	@Test(dependsOnMethods ="checkAboutAfterModify",alwaysRun = true )
	public void changeBackFullnameToOriginalValue() {
	    JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",driver.findElement(By.xpath(PersonalInfomation.FULLNAME_ACTION)));
		action.inputTextFieldBy(By.xpath(PersonalInfomation.FULLNAME_MODIFY_DIALOG_FIRSTNAME), "Audrey");
		action.inputTextFieldBy(By.xpath(PersonalInfomation.FULLNAME_MODIFY_DIALOG_LASTNAME), "Tran");
		action.clickBtn(By.xpath(PersonalInfomation.FULLNAME_MODIFY_DIALOG_SUBMIT_BUTTON));
	}

	@Test(dependsOnMethods ="changeBackFullnameToOriginalValue",alwaysRun = true )
	public void changeBackAboutToOriginalValue() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",driver.findElement(By.xpath(PersonalInfomation.ABOUT_ACTION)));
		action.inputTextFieldBy(By.xpath(PersonalInfomation.ABOUT_MODIFY_DIALOG_ABOUT_TEXT_AREA), "About Testing");
		action.clickBtn(By.xpath(PersonalInfomation.ABOUT_MODIFY_DIALOG_SUBMIT_BUTTON));
	}
	
}
