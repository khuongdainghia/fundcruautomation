package com.fundcru.accountsettings;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Login;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.objects.pagedefinitions.TextHints;
import com.fundcru.objects.pagedefinitions.TextLabels;
import com.fundcru.utilities.BaseTestCaseNoCredential;

public class ForgotPassword extends BaseTestCaseNoCredential {
	int timeout = 30;
	@Test
	public void ClickSignInLink() throws Exception {
		WebElement signInLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Login.SIGNIN_LINK )));
		signInLink.click();
	}
	
	@Test(dependsOnMethods ="ClickSignInLink",alwaysRun = true )
	public void CheckSignInPageDisplayed() {
		// Check sign in header displayed
	    WebElement signInHeader =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Login.SIGNIN_HEADER_LABEL)));
	    signInHeader.isDisplayed();
	}
	
	@Test(dependsOnMethods ="CheckSignInPageDisplayed",alwaysRun = true )
	public void ClickForgotYourPasswordLink() {
		// Click Forgot your password link
	    driver.findElement(By.id(Login.FORGOT_PASSWORD)).click();
	}
	
	@Test(dependsOnMethods ="ClickForgotYourPasswordLink",alwaysRun = true )
	public void CheckResetPasswordScreenDisplayed() {
		// Check header screen
	    WebElement headerLabel =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Login.HEADER_RESET_PW)));
		Assert.assertEquals(TextLabels.HEADER_RESET_PASSWORD_SCREEN, headerLabel.getText());
	}
	
	@Test(dependsOnMethods ="CheckResetPasswordScreenDisplayed",alwaysRun = true )
	public void CheckLabels() {
		// Assert text labels
		String emailLabel = driver.findElement(By.cssSelector(Login.EMAIL_TEXT_FIELD_LABLE)).getText();
		Assert.assertTrue(emailLabel.equals(TextLabels.EMAIL_FIELD_LABLE_SIGNIN), "Wrong label for email displays " + emailLabel);
		String dontHaveAccountText = driver.findElement(By.cssSelector(Login.DONT_HAVE_ACCOUNT)).getText();
		Assert.assertTrue(dontHaveAccountText.equals(TextLabels.DONT_HAVE_ACCOUNT), "Wrong text displays " + dontHaveAccountText);
		String verificationEmailLostText = driver.findElement(By.cssSelector(Login.VERIFICATION_EMAIL_LOST)).getText();
		Assert.assertTrue(verificationEmailLostText.equals(TextLabels.VERIFICATION_EMAIL_LOST), "Wrong text displays " + verificationEmailLostText);
	}
	
	@Test(dependsOnMethods ="CheckLabels",alwaysRun = true )
	public void CheckPasswordFieldHint() {
		// Assert text hints
		String emailPlaceholder=driver.findElement(By.id(Login.EMAIL_TEXT_FIELD_ID)).getAttribute("placeholder");
		assertEquals(TextHints.EMAIL, emailPlaceholder);
	}
	
	@Test(dependsOnMethods ="CheckPasswordFieldHint",alwaysRun = true )
	public void CheckButtonsDisplayed() {
		// Assert all buttons displayed
		driver.findElement(By.id(Login.SIGNIN_BUTTON_ID)).isDisplayed();
		driver.findElement(By.id(Login.CANCEL)).isDisplayed();
	}
	
	@Test(dependsOnMethods ="CheckButtonsDisplayed",alwaysRun = true )
	public void CheckHyperLinks() {
		// Assert all hyper links displayed
		driver.findElement(By.id(Login.REGISTER)).isDisplayed();
		driver.findElement(By.id(Login.SEND_AGAIN)).isDisplayed();
	}
	
	@Test(dependsOnMethods ="CheckHyperLinks",alwaysRun = true )
	public void ClickResetEmailLinkWithoutEmailProvided() {
		// Click button Reset Email Link but doesn't provide the email
		driver.findElement(By.id(Login.SIGNIN_BUTTON_ID)).click();
	}
	
	@Test(dependsOnMethods ="ClickResetEmailLinkWithoutEmailProvided",alwaysRun = true )
	public void CheckRequiredField() {
		// Check error displays for required field
	    WebElement errorRequiredField =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Login.EMAIL_VALIDATION_MESSAGE)));
		assertEquals(Messages.REQUIRED_FIELD, errorRequiredField.getText());
	}
	
	@Test(dependsOnMethods ="CheckRequiredField",alwaysRun = true )
	public void EnterInvalidEmailCaseI() {
		// Enter an invalid email
	    driver.findElement(By.id(Login.EMAIL_TEXT_FIELD_ID)).sendKeys("tt.myantran@gmailtet");
	}
	
	@Test(dependsOnMethods ="EnterInvalidEmailCaseI",alwaysRun = true )
	public void ClickResetEmailI() {
		// Click button Reset Email Link 
		driver.findElement(By.id(Login.SIGNIN_BUTTON_ID)).click();
	}
	
	@Test(dependsOnMethods ="ClickResetEmailI",alwaysRun = true )
	public void CheckErrorInvalidEmail() {
		// Click button Reset Email Link 
	    WebElement errorRequiredField =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Login.EMAIL_VALIDATION_MESSAGE)));
		assertEquals(Messages.INVALID_EMAIL, errorRequiredField.getText());
	}
	
	@Test(dependsOnMethods ="CheckErrorInvalidEmail",alwaysRun = true )
	public void EnterInvalidEmailCaseII() {
		// Enter an invalid email
	    driver.findElement(By.id(Login.EMAIL_TEXT_FIELD_ID)).clear();
	    driver.findElement(By.id(Login.EMAIL_TEXT_FIELD_ID)).sendKeys("112344@");
	}
	
	@Test(dependsOnMethods ="EnterInvalidEmailCaseII",alwaysRun = true )
	public void ClickResetEmailII() {
		// Click button Reset Email Link 
		driver.findElement(By.id(Login.SIGNIN_BUTTON_ID)).click();
	}
	
	@Test(dependsOnMethods ="ClickResetEmailII",alwaysRun = true )
	public void CheckErrorInvalidEmailCaseII() {
		// Click button Reset Email Link 
	    WebElement errorRequiredField =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Login.EMAIL_VALIDATION_MESSAGE)));
		assertEquals(Messages.INVALID_EMAIL, errorRequiredField.getText());
	}
	
	@Test(dependsOnMethods ="CheckErrorInvalidEmailCaseII",alwaysRun = true )
	public void EnterNonExistingUser() {
	    // Enter an email that have not registered yet
		driver.findElement(By.id(Login.EMAIL_TEXT_FIELD_ID)).clear();
		driver.findElement(By.id(Login.EMAIL_TEXT_FIELD_ID)).sendKeys("audrey@enclave.vn");
	}
	
	@Test(dependsOnMethods ="EnterNonExistingUser",alwaysRun = true )
	public void ClickResetEmailIII() {
		// Click button Reset Email Link 
		driver.findElement(By.id(Login.SIGNIN_BUTTON_ID)).click();
	}
	
	@Test(dependsOnMethods ="ClickResetEmailIII",alwaysRun = true )
	public void CheckErrorUserNotFound() {
		// Check error User not found
	    WebElement errorRequiredField =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Login.ERROR_MESSAGE)));
		assertEquals(Messages.USER_NOT_FOUND_ERROR, errorRequiredField.getText());
	}
	
	@Test(dependsOnMethods ="CheckErrorUserNotFound",alwaysRun = true )
	public void ClickCancelButton() {
		// Click cancel button
	    driver.findElement(By.id(Login.CANCEL)).click();
	}
	
	@Test(dependsOnMethods ="ClickCancelButton",alwaysRun = true )
	public void CheckScreenSignInDisplayed() {
	    // Click Cancel button, system will go to Sign In screen
	    WebElement signInPageHeader =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Login.SIGNIN_HEADER_LABEL)));
		assertEquals(TextLabels.HEADER_SIGNIN, signInPageHeader.getText());
	}
	
	@Test(dependsOnMethods ="CheckScreenSignInDisplayed",alwaysRun = true )
	public void ClickForgotPasswordLinkAgain() {
		// Click on forgot password link again
		driver.findElement(By.id(Login.FORGOT_PASSWORD)).click();
	}
	
	@Test(dependsOnMethods ="ClickForgotPasswordLinkAgain",alwaysRun = true )
	public void EnterValidEmailForResetingPW() {
		// Enter valid email to process resetting password
	    WebElement emailField =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Login.EMAIL_TEXT_FIELD_ID)));
	    emailField.sendKeys("fundcrutest@gmail.com");
	}
	
	@Test(dependsOnMethods ="EnterValidEmailForResetingPW",alwaysRun = true )
	public void ClickResetEmailIV() {
		// Click button Reset Email Link 
		driver.findElement(By.id(Login.SIGNIN_BUTTON_ID)).click();
	}
	
	@Test(dependsOnMethods ="ClickResetEmailIV",alwaysRun = true )
	public void CheckSuccessMessage() {
		// Check success message after input valid email for resetting
	    WebElement successMessage =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Login.RESET_PASSWORD_EMAIL_SUCCESS_MESSAGE)));
	    successMessage.getText().equalsIgnoreCase(Messages.SUCCESS_INFOR_EMAIL_SENT);
	}
	
	@Test(dependsOnMethods ="CheckSuccessMessage",alwaysRun = true )
	public void CheckHomePageRefreshed() {
		// Page refreshes to home screen
	    WebElement Popular_campaigns =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Home.HOME_HEADER)));
	    Popular_campaigns.isDisplayed();
	}
}