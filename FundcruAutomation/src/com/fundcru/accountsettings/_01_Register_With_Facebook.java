package com.fundcru.accountsettings;

import static org.testng.Assert.assertEquals;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Login;
import com.fundcru.objects.pagedefinitions.SignUp;
import com.fundcru.objects.pagedefinitions.TextLabels;
import com.fundcru.utilities.BaseTestCaseNoCredential;

public class _01_Register_With_Facebook extends BaseTestCaseNoCredential {
	int timeout =30;
	@Test
	public void ClickSignInLink() throws Exception {
		WebElement signInLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Login.SIGNIN_LINK )));
		signInLink.click();
		signInLink.isSelected();
	}

	@Test(dependsOnMethods ="ClickSignInLink",alwaysRun = true )
	public void ClickRegisterLink() throws Exception {
		WebElement registerLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Login.REGISTER)));
		registerLink.click();
	}
	
	@Test(dependsOnMethods ="ClickRegisterLink",alwaysRun = true )
	public void CheckRegisterScreenDisplayed() {
		// Check page header
		WebElement headerLabel =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(SignUp.HEADER_LABEL)));
		Assert.assertEquals(headerLabel.getText(), TextLabels.HEADER_SIGNUP);
	}	
	
	@Test(dependsOnMethods ="CheckRegisterScreenDisplayed",alwaysRun = true )
	public void ScrollDownThePageToRegisterWithFacebookButton() {
		// Scroll down the page
		JavascriptExecutor jsedown = (JavascriptExecutor)driver;
		jsedown.executeScript("window.scrollBy(0,300)", "");
	}
	
	@Test(dependsOnMethods ="ScrollDownThePageToRegisterWithFacebookButton",alwaysRun = true )
	public void RegisterWithFacebookButton() {
		String mwh=driver.getWindowHandle();
		driver.findElement(By.id(SignUp.REGISTER_WITH_FACEBOOK_BUTTON)).click();
		Set s=driver.getWindowHandles(); //this method will gives the handles of all opened windows
		Iterator ite=s.iterator();
		while(ite.hasNext())
		{
		    String popupHandle=ite.next().toString();
		    if(!popupHandle.contains(mwh))
		    {
		        driver.switchTo().window(popupHandle);
		        driver.getCurrentUrl();
		        boolean facebookWindow = false;
		        if(driver.getCurrentUrl().startsWith("https://www.facebook.com")){
		        	facebookWindow = true;
		        }
		        assertEquals(facebookWindow, true);
		        driver.findElement(By.id(SignUp.FACEBOOK_HOME_LINK_ID));
		        driver.findElement(By.id(SignUp.FACEBOOK_USERNAME_FIELD_ID)).sendKeys("+841649847312");
		        driver.findElement(By.id(SignUp.FACEBOOK_PASSWORD_FIELD_ID)).sendKeys("abc123456abc");
		        driver.findElement(By.id(SignUp.FACEBOOK_LOGIN_BUTTON_ID)).click();
				WebElement FACEBOOK_CONTINUE_AS_CLASS =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(SignUp.FACEBOOK_CONTINUE_AS_CLASS )));
				FACEBOOK_CONTINUE_AS_CLASS.isDisplayed();
		        // select the main window again
		        driver.switchTo().window(mwh);
		    }
		}
	}
	

}
