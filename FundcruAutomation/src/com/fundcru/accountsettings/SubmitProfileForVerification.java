package com.fundcru.accountsettings;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.AccountSettings;
import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.objects.pagedefinitions.TextHints;
import com.fundcru.objects.pagedefinitions.TextLabels;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;

@Credentials(user="fundcrutest@gmail.com", password="123456789")
public class SubmitProfileForVerification extends BaseTestCase {
	int timeout =50;
	@Test
	public void ClickUserLink() {
		WebElement userLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		userLink.click();
	}
	
	@Test(dependsOnMethods ="ClickUserLink",alwaysRun = true )
	public void ClickAccountSettings() {
		WebElement accountSettings =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.ACCOUNT_SETTINGS_LINK)));
		accountSettings.click();
	}
	
	@Test(dependsOnMethods ="ClickAccountSettings",alwaysRun = true )
	public void ClickSignInSecurityLink() {
		WebElement SIGNIN_SECURITY_LINK =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(AccountSettings.SIGNIN_SECURITY_LINK)));
		SIGNIN_SECURITY_LINK.click();
	}
	
	@Test(dependsOnMethods ="ClickSignInSecurityLink",alwaysRun = true )
	public void CheckSignInAndSecurityPageDisplayed() {
		WebElement SIGNIN_SECURITY_HEADER =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(AccountSettings.SIGNIN_SECURITY_HEADER)));
		SIGNIN_SECURITY_HEADER.isDisplayed();
		WebElement SUBMIT_PROFILE_FOR_VERIFICATION_LINK =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(AccountSettings.SUBMIT_PROFILE_FOR_VERIFICATION_LINK)));
		SUBMIT_PROFILE_FOR_VERIFICATION_LINK.isDisplayed();
	}
	
	@Test(dependsOnMethods ="CheckSignInAndSecurityPageDisplayed",alwaysRun = true )
	public void ClickSubmitForVerification() {
		WebElement SUBMIT_PROFILE_FOR_VERIFICATION_LINK =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(AccountSettings.SUBMIT_PROFILE_FOR_VERIFICATION_LINK)));
		SUBMIT_PROFILE_FOR_VERIFICATION_LINK.click();
	}
	
	@Test(dependsOnMethods ="ClickSubmitForVerification",alwaysRun = true )
	public void CheckErrorAsUserHaveNoProfileYet() {
		WebElement NO_PROFILE_ERROR =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(AccountSettings.NO_PROFILE_ERROR)));
		Assert.assertEquals(NO_PROFILE_ERROR.getText().trim(),Messages.NO_PROFILE_ERROR);
	}
	
	@Test(dependsOnMethods ="CheckErrorAsUserHaveNoProfileYet",alwaysRun = true )
	public void ClickSubmitNewProfileButton() {
		WebElement SUBMIT_NEW_PROFILE_BUTTON =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(AccountSettings.SUBMIT_NEW_PROFILE_BUTTON)));
		SUBMIT_NEW_PROFILE_BUTTON.click();
	}
	
	@Test(dependsOnMethods ="ClickSubmitNewProfileButton",alwaysRun = true )
	public void CheckEditUserProfilePageDisplayed() {
		WebElement header =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(AccountSettings.HEADER_EUP)));
		Assert.assertEquals(TextLabels.HEADER_EUP, header.getText());
	}
	
	@Test(dependsOnMethods ="CheckEditUserProfilePageDisplayed",alwaysRun = true )
	public void CheckLabels() {
		// Assert text labels
		String bankName = driver.findElement(By.cssSelector(AccountSettings.BANK_NAME_LABEL_EUP)).getText();
		Assert.assertEquals(TextLabels.BANK_NAME_LABEL_EUP, bankName);
		String bankRoutingNumber = driver.findElement(By.cssSelector(AccountSettings.BANK_ROUTING_NO_LABLE_EUP)).getText();
		Assert.assertEquals(TextLabels.BANK_ROUTING_NO_LABLE_EUP, bankRoutingNumber);
		String bankAccountNumber = driver.findElement(By.cssSelector(AccountSettings.BANK_ACCOUNT_NO_LABLE_EUP)).getText();
		Assert.assertEquals(TextLabels.BANK_ACCOUNT_NO_LABLE_EUP, bankAccountNumber);
		String attachedDocument = driver.findElement(By.cssSelector(AccountSettings.ATTACHED_DOCUMENT_LABEL_EUP)).getText();
		Assert.assertEquals(attachedDocument,TextLabels.ATTACHED_DOCUMENT_LABEL_EUP);
	}
	
	@Test(dependsOnMethods ="CheckLabels",alwaysRun = true )
	public void CheckHints() {
		// Assert text hints
		String bankName=driver.findElement(By.id(AccountSettings.BANK_NAME_TEXT_FIELD_EUP)).getAttribute("placeholder");
		assertEquals(TextHints.BANK_NAME_EUP, bankName);
		String bankAccountNumber=driver.findElement(By.id(AccountSettings.BANK_ACCOUNT_NO_TEXT_FIELD_EUP)).getAttribute("placeholder");
		assertEquals(TextHints.BANK_ACCOUNT_NO_EUP, bankAccountNumber);
	}
	
	@Test(dependsOnMethods ="CheckHints",alwaysRun = true )
	public void CheckButtonDisplayed() {
		// Assert all buttons displayed
		driver.findElement(By.id(AccountSettings.UPLOAD_DOCUMENT_BUTTON_EUP)).isEnabled();
		driver.findElement(By.cssSelector(AccountSettings.SUBMIT_BUTTON_EUP)).isEnabled();
		driver.findElement(By.cssSelector(AccountSettings.CANCEL_BUTTON_EUP)).isEnabled();
	}
	
	@Test(dependsOnMethods ="CheckButtonDisplayed",alwaysRun = true )
	public void ClickCancelButton() {
		// Click Cancel button, Sign in and security screen is displayed
		driver.findElement(By.cssSelector(AccountSettings.CANCEL_BUTTON_EUP)).click();
	}
	
	@Test(dependsOnMethods ="ClickCancelButton",alwaysRun = true )
	public void CheckPreviousScreenDisplayed() {
		WebElement SUBMIT_NEW_PROFILE_BUTTON =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(AccountSettings.SUBMIT_NEW_PROFILE_BUTTON)));
		SUBMIT_NEW_PROFILE_BUTTON.click();
	}
	
	@Test(dependsOnMethods ="CheckPreviousScreenDisplayed",alwaysRun = true )
	public void ClickSubmitButton() {
		WebElement submitButton =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(AccountSettings.SUBMIT_BUTTON_EUP)));
		submitButton.click();
	}
	
	// To be continued
}
