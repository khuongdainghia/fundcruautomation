package com.fundcru.accountsettings;

import static org.testng.Assert.assertEquals;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.AccountSettings;
import com.fundcru.objects.pagedefinitions.FaceBook;
import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Login;
import com.fundcru.objects.pagedefinitions.PersonalInfomation;
import com.fundcru.objects.pagedefinitions.SignUp;
import com.fundcru.objects.pagedefinitions.TextLabels;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.utilities.BaseTestCaseNoCredential;

public class RegisterWithFacebook extends BaseTestCaseNoCredential {
	int timeout =50;
	ScreenAction action;
	String mwh;
	
	String email="fundcrutest3@gmail.com", pass="abc123456abc", name="Anan Tran";
	
	@Test
	public void ClickSignInLink() throws Exception {
		action = new ScreenAction(driver);
		action.clickBtn(By.id(Login.SIGNIN_LINK ));
	}

	@Test(dependsOnMethods ="ClickSignInLink",alwaysRun = true )
	public void ClickRegisterLink() throws Exception {
		action.clickBtn(By.id(Login.REGISTER ));
	}
	
	@Test(dependsOnMethods ="ClickRegisterLink",alwaysRun = true )
	public void CheckRegisterScreenDisplayed() {
		action.assertTextEqual(By.cssSelector(SignUp.HEADER_LABEL), TextLabels.HEADER_SIGNUP);
	}	

	@Test(dependsOnMethods ="CheckRegisterScreenDisplayed",alwaysRun = true )
	public void ClickRegisterWithFacebookButton() {
		action.clickBtn(By.id(SignUp.REGISTER_WITH_FACEBOOK_BUTTON));
	}
	
	@Test(dependsOnMethods ="ClickRegisterWithFacebookButton",alwaysRun = true )
	public void CheckFacebookWindowPopupDisplays() {
		mwh=driver.getWindowHandle();
		Set s=driver.getWindowHandles(); //this method will gives the handles of all opened windows
		Iterator ite=s.iterator();
		while(ite.hasNext())
		{
		    String popupHandle=ite.next().toString();
		    if(!popupHandle.contains(mwh))
		    {
		        driver.switchTo().window(popupHandle);
		        driver.getCurrentUrl();
		        boolean facebookWindow = false;
		        if(driver.getCurrentUrl().startsWith("https://www.facebook.com")){
		        	facebookWindow = true;
		        }
		        assertEquals(facebookWindow, true);
		        driver.findElement(By.id(FaceBook.FACEBOOK_HOME_LINK_ID));
		        action.inputTextFieldBy(By.id(FaceBook.FACEBOOK_USERNAME_FIELD_ID), email);
		        action.inputTextFieldBy(By.id(FaceBook.FACEBOOK_PASSWORD_FIELD_ID), pass);
				action.clickBtn(By.id(FaceBook.FACEBOOK_LOGIN_BUTTON_ID));
//				action.clickBtn(By.cssSelector(FaceBook.FACEBOOK_CONTINUE_AS));
		        break;
		    }
		}
	}
	
	@Test(dependsOnMethods ="CheckFacebookWindowPopupDisplays",alwaysRun = true )
	public void switchToMainWindown() {
		try {
	    	 driver.switchTo().window(mwh);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	@Test(dependsOnMethods ="switchToMainWindown",alwaysRun = true )
	public void CheckAndClickUserLinkAfterSuccessRegisterWithFacebook() {
		WebElement userLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS )));
		Assert.assertTrue(userLink.getText().contains(name),String.format("Wrong expected: %s Actual: %s ",name,userLink.getText()));
		userLink.click();
	}
	
	@Test(dependsOnMethods ="CheckAndClickUserLinkAfterSuccessRegisterWithFacebook",alwaysRun = true )
	public void ClickAccountSettings() {
		action.clickBtn(By.id(Home.ACCOUNT_SETTINGS_LINK));
	}
	@Test(dependsOnMethods ="ClickAccountSettings",alwaysRun = true )
	public void ClickPersonalInfomationLink() {
		action.clickBtn(By.cssSelector(AccountSettings.PERSONAL_INF_LINK));
	}
	
	@Test(dependsOnMethods ="ClickPersonalInfomationLink",alwaysRun = true )
	public void waitForPersonalInfomationScreenVisible() {
		action.waitObjVisible(By.cssSelector(PersonalInfomation.HEADER));
	}
	
	@Test(dependsOnMethods ="waitForPersonalInfomationScreenVisible",alwaysRun = true )
	public void checkEmail() {
		action.assertTextEqual(By.xpath(PersonalInfomation.EMAIL), email);
	}
	
	@Test(dependsOnMethods ="checkEmail",alwaysRun = true )
	public void checkFullname() {
		action.assertTextEqual(By.xpath(PersonalInfomation.FULLNAME), name);
	}
}
