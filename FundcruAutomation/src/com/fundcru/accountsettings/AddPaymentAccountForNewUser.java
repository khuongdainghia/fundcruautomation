package com.fundcru.accountsettings;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.objects.pages.TableFunction;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;

@Credentials(user="fundcrutest@gmail.com", password="123456789")
public class AddPaymentAccountForNewUser extends BaseTestCase {
	int timeout =30;
	WebElement tablePayment;
	ScreenAction action;
	TableFunction table;
	String emailAccount ="fundcrutest@gmail.com";
	String emailAccountInvalid ="fundcrutest123";
	String emailAccountInvalid1 ="fundcrutest123@";
	int rowcount;
	@Test
	public void ClickOnUserLink() {
		table= new TableFunction(driver);
		action = new ScreenAction(driver);
		WebElement userLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		userLink.click();
		(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.PAYMENT_ACCOUNTS))).click();
		action.pause(2000);
		if (action.isElementPresent(By.linkText(Home.ADD_PAYMENT_ACOUNT_LINK_TEXT)))
		{
			rowcount =table.countRow();
			if (rowcount>0)
			{
				for (int i = 1; i <= rowcount; i++) {
					driver.findElement(By.xpath(String.format(Home.DELETE_PAYMENT_ACCOUNT_ROW_XPATH,i))).click();
					WebElement deleteButton =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(Home.DELETE_BUTTON_DIALOGUE)));
					JavascriptExecutor js = (JavascriptExecutor) driver;
					js.executeScript("arguments[0].click();",deleteButton);
				}
			}
				
		}
		WebElement addPaymentAccount= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.CREATE_PAYMENT_ACCOUNT_BUTTON)));
		Assert.assertEquals(addPaymentAccount.getText(),Home.CREATE_PAYMENT_ACOUNT_BUTTON_TEXT);
	}
	@Test(dependsOnMethods="ClickOnUserLink",alwaysRun = true)
	public void AccessToPaymentAccount() {
		driver.get(getServerURL() + "/payment/accounts");
		
	}
	@Test(dependsOnMethods="AccessToPaymentAccount",alwaysRun = true)
	public void ClickOnAddPaymentButton() {
		WebElement addPaymentAccount= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.CREATE_PAYMENT_ACCOUNT_BUTTON)));
		Assert.assertEquals(addPaymentAccount.getText(),Home.CREATE_PAYMENT_ACOUNT_BUTTON_TEXT);
		Assert.assertEquals(action.isElementPresent(By.xpath(Home.TABLE_BODY_CSS)), false, "Grid has record");
		addPaymentAccount.click();
	}
	@Test(dependsOnMethods="ClickOnAddPaymentButton",alwaysRun = true)
	public void InputNameAndEmail() {
	    action.inputTextFieldByID(Home.FIRST_NAME, "1st");
	    action.inputTextFieldByID(Home.LAST_NAME, "Account");
	    action.inputTextFieldByID(Home.EMAIL, emailAccount);
	    driver.findElement(By.cssSelector(Home.CONFIRM_ADD_PAYMENT_ACCOUNT_BUTTON)).click();
	}
	@Test(dependsOnMethods="InputNameAndEmail",alwaysRun = true)
	public void CheckConfirmScreen() {
	    driver.findElement(By.linkText(Home.RETURN_TO_PAYMENT_ACCOUNT_LINK_TEXT)).click();
	}
	@Test(dependsOnMethods="InputNameAndEmail",alwaysRun = true)
	public void CheckInformationAfterAdded() {
		tablePayment= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(Home.TABLE_BODY_WITH_DIV_XPATH)));
		List<WebElement> tablePaymentRow = tablePayment.findElements(By.tagName("tr"));
		Assert.assertEquals(tablePaymentRow.size(), 1);
	    Assert.assertEquals(driver.findElement(By.xpath(Home.NAME_PAYMENT_ACCOUNT_ROW1_XPATH)).getText(), "1st Account");
	    Assert.assertEquals(driver.findElement(By.xpath(Home.EMAIL_PAYMENT_ACCOUNT_ROW1)).getText(),emailAccount);
	}
	@Test(dependsOnMethods="CheckInformationAfterAdded",alwaysRun = true)
	public void ClickOnAddPaymentButtonAgain() {
		WebElement linkAddPaymentAccount= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.linkText(Home.ADD_PAYMENT_ACOUNT_LINK_TEXT)));
		Assert.assertEquals(linkAddPaymentAccount.getText(),Home.ADD_PAYMENT_ACOUNT_LINK_TEXT);
		linkAddPaymentAccount.click();
	}
	@Test(dependsOnMethods="ClickOnAddPaymentButtonAgain",alwaysRun = true)
	public void InputNameAndEmailToCheckIfEmailExist() {
	    action.inputTextFieldByID(Home.FIRST_NAME, "2nd");
	    action.inputTextFieldByID(Home.LAST_NAME, "Account");
	    action.inputTextFieldByID(Home.EMAIL, emailAccount);
	    driver.findElement(By.cssSelector(Home.CONFIRM_ADD_PAYMENT_ACCOUNT_BUTTON)).click();
	    WebElement errorMessage= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(Home.ERROR_EXIST_PAYMENT_ACCOUNT)));
		Assert.assertTrue(errorMessage.getText().contains(Messages.PAYMENT_ACCOUNT_EXIST),String.format("Expected message: '%s' but actual: '%s'",Messages.PAYMENT_ACCOUNT_EXIST,errorMessage.getText()));
	    
	}
	@Test(dependsOnMethods="InputNameAndEmailToCheckIfEmailExist",alwaysRun = true)
	public void InputNameAndEmailToCheckIfEmailInvalid() {
		action.inputTextFieldByID(Home.EMAIL, emailAccountInvalid);
	    driver.findElement(By.cssSelector(Home.CONFIRM_ADD_PAYMENT_ACCOUNT_BUTTON)).click();
		action.assertTextEqual(By.className(Home.INVALID_EMAIL_MESSAGE_CLASS), Messages.INVALID_EMAIL_MESSAGE);
		
	}
	@Test(dependsOnMethods="InputNameAndEmailToCheckIfEmailInvalid",alwaysRun = true)
	public void InputNameAndEmailToCheckIfEmailInvalidAgain() {
		action.inputTextFieldByID(Home.EMAIL, emailAccountInvalid1);
	    
		driver.findElement(By.cssSelector(Home.CONFIRM_ADD_PAYMENT_ACCOUNT_BUTTON)).click();
	    
		action.assertTextEqual(By.className(Home.INVALID_EMAIL_MESSAGE_CLASS), Messages.INVALID_EMAIL_MESSAGE);
		
	}
	@Test(dependsOnMethods="InputNameAndEmailToCheckIfEmailInvalidAgain",alwaysRun = true)
	public void DelelePaymentAccount() {

		driver.get(getServerURL() + "/payment/accounts");
		(new WebDriverWait(driver, timeout*2)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(Home.DELETE_PAYMENT_ACCOUNT_ROW1_XPATH))).click();
		WebElement deleteButton =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(Home.DELETE_BUTTON_DIALOGUE)));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();",deleteButton);
	}
	@Test(dependsOnMethods="DelelePaymentAccount",alwaysRun = true)
	public void RecheckPaymentAfterDeleted() {
		action.pause(1000);
		Assert.assertEquals(action.isElementPresent(By.xpath(Home.TABLE_BODY_WITH_DIV_XPATH)), false, "Grid has record");
	}
}
