package com.fundcru.accountsettings;

import static org.testng.Assert.assertEquals;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.AccountSettings;
import com.fundcru.objects.pagedefinitions.FaceBook;
import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Login;
import com.fundcru.objects.pagedefinitions.PersonalInfomation;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.utilities.BaseTestCaseNoCredential;

public class SignInWithFaceBook extends BaseTestCaseNoCredential {
	int timeout =30;
	String email="khuongdainghia@outlook.com",pass="123loveyou", name="Cru Nick";
	ScreenAction action;
	@Test
	public void ClickSignInLink() throws Exception {
		action = new ScreenAction(driver);
		action.clickBtn(By.id(Login.SIGNIN_LINK ));
	}
	
	@Test(dependsOnMethods ="ClickSignInLink",alwaysRun = true )
	public void CheckButtonDisplayed() {
		// Assert all buttons displayed
		driver.findElement(By.id(Login.SIGNIN_BUTTON_ID)).isEnabled();
		driver.findElement(By.id(Login.SIGNIN_WITH_FACEBOOK)).isEnabled();
		action.clickBtn(By.id(Login.SIGNIN_WITH_FACEBOOK));
	}
	
	@Test(dependsOnMethods ="CheckButtonDisplayed",alwaysRun = true )
	public void CheckTheFaceBookLogin() {
		String mwh=driver.getWindowHandle();
		Set s=driver.getWindowHandles(); //this method will gives the handles of all opened windows
		Iterator ite=s.iterator();
		while(ite.hasNext())
		{
		    String popupHandle=ite.next().toString();
		    if(!popupHandle.contains(mwh))
		    {
		        driver.switchTo().window(popupHandle);
		        ((JavascriptExecutor) driver).executeScript("window.showModalDialog = window.open;");
		        driver.getCurrentUrl();
		        boolean facebookWindow = false;
		        if(driver.getCurrentUrl().startsWith("https://www.facebook.com")){
		        	facebookWindow = true;
		        }
		        assertEquals(facebookWindow, true);
		        driver.findElement(By.id(FaceBook.FACEBOOK_HOME_LINK_ID));
		        driver.findElement(By.id(FaceBook.FACEBOOK_USERNAME_FIELD_ID)).sendKeys(email);
		        driver.findElement(By.id(FaceBook.FACEBOOK_PASSWORD_FIELD_ID)).sendKeys(pass);
		        driver.findElement(By.id(FaceBook.FACEBOOK_LOGIN_BUTTON_ID)).click();    
		    }
		    
		}
		try {
	    	 driver.switchTo().window(mwh);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	@Test(dependsOnMethods ="CheckTheFaceBookLogin",alwaysRun = true )
	public void ClickUserLink() {
		WebElement userLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS )));
		Assert.assertTrue(userLink.getText().contains(name),String.format("Wrong expected: %s Actual: %s ",name,userLink.getText()));
		userLink.click();
	}
	
	@Test(dependsOnMethods ="ClickUserLink",alwaysRun = true )
	public void ClickAccountSettings() {
		action.clickBtn(By.id(Home.ACCOUNT_SETTINGS_LINK));
	}
	@Test(dependsOnMethods ="ClickAccountSettings",alwaysRun = true )
	public void ClickPersonalInfomationLink() {
		action.clickBtn(By.cssSelector(AccountSettings.PERSONAL_INF_LINK));
	}
	
	@Test(dependsOnMethods ="ClickPersonalInfomationLink",alwaysRun = true )
	public void waitForPersonalInfomationScreenVisible() {
		action.waitObjVisible(By.cssSelector(PersonalInfomation.HEADER));
	}

	@Test(dependsOnMethods ="waitForPersonalInfomationScreenVisible",alwaysRun = true )
	public void checkEmail() {
		action.assertTextEqual(By.xpath(PersonalInfomation.EMAIL), email);
	}
	
	@Test(dependsOnMethods ="checkEmail",alwaysRun = true )
	public void checkFullname() {
		action.assertTextEqual(By.xpath(PersonalInfomation.FULLNAME), name);
	}
}
