package com.fundcru.accountsettings;

import static org.testng.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Login;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.objects.pagedefinitions.TextHints;
import com.fundcru.objects.pagedefinitions.TextLabels;
import com.fundcru.utilities.BaseTestCaseNoCredential;
import com.fundcru.utilities.DriverCreator;

public class CheckEmailForResetPassword extends BaseTestCaseNoCredential {
	int emailRow = 0;
	int timeout = 30;
	List<WebElement> unreademailList;
	
	@Test
	public void NavigateToEmailApp() throws Exception {
		//check email for the link to reset password 
		DriverCreator driverCreator = new DriverCreator(getProperty(TEST_BROWSER),getProperty(TEST_SERVER_FLATFORM));
		driver = driverCreator.getWebDriver();
		driver.navigate().to("https://www.google.com.vn");
	}
	
	@Test(dependsOnMethods ="NavigateToEmailApp",alwaysRun = true )
	public void ClickSignIn() throws Exception {
		// Click sign in button
		WebElement signInButton =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Login.gmailSignInButton_id)));
		signInButton.click();
	}
	
	@Test(dependsOnMethods ="ClickSignIn",alwaysRun = true )
	public void EnterEmailUserName() throws Exception {
		// Enter Email for sign in
		WebElement gmailId =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Login.gmailId)));
		gmailId.sendKeys("fundcrutest@gmail.com");
	}
	
	@Test(dependsOnMethods ="EnterEmailUserName",alwaysRun = true )
	public void ClickNextI() throws Exception {
		// Click Next button
		driver.findElement(By.cssSelector(Login.gmailNextButton)).click();
	}
	
	@Test(dependsOnMethods ="ClickNextI",alwaysRun = true )
	public void EnterEmailPassword() throws Exception {
		// Enter password
		WebElement gmailPassword =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Login.gmailPassword)));
		gmailPassword.sendKeys("abc123456abcd");
	}
	
	@Test(dependsOnMethods ="EnterEmailPassword",alwaysRun = true )
	public void ClickNextII() throws Exception {
		// Click Next button
		driver.findElement(By.cssSelector(Login.gmailPasswordNextButton)).click();
	}
	
	@Test(dependsOnMethods ="ClickNextII",alwaysRun = true )
	public void ClickGmailLink() throws Exception {
		// After log in email successfully, click gmail button
		WebElement gmailLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.linkText("Gmail")));
		gmailLink.click();
	}
	
	@Test(dependsOnMethods ="ClickGmailLink",alwaysRun = true )
	public void CheckResetEmailPresented() throws Exception {
		WebElement emailList =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@class='bog']")));
		unreademailList = driver.findElements(By.xpath("//*[@class='bog']"));
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Mailer name for which i want to check if have an email in my inbox 
		String MyMailer = "[FundCru] Verify Your Email Address";
		
		// real logic starts here
		for(int i=0;i<unreademailList.size();i++){
		    if(unreademailList.get(i).isDisplayed()==true){
		        // now verify if you have got mail form a specific mailer (Note Un-read mails)
		        if(unreademailList.get(i).getText().startsWith(MyMailer)){
		            // open Fundcru email, then click the link for reseting password
		            System.out.println("Yes we have got mail from Fundcru to " + MyMailer);
		            unreademailList.get(i);
		            emailRow = i;
		            break;
		        }else{
		            System.out.println("No mail from Fundcru to " + MyMailer);
		        }
		    }
		}
	}
	
	@Test(dependsOnMethods ="CheckResetEmailPresented",alwaysRun = true )
	public void ClickResetLink() throws Exception {
		// talking un-read email form inbox into a list
        unreademailList.get(emailRow).click();
		java.util.List<WebElement> links = driver.findElements(By.tagName("a"));
		for (int j = 1; j<=links.size(); j=j+1)
		{
			if(links.get(j).getText().startsWith(getServerURL()+"/reset-password/")){
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// Navigate to Reset Password screen
				driver.navigate().to(links.get(j).getText());
	            break;
			}
		}
	}
	
	@Test(dependsOnMethods ="ClickResetLink",alwaysRun = true )
	public void CheckLabelsOnResetPasswordScreen() throws Exception {
		// Assert text labels in Reset Password screen
		WebElement headerLabel =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Login.HEADER_RESET_PW)));
		Assert.assertTrue(headerLabel.getText().equals(TextLabels.HEADER_RESET_PW), "Wrong header displays " + headerLabel);
		String firstPasswordLabel = driver.findElement(By.cssSelector(Login.FIRSTPASSWORD_TEXT_FIELD_LABLE_RESET_PW)).getText();
		Assert.assertTrue(firstPasswordLabel.equals(TextLabels.FIRST_PASSWORD_RESET_PW), "Wrong label for the first password field displays " + firstPasswordLabel);
		String secondPasswordLabel = driver.findElement(By.cssSelector(Login.SECONDPASSWORD_TEXT_FIELD_LABLE_RESET_PW)).getText();
		Assert.assertTrue(secondPasswordLabel.equals(TextLabels.SECOND_PASSWORD_RESET_PW), "Wrong label for the second password field displays " + secondPasswordLabel);
	}
	
	@Test(dependsOnMethods ="CheckLabelsOnResetPasswordScreen",alwaysRun = true )
	public void CheckHints() throws Exception {
		// Assert text hints in Reset Password screen
		String firstPasswordPlaceholder=driver.findElement(By.id(Login.FIRSTPASSWORD_TEXT_FIELD_RESET_PW)).getAttribute("placeholder");
		assertEquals(TextHints.FIRST_PASSWORD_RESET_PW, firstPasswordPlaceholder);
		String secondPasswordPlaceholder=driver.findElement(By.id(Login.SECONDPASSWORD_TEXT_FIELD_RESET_PW)).getAttribute("placeholder");
		assertEquals(TextHints.SECOND_PASSWORD_RESET_PW, secondPasswordPlaceholder);
	}
	
	@Test(dependsOnMethods ="CheckHints",alwaysRun = true )
	public void CheckButtons() throws Exception {
		// Assert all buttons displayed in Reset Password screen
		driver.findElement(By.id(Login.SETPASSWORD_BUTTON_RESET_PW)).isEnabled();
		driver.findElement(By.id(Login.CANCEL_BUTTON_RESET_PW)).isEnabled();
	}
	
	@Test(dependsOnMethods ="CheckButtons",alwaysRun = true )
	public void ClickSetPasswordButtonWithoutPasswordsEntered() throws Exception {
		driver.findElement(By.id(Login.SETPASSWORD_BUTTON_RESET_PW)).click();
	}
	
	@Test(dependsOnMethods ="ClickSetPasswordButtonWithoutPasswordsEntered",alwaysRun = true )
	public void CheckErrorForRequiredFields() throws Exception {
		// Check error when clicking Set Password Button without any fields entered
		WebElement errorPassword =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Login.FIRSTPASSWORD_ERROR_RESET_PW)));
		Assert.assertEquals(Messages.REQUIRED_FIELD, errorPassword.getText());
		String errorPasswordAgain = driver.findElement(By.cssSelector(Login.SECONDPASSWORD_ERROR_RESET_PW)).getText();
		Assert.assertEquals(Messages.REQUIRED_FIELD, errorPasswordAgain);
	}
	
	@Test(dependsOnMethods ="CheckErrorForRequiredFields",alwaysRun = true )
	public void CheckMinimumCharacterValitionPWI() throws Exception {
		// Enter invalid data in the password fields 
		driver.findElement(By.id(Login.FIRSTPASSWORD_TEXT_FIELD_RESET_PW)).sendKeys("123");
	}
	
	@Test(dependsOnMethods ="CheckMinimumCharacterValitionPWI",alwaysRun = true )
	public void ClickSetPasswordI() throws Exception {
		driver.findElement(By.id(Login.SETPASSWORD_BUTTON_RESET_PW)).click();
	}
	
	@Test(dependsOnMethods ="ClickSetPasswordI",alwaysRun = true )
	public void CheckErrorForMinimumCharacterValition() throws Exception {
		// Check error for minimum character
		WebElement minimumrequiredlengthError =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Login.FIRSTPASSWORD_ERROR_RESET_PW)));
		Assert.assertEquals(Messages.MINIMUM_REQUIRED_LEAGTH_RESET_PW, minimumrequiredlengthError.getText());
		String equiredPassError = driver.findElement(By.cssSelector(Login.SECONDPASSWORD_ERROR_RESET_PW)).getText();
		Assert.assertEquals(Messages.REQUIRED_FIELD, equiredPassError);
	}
	
	@Test(dependsOnMethods ="CheckErrorForMinimumCharacterValition",alwaysRun = true )
	public void CheckMinimumCharacterValitionPWII() throws Exception {
		// Enter invalid data in the second password fields 
		driver.findElement(By.id(Login.SECONDPASSWORD_TEXT_FIELD_RESET_PW)).sendKeys("123");
	}
	
	@Test(dependsOnMethods ="CheckMinimumCharacterValitionPWII",alwaysRun = true )
	public void ClickSetPasswordII() throws Exception {
		driver.findElement(By.id(Login.SETPASSWORD_BUTTON_RESET_PW)).click();
	}
	
	@Test(dependsOnMethods ="ClickSetPasswordII",alwaysRun = true )
	public void CheckErrorForMinimumCharacterValitionII() throws Exception {
		// Check error for minimum character
		WebElement minimumrequiredlengthError1 =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Login.FIRSTPASSWORD_ERROR_RESET_PW)));
		Assert.assertEquals(Messages.MINIMUM_REQUIRED_LEAGTH_RESET_PW, minimumrequiredlengthError1.getText());
		String minimumrequiredlengthError2 = driver.findElement(By.cssSelector(Login.SECONDPASSWORD_ERROR_RESET_PW)).getText();
		Assert.assertEquals(Messages.MINIMUM_REQUIRED_LEAGTH_RESET_PW, minimumrequiredlengthError2);
	}
	
	@Test(dependsOnMethods ="CheckErrorForMinimumCharacterValitionII",alwaysRun = true )
	public void EnterDifferentPasswords() throws Exception {
		// Enter two different passwords in two fields
		driver.findElement(By.id(Login.FIRSTPASSWORD_TEXT_FIELD_RESET_PW)).clear();
		driver.findElement(By.id(Login.SECONDPASSWORD_TEXT_FIELD_RESET_PW)).clear();
		driver.findElement(By.id(Login.FIRSTPASSWORD_TEXT_FIELD_RESET_PW)).sendKeys("123456");
		driver.findElement(By.id(Login.SECONDPASSWORD_TEXT_FIELD_RESET_PW)).sendKeys("1234567");
	}
	
	@Test(dependsOnMethods ="EnterDifferentPasswords",alwaysRun = true )
	public void ClickSetPasswordIII() throws Exception {
		driver.findElement(By.id(Login.SETPASSWORD_BUTTON_RESET_PW)).click();
	}
	
	@Test(dependsOnMethods ="ClickSetPasswordIII",alwaysRun = true )
	public void CheckErrorUnmatchedPasswords() throws Exception {
		// Check error for unmatched passwords
		WebElement pwNotMatchError =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Login.SECONDPASSWORD_ERROR_RESET_PW)));
		Assert.assertEquals(Messages.PASSWORD_DONT_MATCH_RESET_PW, pwNotMatchError.getText());
		Assert.assertEquals("", driver.findElement(By.cssSelector(Login.FIRSTPASSWORD_ERROR_RESET_PW)).getText());
	}
	
	@Test(dependsOnMethods ="CheckErrorUnmatchedPasswords",alwaysRun = true )
	public void EnterValidPasswords() throws Exception {
		// Enter valid new passwords in two fields
		driver.findElement(By.id(Login.FIRSTPASSWORD_TEXT_FIELD_RESET_PW)).clear();
		driver.findElement(By.id(Login.SECONDPASSWORD_TEXT_FIELD_RESET_PW)).clear();
		driver.findElement(By.id(Login.FIRSTPASSWORD_TEXT_FIELD_RESET_PW)).sendKeys("123456789");
		driver.findElement(By.id(Login.SECONDPASSWORD_TEXT_FIELD_RESET_PW)).sendKeys("123456789");
	}
	
	@Test(dependsOnMethods ="EnterValidPasswords",alwaysRun = true )
	public void ClickSetPasswordIV() throws Exception {
		driver.findElement(By.id(Login.SETPASSWORD_BUTTON_RESET_PW)).click();
	}
	
	@Test(dependsOnMethods ="ClickSetPasswordIV",alwaysRun = true )
	public void CheckSuccessMessage() throws Exception {		
		// Check success message
	    WebElement successMessage =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Login.RESET_PASSWORD_EMAIL_SUCCESS_MESSAGE)));
	    successMessage.getText().equalsIgnoreCase(Messages.SUCCESS_PASSWORD_RESET_PW);
		WebElement userLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		userLink.isDisplayed();
		}
	}