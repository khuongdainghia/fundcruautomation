package com.fundcru.accountsettings;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.AccountSettings;
import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.objects.pagedefinitions.PersonalInfomation;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;

@Credentials(user="fundcrutest@gmail.com", password="123456789")
public class UpdatePersonalInfomationNegativeTest extends BaseTestCase {
	
	int timeout =30;
	ScreenAction action;
	
	@Test
	public void ClickUserLink() {
		action = new ScreenAction(driver);
		action.clickBtn(By.className(Home.USER_LINK_CLASS));
	}
	
	@Test(dependsOnMethods ="ClickUserLink",alwaysRun = true )
	public void ClickAccountSettings() {
		action.clickBtn(By.id(Home.ACCOUNT_SETTINGS_LINK));
	}
	
	@Test(dependsOnMethods ="ClickAccountSettings",alwaysRun = true )
	public void ClickPersonalInfomationLink() {
		action.clickBtn(By.cssSelector(AccountSettings.PERSONAL_INF_LINK));
	}
	
	@Test(dependsOnMethods ="ClickPersonalInfomationLink",alwaysRun = true )
	public void waitForPersonalInfomationScreenVisible() {
		action.waitObjVisible(By.cssSelector(PersonalInfomation.HEADER));
	}
	
	@Test(dependsOnMethods ="waitForPersonalInfomationScreenVisible",alwaysRun = true )
	public void clickEditFullname() {
	    JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",driver.findElement(By.xpath(PersonalInfomation.FULLNAME_ACTION)));
	}
	
	@Test(dependsOnMethods ="clickEditFullname",alwaysRun = true )
	public void updateFirstnameToEmpty() {
		action.inputTextFieldBy(By.xpath(PersonalInfomation.FULLNAME_MODIFY_DIALOG_FIRSTNAME), "");
	}
	
	@Test(dependsOnMethods ="updateFirstnameToEmpty",alwaysRun = true )
	public void updateLastnameToEmpty() {
		action.inputTextFieldBy(By.xpath(PersonalInfomation.FULLNAME_MODIFY_DIALOG_LASTNAME), "");
	}
	
	@Test(dependsOnMethods ="updateLastnameToEmpty",alwaysRun = true )
	public void clickSubmitButtonFullnameDialog() {
		action.clickBtn(By.xpath(PersonalInfomation.FULLNAME_MODIFY_DIALOG_SUBMIT_BUTTON));
	}
	
	@Test(dependsOnMethods ="clickSubmitButtonFullnameDialog",alwaysRun = true )
	public void checkDialogDisplaysErrorMessage() {
		action.assertTextEndWith(By.xpath(PersonalInfomation.FULLNAME_MODIFY_DIALOG_ERROR_MESSAGE), Messages.FAILED_ERROR_MODIFYING_PERSONAL_INFORMATION);
	}
	
	@Test(dependsOnMethods ="checkDialogDisplaysErrorMessage",alwaysRun = true )
	public void checkErrorOnFirstnameField() {
		action.assertTextEqual(By.xpath(PersonalInfomation.ERROR_FOR_FIRSTNAME_VALIDATION), Messages.FIRSTNAME_REQUIRED_PERSONAL_INFORMATION);
	}
	
	@Test(dependsOnMethods ="checkErrorOnFirstnameField",alwaysRun = true )
	public void checkErrorOnLastnameField() {
		action.assertTextEqual(By.xpath(PersonalInfomation.ERROR_FOR_LASTNAME_VALIDATION), Messages.LASTNAME_REQUIRED_PERSONAL_INFORMATION);
	}
	
	@Test(dependsOnMethods ="checkErrorOnLastnameField",alwaysRun = true )
	public void clickCancelButton() {
		action.clickBtn(By.xpath(PersonalInfomation.FULLNAME_MODIFY_DIALOG_CANCEL_BUTTON));
	}
	
	@Test(dependsOnMethods ="clickCancelButton",alwaysRun = true )
	public void verifyFullnameNotUpdated() {
		action.assertTextEqual(By.xpath(PersonalInfomation.FULLNAME), "Audrey Tran");
	}
	
	@Test(dependsOnMethods ="verifyFullnameNotUpdated",alwaysRun = true )
	public void clickEditAboutLink() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",driver.findElement(By.xpath(PersonalInfomation.ABOUT_ACTION)));
	}
	
	@Test(dependsOnMethods ="clickEditAboutLink",alwaysRun = true )
	public void updateAboutValueToEmpty() {
		action.inputTextFieldBy(By.xpath(PersonalInfomation.ABOUT_MODIFY_DIALOG_ABOUT_TEXT_AREA), "");
	}
	
	@Test(dependsOnMethods ="updateAboutValueToEmpty",alwaysRun = true )
	public void clickSubmitButtonAboutDialog() {
		action.clickBtn(By.xpath(PersonalInfomation.ABOUT_MODIFY_DIALOG_SUBMIT_BUTTON));
	}
	
	@Test(dependsOnMethods ="clickSubmitButtonAboutDialog",alwaysRun = true )
	public void verifyAboutCanBeEmpty() {
		int aboutAfterUpdated = driver.findElement((By.xpath(PersonalInfomation.ABOUT))).getText().length();
		assertEquals(aboutAfterUpdated, 0);
	}

	@Test(dependsOnMethods ="verifyAboutCanBeEmpty",alwaysRun = true )
	public void changeBackAboutToOriginalValue() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",driver.findElement(By.xpath(PersonalInfomation.ABOUT_ACTION)));
		action.inputTextFieldBy(By.xpath(PersonalInfomation.ABOUT_MODIFY_DIALOG_ABOUT_TEXT_AREA), "About Testing");
		action.clickBtn(By.xpath(PersonalInfomation.ABOUT_MODIFY_DIALOG_SUBMIT_BUTTON));
	}
	
}
