package com.fundcru.others;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.BrowseFundraiser;
import com.fundcru.objects.pagedefinitions.BrowseOffers;
import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Login;
import com.fundcru.objects.pagedefinitions.TextLabels;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.utilities.BaseTestCaseNoCredential;

public class LinksDirectionNonUser extends BaseTestCaseNoCredential {
	
	int timeout =30;
	ScreenAction action;
	String fundcruURL = "https://user122.fundcru.com/";

	@Test
	public void getCurrentURL() throws Exception {
		action = new ScreenAction(driver);
		action.assertCorrectURL(fundcruURL);
		action.assertCorrectURL(getServerURL());
	}
	
	@Test(dependsOnMethods ="getCurrentURL",alwaysRun = true )
	public void checkHomeScreenDisplaysI () throws Exception {
		action.checkObjVisible(By.cssSelector(Home.HOME_HEADER));
		action.assertTextEqualCssSelector(Home.HOME_HEADER, TextLabels.HOME_HEADER);
	}
	
	@Test(dependsOnMethods ="checkHomeScreenDisplaysI",alwaysRun = true )
	public void checkSignInButtonDisplayForNonUser () throws Exception {
		action.waitObjVisible(By.id(Login.SIGNIN_LINK));
	}
	
	@Test(dependsOnMethods ="checkSignInButtonDisplayForNonUser",alwaysRun = true )
	public void clickSignInLink () throws Exception {
		action.clickBtn(By.id(Login.SIGNIN_LINK));
	}
	
	@Test(dependsOnMethods ="clickSignInLink",alwaysRun = true )
	public void checkSigninScreenDisplays () throws Exception {
		action.assertCorrectURL(fundcruURL + "sign-in");
		action.assertTextEqual(By.cssSelector(Login.SIGNIN_HEADER_LABEL),TextLabels.HEADER_SIGNIN);
	}
	
	@Test(dependsOnMethods ="checkSigninScreenDisplays",alwaysRun = true )
	public void clickCreateFundraiser() throws Exception {
		action.waitObjVisibleAndClick(By.id(Home.CREATE_A_FUNDRAISER));
	}
	
	@Test(dependsOnMethods ="clickCreateFundraiser",alwaysRun = true )
	public void checkCreateFundraiserLinkSelected () throws Exception {
		driver.findElement(By.id(Home.CREATE_A_FUNDRAISER)).isSelected();
	}
	
	@Test(dependsOnMethods ="checkCreateFundraiserLinkSelected",alwaysRun = true )
	public void checkSignInScreenDisplaysRequiredForSignIn () throws Exception {
		action.assertCorrectURL(fundcruURL + "sign-in");
		action.assertTextEqual(By.cssSelector(Login.SIGNIN_HEADER_LABEL),TextLabels.HEADER_SIGNIN);
		action.waitObjInvisible(By.cssSelector(Login.ERROR_MESSAGE));
	}
	
	@Test(dependsOnMethods ="checkSignInScreenDisplaysRequiredForSignIn",alwaysRun = true )
	public void clickBrowseFundraiser () throws Exception {
		action.clickBtn(By.id(Home.BROWSE_FUNDRAISER_LINK));
	}
	
	@Test(dependsOnMethods ="clickBrowseFundraiser",alwaysRun = true )
	public void checkBrowseFundraisersLinkSelected () throws Exception {
		driver.findElement(By.id(BrowseFundraiser.BROWSE_FUNDRAISER_LINK)).isSelected();
	}
	
	@Test(dependsOnMethods ="checkBrowseFundraisersLinkSelected",alwaysRun = true )
	public void checkFundraiserScreenDisplays () throws Exception {
		action.assertCorrectURL(fundcruURL + "fundraisers");
		action.assertTextEqual(By.cssSelector(BrowseFundraiser.POPULAR_FUNDRAISER_LABEL),TextLabels.POPULAR_FUNDRAISER_LABEL);
	}
	
	@Test(dependsOnMethods ="checkFundraiserScreenDisplays",alwaysRun = true )
	public void clickBrowseOffers () throws Exception {
		action.clickBtn(By.id(BrowseOffers.BROWSER_OFFERS_LINK));
	}
	
	@Test(dependsOnMethods ="clickBrowseOffers",alwaysRun = true )
	public void checkBrowseOffersLinkSelected () throws Exception {
		driver.findElement(By.id(BrowseOffers.BROWSER_OFFERS_LINK)).isSelected();
	}
	
	@Test(dependsOnMethods ="checkBrowseOffersLinkSelected",alwaysRun = true )
	public void checkOfferScreenDisplays () throws Exception {
		action.assertCorrectURL(fundcruURL + "offers");
		action.assertTextEqual(By.cssSelector(BrowseOffers.POPULAR_OFFER_LABEL),TextLabels.POPULAR_OFFERS_LABEL);
		action.checkObjVisible(By.cssSelector(BrowseOffers.FOOD_AND_DRINK_BTN));
		action.checkObjVisible(By.cssSelector(BrowseOffers.GOODS_AND_RETAIL_BTN));
	}
	
	@Test(dependsOnMethods ="checkOfferScreenDisplays",alwaysRun = true )
	public void clickFundcruLogo () throws Exception {
		action.clickBtn(By.cssSelector(Home.FUNDCRU_LOGO));
	}
	
	@Test(dependsOnMethods ="clickFundcruLogo",alwaysRun = true )
	public void checkHomeScreenDisplaysII () throws Exception {
		action.assertCorrectURL(fundcruURL);
		action.checkObjVisible(By.cssSelector(Home.HOME_HEADER));
		action.assertTextEqualCssSelector(Home.HOME_HEADER, TextLabels.HOME_HEADER);
	}
	
	@Test(dependsOnMethods ="checkHomeScreenDisplaysII",alwaysRun = true )
	public void checkUserIconInvisible () throws Exception {
		action.waitObjInvisible(By.cssSelector(Home.USER_ICON));
	}
	
	@Test(dependsOnMethods ="checkUserIconInvisible",alwaysRun = true )
	public void checkEmailIconInvisible () throws Exception {
		action.waitObjInvisible(By.cssSelector(Home.EMAIL_ICON));
	}
	
	@Test(dependsOnMethods ="checkEmailIconInvisible",alwaysRun = true )
	public void checkDashboardLinkInvisible () throws Exception {
		action.waitObjInvisible(By.cssSelector(Home.DASHBOARD));
	}
}
