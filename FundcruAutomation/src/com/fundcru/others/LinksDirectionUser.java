package com.fundcru.others;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.BrowseFundraiser;
import com.fundcru.objects.pagedefinitions.BrowseOffers;
import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.Dashboard;
import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Login;
import com.fundcru.objects.pagedefinitions.MessageCenter;
import com.fundcru.objects.pagedefinitions.TextLabels;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;

@Credentials(user="khuongdainghia@gmail.com", password="loveyou")
public class LinksDirectionUser extends BaseTestCase {
	
	int timeout =30;
	ScreenAction action;
	String fundcruURL = "https://user122.fundcru.com/";

	@Test
	public void getCurrentURL() throws Exception {
		action = new ScreenAction(driver);
		action.assertCorrectURL(fundcruURL);
	}
	
	@Test(dependsOnMethods ="getCurrentURL",alwaysRun = true )
	public void checkHomeScreenDisplaysI () throws Exception {
		action.checkObjVisible(By.cssSelector(Home.HOME_HEADER));
		action.assertTextEqualCssSelector(Home.HOME_HEADER, TextLabels.HOME_HEADER);
	}
	
	@Test(dependsOnMethods ="checkHomeScreenDisplaysI",alwaysRun = true )
	public void checkSignInButtonInvisible () throws Exception {
		action.waitObjInvisible(By.id(Login.SIGNIN_LINK));
	}
	
	@Test(dependsOnMethods ="checkSignInButtonInvisible",alwaysRun = true )
	public void clickCreateFundraiser() throws Exception {
		action.waitObjVisibleAndClick(By.id(Home.CREATE_A_FUNDRAISER));
	}
	
	@Test(dependsOnMethods ="clickCreateFundraiser",alwaysRun = true )
	public void checkCreateFundraiserLinkSelected () throws Exception {
		driver.findElement(By.id(Home.CREATE_A_FUNDRAISER)).isSelected();
	}
	
	@Test(dependsOnMethods ="checkCreateFundraiserLinkSelected",alwaysRun = true )
	public void checkCreateFundraiserScreenDisplays () throws Exception {
		action.assertCorrectURL(fundcruURL + "create/fundraiser");
		action.assertTextEqual(By.cssSelector(Campaign.CAMPAIGN_CREATE_FORM_TITLE),TextLabels.CAMPAIGN_CREATE_FORM_TITLE);
	}
	
	@Test(dependsOnMethods ="checkCreateFundraiserScreenDisplays",alwaysRun = true )
	public void clickBrowseFundraiser () throws Exception {
		action.clickBtn(By.id(Home.BROWSE_FUNDRAISER_LINK));
	}
	
	@Test(dependsOnMethods ="clickBrowseFundraiser",alwaysRun = true )
	public void checkBrowseFundraisersLinkSelected () throws Exception {
		driver.findElement(By.id(BrowseFundraiser.BROWSE_FUNDRAISER_LINK)).isSelected();
	}
	
	@Test(dependsOnMethods ="checkBrowseFundraisersLinkSelected",alwaysRun = true )
	public void checkFundraiserScreenDisplays () throws Exception {
		action.assertCorrectURL(fundcruURL + "fundraisers");
		action.assertTextEqual(By.cssSelector(BrowseFundraiser.POPULAR_FUNDRAISER_LABEL),TextLabels.POPULAR_FUNDRAISER_LABEL);
	}
	
	@Test(dependsOnMethods ="checkFundraiserScreenDisplays",alwaysRun = true )
	public void clickBrowseOffers () throws Exception {
		action.clickBtn(By.id(BrowseOffers.BROWSER_OFFERS_LINK));
	}
	
	@Test(dependsOnMethods ="clickBrowseOffers",alwaysRun = true )
	public void checkBrowseOffersLinkSelected () throws Exception {
		driver.findElement(By.id(BrowseOffers.BROWSER_OFFERS_LINK)).isSelected();
	}
	
	@Test(dependsOnMethods ="checkBrowseOffersLinkSelected",alwaysRun = true )
	public void checkOfferScreenDisplays () throws Exception {
		action.assertCorrectURL(fundcruURL + "offers");
		action.assertTextEqual(By.cssSelector(BrowseOffers.POPULAR_OFFER_LABEL),TextLabels.POPULAR_OFFERS_LABEL);
		action.checkObjVisible(By.cssSelector(BrowseOffers.FOOD_AND_DRINK_BTN));
		action.checkObjVisible(By.cssSelector(BrowseOffers.GOODS_AND_RETAIL_BTN));
	}
	
	@Test(dependsOnMethods ="checkOfferScreenDisplays",alwaysRun = true )
	public void clickFundcruLogo () throws Exception {
		action.clickBtn(By.cssSelector(Home.FUNDCRU_LOGO));
	}
	
	@Test(dependsOnMethods ="clickFundcruLogo",alwaysRun = true )
	public void checkHomeScreenDisplaysII () throws Exception {
		action.assertCorrectURL(fundcruURL);
		action.checkObjVisible(By.cssSelector(Home.HOME_HEADER));
		action.assertTextEqualCssSelector(Home.HOME_HEADER, TextLabels.HOME_HEADER);
	}
	
	@Test(dependsOnMethods ="checkHomeScreenDisplaysII",alwaysRun = true )
	public void checkUserIconVisible () throws Exception {
		action.waitObjVisible(By.cssSelector(Home.USER_ICON));
	}
	
	@Test(dependsOnMethods ="checkUserIconVisible",alwaysRun = true )
	public void checkEmailIconVisible () throws Exception {
		action.waitObjVisible(By.cssSelector(Home.EMAIL_ICON));
	}
	
	@Test(dependsOnMethods ="checkEmailIconVisible",alwaysRun = true )
	public void checkDashboardLinkVisible () throws Exception {
		action.waitObjVisible(By.cssSelector(Home.DASHBOARD));
	}
	
	@Test(dependsOnMethods ="checkDashboardLinkVisible",alwaysRun = true )
	public void clickOnDashboardLink () throws Exception {
		action.clickBtn(By.cssSelector(Home.DASHBOARD));
	}
	
	@Test(dependsOnMethods ="clickOnDashboardLink",alwaysRun = true )
	public void checkDashboardLinkSelected () throws Exception {
		driver.findElement(By.cssSelector(Home.DASHBOARD)).isSelected();
	}
	
	@Test(dependsOnMethods ="checkDashboardLinkSelected",alwaysRun = true )
	public void checkDashboardScreenDisplays () throws Exception {
		action.assertCorrectURL(fundcruURL+"dashboard");
		(new WebDriverWait(driver, timeout*3)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Dashboard.HEADER)));
		action.checkObjVisible(By.cssSelector(Dashboard.TIME_RANGE_OPTION_BUTTON));
	}
	
	@Test(dependsOnMethods ="checkDashboardScreenDisplays",alwaysRun = true )
	public void clickOnEmailIcon () throws Exception {
		action.clickBtn(By.cssSelector(Home.EMAIL_ICON));
	}
	
	@Test(dependsOnMethods ="clickOnEmailIcon",alwaysRun = true )
	public void checkEmailIconSelected () throws Exception {
		driver.findElement(By.cssSelector(Home.EMAIL_ICON)).isSelected();
	}
	
	@Test(dependsOnMethods ="checkEmailIconSelected",alwaysRun = true )
	public void checkMessageCenterDisplays () throws Exception {
		action.assertCorrectURL(fundcruURL+"messageCenter");
		action.checkObjVisible(By.cssSelector(MessageCenter.HEADER));
	}
}
