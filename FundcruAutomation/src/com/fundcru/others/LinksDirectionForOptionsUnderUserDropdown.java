package com.fundcru.others;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.AccountSettings;
import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Login;
import com.fundcru.objects.pagedefinitions.MyDonationsScreen;
import com.fundcru.objects.pagedefinitions.MyFavoriteFundraisers;
import com.fundcru.objects.pagedefinitions.MyFavoriteOffers;
import com.fundcru.objects.pagedefinitions.MyFundraiserScreen;
import com.fundcru.objects.pagedefinitions.MyOdersScreen;
import com.fundcru.objects.pagedefinitions.MyWithdrawalsScreen;
import com.fundcru.objects.pagedefinitions.PaymentAccountScreen;
import com.fundcru.objects.pagedefinitions.TextLabels;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;

@Credentials(user="khuongdainghia@gmail.com", password="loveyou")
public class LinksDirectionForOptionsUnderUserDropdown extends BaseTestCase {
	
	int timeout =30;
	ScreenAction action;
	String fundcruURL = "https://user122.fundcru.com/";

	@Test
	public void getCurrentURL() throws Exception {
		action = new ScreenAction(driver);
		action.assertCorrectURL(fundcruURL);
	}
	
	@Test(dependsOnMethods ="getCurrentURL",alwaysRun = true )
	public void checkUserIconVisible () throws Exception {
		action.waitObjVisible(By.cssSelector(Home.USER_ICON));
	}
	
	@Test(dependsOnMethods ="checkUserIconVisible",alwaysRun = true )
	public void clickOnUserIconI () throws Exception {
		action.clickBtn(By.cssSelector(Home.USER_ICON));
	}
	
	@Test(dependsOnMethods ="clickOnUserIconI",alwaysRun = true )
	public void checkAccountSettingsOptionVisible () throws Exception {
		action.waitObjVisible(By.id(Home.ACCOUNT_SETTINGS_LINK));
	}
	
	@Test(dependsOnMethods ="checkAccountSettingsOptionVisible",alwaysRun = true )
	public void checkPaymentAccountOptionVisible () throws Exception {
		action.waitObjVisible(By.id(Home.PAYMENT_ACCOUNTS));
	}
	
	@Test(dependsOnMethods ="checkPaymentAccountOptionVisible",alwaysRun = true )
	public void checkMyFundraiserOptionVisible () throws Exception {
		action.waitObjVisible(By.id(Home.MY_CAMPAIGN_LINK));
	}
	
	@Test(dependsOnMethods ="checkMyFundraiserOptionVisible",alwaysRun = true )
	public void checkMyDonationsOptionVisible () throws Exception {
		action.waitObjVisible(By.id(Home.MY_DONATIONS_LINK));
	}
	
	@Test(dependsOnMethods ="checkMyDonationsOptionVisible",alwaysRun = true )
	public void checkMyOthersOptionVisible () throws Exception {
		action.waitObjVisible(By.id(Home.MY_ORDERS_LINK));
	}
	
	@Test(dependsOnMethods ="checkMyOthersOptionVisible",alwaysRun = true )
	public void checkEthereumWithdrawalsOptionVisible () throws Exception {
		action.waitObjVisible(By.id(Home.MY_WITHDRAWALS_LINK));
	}
	
	@Test(dependsOnMethods ="checkEthereumWithdrawalsOptionVisible",alwaysRun = true )
	public void checkMyFavoriteFundraisersOptionVisible () throws Exception {
		action.waitObjVisible(By.id(Home.MY_FAVARITE_FUNDRAISERS_LINK));
	}
	
	@Test(dependsOnMethods ="checkMyFavoriteFundraisersOptionVisible",alwaysRun = true )
	public void checkMyFavoriteOffersOptionVisible () throws Exception {
		action.waitObjVisible(By.id(Home.MY_FAVARITE_FUNDRAISERS_LINK));
	}
	
	@Test(dependsOnMethods ="checkMyFavoriteOffersOptionVisible",alwaysRun = true )
	public void checkSignOutOptionVisible () throws Exception {
		action.waitObjVisible(By.id(Home.SIGN_OUT));
	}
	
	@Test(dependsOnMethods ="checkSignOutOptionVisible",alwaysRun = true )
	public void clickAccountSettings () throws Exception {
		action.clickBtn(By.id(Home.ACCOUNT_SETTINGS_LINK));
	}
	
	@Test(dependsOnMethods ="clickAccountSettings",alwaysRun = true )
	public void checkAccountSettingsScreenDisplays () throws Exception {
		action.assertCorrectURL(fundcruURL+"settings");
		action.assertTextEqual(By.cssSelector(AccountSettings.ACCOUNT_SETTINGS_HEADER), TextLabels.ACCOUNT_SETTING_HEADER);
		action.waitObjVisible(By.cssSelector(AccountSettings.PERSONAL_INF_LINK));
		action.waitObjVisible(By.cssSelector(AccountSettings.SIGNIN_SECURITY_LINK));
	}
	
	@Test(dependsOnMethods ="checkAccountSettingsScreenDisplays",alwaysRun = true )
	public void clickOnUserIconII () throws Exception {
		action.clickBtn(By.cssSelector(Home.USER_ICON));
	}
	
	@Test(dependsOnMethods ="clickOnUserIconII",alwaysRun = true )
	public void clickPaymentAccount () throws Exception {
		action.clickBtn(By.id(Home.PAYMENT_ACCOUNTS));
	}
	
	@Test(dependsOnMethods ="clickPaymentAccount",alwaysRun = true )
	public void checkPaymentAccountScreenDisplays () throws Exception {
		action.assertCorrectURL(fundcruURL+"payment/accounts");
		action.assertTextEqual(By.cssSelector(PaymentAccountScreen.HEADER), TextLabels.PAYMENT_ACCOUNT_HEADER);
		action.assertTextEqual(By.cssSelector(PaymentAccountScreen.INFORMATION_MESSAGE), TextLabels.PAYMENT_ACCOUNT_INFOR_MESSAGE);
		action.waitObjVisible(By.cssSelector(PaymentAccountScreen.ADD_PAYMENT_ACCOUNT_BUTTON));
	}
	
	@Test(dependsOnMethods ="checkPaymentAccountScreenDisplays",alwaysRun = true )
	public void clickOnUserIconIII () throws Exception {
		action.clickBtn(By.cssSelector(Home.USER_ICON));
	}
	
	@Test(dependsOnMethods ="clickOnUserIconIII",alwaysRun = true )
	public void clickMyFundraiser () throws Exception {
		action.clickBtn(By.id(Home.MY_CAMPAIGN_LINK));
	}
	
	@Test(dependsOnMethods ="clickMyFundraiser",alwaysRun = true )
	public void checkMyFundraiserScreenDisplays () throws Exception {
		action.assertCorrectURL(fundcruURL+"dashboard/fundraisers");
		action.assertTextEqual(By.cssSelector(MyFundraiserScreen.HEADER), TextLabels.MY_FUNDRAISERS_SCREEN_HEADER);
		action.waitObjVisible(By.cssSelector(MyFundraiserScreen.CREATE_NEW_FUNDRAISER_BUTTON));
	}
	
	@Test(dependsOnMethods ="checkMyFundraiserScreenDisplays",alwaysRun = true )
	public void clickOnUserIconIV () throws Exception {
		action.clickBtn(By.cssSelector(Home.USER_ICON));
	}
	
	@Test(dependsOnMethods ="clickOnUserIconIV",alwaysRun = true )
	public void clickMyDonations () throws Exception {
		action.clickBtn(By.id(Home.MY_DONATIONS_LINK));
	}
	
	@Test(dependsOnMethods ="clickMyDonations",alwaysRun = true )
	public void checkMyDonationsScreenDisplays () throws Exception {
		action.assertCorrectURL(fundcruURL+"dashboard/donations");
		action.assertTextEqual(By.cssSelector(MyDonationsScreen.HEADER), TextLabels.MY_DONATIONS_SCREEN_HEADER);
	}
	
	@Test(dependsOnMethods ="checkMyDonationsScreenDisplays",alwaysRun = true )
	public void clickOnUserIconV () throws Exception {
		action.clickBtn(By.cssSelector(Home.USER_ICON));
	}
	
	@Test(dependsOnMethods ="clickOnUserIconV",alwaysRun = true )
	public void clickMyOders () throws Exception {
		action.clickBtn(By.id(Home.MY_ORDERS_LINK));
	}
	
	@Test(dependsOnMethods ="clickMyOders",alwaysRun = true )
	public void checkMyOdersScreenDisplays () throws Exception {
		action.assertCorrectURL(fundcruURL+"dashboard/orders");
		action.assertTextEqual(By.cssSelector(MyOdersScreen.HEADER), TextLabels.MY_ODERS_SCREEN_HEADER);
	}
	
	@Test(dependsOnMethods ="checkMyOdersScreenDisplays",alwaysRun = true )
	public void clickOnUserIconVI () throws Exception {
		action.clickBtn(By.cssSelector(Home.USER_ICON));
	}
	
	@Test(dependsOnMethods ="clickOnUserIconVI",alwaysRun = true )
	public void clickEthereumWithdrawals () throws Exception {
		action.clickBtn(By.id(Home.MY_WITHDRAWALS_LINK));
	}
	
	@Test(dependsOnMethods ="clickEthereumWithdrawals",alwaysRun = true )
	public void checkMyWithdrawalsScreenDisplays () throws Exception {
		action.assertCorrectURL(fundcruURL+"dashboard/withdrawals");
		action.assertTextEqual(By.cssSelector(MyWithdrawalsScreen.HEADER), TextLabels.MY_WITHDRAWALS_SCREEN_HEADER);
	}
	
	@Test(dependsOnMethods ="checkMyWithdrawalsScreenDisplays",alwaysRun = true )
	public void clickOnUserIconVII () throws Exception {
		action.clickBtn(By.cssSelector(Home.USER_ICON));
	}
	
	@Test(dependsOnMethods ="clickOnUserIconVII",alwaysRun = true )
	public void clickMyFavoriteFundraisers () throws Exception {
		action.clickBtn(By.id(Home.MY_FAVARITE_FUNDRAISERS_LINK));
	}
	
	@Test(dependsOnMethods ="clickMyFavoriteFundraisers",alwaysRun = true )
	public void checkMyFavoriteFundraiserScreenDisplays () throws Exception {
		action.assertCorrectURL(fundcruURL+"dashboard/favorite-fundraisers");
		action.assertTextEqual(By.cssSelector(MyFavoriteFundraisers.INFORMATION_MESSAGE), TextLabels.MY_FAVORITE_FUNDRAISERS_SCREEN_INFOMATION_MESSAGE);
	}
	
	@Test(dependsOnMethods ="checkMyFavoriteFundraiserScreenDisplays",alwaysRun = true )
	public void clickOnUserIconVIII () throws Exception {
		action.clickBtn(By.cssSelector(Home.USER_ICON));
	}
	
	@Test(dependsOnMethods ="clickOnUserIconVIII",alwaysRun = true )
	public void clickMyFavoriteOffers () throws Exception {
		action.clickBtn(By.id(Home.MY_FAVORITE_OFFERS_LINK));
	}
	
	@Test(dependsOnMethods ="clickMyFavoriteOffers",alwaysRun = true )
	public void checkMyFavoriteOffersScreenDisplays () throws Exception {
		action.assertCorrectURL(fundcruURL+"dashboard/favorite-offers");
		action.assertTextEqual(By.cssSelector(MyFavoriteOffers.INFORMATION_MESSAGE), TextLabels.MY_FAVORITE_OFFERS_SCREEN_INFOMATION_MESSAGE);
	}
	
	@Test(dependsOnMethods ="checkMyFavoriteOffersScreenDisplays",alwaysRun = true )
	public void clickOnUserIconIX () throws Exception {
		action.clickBtn(By.cssSelector(Home.USER_ICON));
	}
	
	@Test(dependsOnMethods ="clickOnUserIconIX",alwaysRun = true )
	public void clickSignOut () throws Exception {
		action.clickBtn(By.id(Home.SIGN_OUT));
	}
	
	@Test(dependsOnMethods ="clickSignOut",alwaysRun = true )
	public void checkSignInLinkDisplaysAfterSignOut () throws Exception {
		action.assertCorrectURL(fundcruURL);
		action.waitObjVisible(By.id(Login.SIGNIN_LINK));
	}
}
