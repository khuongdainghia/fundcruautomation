package com.fundcru.utilities;

import java.lang.annotation.Annotation;

import org.openqa.selenium.Dimension;
import org.testng.annotations.BeforeClass;

import com.fundcru.objects.pages.LoginPage;

public class BaseTestCase extends BaseTestCaseNoCredential {
	public TestLoginCredentials defaultCredentials;
	public TestLoginCredentials currentCredentials;

	public BaseTestCase() {
		super();
		defaultCredentials = new TestLoginCredentials( getProperty("test.username"),getProperty("test.password"));
		currentCredentials = defaultCredentials;
	}
	@BeforeClass
	@Override
	public void beforeClass() throws Exception {
		try {
	
			currentCredentials = getClassCredentials();
			if (null == currentCredentials)
			{
				currentCredentials=defaultCredentials;
			}
			this.expectedResult = "";
			DriverCreator driverCreator = new DriverCreator(getProperty(TEST_BROWSER),getProperty(TEST_SERVER_FLATFORM));
			driver = driverCreator.getWebDriver();
			try {
	//			driver.manage().window().maximize();
				driver.manage().window().setSize(new Dimension(1600, 900));
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e.getMessage());
			}
			LoginPage login = new LoginPage(driver);
			login.login(getServerURL(), currentCredentials);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
		
	public TestLoginCredentials getCurrentCredentials() {
		
		return currentCredentials;
	}
	protected TestLoginCredentials getClassCredentials() {
		for ( Annotation a : this.getClass().getAnnotations()) {
			if ( a instanceof Credentials ) {
				Credentials c = (Credentials) a;
				return new TestLoginCredentials(c.user(), c.password());
			}
		}
		return null;
	}
	
}
