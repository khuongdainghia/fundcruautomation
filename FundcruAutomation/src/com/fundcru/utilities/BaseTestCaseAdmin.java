package com.fundcru.utilities;

import org.openqa.selenium.Dimension;
import org.testng.annotations.BeforeClass;

import com.fundcru.objects.pages.LoginPage;

public class BaseTestCaseAdmin extends BaseTestCase {
	public static final String TEST_ADMIN_SERVER_URL = "test.admin.server.url";
	public BaseTestCaseAdmin() {
		super();
		
	}
	@BeforeClass
	@Override
	public void  beforeClass() throws Exception {
		try {
		currentCredentials = getClassCredentials();
		if (null == currentCredentials)
		{
			currentCredentials=defaultCredentials;
		}
		this.expectedResult = "";
		DriverCreator driverCreator = new DriverCreator(getProperty(TEST_BROWSER),getProperty(TEST_SERVER_FLATFORM));
		driver = driverCreator.getWebDriver();
		try {
//			driver.manage().window().maximize();
			driver.manage().window().setSize(new Dimension(1600, 900));
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		LoginPage login = new LoginPage(driver);
		login.loginAdmin(getAdminServerURL(), currentCredentials);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
		
	public String getAdminServerURL() {
		return getProperty(TEST_ADMIN_SERVER_URL);
	}
}
