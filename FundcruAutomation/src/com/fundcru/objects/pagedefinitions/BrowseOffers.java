package com.fundcru.objects.pagedefinitions;

public class BrowseOffers {
	public static final String BROWSER_OFFERS_LINK 	= "btn-offer-list-lg" ;
	
	public static final String FOOD_AND_DRINK_BTN   = "#offer-home-contents > div:nth-child(1) > div > div:nth-child(1) > a";
	public static final String GOODS_AND_RETAIL_BTN = "#offer-home-contents > div:nth-child(1) > div > div:nth-child(2) > a";
	
	public static final String POPULAR_OFFER_LABEL 	= "#offer-home-contents > div:nth-child(2) > h3";
}
