package com.fundcru.objects.pagedefinitions;

public class PaymentAccountScreen {
	public static final String HEADER    					= "#wrapper > div > div > main > div.app-content > div > div.page-heading.hidden-md-down > div > h2";
	
	public static final String ADD_PAYMENT_ACCOUNT_BUTTON   = "#wrapper > div > div > main > div.app-content > div > div:nth-child(2) > a";
	public static final String INFORMATION_MESSAGE   		= "#wrapper > div > div > main > div.app-content > div > div:nth-child(2) > div.alert.alert-info";
}
