package com.fundcru.objects.pagedefinitions;

public class Dashboard {
	public static final String HEADER 					  = "#wrapper > div > div > main > div.app-content > div > div.page-heading.hidden-md-down > div > h2" ;
	public static final String TIME_RANGE_OPTION_BUTTON   = "#wrapper > div > div > main > div.app-content > div > div.card.user-dashboard-datepicker > div > div > div.date-range-button > button";
	public static final String SELECT_DATE_RANGE_LABEL    = "#wrapper > div > div > main > div.app-content > div > div.card.user-dashboard-datepicker > div > div > div.collapse.show > form > div > div > div > label";
	public static final String DATE_RANGE    = "dateRange";

}
