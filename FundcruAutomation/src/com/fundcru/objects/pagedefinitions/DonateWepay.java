package com.fundcru.objects.pagedefinitions;

public class DonateWepay {
	public static final String AMOUNT = "amount";
	public static final String SUBMIT_CARD_INFORMATION_SCREEN = "#wrapper > div > div > main > div.app-content > div > div.campaign-donate-group > div:nth-child(2) > div.card-body > div > div.col > div > div > div > div > button";
	public static final String CARD_NUMBER = "creditCardNumber";
	public static final String CVV = "cvv";
	public static final String EMAIL = "email";
	public static final String EXPIRATION = "expiration";
	public static final String DONATE_NAME = "name";
	public static final String ZIPCODE = "zipCode";
	public static final String CARD_NAME = "div.row:nth-child(3) > div:nth-child(2) > b:nth-child(1)";
	public static final String CARD_NUMBER_LAST_4 = ".card > div:nth-child(2) > div:nth-child(2) > b:nth-child(1)";
	public static final String SHARE_BUTTON = "#wrapper > div > div > main > div.app-content > div > div:nth-child(2) > button";
	public static final String SHARE_BUTTON_XPATH = "//*[@id='wrapper']/div/div/main/div[1]/div/div[2]/button";
	public static final String DONATE_NAME_ON_TESTIMONIAL = "nameInput";
	public static final String DONATE_EMAIL_ON_TESTIMONIAL = "emailInput";
	public static final String DONATE_MESSAGE_ON_TESTIMONIAL = "messageInput";
	
	// Error message 
	public static final String ERROR_ON_CARD_NUMBER = "#wrapper > div > div > main > div.container-fluid > div.campaign-donate-group > div:nth-child(2) > div.card-body > div > div > div > div:nth-child(4) > div";
	public static final String ERROR_ON_CCV = "#wrapper > div > div > main > div.container-fluid > div.campaign-donate-group > div:nth-child(2) > div.card-body > div > div > div > div:nth-child(6) > div";
	public static final String ERROR_ON_EXPIRATION_DATE = "#wrapper > div > div > main > div.container-fluid > div.campaign-donate-group > div:nth-child(2) > div.card-body > div > div > div > div:nth-child(5) > div";
	public static final String ERROR_ON_ZIP_CODE = "#wrapper > div > div > main > div.container-fluid > div.campaign-donate-group > div:nth-child(2) > div.card-body > div > div > div > div:nth-child(7) > div";
	public static final String ERROR_ON_NAME_ON_CARD = "#wrapper > div > div > main > div.container-fluid > div.campaign-donate-group > div:nth-child(2) > div.card-body > div > div > div > div:nth-child(2) > div";
	public static final String ERROR_ON_EMAIL = "#wrapper > div > div > main > div.container-fluid > div.campaign-donate-group > div:nth-child(2) > div.card-body > div > div > div > div:nth-child(3) > div";
	public static final String ERROR_ON_AMOUNT = "#wrapper > div > div > main > div.container-fluid > div.campaign-donate-group > div:nth-child(2) > div.card-body > div > div > div > div:nth-child(1) > div > div.invalid-feedback";
	public static final String ERROR_ON_NAME_ON_CARD_FROM_WEPAY = "#wrapper > div > div > main > div.container-fluid > div.campaign-donate-group > div:nth-child(2) > div.card-body > div > div > div > div.alert.alert-danger.alert-dismissible.fade.show";
	
	// Confirmation after entering information for donation
	public static final String CONFIRMATION_HEADER = "#wrapper > div > div > main > div.container > div:nth-child(2) > form > div:nth-child(4) > div";
	
	public static final String AMOUNT_FOR_CONFIRMATION = "div.row:nth-child(1) > div:nth-child(2)";
	public static final String CARD_ENDING_CONFIRMATION = ".card > div:nth-child(2) > div:nth-child(2) > b:nth-child(1)";
	public static final String NAME_ON_CARD_CONFIRMATION = "div.row:nth-child(3) > div:nth-child(2) > b:nth-child(1)";
	public static final String CONFIRM = "div.form-group > button.btn.btn-primary";
	public static final String CANCEL_DONATION = "#wrapper > div > div > main > div.container > div:nth-child(2) > form > div:nth-child(4) > div";

	//Ethereum Donation
	public static final String ETHEREUM_DONATION_LINK = "#wrapper > div > div > main > div.app-content > div > div.campaign-donate-group > div:nth-child(2) > div.card-body > div > div.col > div.row.justify-content-center > div:nth-child(2) > a";
	public static final String SUBMIT_ETHEREUM_INFORMATION_SCREEN = "#wrapper > div > div > main > div.app-content > div > div.campaign-donate-group > div:nth-child(2) > div.card-body > div > div.col > div:nth-child(2) > div:nth-child(3) > div:nth-child(3) > button";
	public static final String ETHEREUM_CODE = "ether-address-value";
	public static final String ETHEREUM_ACCOUNT = "account";
	public static final String ETHEREUM_SUCCESS_MESSAGE = "#wrapper > div > div > main > div.app-content > div > div.campaign-donate-group > div:nth-child(2) > div.card-body > div > div.col > div:nth-child(2) > div.alert.alert-success";
	public static final String ETHEREUM_WARNING_MESSAGE = "#wrapper > div > div > main > div.app-content > div > div.campaign-donate-group > div:nth-child(2) > div.card-body > div > div.col > div:nth-child(2) > div.alert.alert-danger";

}
