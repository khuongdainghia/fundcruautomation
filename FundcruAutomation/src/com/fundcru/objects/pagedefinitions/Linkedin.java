package com.fundcru.objects.pagedefinitions;

public class Linkedin {
	// Pop up signin with Linkedin
	public static final String LOGO          = "#control_gen_1 > div.inner-wrapper > div";
	public static final String USERNAME      = "session_key-login";
	public static final String PASSWORD      = "session_password-login";
	public static final String SIGNIN        = "#uno-reg-join > div > div > div > div.content-container > div.reg-content-wrapper.single > div > div > p > a";
	public static final String LOGIN_ID 		 = "btn-primary";
	
	public static final String SHARE_OWNER 	 = "share-view-title";
	public static final String SHARE_TEXT 	 = "share-text";
	public static final String SHARE_BUTTON  = "#yui-gen3 > div.submit-container > input";
	public static final String SHARE_ALERT_MESSAGE  = "share-alert-message";
	
	public static final String WINDOW_CLOSURE_BUTTON  = "#footer > p:nth-child(1) > a";
}
