package com.fundcru.objects.pagedefinitions;

public class AccountSettings {
	public static final String APP_CONTENT = "#wrapper > div > div > main > div.app-content >";
	public static final String CONTAINER = APP_CONTENT +" div > div:nth-child(2) >";
	public static final String ACCOUNT_SETTINGS_HEADER = "#wrapper > div > div > main > div.app-content > div > div > div > h2";
	public static final String PERSONAL_INF_LINK = "#wrapper > div > div > main > div.app-content > div > ul > a:nth-child(1)";
	public static final String SIGNIN_SECURITY_LINK = APP_CONTENT +" div > ul > a:nth-child(2)";
	
	// Sign in and security screen
	public static final String SIGNIN_SECURITY_HEADER = APP_CONTENT +" div > div.page-heading.hidden-md-down > div > h2";
	public static final String CHANGE_PASSWORK_LINK = "#no-more-tables > table > tbody > tr:nth-child(1) > td > a";
	public static final String CONNECT_TO_FACEBOOK_LINK = "#no-more-tables > table > tbody > tr:nth-child(2) > td > span > a";
	public static final String SUBMIT_PROFILE_FOR_VERIFICATION_LINK = "#no-more-tables > table > tbody > tr:nth-child(3) > td > a";
	public static final String BACK_BUTTON = "#wrapper > div > div > main > div.container > div.d-block.bottom-back-button > button";

	
	// Change password screen
	public static final String HEADER = APP_CONTENT +" div > div.page-heading.hidden-md-down > div > h2";
	
	public static final String  INCORRECT_PASSWORD_ERROR = "#wrapper .app .app-body .main .app-content .change-password-wrapper .at-form .at-error.alert-danger p";
	public static final String  CHANGE_PW_SUCCESS_MESSAGE = "#wrapper > div > div > main > div.container-fluid > div.change-password-wrapper > span > div > div.at-result.alert.alert-success";
	
	public static final String CURRENT_PW_LABEL = "#at-pwd-form > fieldset > div:nth-child(1) > label";
	public static final String NEW_PW_FIRST_FIELD_LABLE = "#at-pwd-form > fieldset > div:nth-child(2) > label";
	public static final String NEW_PW_SECOND_FIELD_LABLE = "#at-pwd-form > fieldset > div:nth-child(3) > label";

	public static final String CURRENT_PW_FIELD_ID = "at-field-current_password";
	public static final String NEW_PW_FIRST_FIELD_ID = "at-field-password";
	public static final String NEW_PW_SECOND_FIELD_ID = "at-field-password_again";
	
	public static final String UPDATE_PW_BUTTON = "at-btn-submit";
	public static final String CANCEL_BUTTON = "at-btn-cancel";
	
	public static final String CURRENT_PW_VALIDATION_ERROR = "#at-pwd-form > fieldset > div:nth-child(1) > span";
	public static final String NEW_PW_FIRST_FIELD_VALIDATION_ERROR = "#at-pwd-form > fieldset > div:nth-child(2) > span";
	public static final String NEW_PW_SECOND_FIELD_VALIDATION_ERROR = "#at-pwd-form > fieldset > div:nth-child(3) > span";
	
	// Edit user profile screen
	
	public static final String NO_PROFILE_ERROR =  APP_CONTENT +" div";
	public static final String SUBMIT_NEW_PROFILE_BUTTON =  APP_CONTENT +" div > a";
	public static final String HEADER_EUP = "div > div.page-heading.hidden-md-down > div > h2";
	public static final String FORM = CONTAINER +" form >";
	public static final String BANK_NAME_LABEL_EUP = FORM +" div:nth-child(1) > label";
	public static final String BANK_ROUTING_NO_LABLE_EUP = FORM +" div:nth-child(2) > label";
	public static final String BANK_ACCOUNT_NO_LABLE_EUP = FORM +" div:nth-child(3) > label";
	public static final String ATTACHED_DOCUMENT_LABEL_EUP = FORM +" div:nth-child(4) > label";
	public static final String BANK_NAME_TEXT_FIELD_EUP = "bankName";
	public static final String BANK_ACCOUNT_NO_TEXT_FIELD_EUP = "bankAccountId";
	public static final String UPLOAD_DOCUMENT_BUTTON_EUP = "yourDocument";
	public static final String SUBMIT_BUTTON_EUP = CONTAINER +" button";
	public static final String CANCEL_BUTTON_EUP = CONTAINER +" a";
	
}
