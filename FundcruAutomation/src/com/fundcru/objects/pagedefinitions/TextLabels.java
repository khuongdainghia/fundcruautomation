package com.fundcru.objects.pagedefinitions;
public class TextLabels {
	// Home page
	public static final String  HOME_HEADER = "THE FUNDCRU ECOSYSTEM";
	
	// Sign up page
	public static final String  HEADER_SIGNUP = "Create an account";
	public static final String  FIRSTNAME_FIELD_LABLE_SIGNUP = "First name";
	public static final String  LASTNAME_FIELD_LABLE_SIGNUP = "Last name";
	public static final String  EMAIL_FIELD_LABLE_SIGNUP = "Email";
	public static final String  FIRST_PASSWORD_FIELD_LABLE_SIGNUP = "Password";
	public static final String  SECOND_PASSWORD_FIELD_LABLE_SIGNUP = "Password (again)";
	public static final String  OR_TEXT_LABLE_SIGNUP = "OR";

	// Sign In page
	public static final String  HEADER_SIGNIN = "Sign in";
	public static final String  HEADER_RESET_PASSWORD_SCREEN = "Reset your password";
	public static final String  EMAIL_FIELD_LABLE_SIGNIN = "Email";
	public static final String  PASSWORD_FIELD_LABLE_SIGNIN = "Password";
	public static final String  OR_TEXT_LABLE_SIGNIN = "OR";
	public static final String  DONT_HAVE_ACCOUNT = "Don't have an account? ";
	public static final String  VERIFICATION_EMAIL_LOST = "Verification email lost? Send again";
	
	// Reset Password page
	public static final String  HEADER_RESET_PW = "Reset your password";
	public static final String  FIRST_PASSWORD_RESET_PW = "New Password";
	public static final String  SECOND_PASSWORD_RESET_PW = "New Password (again)";
	
	// Change Password page
	public static final String  HEADER_CHANGE_PW = "Change password";
	public static final String  CURRENT_PASSWORD_CHANGE_PW = "Current Password";
	public static final String  FIRST_PASSWORD_CHANGE_PW = "Password";
	public static final String  SECOND_PASSWORD_CHANGE_PW = "Password (again)";
	
	// Edit user profile page
	public static final String  HEADER_EUP = "Edit user profile";
	public static final String  BANK_NAME_LABEL_EUP = "Bank name";
	public static final String  BANK_ROUTING_NO_LABLE_EUP = "Bank routing number";
	public static final String  BANK_ACCOUNT_NO_LABLE_EUP = "Bank account number";
	public static final String  ATTACHED_DOCUMENT_LABEL_EUP = "Attach documents (passport, driver license, etc.)";
	
	// Register With Facebook Screen
	public static final String  FACEBOOK_CONTENT = "Log in to use your Facebook account with FundCru.";	
	
	// My Favourite Offer Screen
	public static final String RECOMMENDED_OFFER= "Featured offers";
	
	public static final String RECOMMENDED_FUNDRAISER= "Featured fundraisers";
	
	// Browse Fundraiser screen
	public static final String POPULAR_FUNDRAISER_LABEL= "Popular fundraisers";
	
	// Browse Offers screen
	public static final String POPULAR_OFFERS_LABEL= "Popular offers";
	public static final String LOCAL_OFFERS_TEXT= "Local offers";
	
	// Campaign creation screen
	public static final String CAMPAIGN_CREATE_FORM_TITLE= "Basic information";
	
	// Personal Infomation screen
	public static final String EMAIL_TITLE= "Email";
	public static final String PROFILE_TITLE = "Profile Picture";
	public static final String FULLNAME_TITLE = "Fullname";
	public static final String ABOUT_TITLE = "About me";
	
	// Fullname modifying modal dialog in personal infomation screen
	public static final String FULLNAME_MODIFY_DIALOG_HEADER= "Change fullname";
	public static final String FULLNAME_MODIFY_DIALOG_FIRSTNAME_LABEL = "Firstname";
	public static final String FULLNAME_MODIFY_DIALOG_LASTNAME_LABEL = "Lastname";
	
	// About modifying modal dialog in personal infomation screen
	public static final String ABOUT_MODIFY_DIALOG_HEADER= "Change about me";
	public static final String ABOUT_MODIFY_DIALOG_LABEL = "About";
	
	// Search screen
	public static final String SEACRH_HEADER = "Search results for query:";
	
	// Account Settings screen
	public static final String ACCOUNT_SETTING_HEADER = "Account settings";
	
	// Payment Account Settings screen
	public static final String PAYMENT_ACCOUNT_HEADER = "Payment accounts";
	public static final String PAYMENT_ACCOUNT_INFOR_MESSAGE = "Accounts with status \"ACTIVE\" or \"ACTION REQUIRED\" are able to receive funds. You can create fundraisers with those accounts.";

	// My fundraisers screen
	public static final String MY_FUNDRAISERS_SCREEN_HEADER = "My fundraisers";
	
	// My favorite fundraisers screen
	public static final String MY_FAVORITE_FUNDRAISERS_SCREEN_INFOMATION_MESSAGE = "You have no favorite fundraiser!";
	
	// My favorite offers screen
	public static final String MY_FAVORITE_OFFERS_SCREEN_INFOMATION_MESSAGE = "You have no favorite offer!";
	
	// My donations screen
	public static final String MY_DONATIONS_SCREEN_HEADER = "My donations";
	
	// My Oders screen
	public static final String MY_ODERS_SCREEN_HEADER = "My orders";
	
	// My withdrawals screen
	public static final String MY_WITHDRAWALS_SCREEN_HEADER = "My withdrawals";
}
