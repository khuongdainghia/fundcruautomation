package com.fundcru.objects.pagedefinitions;

public class MessageCenter {
	public static final String HEADER 					  = "#wrapper > div > div > main > div.app-content > div > div.page-heading.hidden-md-down > div > h2" ;
	public static final String REPLY_VIA_EMAIL_BUTTON   = "btn btn-primary";
	
}
