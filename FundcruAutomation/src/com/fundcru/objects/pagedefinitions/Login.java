package com.fundcru.objects.pagedefinitions;

public class Login {
	public static final String SIGNIN_LINK = "signin-button-lg";
	public static final String SIGNIN_HEADER_LABEL = "#wrapper > div > div > main > div.app-content > div > div > div > div > div.at-title.text-center > h3";												
	
	// Text lable
	public static final String EMAIL_TEXT_FIELD_LABLE = "#at-pwd-form > fieldset > div:nth-child(1) > label";
	public static final String PASSWORD_TEXT_FIELD_LABLE = "#at-pwd-form > fieldset > div:nth-child(2) > label";
	
	// Text field
	public static final String EMAIL_TEXT_FIELD_ID = "at-field-email";
	public static final String PASSWORD_TEXT_FIELD_ID = "at-field-password";
	
	// Button
	public static final String SIGNIN_BUTTON_ID = "at-btn-submit";
	public static final String SIGNIN_WITH_FACEBOOK = "at-facebook";
	public static final String SIGNIN_WITH_GOOGLE = "at-google";
	
	// Hyperlink
	public static final String FORGOT_PASSWORD = "at-forgotPwd";
	public static final String REGISTER = "at-signUp";
	public static final String SEND_AGAIN = "at-resend-verification-email";
	public static final String CANCEL = "at-btn-cancel";
	
	public static final String DONT_HAVE_ACCOUNT = "#wrapper > div > div > main > div.app-content > div > div > div > div > div.at-signup-link > p > p.sign-up-pre-text";
	public static final String OR_LABLE = "#wrapper > div > div > main > div.app-content > div > div > div > div > div.at-sep > strong";
	public static final String VERIFICATION_EMAIL_LOST = "#wrapper > div > div > main > div.app-content > div > div > div > div > div.at-resend-verification-email-link.at-wrap > p";
	

	public static final String ENVELOPE_ICON = "#mainNav > div > div.lg-nav-content.hidden-md-down > ul.navbar-nav.ml-auto > li:nth-child(3) > a > div > i";
	public static final String ENVELOPE_LINK = "#mainNav > div > div.lg-nav-content.hidden-md-down > ul.navbar-nav.ml-auto > li:nth-child(3) > a";
	public static final String ENVELOPE_ICON_CLASS="message-icon";
	public static final String ENVELOPE_ICON_NO="#mainNav > div > div.lg-nav-conten-t.hidden-md-down > ul.navbar-nav.ml-auto > li:nth-child(3) > a > div";
	
	public static final String ERROR_MESSAGE = "#wrapper > div > div > main > div.app-content > div > div > div > div > div.at-error.alert.alert-danger > p";
	
	public static final String EMAIL_VALIDATION_MESSAGE = "#at-pwd-form > fieldset > div > span";
	public static final String RESET_PASSWORD_EMAIL_SUCCESS_MESSAGE = "#wrapper .app .app-body .main .app-content .at-form.loginscreen div.at-result.alert.alert-success";

	// Forgot password screen
	// Text lable
	public static final String EMAIL_TEXT_LABLE_RESET_PASSWORD = "#at-pwd-form > fieldset > div > label";
	
	// Google
	public static final String gmailProfile = "#gbw > div > div > div.gb_uc.gb_tg.gb_R > div.gb_eb.gb_Zc.gb_tg.gb_R > div.gb_Fc.gb_gb.gb_tg.gb_R > a > span";
	public static final String gmailSignInButton_id = "gb_70";
	public static final String gmailId = "identifierId";
	public static final String gmailNextButton = "#identifierNext > content > span";
	public static final String gmailPasswordNextButton = "#passwordNext > content > span";
	public static final String gmailPassword = "#password > div.aCsJod.oJeWuf > div > div.Xb9hP > input";
	public static final String gmailLogedIcon1 = "#gb > div.gb_uf.gb_tg > div.gb_hb.gb_tg.gb_R.gb_sg.gb_T > div.gb_uc.gb_tg.gb_R > div.gb_eb.gb_Zc.gb_tg.gb_R > div.gb_Fc.gb_gb.gb_tg.gb_R > a > span";
	public static final String gmailLogedIcon2 = "#gbw > div:nth-child(3) > div > div.gb_he.gb_R.gb_yg.gb_pg > div:nth-child(2) > a";
	public static final String gmailOptionButton = "#gbwa > div.gb_Fc > a";
	public static final String gmailButton = "#gbw > div > div > div.gb_ce.gb_R.gb_tg.gb_kg > div:nth-child(2) > a";
	public static final String gmailPrimaryTabId = ":2i";

	public static final String gmailInboxButton = "//*[@id=':46']/div/div[2]/span/a";
	public static final String gmailTickBox_Id = ":92";
	public static final String gmailRemoveEmail = "//*[@id=':5']/div[2]/div[1]/div/div[2]/div[3]/div/div";
	
	// Reset Password Screen
	public static final String HEADER_RESET_PW = "#wrapper > div > div > main > div.app-content > div > div > div > div > div.at-title.text-center > h3";
	
	public static final String FIRSTPASSWORD_TEXT_FIELD_LABLE_RESET_PW = "#at-pwd-form > fieldset > div:nth-child(1) > label";
	public static final String SECONDPASSWORD_TEXT_FIELD_LABLE_RESET_PW = "#at-pwd-form > fieldset > div:nth-child(2) > label";
	public static final String FIRSTPASSWORD_TEXT_FIELD_RESET_PW = "at-field-password";
	public static final String SECONDPASSWORD_TEXT_FIELD_RESET_PW = "at-field-password_again";
	public static final String SETPASSWORD_BUTTON_RESET_PW = "at-btn-submit";
	public static final String CANCEL_BUTTON_RESET_PW = "at-btn-cancel";
	public static final String FIRSTPASSWORD_ERROR_RESET_PW = "#at-pwd-form > fieldset > div:nth-child(1) > span";
	public static final String SECONDPASSWORD_ERROR_RESET_PW = "#at-pwd-form > fieldset > div:nth-child(2) > span";
	
	public static final String TOKEN_EXPIRED_ERROR_RESET_PW = "#wrapper > div > div > main > div:nth-child(1) > div > div > div > div.at-error.alert.alert-danger > p";
}
