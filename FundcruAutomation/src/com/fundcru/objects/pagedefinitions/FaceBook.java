package com.fundcru.objects.pagedefinitions;

public class FaceBook {
	
	public static final String POST_TO_FACEBOOK = "u_0_1v";
	// Register With Facebook
	public static final String FACEBOOK_HOME_LINK_ID = "homelink";
	public static final String FACEBOOK_CONTENT_ID = "content";
	public static final String FACEBOOK_USERNAME_FIELD_ID= "email";
	public static final String FACEBOOK_PASSWORD_FIELD_ID = "pass";
	public static final String FACEBOOK_LOGIN_BUTTON_ID = "u_0_0";
	
	public static final String FACEBOOK_CONTINUE_AS_NAME = "__CONFIRM__";
	public static final String FACEBOOK_CONTINUE_AS_CLASS = "_42ft _4jy0 layerConfirm _1fm0 _51_n autofocus _4jy3 _4jy1 selected _51sy";
	public static final String FACEBOOK_CONTINUE_AS = "#u_0_4 > div._58xh._1flz > div._1fl- > div._2mgi._4k6n > button";

}
