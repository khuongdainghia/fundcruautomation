package com.fundcru.objects.pagedefinitions;

public class Admin {
	// Text field
	public static final String EMAIL_TEXT_FIELD_ID = "at-field-email";
	public static final String PASSWORD_TEXT_FIELD_ID = "at-field-password";
	public static final String SIGN_IN_BUTTON = "at-btn-submit";
	public static final String BASIC_INFORMATION = "Basic information";
	public static final String FIRST_NAME = "firstname";
	public static final String LAST_NAME = "lastname";
	public static final String GIFT_CARDS = "Gift Cards";
	public static final String SHOW_ALL_GIFTCARDS = "btn-show-all-giftcards";
	public static final String SHOW_NEEDS_DELIVERY_GIFTCARDS = "btn-need-delivery-giftcards";
	public static final String SHOW_DELIVERIED_GIFTCARDS = "btn-delivered-giftcards";
	public static final String TABLE_BODY_XPATH = "//*[@id='no-more-tables']/tbody";
	public static final String TABLE_ACTION_ID = "li-giftcard-actions";
	public static final String UPDATE_CODE_ACTION = "Update codes";
	public static final String SUCCESSFUL_MESSAGE = "#adminAddGiftCardCodes > div:nth-child(1) > div";
	public static final String SAVE_BUTTON = "#adminAddGiftCardCodes > div.form-group.create-campaign-buttons > button.btn.btn-primary.save-codes";
	public static final String GOBACK_BUTTON = "#adminAddGiftCardCodes > div.form-group.create-campaign-buttons > button.btn.btn-secondary.back-to-giftcards";
	public static final String GIFT_CODE = "#adminAddGiftCardCodes > div:nth-child(2) > div > ul > li.list-group-item.autoform-array-item > div > div.autoform-array-item-body > div > input";
	public static final String ADMIN_LOGGED_LINK = "#wrapper > div > header > ul.nav.navbar-nav.ml-auto > li > div > a";
}
