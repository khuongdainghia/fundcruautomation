package com.fundcru.objects.pagedefinitions;

public class Home {
	public static final String USER_ICON = "#mainNav > div > div.lg-nav-content.hidden-md-down > ul.navbar-nav.ml-auto > li.nav-item.dropdown > button > span";
	public static final String USER_LINK_CLASS = "dropdown-toggle";
	
	public static final String ACCOUNT_SETTINGS_LINK = "account-settings-lg";
	public static final String PAYMENT_ACCOUNTS = "payment-account-lg";
	public static final String MY_CAMPAIGN_LINK = "my-fundraisers-lg";
	public static final String MY_DONATIONS_LINK = "my-donations-lg";
	public static final String MY_ORDERS_LINK = "my-orders-lg";
	public static final String MY_WITHDRAWALS_LINK = "my-withdrawals-lg";
	public static final String MY_FAVARITE_FUNDRAISERS_LINK = "my-favorite-fundraisers-lg";
	public static final String MY_FAVORITE_OFFERS_LINK = "my-favorite-offers-lg";
	public static final String NO_MY_FAVORITE_MESSAGE = "#wrapper > div > div > main > div.app-content > div > div.alert.alert-info.text-center";
	public static final String SIGN_OUT = "signout-button-lg";
	
	public static final String VCODE_TEXTFIELD_ID = "vCode";
	public static final String VERIFY_BUTTON_ID = "verifyButton";
	public static final String SEND_AGAIN = "at-resend-verification-email";
	
	public static final String FUNDCRU_LOGO = "#mainNav > div > a > h1";
	public static final String CREATE_A_FUNDRAISER_LINK_TEXT = "Create a fundraiser";
	public static final String CREATE_A_FUNDRAISER = "btn-create-fundraiser-lg";
	public static final String EXPLORE_FUNDRAISERS = "btn-campaign-list-lg" ;
	public static final String BROWSER_OFFERS = "#btn-offer-list-lg > a" ;
	public static final String BROWSE_FUNDRAISER_LINK   = "btn-campaign-list-lg";
	
	public static final String SEARCH_TEXT_BOX = "typeahead-search-box" ;
	public static final String SEARCH_TEXT_BOX_FULL = "#mainNav > div > div.lg-nav-content.hidden-md-down > ul.navbar-nav.ml-auto > form > span > input[type='hidden']" ;
	public static final String SEARCH_BUTTON = "#mainNav > div > div.lg-nav-content.hidden-md-down > ul.navbar-nav.ml-auto > form > span > button" ;

	public static final String SIGN_UP_LINK_ID = "signup-button-lg";
	
	public static final String DASHBOARD  = "#mainNav > div > div.lg-nav-content.hidden-md-down > ul.navbar-nav.ml-auto > li:nth-child(2) > a" ;
	public static final String EMAIL_ICON = "#mainNav > div > div.lg-nav-content.hidden-md-down > ul.navbar-nav.ml-auto > li:nth-child(3) > a > div > i" ;
	public static final String MY_WAY = "#mainNav > ul:nth-child(3) > li:nth-child(4) > a.nav-link.page-scroll" ;
	
	public static final String POPULAR_CAMPAIGNS = "#campaign-home-contents > div:nth-child(1) > div:nth-child(1) > a:nth-child(1) > h3";
	public static final String HOME_HEADER = "#features > div > div.section-title-area > h2";
	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String EMAIL = "email";
	public static final String ADD_PAYMENT_ACOUNT_LINK_TEXT = "Add payment account";
	public static final String CREATE_PAYMENT_ACOUNT_BUTTON_TEXT = "Create a payment account";
	public static final String CREATE_PAYMENT_ACCOUNT_BUTTON = "btn-add-payment-account";
	public static final String ADD_PAYMENT_ACCOUNT_BUTTON = "#wrapper > div > div > main > div.app-content > div > div:nth-child(2) > a";
	public static final String INVALID_EMAIL_MESSAGE_CLASS = "invalid-feedback";
	public static final String RETURN_PAYMENT_ACCOUNT_BUTTON_TEXT = "Return to payment accounts";
	public static final String ERROR_EXIST_PAYMENT_ACCOUNT = "/html/body/div[3]/div/div/main/div[1]/div/div[2]/div/div[1]";
	public static final String RETURN_TO_PAYMENT_ACCOUNT_LINK_TEXT = "Return to payment accounts";
	public static final String RETURN_TO_PAYMENT_ACCOUNT_BUTTON = ".main > .app-content > .container > div > div.wepay-account-form > #li-return-to-payment-accounts";
	public static final String CONFIRM_ADD_PAYMENT_ACCOUNT_LINK_TEXT = "Confirm";
	public static final String CONFIRM_ADD_PAYMENT_ACCOUNT_BUTTON = "#wrapper > div > div > main > div.app-content > div > div:nth-child(2) > div > button";
	public static final String DELETE_DIALOGUE = "//*[@id='no-more-tables']/table/tbody/tr/td[4]/div/div/div/div[3]/";
	public static final String CANCEL_BUTTON_DIALOGUE = DELETE_DIALOGUE+"button[2]";
	public static final String DELETE_BUTTON_DIALOGUE = DELETE_DIALOGUE+"button[1]";
	public static final String TABLE_BODY_WITH_DIV_CSS = "#no-more-tables > table > tbody";
	public static final String TABLE_BODY_WITH_DIV_XPATH = "//*[@id='no-more-tables']/table/tbody";
	public static final String TABLE_HEAD_WITH_DIV_XPATH = "//*[@id='no-more-tables']/table/thead";
	public static final String TABLE_BODY_CSS = "#no-more-tables > tbody";
	public static final String TABLE_BODY_XPATH = "//*[@id='no-more-tables']/tbody";
	public static final String TABLE_HEAD_XPATH = "//*[@id='no-more-tables']/thead";
	public static final String DELETE_PAYMENT_ACCOUNT_ROW_XPATH = TABLE_BODY_WITH_DIV_XPATH + "/tr[%s]/td[4]/button[1]";
	public static final String DELETE_PAYMENT_ACCOUNT_ROW1_XPATH = TABLE_BODY_WITH_DIV_XPATH + "/tr/td[4]/button[1]";
	public static final String DELETE_PAYMENT_ACCOUNT_ROW2_XPATH = TABLE_BODY_WITH_DIV_XPATH + "/tr[2]/td[4]/button[2]";
	public static final String NAME_PAYMENT_ACCOUNT_ROW2_XPATH = TABLE_BODY_WITH_DIV_XPATH + "/tr[2]/td[2]";
	public static final String EMAIL_PAYMENT_ACCOUNT_ROW2_XPATH = TABLE_BODY_WITH_DIV_XPATH + "/tr[2]/td";
	public static final String NAME_PAYMENT_ACCOUNT_ROW1_XPATH = TABLE_BODY_WITH_DIV_XPATH + "/tr/td[2]";
	public static final String EMAIL_PAYMENT_ACCOUNT_ROW1 = TABLE_BODY_WITH_DIV_XPATH+"/tr/td[1]";
	public static final String CHECKBOX_XPATH = "//input[@type='checkbox']";
	public static final String FILTER_BTN_CSS = "";
	public static final String VERTICAL_SCROLLBAR_CLASS = "";
	public static final String HORIZONTAL_SCROLLBAR_CLASS = "";
	public static final String SEARCH_CAMPAGIN_LIST_CLASSNAME = "campaign-standard-preview";
	public static final String SEARCH_CAMPAGIN_ITEM_TITLE = " > div > h4 > a";
	
}
