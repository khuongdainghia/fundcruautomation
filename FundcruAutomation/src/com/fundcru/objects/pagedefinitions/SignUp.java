package com.fundcru.objects.pagedefinitions;

public class SignUp {
	// Label
	public static final String HEADER_LABEL = "#wrapper > div > div > main > div.app-content > div > div > div > div > div.at-title.text-center > h3";	
	public static final String FIRST_NAME_LABEL = "#at-pwd-form > fieldset > div:nth-child(1) > label";
	public static final String LAST_NAME_LABEL = "#at-pwd-form > fieldset > div:nth-child(2) > label";
	public static final String EMAIL_LABEL = "#at-pwd-form > fieldset > div:nth-child(3) > label";
	public static final String PASSWORD1_LABEL = "#at-pwd-form > fieldset > div:nth-child(4) > label";
	public static final String PASSWORD2_LABEL = "#at-pwd-form > fieldset > div:nth-child(5) > label";
	public static final String OR_LABEL = "#wrapper > div > div > main > div.app-content > div > div > div > div > div.at-sep > strong";

	// Field
	public static final String FIRST_NAME_FIELD = "at-field-firstname";
	public static final String LAST_NAME_FIELD = "at-field-lastname";
	public static final String EMAIL_FIELD = "at-field-email";
	public static final String PASSWORD_FIELD = "at-field-password";
	public static final String PASSWORD_AGAIN_FIELD = "at-field-password_again";

	// Button
	public static final String REGISTER_BUTTON = "at-btn-submit";
	public static final String CANCEL_BUTTON = "at-btn-cancel";

	public static final String REGISTER_WITH_FACEBOOK_BUTTON = "at-facebook";
	public static final String REGISTER_WITH_GOOGLE_BUTTON = "at-google";
	
	// Message 
	public static final String FIRSTNAME_ERROR = "#at-pwd-form > fieldset > div:nth-child(1) > span";
	public static final String LASTNAME_ERROR = "#at-pwd-form > fieldset > div:nth-child(2) > span";
	public static final String EMAIL_ERROR = "#at-pwd-form > fieldset > div:nth-child(3) > span";
	public static final String PASSWORD_ERROR = "#at-pwd-form > fieldset > div:nth-child(4) > span";
	public static final String PASSWORD_AGAIN_ERROR = "#at-pwd-form > fieldset > div:nth-child(5) > span";
	public static final String SIGNUP_SUCCESS_MESSAGE = "#wrapper > div > div > main > div.app-content > div > div > div > div > div.at-result.alert.alert-success";
	public static final String HEADER_MESSAGE = "#wrapper > div > div > main > div.app-content > div > div > div > div > div.at-error.alert.alert-danger > p";	
	
	


}
