package com.fundcru.objects.pagedefinitions;

public class MyFundraiserScreen {
	public static final String HEADER    					= "#wrapper > div > div > main > div.app-content > div > div.page-heading.hidden-md-down > div > h2";
	
	public static final String CREATE_NEW_FUNDRAISER_BUTTON   = "#wrapper > div > div > main > div.app-content > div > div.form-group > a";
}
