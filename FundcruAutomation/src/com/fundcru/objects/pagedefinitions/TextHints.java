package com.fundcru.objects.pagedefinitions;
public class TextHints {
	
	// Sign up and sign in page
	public static final String  FIRSTNAME = "Firstname";
	public static final String  LASTNAME = "Lastname";
	public static final String  EMAIL = "Email";
	public static final String  FIRST_PASSWORD = "Password";
	public static final String  SECOND_PASSWORD = "Password (again)";

	// Reset Password page
	public static final String  FIRST_PASSWORD_RESET_PW = "New Password";
	public static final String  SECOND_PASSWORD_RESET_PW = "New Password (again)";

	// Change Password page
	public static final String  CURRENT_PASSWORD_CHANGE_PW = "Current Password";
	public static final String  FIRST_PASSWORD_CHANGE_PW = "Password";
	public static final String  SECOND_PASSWORD_CHANGE_PW = "Password (again)";
	
	// Edit user profile page
	public static final String  BANK_NAME_EUP = "Enter your bank name";
	public static final String  BANK_ACCOUNT_NO_EUP = "Enter your bank account number";
}
