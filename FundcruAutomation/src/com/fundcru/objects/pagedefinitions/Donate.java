package com.fundcru.objects.pagedefinitions;

public class Donate {
	public static final String CREDIT_CARD_DONATE = "#wrapper > div > div > main > div.app-content > div > div.campaign-donate-group > div:nth-child(2) > div > div.card-block > div > div:nth-child(1) > a";
	public static final String PAYPAL_DONATE = "#wrapper > div > div > main > div.container-fluid > div.campaign-donate-group > div:nth-child(1) > div > div.card-block > div.justify-content-center.row > div:nth-child(2) > a > img";
	public static final String AMOUNT = "amount";
	public static final String PAYMENT_TYPE = "paymentType";
	public static final String SUBMIT = "#wrapper > div > div > main > div.container-fluid > div.campaign-donate-group > div:nth-child(2) > div > div.card-block > div.card > div.card-body > div > form > div:nth-child(2) > button.btn.btn-primary";
	public static final String CANCEL = "#content > div > div:nth-child(2) > form > div:nth-child(3) > button.btn.btn-secondary";
	public static final String CARD_NUMBER = "creditCardNumber";
	public static final String CCV = "ccv";
	public static final String CVV = "cvv";
	public static final String FULL_NAME = "fullname";
	public static final String EMAIL = "email";
	public static final String EXPIRATION = "expiration";
	public static final String ZIPCODE = "zipCode";
	public static final String DONATE_NAME = "name";
	public static final String DONATE_TESTIMONIAL = "testimonial";
	public static final String DONATE_AMOUNT_CONFIRM = "div.row:nth-child(1) > div:nth-child(2)";
	public static final String CARD_NAME = "div.row:nth-child(3) > div:nth-child(2) > b:nth-child(1)";
	public static final String CARD_NUMBER_LAST_4 = ".card > div:nth-child(2) > div:nth-child(2) > b:nth-child(1)";
	public static final String DONATE_NAME_ON_CARD = "fullname";
	public static final String SHARE_BUTTON = "#wrapper > div > div > main > div.container > div:nth-child(2) > form > div:nth-child(3) > button.btn.btn-primary";
	public static final String SUBMIT_CARD_INFORMATION_SCREEN = "#wrapper > div > div > main > div.app-content > div > div.campaign-donate-group > div:nth-child(2) > div.card-body > div > div.col > div > div > div > div > button";
	public static final String SUBMIT_CARD_INFORMATION_SCREEN_OFFER = "#wrapper > div > div > main > div.app-content > div > div:nth-child(3) > form > div:nth-child(5) > button.btn.btn-primary";
	
	// Error message 
	public static final String ERROR_ON_CARD_NUMBER = "#wrapper > div > div > main > div.app-content > div > div.campaign-donate-group > div:nth-child(2) > div.card-body > div > div.col > div > div > div > div > div:nth-child(4) > div";
	public static final String ERROR_ON_CCV = "#wrapper > div > div > main > div.app-content > div > div.campaign-donate-group > div:nth-child(2) > div.card-body > div > div.col > div > div > div > div > div:nth-child(6) > div";
	public static final String ERROR_ON_EXPIRATION_DATE = "#wrapper > div > div > main > div.app-content > div > div.campaign-donate-group > div:nth-child(2) > div.card-body > div > div.col > div > div > div > div > div:nth-child(5) > div";
	public static final String ERROR_ON_NAME_ON_CARD = "#wrapper > div > div > main > div.app-content > div > div.campaign-donate-group > div:nth-child(2) > div.card-body > div > div.col > div > div > div > div > div:nth-child(2) > div";
	public static final String ERROR_ON_ZIP_CODE = "#wrapper > div > div > main > div.app-content > div > div.campaign-donate-group > div:nth-child(2) > div.card-body > div > div.col > div > div > div > div > div:nth-child(7) > div";
	public static final String ERROR_ON_EMAIL = "#wrapper > div > div > main > div.app-content > div > div.campaign-donate-group > div:nth-child(2) > div.card-body > div > div.col > div > div > div > div > div:nth-child(3) > small";
	public static final String ERROR_ON_AMOUNT = "#wrapper > div > div > main > div.app-content > div > div.campaign-donate-group > div:nth-child(2) > div.card-body > div > div.col > div > div > div > div > div:nth-child(1) > div.invalid-feedback.d-flex";
	public static final String ERROR_ON_NAME_ON_CARD_FROM_WEPAY = "#wrapper > div > div > main > div.app-content > div > div.campaign-donate-group > div:nth-child(2) > div.card-body > div > div.col > div > div > div > div > div.alert.alert-danger.alert-dismissible.show";
	

	// Confirmation after entering information for donation
	public static final String CONFIRMATION_HEADER = "#wrapper > div > div > main > div.container > div:nth-child(2) > form > div:nth-child(4) > div";
	
	public static final String AMOUNT_FOR_CONFIRMATION = "div.row:nth-child(1) > div:nth-child(2)";
	public static final String CARD_ENDING_CONFIRMATION = ".card > div:nth-child(2) > div:nth-child(2) > b:nth-child(1)";
	public static final String NAME_ON_CARD_CONFIRMATION = "div.row:nth-child(3) > div:nth-child(2) > b:nth-child(1)";
	public static final String CONFIRM = "div.form-group > button.btn.btn-primary";
	public static final String CANCEL_DONATION = "#wrapper > div > div > main > div.container > div:nth-child(2) > form > div:nth-child(4) > div";
	
}
