package com.fundcru.objects.pagedefinitions;

public class Offer {
	// My Favourite Offer Screen
	public static final String ALERT_MESSAGE_NO_OFFER_EXIST= "#wrapper > div > div > main > div.app-content > div > div.alert.alert-info.text-center";
	public static final String TABELE_LIST_OF_FAVOURITE_OFFERS_ID = "no-more-tables";
	public static final String RECOMMENDED_OFFER_LIST = "#wrapper > div > div > main > div.app-content > div > div.preview-list-block.mt-5";
	public static final String RECOMMENDED_FUNDRAISER_LIST = "#wrapper > div > div > main > div.app-content > div > div.preview-list-block.mt-5 > a > h4";
	public static final String RECOMMENDED_OFFER = "offer-slider-Wgaq98tMgNnESG5WJ";
	
	public static final String FOOD_AND_DRINK_BUTTON = "#offer-home-contents > div:nth-child(1) > div > div:nth-child(1) > a";
	public static final String GOODS_AND_RETAIL_BUTTON = "#offer-home-contents > div:nth-child(1) > div > div:nth-child(2) > a";
	public static final String POPULAR_OFFERS_TEXT = "#offer-home-contents > div:nth-child(2) > h3";
	public static final String LOCAL_OFFERS_TEXT = "#offer-home-contents > div:nth-child(3) > h3";
	
	public static final String OFFER_TITLE = "#wrapper > div > div > main > div.app-content > div > div > div.col-lg-5.col-12 > div.card.infor-group.hidden-md-down > div > h3";
	public static final String BUY_BUTTON = "btn-buy-offer";
	public static final String ADD_TO_FAVORITE_BUTTON = "btn-bookmark-offer";
	public static final String REMOVE_FROM_FAVORITE_BUTTON = "btn-unbookmark-offer";
	public static final String OFFER_ITEM_1 = "#no-more-tables > tbody > tr > td:nth-child(2) > a";
		
	public static final String OFFER_PRICE = "#wrapper > div > div > main > div.app-content > div > div > div.col-lg-5.col-12 > div.card.infor-group.hidden-md-down > div > div.multiple-offer-items > div:nth-child(2) > div.price-tags > span.price.text-success";
	public static final String OFFER_PRICE2 = "#wrapper > div > div > main > div.app-content > div > div > div.col-lg-5.col-12 > div.card.infor-group.hidden-md-down > div > div.multiple-offer-items > div:nth-child(3) > div.price-tags > span.price.text-success";
	public static final String OFFER_PRICE3 = "#wrapper > div > div > main > div.app-content > div > div > div.col-lg-5.col-12 > div.card.infor-group.hidden-md-down > div > div.multiple-offer-items > div:nth-child(4) > div.price-tags > span.price.text-success";
	
	public static final String OPTION1 = "#wrapper > div > div > main > div.app-content > div > div > div.col-lg-5.col-12 > div.card.infor-group.hidden-md-down > div > div.multiple-offer-items > div:nth-child(2) > div.product-name > label > input[type='radio']";
	public static final String OPTION2 = "#wrapper > div > div > main > div.app-content > div > div > div.col-lg-5.col-12 > div.card.infor-group.hidden-md-down > div > div.multiple-offer-items > div:nth-child(3) > div.product-name > label > input[type='radio']";
	public static final String OPTION3 = "#wrapper > div > div > main > div.app-content > div > div > div.col-lg-5.col-12 > div.card.infor-group.hidden-md-down > div > div.multiple-offer-items > div:nth-child(4) > div.product-name > label > input[type='radio']";
	
	public static final String ADD_TO_FAVORITES = "#wrapper > div > div > main > div.offer-detail.container-fluid > div > div.col-md-5.col-sm-12 > div.card.infor-group.hidden-sm-down > div > div.row > div:nth-child(2) > button";
	public static final String GIFT_CARD_LEFT = "#quoteItems > tr > td.row-name > span.lead-time > em";
	public static final String GIFT_CARD_VALIDATION = "#quoteItems > tr > td.row-name > span.removeQi";
	public static final String DECREMENT_QUANTITY = "decrementQty";
	public static final String INCREMENT_QUANTITY = "incrementQty";
	public static final String DONATE_AMOUNT = "#wrapper > div > div > main > div.app-content > div > div.offer-summary-container > div > div.col-md-7.col-12 > div > div.card-body > p > em > strong";
	public static final String SEARCH_CAMPAIGN = "donatedOtherOffers";
	public static final String SEARCH_CAMPAIGN_DROPDOWN = "donatedCampaignSelect";
	public static final String CHANGE_CAMPAIGN_LINK = "#accordion > div:nth-child(1) > span > a.collapsed.d-block";
	public static final String SEARCH_AUTOCOMPLETE_RESULT = "#collapseCampaigns > div > fieldset:nth-child(3) > span > span > div > div > div";
	public static final String ITEM_PRICE = "#quoteItems > tr > td.row-unit-price > div > span > span";
	public static final String ITEM_PRICE_CLASS = "price";
	public static final String ORDER_TOTAL_GRID = "#quoteItems > tr > td.row-price > div > span";
	public static final String ORDER_TOTAL = "#wrapper > div > div > main > div.app-content > div > div.offer-summary-container > div > div.col-md-7.col-12 > div > div.card-body > div > span.order-total-value";
	public static final String ORDER_TOTAL_CLASS = "order-total-value";
	public static final String OFFER_QUANTITY = "#quoteItems > tr > td.text-center.qty > div > div.qty-update-container > input";
	public static final String CAMPAIGN_NAME_LINK = "#accordion > div:nth-child(1) > span > a:nth-child(1) > strong";
	public static final String CHECKOUT_BUTTON = "btn-check-out";
	
	public static final String CARD_NUMBER = "creditCardNumber";
	public static final String CVV = "cvv";
	public static final String EXPIRATION = "expiration";
	public static final String DONATE_NAME = "name";
	public static final String ZIPCODE = "zipCode";
	public static final String EMAIL = "email";
	public static final String SUBMIT_BUTTON_CARD_SCREEN = "#wrapper > div > div > main > div.app-content > div > div.row > div.col-lg-7.col-md-6.col-12 > div > div > div > div > div > div:nth-child(7) > button";
	public static final String CONFIRM_OFFER_AMOUNT_CARD_SCREEN = "#wrapper > div > div > main > div.app-content > div > div.row > div.col-lg-5.col-md-6.col-12 > div > table > tbody > tr:nth-child(3) > td:nth-child(2) > strong";
	public static final String CONFIRM_QUANTITY_CARD_SCREEN = "#wrapper > div > div > main > div.app-content > div > div.row > div.col-lg-5.col-md-6.col-12 > div > table > tbody > tr:nth-child(2) > td:nth-child(2)";
	public static final String CONFIRM_DONATION_AMOUNT_CARD_SCREEN = "#wrapper > div > div > main > div.app-content > div > div.row > div.col-lg-5.col-md-6.col-12 > div > table > tbody > tr:nth-child(4) > td:nth-child(2)";
	
	public static final String CONFIRM_OFFER_AMOUNT = "#wrapper > div > div > main > div.app-content > div > div:nth-child(2) > form > div:nth-child(1) > div > div:nth-child(1) > div.col.col-7 > b";
	public static final String CONFIRM_CARD = "#wrapper > div > div > main > div.app-content > div > div:nth-child(2) > form > div:nth-child(1) > div > div:nth-child(2) > div.col.col-7 > b";
	public static final String CONFIRM_NAME = "#wrapper > div > div > main > div.app-content > div > div:nth-child(2) > form > div:nth-child(1) > div > div:nth-child(3) > div.col.col-7 > b";
	public static final String CONFIRM_BUTTON = "#wrapper > div > div > main > div.app-content > div > div:nth-child(2) > form > div:nth-child(2) > button.btn.btn-primary";
	public static final String TESTIMONIAL_NAME = "nameInput";
	public static final String TESTIMONIAL_EMAIL = "emailInput";
	public static final String TESTIMONIAL_TEXT = "messageInput";
	public static final String TESTIMONIAL_SHOWNAME_CHECKBOX = "#wrapper > div > div > main > div.app-content > div > div:nth-child(2) > div.form-check > label > input";
	public static final String SUBMIT_TESTIMONIAL_BUTTON = "#wrapper > div > div > main > div.app-content > div > div:nth-child(2) > button";
	public static final String SKIP_BUTTON = "#wrapper > div > div > main > div.app-content > div > div:nth-child(2) > a";
	public static final String MY_ORDER_TEXT = "#wrapper > div > div > main > div.app-content > div > div.page-heading.hidden-md-down > div > h2";
	public static final String MY_1ST_ORDER = "#wrapper > div > div > main > div.app-content > div > div:nth-child(2) > div.card-header > div > div.col-sm-8.col-12.mr-auto";
	public static final String VIEW_ORDER = "#wrapper > div > div > main > div.app-content > div > div:nth-child(2) > div.card-body.p-1 > div > div.col-lg-2.col-md-3.col-12.order-actions > div > button";
	public static final String MY_ORDER_ITEM_CLASS = "my-order-items";
	public static final String MY_ORDERS_1_NAME = "#wrapper > div > div > main > div.app-content > div > div:nth-child(2) > div.card-body.p-1 > div > div.col-lg-7.col-md-6.col-12 > div > p:nth-child(1) > a";
	public static final String MY_ORDERS_1_AMOUNT = "#wrapper > div > div > main > div.app-content > div > div:nth-child(2) > div.card-header > div > div.col-sm-4.col-12.order-total-price > strong";
	public static final String MY_ORDERS_1_STATUS = "#wrapper > div > div > main > div.app-content > div > div:nth-child(2) > div.card-body.p-1 > div > div.col-lg-7.col-md-6.col-12 > div > div > span";
	public static final String ORDETAIL_NAME = "#wrapper > div > div > main > div.app-content > div > div.card > div.card-body > div > div.col.order-detail > h3:nth-child(1) > a";
	public static final String ORDETAIL_STATUS = "#wrapper > div > div > main > div.app-content > div > div.card > div.card-header > span > span";
	public static final String SHARE_FACEBOOK = "#wrapper > div > div > main > div.app-content > div > div > div.col-lg-7.col-12 > div:nth-child(1) > div.offer-large-shares.hidden-xs-down > ul > li:nth-child(1) > div > a";
	public static final String ORDETAIL_TOTAL = "#wrapper > div > div > main > div.app-content > div > div.card > div.card-body > div > div.col.order-detail > h3:nth-child(6)";
}
