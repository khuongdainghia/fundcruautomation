package com.fundcru.objects.pagedefinitions;

public class Campaign {
	public static final String CREATE_NEW_COMPAIGN = "#btn-create-fundraiser-lg > a";
	public static final String GOAL = "#campaignCreate > div:nth-child(2) > div.card-body > div:nth-child(1) > div > input";
	public static final String TITLE = "#campaignCreate > div:nth-child(2) > div.card-body > div:nth-child(3) > input";
	public static final String CITY_CODE_NAME = "city";
	public static final String CITY_CODE = "#campaignCreate > div:nth-child(2) > div.card-body > div:nth-child(5) > input";
	public static final String CITY_LIST = "body > div.pac-container.pac-logo";	
	public static final String CITY_LIST_1 = "body > div.pac-container.pac-logo > div";
	public static final String CITY_LIST_1_XPATH = "/html/body/div[5]/div";	
	public static final String FORMATTED_ADDRESS = "formattedAddress";
	public static final String PRIMARY_CATERGORY = "categories.0";
	public static final String SECONDARY_CATERGORY = "categories.1";
	public static final String SUMMARY = "summary";
	public static final String ADD_NEW_PHOTO = "campaignCreate > div:nth-child(3) > div.card-body > div > ul > li:nth-child(3) > button > span";
	public static final String UPLOAD_PHOTO = "inputImage";
	public static final String UPLOAD_PHOTO_1 = "#campaignCreate > div:nth-child(3) > div.card-body > div > div > ul > li.list-group-item.autoform-array-item > div > div.autoform-array-item-body > div > div > div > span";
	public static final String CHOOSE_IMAGE = "/html/body/div[7]/div/div[1]/div/div/div[2]/div/div[1]/span/input";
	public static final String CHOOSE_IMAGE_XPATH = "//input[@type='file']";
	public static final String CHOOSE_2ND_IMAGE_XPATH = "//*[@id='campaignCreate']/div[3]/div[2]/div/div/ul/li[2]/button";
	public static final String UPLOAD_BUTTON = ".modal.fade.show > #photo-editor-modal > .modal-content > .modal-footer > #btn-submit-photo";
	public static final String CANCEL_UPLOAD_BUTTON = ".modal.fade.show > #photo-editor-modal > .modal-content > .modal-footer > #btn-cancel-upload-photo";
	public static final String UPLOAD_BUTTON_WEPAY_XPATH = "/html/body/div[6]/div/div[1]/div/div/div[3]/button[1]";
	public static final String STORY = "#campaignCreate > div:nth-child(4) > div.card-body > div > div > div.editor.trumbowyg-editor";
	public static final String STORY_TEXTAREA = "#campaignCreate > div.card.campaign-story.edit-campaign-field-group.has-hint > div.card-body > div.form-group > div > textarea";
	public static final String IMAGE = "#campaignCreate > div:nth-child(3) > div.card-body > div > div > ul > li.list-group-item.autoform-array-item > div > div.autoform-array-item-body > div > img";
	public static final String IMAGE_LIST_CLASS = "autoform-array-item";
	public static final String SAVE_PUBLISH = "#campaignCreate > div.form-group.create-campaign-buttons > button:nth-child(1)";
	public static final String SAVE_PUBLISH_XPATH = "//button[@type='submit']";
	public static final String SAVE = "#campaignCreate > div.form-group.create-campaign-buttons > button";
	public static final String SAVE_WEPAY_XPATH = "//*[@id='campaignCreate']/div[5]/button";
	public static final String SAVE_DRAFT = "#campaignCreate > div.form-group.create-campaign-buttons > button.btn.btn-primary.save-as-draft";
	public static final String TITLE_SUCCESS = "#wrapper > div > div > main > div.app-content > div > div > div.col-lg-5.col-12 > div.card.infor-group.large-campaign-infor.hidden-md-down > div > h3";
	public static final String TITLE_SUCCESS_WEPAY = "#wrapper > div > div > main > div.campaign-detail-view.container-fluid > div > div.col-lg-5.col-12 > div.card.infor-group.large-campaign-infor.hidden-md-down > div > h3";
	public static final String SUMMARY_SUCCESS = "#wrapper > div > div > main > div.app-content > div > div > div.col-lg-5.col-12 > div.card.infor-group.large-campaign-infor.hidden-md-down > div > div.campaign-detail-summary";
	public static final String SUMMARY_SUCCESS_WEPAY = "#wrapper > div > div > main > div.campaign-detail-view.container-fluid > div.card-columns.row > div.col-lg-5.col-12 > div.card.infor-group.large-campaign-infor.hidden-md-down > div > div.campaign-detail-summary";
	public static final String STORY_SUCCESS = "#wrapper > div > div > main > div.app-content > div > div > div.col-lg-7.col-12 > div:nth-child(3) > div > div > div.span8 > span";
	public static final String STORY_SUCCESS_WEPAY = "#wrapper > div > div > main > div.campaign-detail-view.container-fluid > div > div.col-lg-7.col-12 > div:nth-child(3) > div > div > div.span8 > span";
	public static final String DRAFTSTORY_FOR_UPDATING = "#wrapper > div > div > main > div.app-content > div > div.card-columns.row > div.col-lg-7.col-12 > div:nth-child(3) > div > div > div.span8 > span";
	public static final String DRAFTSTORY_FOR_PUBLISHED_UPDATING = "#wrapper > div > div > main > div.app-content > div > div.card-columns.row > div.col-lg-7.col-12 > div:nth-child(3) > div > div:nth-child(3) > div.span8 > span";
	public static final String SECONDARY_CATERGORY_SUCCESS = "#wrapper > div > div > main > div.app-content > div > div > div.col-lg-5.col-12 > div.card.infor-group.large-campaign-infor.hidden-md-down > div > div.campaign-detail-location-tags > span:nth-child(2) > div > span:nth-child(3) > a";
	public static final String SECONDARY_CATERGORY_SUCCESS_WEPAY = "#wrapper > div > div > main > div.campaign-detail-view.container-fluid > div > div.col-lg-5.col-12 > div.card.infor-group.large-campaign-infor.hidden-md-down > div > div.campaign-detail-location-tags > span:nth-child(2) > div > span:nth-child(3) > a";
	public static final String PRIMARY_CATERGORY_SUCCESS = "#wrapper > div > div > main > div.app-content > div > div > div.col-lg-5.col-12 > div.card.infor-group.large-campaign-infor.hidden-md-down > div > div.campaign-detail-location-tags > span:nth-child(2) > div > span:nth-child(2) > a";
	public static final String PRIMARY_CATERGORY_SUCCESS_WEPAY = "#wrapper > div > div > main > div.campaign-detail-view.container-fluid > div > div.col-lg-5.col-12 > div.card.infor-group.large-campaign-infor.hidden-md-down > div > div.campaign-detail-location-tags > span:nth-child(2) > div > span:nth-child(2) > a";
	public static final String PRIMARY_CATERGORY_SUCCESS_1 ="#wrapper > div > div > main > div.app-content > div > div > div.col-lg-5.col-12 > div.card.infor-group.large-campaign-infor.hidden-md-down > div > div.campaign-detail-location-tags > span:nth-child(2) > div > span > a";
	public static final String CITY_SUCCESS = "#wrapper > div > div > main > div.app-content > div > div > div.col-lg-5.col-12 > div.card.infor-group.large-campaign-infor.hidden-md-down > div > div.campaign-detail-location-tags > span:nth-child(1) > div > span > a";
	public static final String CITY_SUCCESS_AFTER_UPDATE = "#wrapper > div > div > main > div.app-content > div > div.card-columns.row > div.col-lg-5.col-12 > div.card.infor-group.large-campaign-infor.hidden-md-down > div > div.campaign-detail-location-tags > span:nth-child(1) > div > span > a";
	public static final String CITY_SUCCESS_WEPAY = "#wrapper > div > div > main > div.campaign-detail-view.container-fluid > div.card-columns.row > div.col-lg-5.col-12 > div.card.infor-group.large-campaign-infor.hidden-md-down > div > div.campaign-detail-location-tags > span:nth-child(1) > div > span > a";
	public static final String GOAL_SUCCESS = "#wrapper > div > div > main > div.app-content > div > div > div.col-lg-5.col-12 > div.card.infor-group.large-campaign-infor.hidden-md-down > div > div.campaign-detail-progress > div > div.campaign-preview-progress-goals.row > div.campaign-preview-progress-stats.col.text-right > div.campaign-preview-progress-count";
	public static final String GOAL_SUCCESS_WEPAY = "#wrapper > div > div > main > div.campaign-detail-view.container-fluid > div > div.col-lg-5.col-12 > div.card.infor-group.large-campaign-infor.hidden-md-down > div > div.campaign-detail-progress > div > div.campaign-preview-progress-goals.row > div.campaign-preview-progress-stats.col.text-right > div.campaign-preview-progress-count";
	public static final String DONATE_BUTTON = "donateButton";
	public static final String BOOKMARK_CAMPAIGN_BUTTON_LG = "bookmarkCampaignButton";
	public static final String UNBOOKMARK_CAMPAIGN_BUTTON_LG = "unsaveCampaignButton";
	public static final String CAMPAIGN_RAISED = "#wrapper > div > div > main > div.app-content > div > div > div.col-lg-5.col-12 > div.card.infor-group.large-campaign-infor.hidden-md-down > div > div.campaign-detail-progress > div.campaign-preview-progress > div.campaign-preview-progress-goals.row > div.campaign-preview-progress-stats.col.text-left > div.campaign-preview-progress-count";
	public static final String CAMPAIGN_RAISED_PROGRESS = "#wrapper > div > div > main > div.app-content > div > div.card-columns.row > div.col-lg-5.col-12 > div.card.infor-group.large-campaign-infor.hidden-md-down > div > div.campaign-detail-progress > div:nth-child(1) > p > small";
	public static final String CAMPAIGN_RAISED_WEPAY = "#wrapper > div > div > main > div.app-content > div > div.card-columns.row > div.col-lg-5.col-12 > div.card.infor-group.large-campaign-infor.hidden-md-down > div > div.campaign-detail-progress > div > div.campaign-preview-progress-goals.row > div.campaign-preview-progress-stats.col.text-left > div.campaign-preview-progress-count";
	public static final String CAMPAIGN_RAISED_WEPAY_CLASS = "campaign-preview-progress-count";
	public static final String CAMPAIGN_DONORS = "#wrapper > div > div > main > div.app-content > div > div.card-columns.row > div.col-lg-5.col-12 > div.card.infor-group.large-campaign-infor.hidden-md-down > div > div.campaign-detail-progress > div > div.campaign-preview-progress-goals.row > div.campaign-preview-progress-stats.col.text-center > div.campaign-preview-progress-count";
	public static final String CAMPAIGN_TITLES = "#wrapper .app .app-body .main .app-content .campaign-detail-view .large-campaign-infor .campaign-detail-desc .campaign-title";
	public static final String NUMBER_DONORS = "#content > div > div > div:nth-child(1) > div.col-md-5 > div > div.campaign-detail-progress > div > div.campaign-preview-progress-goals.row > div.campaign-preview-progress-stats.col.text-center > div.campaign-preview-progress-count";
	public static final String SWITCH_TO_EDIT_MODE = "#wrapper > div > div > main > div.app-content > div > div:nth-child(1) > div.alert.alert-warning > button:nth-child(4)";
	public static final String SWITCH_TO_EDIT_MODE_LINK = "#wrapper > div > div > main > div.app-content > div > div:nth-child(1) > div.alert.alert-warning > a";
	public static final String DRAFT_TEXT = "#content > div > div.row > div > div > p:nth-child(2)";
	public static final String DRAFT_WARNING = "#wrapper > div > div > main > div.campaign-detail-view.container-fluid > div:nth-child(1) > div > div > p";
	public static final String CANCEL_CHANGES_AND_PREVIEW = "Cancel changes and preview";
	
	// Edit campaign page
	public static final String SAVE_AND_PREVIEW = "#campaignEdit > div.alert.alert-warning > button";
													
	public static final String PUBLISH = "campaignPublish";
	public static final String CAMPAIGN_URL_PREFIX = "#campaignEdit > div:nth-child(3) > div.card-body > div > div > span";
	public static final String CAMPAIGN_URL_NAME = "shortId";
	public static final String CAMPAIGN_URL_CSS = "#campaignEdit > div:nth-child(3) > div.card-body > div > div > input";
	public static final String CAMPAIGN_URL = "#campaignEdit > div:nth-child(3) > div.card-body > div > div > input";
	public static final String GOAL_ECP_NAME = "goal";
	public static final String GOAL_ECP = "#campaignEdit > div:nth-child(4) > div.card-body > div:nth-child(1) > div > input";
	
	public static final String CAMPAIGN_TITLE_ECP = "#campaignEdit > div:nth-child(4) > div.card-body > div:nth-child(2) > input";
	public static final String City_Zip_Code = "#campaignEdit > div:nth-child(4) > div.card-body > div:nth-child(3) > input";
	public static final String PRIMARY_CATEGORY_ECP = "#campaignEdit > div:nth-child(4) > div.card-body > div:nth-child(4) > div > select";
	public static final String SECONDARY_CATEGORY_ECP = "#campaignEdit > div:nth-child(4) > div.card-body > div:nth-child(5) > select";
	public static final String CAMPAIGN_SUMMARY = "#campaignEdit > div:nth-child(4) > div.card-body > div:nth-child(6) > textarea";
	public static final String CAMPAIGN_STORY = "#campaignEdit > div:nth-child(6) > div.card-body > div > div > div.editor.trumbowyg-editor";
	public static final String CAMPAIGN_SUMMARY_AFTER_EDIT = "#wrapper > div > div > main > div.app-content > div > div.card-columns.row > div.col-lg-5.col-12 > div.card.infor-group.large-campaign-infor.hidden-md-down > div > div.campaign-detail-summary";
	
	public static final String ERROR_MESSAGE_FAILED_TO_UPDATE_CAMPAIGN = "#campaignEdit > div:nth-child(2) > div";
	
	public static final String ERROR_MESSAGE_TITLE_REQUIRED_UPDATE_CAMPAIGN = "#campaignEdit > div:nth-child(4) > div.card-body > div:nth-child(2) > span";
	public static final String ERROR_MESSAGE_FAILED_TO_UPDATE_CAMPAIGN_XPATH = "//*[@id='campaignEdit']/div[2]/div";
	public static final String TESTIMONIAL_NAME = "#wrapper > div > div > main > div.app-content > div > div > div.col-lg-5.col-12 > div:nth-child(2) > div > div:nth-child(1) > div > div:nth-child(1) > strong";
	public static final String TESTIMONIAL_TEXT = "#wrapper > div > div > main > div.app-content > div > div > div.col-lg-5.col-12 > div:nth-child(2) > div > div:nth-child(1) > div > div:nth-child(1) > p.feed-message";
	public static final String TESTIMONIAL_LIST = "//*[@id='wrapper']/div/div/main/div[1]/div/div[2]/div[2]/div/div[1]/div/div";
	public static final String CAMPAIGN_NOT_FOUND = "#wrapper > .app > .app-body > .main > .app-content > div";
	public static final String CAMPAIGN_URL_NAME_EDIT_MODE = "#campaignEdit > div:nth-child(3) > div.card-body > div > div > input";
	public static final String PUBLISH_FROM_DRAFT = "#wrapper > div > div > main > div.app-content > div > div:nth-child(1) > div.alert.alert-warning > button:nth-child(5)";
	public static final String SELECT_PAYMENT_ACCOUNT_TEXT = "Select one";
	public static final String SELECT_PAYMENT_ACCOUNT_LINK = "#wrapper > div > div > main > div.app-content > div > div:nth-child(1) > div.alert.alert-warning > p:nth-child(1) > a";
	public static final String CHANGE_PAYMENT_ACCOUNT_TEXT = "Change";
	public static final String SELECT_PAYMENT_ACCOUNT_TEXT_CONTENT = "#wrapper > div > div > main > div.app-content > div > div:nth-child(1) > div.alert.alert-warning > p:nth-child(1)";
	public static final String CHANGE_PAYMENT_ACCOUNT_LINK = "#wrapper > div > div > main > div.app-content > div > div:nth-child(1) > div.alert.alert-warning > p > a";
	public static final String PAYMENT_ACCOUNT = "#paymentAccountChooser > div > div > div.modal-body > ul > li";
	public static final String PAYMENT_ACCOUNT_1 = "payment-account-0";
	public static final String PAYMENT_ACCOUNT_2 = "payment-account-1";
	public static final String PAYMENT_ACCOUNT_FOR_FUNRAISER = "#wrapper > div > div > main > div.app-content > div > div:nth-child(1) > div.alert.alert-warning > p > span";
	public static final String ETHER_RAISED = "#wrapper > div > div > main > div.app-content > div > div > div.col-lg-5.col-12 > div.card.infor-group.large-campaign-infor.hidden-md-down > div > div.campaign-detail-progress > div.campaign-preview-progress-name.row > div.col-7.text-left";
	public static final String ETHER_DONNORS = "#wrapper > div > div > main > div.app-content > div > div > div.col-lg-5.col-12 > div.card.infor-group.large-campaign-infor.hidden-md-down > div > div.campaign-detail-progress > div.campaign-preview-progress-name.row > div.col-5.text-right";
	public static final String PRIVATE_FUNDRAISER_LIST = "#campaign-home-contents > div:nth-child(3)";
	public static final String PUBLISHED_FUNDRAISER_LIST = "#campaign-home-contents > div.row.app-row";
	public static final String FUNDRAISER_ITEM_CLASS = "my-campaign-view";
	public static final String FUNDRAISER_ITEM_ID = "dropdown-%s";
	public static final String ACTION_ITEM_CLASS = "dropdown-item";
	public static final String PRIVATE_FUNDRAISER_LABEL = "#campaign-home-contents > h4:nth-child(1)";
	public static final String PUBLISHED_FUNDRAISER_LABEL = "#campaign-home-contents > h4:nth-child(4)";
	
	public static final String MY_DONATION_LIST = "#wrapper > div > div > main > div.app-content > div > div.page-heading.hidden-md-down > div > h2";
	public static final String DONATION_LIST = "#wrapper > div > div > main > div.app-content > div > h4";
	public static final String DONATION_LIST_LABLE = "#wrapper > div > div > main > div.app-content > div > div:nth-child(1) > h4";
	public static final String DONATION_LIST_REFRESH_LINK = "Click here to refresh";
	public static final String DONATION_LIST_NO_ITEM = "#wrapper > div > div > main > div.app-content > div > p > em";
	public static final String DONATION_PAGINATION = "#wrapper > div > div > main > div.app-content > div > div:nth-child(2) > ul";
	
	public static final String CAMPAIGN_CREATE_FORM_TITLE = "#campaignCreate > div:nth-child(2) > div.card-header";
	
	// Field validation error 
	public static final String FUND_DATA_VALIDATION_ERROR_MESSAGE = "#campaignCreate > div:nth-child(1) > div";
	public static final String GOAL_VALIDATION_ERROR             = "#campaignCreate > div:nth-child(2) > div.card-body > div:nth-child(1) > span";
	public static final String FUNDRAISER_TITLE_VALIDATION_ERROR = "#campaignCreate > div:nth-child(2) > div.card-body > div:nth-child(3) > span";
	public static final String CITY_CODE_VALIDATION_ERROR        = "#campaignCreate > div:nth-child(2) > div.card-body > div:nth-child(5) > span";
	public static final String PRIMARY_CATEGORY_VALIDATION_ERROR = "#campaignCreate > div:nth-child(2) > div.card-body > div:nth-child(7) > span";
	public static final String SUMMARY_VALIDATION_ERROR          = "#campaignCreate > div:nth-child(2) > div.card-body > div:nth-child(11) > span";
	public static final String IMAGE_VALIDATION_ERROR            = "#campaignCreate > div.card.campaign-images.edit-campaign-field-group > div.card-body > div > div > div > span";
	public static final String STORY_VALIDATION_ERROR            = "#campaignCreate > div.card.campaign-story.edit-campaign-field-group.has-hint > div.card-body > div.form-group.has-error > span";
	public static final String ASK_QUESTION = "Ask a question";
	
	public static final String MESSAGE_SUBJECT = "#wrapper > div > div > main > div.app-content > div > div.card-columns.row > div.col-lg-5.col-12 > div.card.infor-group.large-campaign-infor.hidden-md-down > div > div.campaign-detail-misc > div.campaign-detail-owner > span > div > div > div > div.modal-body > form > div:nth-child(3) > input";
	public static final String MESSAGE_EMAIL = "#wrapper > div > div > main > div.app-content > div > div > div.col-lg-5.col-12 > div.card.infor-group.large-campaign-infor.hidden-md-down > div > div.campaign-detail-misc > div.campaign-detail-owner > span > div > div > div > div.modal-body > form > div:nth-child(2) > input";
	public static final String MESSAGE_CONTENT = "#wrapper > div > div > main > div.app-content > div > div > div.col-lg-5.col-12 > div.card.infor-group.large-campaign-infor.hidden-md-down > div > div.campaign-detail-misc > div.campaign-detail-owner > span > div > div > div > div.modal-body > form > div:nth-child(4) > textarea";
	public static final String MESSAGE_SEND_BUTTON = "#wrapper > div > div > main > div.app-content > div > div.card-columns.row > div.col-lg-5.col-12 > div.card.infor-group.large-campaign-infor.hidden-md-down > div > div.campaign-detail-misc > div.campaign-detail-owner > span > div > div > div > div.modal-footer > button.btn.btn-primary";
	public static final String MESSAGE_CANCEL_BUTTON = "#wrapper > div > div > main > div.app-content > div > div.card-columns.row > div.col-lg-5.col-12 > div.card.infor-group.large-campaign-infor.hidden-md-down > div > div.campaign-detail-misc > div.campaign-detail-owner > span > div > div > div > div.modal-footer > button.btn.btn-secondary";
	public static final String MESSAGE_ALERT_SUCCESSFUL = "body > div.bert-alert.growl-top-right.success.clearfix.show.animate > div > div.bert-content > p";
	
	public static final String MESSAGE_CENTER_TEXT = "#wrapper > div > div > main > div.app-content > div > div.page-heading.hidden-md-down > div > h2";
	public static final String MESSAGE_SUBJECT_DETAIL= "#wrapper > div > div > main > div.app-content > div > div.chat-view > div.card.card-block > div > div.reply-section.col.col-sm-12.col-md-9 > div.conversation-header.row > div > span";
	public static final String MESSAGE_EMAIL_DETAIL= "#wrapper > div > div > main > div.app-content > div > div.chat-view > div.card.card-block > div > div.reply-section.col.col-sm-12.col-md-9 > div.chat-discussion > div > div > a > p > strong";
	public static final String MESSAGE_CONTENT_DETAIL= "#wrapper > div > div > main > div.app-content > div > div.chat-view > div.card.card-block > div > div.reply-section.col.col-sm-12.col-md-9 > div.chat-discussion > div > div > span";
	public static final String SHARE_FACEBOOK= "#wrapper > div > div > main > div.app-content > div > div > div.col-lg-7.col-12 > div:nth-child(1) > div.campaign-large-shares.hidden-xs-down > ul > li:nth-child(1) > div > a";
	public static final String SHARE_LINKEDIN= "#wrapper > div > div > main > div.app-content > div > div > div.col-lg-7.col-12 > div:nth-child(1) > div.campaign-large-shares.hidden-xs-down > ul > li:nth-child(4) > div > a";
	public static final String TOTAL_NUMBER_OF_SHARING = "#wrapper > div > div > main > div.app-content > div > div > div.col-lg-7.col-12 > div:nth-child(1) > div.campaign-large-shares.hidden-xs-down > ul > li:nth-child(5) > div > div";
}
