package com.fundcru.objects.pagedefinitions;
public class Messages {
	
	public static final String  UNSAVED_CHANGE = "Are you sure you want to exit without saving your changes?";
	public static final String  DELETE_CONFIRM = "Are you sure you want to delete this document type?";
	public static final String  DOCUMENT_UPADTED_SUCCESSFULLY = "Document Types updated successfully";
	public static final String  DOCUMENT_DELETE_SUCCESSFULLY = "Delete success";
	public static final String  DOCUMENT_CREATE_SUCCESSFULLY = "Document Types created successfully";
	public static final String  DOC_TYPES_EXIST = "Document Types exist";
	public static final String  ENTER_MANDATORY_FIELDS = "Please enter a value for all mandatory fields";
	public static final String  LOGIN_FAILED = "Login failed";
	public static final String  INVALID_EMAIL = "Invalid email";
	public static final String  USER_NOT_FOUND_ERROR = "User not found";
	public static final String  SUCCESS_INFOR_EMAIL_SENT = "Email sent";
	public static final String  REQUIRED_FIELD = "Required Field";
	public static final String  PASSWORD_DONT_MATCH_SIGNUP = "Passwords don't match";
	public static final String  ERROR_LOGIN_REQUIRED = "Must be logged in";
	
	// Sign Up
	public static final String  SUCCESS_MESSAGE_SIGNUP = "Successful registration! Please check your email and follow the instructions.";
	public static final String  ERROR_MESSAGE_SIGNUP = "Email already exists.";
	public static final String  MINIMUM_CHARACTER_REQUIRED_LENGTH_ERROR = "Minimum required length: 6";
	
	// Reset Password page
	public static final String  MINIMUM_REQUIRED_LEAGTH_RESET_PW = "Minimum required length: 6";
	public static final String  MAXIMUM_LEAGTH_RESET_PW = "Maximum password length: 128";
	public static final String  PASSWORD_DONT_MATCH_RESET_PW = "Passwords don't match";
	public static final String  TOKEN_EXPIRED_ERROR_RESET_PW = "Token expired";
	public static final String  SUCCESS_PASSWORD_RESET_PW = "info.passwordReset";
	
	// Change Password 
	public static final String  INCORRECT_PASSWORD = "Incorrect password";
	public static final String  CHANGE_PW_SUCCESS_MESSAGE = "info.passwordChanged";
	
	// Edit Campaign
	public static final String  FAILED_TO_UPDATE_CAMPAIGN = "Failed to update fundraiser! Goal is required";
	public static final String  FAILED_TO_UPDATE_CAMPAIGN_TITLE_REQUIRED = "Failed to update fundraiser! Fundraiser title is required";
	public static final String  FAILED_TO_UPDATE_CAMPAIGN_TITLE_REQUIRED_IN_LINE = "Fundraiser title is required";
	public static final String  FAILED_TO_UPDATE_CAMPAIGN_URL_EXIST = "Failed to update fundraiser! [The fundraiser URL has been used by another fundraiser]";
	
	// Donate screen - Secure transaction with Paypal
	public static final String  INVALID_CARD_NUMBER = "Invalid credit card number";
	public static final String  INVALID_CCV = "Invalid CCV";
	public static final String  INVALID_DATE = "Invalid date";
	public static final String  INVALID_NAME_ON_CARD = "Invalid name";
	public static final String  REQUIRED_FIELD_DONATE = "Required";

	// Donate screen - Secure transaction with Paypal
	public static final String  INVALID_CARD_NUMBER_WEPAY = "Invalid credit card number.";
	public static final String  INVALID_CCV_WEPAY = "Invalid CVV.";
	public static final String  INVALID_DATE_WEPAY = "Invalid expiration. Needs to be MM/YY.";
	public static final String  INVALID_DATE_WEPAY_GATEWAY = "Error from WePay: That credit card expiration date has already passed.";
	public static final String  INVALID_NAME_ON_CARD_WEPAY = "Name is required.";
	public static final String  INVALID_FIELD_DONATE_WEPAY = "Required";
	public static final String  INVALID_EMAIL_WEPAY = "Valid email address of the user the card belongs to.";
	public static final String  INVALID_ZIPCODE_WEPAY = "Invalid zipcode.";
	public static final String  INVALID_AMOUNT_WEPAY = "Invalid amount. Amount needs to be a whole number and greater than 0.";
	public static final String  INVALID_NAME_ON_CARD_WEPAY_GATEWAY = "Error from WePay: Full name is required";
	public static final String  PAYMENT_ACCOUNT_EXIST = "You already have a WePay account with the same email address";
	public static final String  FUNDRAISER_PAYMENT_ACCOUNT_TEXT = "This fundraiser is using %s's WePay account (%s) to receive donations.";

	public static final String  NO_PROFILE_ERROR = "You have no profile yet. Submit one";
	public static final String SUCCESS_MESSAGE = "";
	public static final String ETHEREUM_SUCCESS_MESSAGE = "Thank you for your information. We will send you an email after we receive the donation.";
	public static final String ETHEREUM_WARNING_MESSAGE = "There is already a pending donation from this account";
	public static final String ETHEREUM_SUCCESS_MESSAGE_SEND_ETHER = "Your request was submitted.";
	public static final String MINIMUM_NUMBER_GIFT_CARD = "Please order at least 1 item.";
	public static final String MAXIMUM_NUMBER_GIFT_CARD = "Maximum eligible quantity is 10.";
	
	// My Favourite Offer Screen
	public static final String ALERT_MESSAGE_NO_FUNDRAISER_EXIST= "You have no favorite fundraiser!";
	public static final String ALERT_MESSAGE_NO_OFFER_EXIST= "You have no favorite offer!";
	public static final String INVALID_EMAIL_MESSAGE= "You need to enter a valid email.";
	public static final String SELECT_PAYMENT_ACCOUNT_TEXT_CONTENT= "You need to choose a payment account for this fundraiser in order to publish it.";
	public static final String CHANGE_PAYMENT_ACCOUNT_TEXT_CONTENT= "Your payment account for this fundraiser is not available.";

	// Add Fundcru screen
	public static final String FUND_DATA_VALIDATION_ERROR_MESSAGE= "Validate fundraiser data failed!";
	
	public static final String REQUIRED_FIELD_GOAL_ERROR= "Goal is required";
	public static final String GOAL_MAXIMUM_VALIDATION_ERROR= "Goal is not a number";
	public static final String GOAL_MINIMUM_VALIDATION_ERROR= "Money must greater than 0";
	public static final String REQUIRED_FIELD_FUNDRAISER_TITLE_ERROR= "Fundraiser title is required";
	public static final String REQUIRED_FIELD_CITY_CODE_ERROR= "City/Zip Code is required";
	public static final String REQUIRED_FIELD_PRIMARY_CATEGORY_ERROR= "Primary category is required";
	public static final String REQUIRED_FIELD_SUMMARY_ERROR= "Fundraiser summary is required";
	public static final String REQUIRED_FIELD_IMAGE_ERROR= "Images is required";
	public static final String REQUIRED_FIELD_STORY_ERROR= "Story is required";
	public static final String STORY_MINIMUM_VALIDATION_ERROR= "Story must be at least 40 characters";
	
	// Personal Information screen
	public static final String FAILED_ERROR_MODIFYING_PERSONAL_INFORMATION = "Failed to validate submitted data";
	public static final String FIRSTNAME_REQUIRED_PERSONAL_INFORMATION = "Firstname is required";
	public static final String LASTNAME_REQUIRED_PERSONAL_INFORMATION = "Lastname is required";
	public static final String MESSAGE_SENT = "Your message was sent";
	public static final String GIFT_CARD_CODE_UPDATED = "Gift card codes updated!";
	
	// Linkedin Sharing pop up 
	public static final String LINKEDIN_SHARE_ALERT_MESSAGE = "Great! You have successfully shared this update.";

}
