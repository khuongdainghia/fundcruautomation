package com.fundcru.objects.pagedefinitions;

public class BrowseFundraiser {
	public static final String BROWSE_FUNDRAISER_LINK   = "btn-campaign-list-lg";
	
	public static final String ANIMALS_BTN                 	= "#campaign-home-contents > div:nth-child(2) > div > div:nth-child(1) > a";
	public static final String EDUCATION_BTN 				= "#campaign-home-contents > div:nth-child(2) > div > div:nth-child(2) > a";
	public static final String EMERGENCIES_BTN 				= "#campaign-home-contents > div:nth-child(2) > div > div:nth-child(3) > a";
	public static final String MEDICAL_BTN 					= "#campaign-home-contents > div:nth-child(2) > div > div:nth-child(4) > a";
	public static final String CHARITY_BTN 					= "#campaign-home-contents > div:nth-child(2) > div > div:nth-child(5) > a";
	public static final String WISHES_BTN                   = "#campaign-home-contents > div:nth-child(2) > div > div:nth-child(6) > a";
	
	public static final String POPULAR_FUNDRAISER_LABEL 	= "#campaign-home-contents > div:nth-child(3) > h3";
}
