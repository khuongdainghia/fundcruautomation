package com.fundcru.objects.pagedefinitions;

public class SendEther {
	//Ethereum Donation
	public static final String ETHEREUM_EMAIL = "from";
	public static final String ETHEREUM_PASS = "//*[@id='from'][1]";
	public static final String ETHEREUM_TO = "to";
	public static final String ETHEREUM_AMOUNT = "amount";
	public static final String SUBMIT_ETHEREUM_INFORMATION_SCREEN = "//button[@type='submit']";
	public static final String ETHEREUM_SUCCESS_MESSAGE = "#wrapper > div > div > div > div.alert.alert-success";

}
