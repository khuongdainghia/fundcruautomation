package com.fundcru.objects.pagedefinitions;

public class PersonalInfomation {		
	public static final String HEADER = "#wrapper > div > div > main > div.app-content > div > div.page-heading.hidden-md-down > div > h2";
	public static final String BACK_BUTTON = "#wrapper > div > div > main > div.app-content > div > div.bottom-back-button.row > button";
	
	public static final String TABLE = "//*[@id='no-more-tables']/table";
	public static final String TABLE_BODY = "//*[@id='no-more-tables']/table/tbody";
	public static final String EMAIL_TITLE  = "//*[@id='no-more-tables']/table/tbody/tr[1]/th";
	public static final String EMAIL        = "//*[@id='no-more-tables']/table/tbody/tr[1]/td[1]";
	
	public static final String PROFILE_TITLE  = "//*[@id='no-more-tables']/table/tbody/tr[2]/th";
	public static final String PROFILE        = "//*[@id='no-more-tables']/table/tbody/tr[2]/td[1]";
	public static final String PROFILE_ACTION = "//*[@id='no-more-tables']/table/tbody/tr[2]/td[2]/div/span";
	
	public static final String FULLNAME_TITLE  = "//*[@id='no-more-tables']/table/tbody/tr[3]/th";
	public static final String FULLNAME        = "//*[@id='no-more-tables']/table/tbody/tr[3]/td[1]";
	public static final String FULLNAME_ACTION = "//*[@id='no-more-tables']/table/tbody/tr[3]/td[2]/button";
	
	public static final String ABOUT_TITLE  = "//*[@id='no-more-tables']/table/tbody/tr[4]/th";
	public static final String ABOUT        = "//*[@id='no-more-tables']/table/tbody/tr[4]/td[1]";
	public static final String ABOUT_ACTION = "//*[@id='no-more-tables']/table/tbody/tr[4]/td[2]/button";
	
	public static final String IMAGE_MODIFY_DIALOG_SAVE_IMAGE_BUTTON = ".modal.fade.show > #photo-editor-modal > .modal-content > .modal-footer > #btn-submit-photo";
	
	public static final String MODIFY_DIALOG_HEADER = ".modal.fade.show > .modal-dialog > .modal-content > .modal-header > .modal-title";
	
	public static final String FULLNAME_MODIFY_DIALOG_ERROR_MESSAGE   = "//*[@id='updateUserFullname']/div";
	public static final String FULLNAME_MODIFY_DIALOG_FIRSTNAME_LABEL = "//*[@id='updateUserFullname']/fieldset/div[1]/label";
	public static final String FULLNAME_MODIFY_DIALOG_FIRSTNAME       = "//*[@id='updateUserFullname']/fieldset/div[1]/input";	
	public static final String FULLNAME_MODIFY_DIALOG_LASTNAME_LABEL  = "//*[@id='updateUserFullname']/fieldset/div[2]/label";
	public static final String FULLNAME_MODIFY_DIALOG_LASTNAME        = "//*[@id='updateUserFullname']/fieldset/div[2]/input";	
	public static final String ERROR_FOR_FIRSTNAME_VALIDATION          = "//*[@id='updateUserFullname']/fieldset/div[1]/span";
	public static final String ERROR_FOR_LASTNAME_VALIDATION          = "//*[@id='updateUserFullname']/fieldset/div[2]/span";
	
	public static final String ABOUT_MODIFY_DIALOG_ABOUT_LABEL      = "//*[@id='updateUserAbout']/fieldset/div/label";
	public static final String ABOUT_MODIFY_DIALOG_ABOUT_TEXT_AREA  = "//*[@id='updateUserAbout']/fieldset/div/textarea";
	
	public static final String FULLNAME_MODIFY_DIALOG_SUBMIT_BUTTON = "//*[@id='updateUserFullname']/button[1]";
	public static final String FULLNAME_MODIFY_DIALOG_CANCEL_BUTTON = "//*[@id='updateUserFullname']/button[2]";
	
	public static final String ABOUT_MODIFY_DIALOG_SUBMIT_BUTTON = "//*[@id='updateUserAbout']/button[1]";
	public static final String ABOUT_MODIFY_DIALOG_CANCEL_BUTTON = "//*[@id='updateUserAbout']/button[2]";
}
