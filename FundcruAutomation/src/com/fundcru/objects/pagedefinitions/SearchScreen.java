package com.fundcru.objects.pagedefinitions;

public class SearchScreen {
	public static final String SEARCH_BUTTON_XPATH = "//*[@id='searchDropdown']/a/i" ;
	public static final String SEARCH_BUTTON_ID = "searchDropdown" ;
	public static final String HEADER 			= "#wrapper > div > div > main > div.app-content > div > h4" ;	
}
