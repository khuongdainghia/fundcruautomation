package com.fundcru.objects.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.fundcru.objects.pagedefinitions.Admin;
import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Login;
import com.fundcru.utilities.InputController;
import com.fundcru.utilities.TestLoginCredentials;

public class LoginPage extends BasePage{
	int timeout = 60;
	public LoginPage(WebDriver driver) {
		super(driver);
	}
	
	public LoginPage() {
		super(BasePage.driver);
	}
	
	public void login(String url,String username, String password){
		driver.navigate().to(url);
		WebElement signInLink = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Login.SIGNIN_LINK)));
		signInLink.click();
		WebElement userNameTextField = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Login.EMAIL_TEXT_FIELD_ID)));
		InputController.inputToTextFiled(userNameTextField, username);
		InputController.inputToTextFiled(getPasswordTextField(), password);
		getLoginButton().click();
		checkLoginSuccessful();
	}
	
	public void login(String url,TestLoginCredentials credential){
		login(url,credential.getUsername(), credential.getPassword());
	}
	// Get ELELEMENTS
	
	public static WebElement getUsernameTextField(){
		return driver.findElement(By.id(Login.EMAIL_TEXT_FIELD_ID));
	}
	
	public static WebElement getPasswordTextField(){
		return driver.findElement(By.id(Login.PASSWORD_TEXT_FIELD_ID));
	}
	
	public static WebElement getLoginButton(){
		return driver.findElement(By.id(Login.SIGNIN_BUTTON_ID));
	}
	
	public  void checkLoginSuccessful(){
		WebElement loginUser = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Home.USER_ICON)));
		Assert.assertEquals(true,loginUser.isDisplayed(), "Login failed!");
	}
	public void loginAdmin(String url,TestLoginCredentials credential){
		loginAdmin(url,credential.getUsername(), credential.getPassword());
	}
	public void loginAdmin(String url,String username, String password){
		driver.navigate().to(url);
		WebElement userNameTextField = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Admin.EMAIL_TEXT_FIELD_ID)));
		WebElement passwordTextField = driver.findElement(By.id(Admin.PASSWORD_TEXT_FIELD_ID));
		InputController.inputToTextFiled(userNameTextField, username);
		InputController.inputToTextFiled(passwordTextField, password);
		WebElement sigIn = driver.findElement(By.id(Admin.SIGN_IN_BUTTON));
		sigIn.click();
		checkAdminLoginSuccessful();
	}
	public void checkAdminLoginSuccessful(){
		WebElement loginUser = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Admin.ADMIN_LOGGED_LINK)));
		Assert.assertEquals(true,loginUser.isDisplayed(), "Login failed!");
	}
}
