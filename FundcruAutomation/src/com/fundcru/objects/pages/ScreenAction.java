package com.fundcru.objects.pages;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Login;
import com.fundcru.objects.pagedefinitions.Messages;

public class ScreenAction {
	WebDriver driver;
	int timeout = 30;

	public ScreenAction(WebDriver driver) {
		this.driver = driver;

		}
	public void clickOkOnDilogueBox() {
		Alert alert = driver.switchTo().alert();
		alert.accept();
		driver.switchTo().defaultContent();
	}
//	public void waitObjInvisible(WebElement obj) {
//		pause(8000);
//		(new WebDriverWait(driver, timeout)).until(ExpectedConditions.invisibilityOf(obj));
//	}
	public void waitObjInvisible(By obj) {
		pause(1000);
		(new WebDriverWait(driver, timeout)).until(ExpectedConditions.invisibilityOfElementLocated(obj));
	}

	public void waitObjVisible(By obj) {
		(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(obj));
	}

	public void waitObjVisibleAndClick(By obj) {

		waitObjVisibleAndClick(obj, timeout);
	}

	public void waitObjVisibleAndClick(By obj, int timeout) {
		WebElement element = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(obj));
		element.click();
	}

	public void checkObjVisible(By obj) {
		driver.findElement(obj).isDisplayed();
	}
	
	public void clickBtn(By obj) {
		WebElement btn = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(obj));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn);
		btn.click();
	}

	public void inputEmailField(String obj, String value) {
		WebElement txtField = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(obj)));
		txtField.clear();
		pause(1000);
		for (int i = 0; i < value.length(); i++) {
			char c = value.charAt(i);
			String s = new StringBuilder().append(c).toString();
			txtField.sendKeys(s);
		}
		if (!((txtField.getText().trim() == value) || (txtField.getAttribute("value").trim() == value))) {
			txtField.clear();
			txtField.sendKeys(value);
		}
	}

	public void inputTextFieldByID(String obj, String value) {
		inputTextFieldBy(By.id(obj), value);
	}

	public void inputTextFieldBy(By by, String value) {
		WebElement txtField = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(by));
		txtField.clear();
		for (int i = 0; i < value.length(); i++) {
			char c = value.charAt(i);
			String s = new StringBuilder().append(c).toString();
			txtField.sendKeys(s);
		}
	}

	public void assertFieldReadOnly(By by) {
		WebElement field = driver.findElement(by);
		String readonly = field.getAttribute("readonly");
		assertNotNull(readonly);
	}

	public boolean isFieldDisable(By by) {
		WebElement field = driver.findElement(by);
		String disabled = field.getAttribute("aria-disabled");
		if (null != disabled && disabled.equals("true"))
			return true;
		return false;
	}

	public boolean isFieldDisable(WebElement field) {
		String disabled = field.getAttribute("aria-disabled");
		if (disabled != null && disabled.equals("true"))
			return true;
		return false;
	}

	public void assertTextBoxDisable(By by) {
		WebElement field = driver.findElement(by);
		String disabled = field.getAttribute("disabled");
		assertNotNull(disabled);
	}
	public void assertTextEqualCssSelector(String obj, String exptectedValue) {
		assertTextEqual(By.cssSelector(obj),exptectedValue);
	}
	public void assertTextEqual(By by, String exptectedValue) {
		WebElement screenTitle = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(by));
		if (screenTitle.getText().trim().isEmpty()) {
			assertEquals(screenTitle.getAttribute("value").trim(), exptectedValue, "Value is wrong");
		} else {
			assertEquals(screenTitle.getText().trim(), exptectedValue, "Value is wrong");

		}
	}
	
	public void assertTextEndWith(By by, String exptectedValue) {
		WebElement screenTitle = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(by));
		if (screenTitle.getText().trim().isEmpty()) {
			screenTitle.getAttribute("value").trim().endsWith(exptectedValue);
		} else {
			screenTitle.getText().trim().endsWith(exptectedValue);
		}
	}

	public String getAttribute(By by) {
		(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(by));
		return driver.findElement(by).getAttribute("value");
	}

	public void clickCheckBoxByText(String text) {
		clickCheckBoxByText(2, text, Home.CHECKBOX_XPATH);
	}

	public void clickCheckBoxByText(int columnn, String text) {
		clickCheckBoxByText(columnn, text, Home.CHECKBOX_XPATH);
	}
	public void clickCheckBoxByText(int columnn, String text, String tableBody) {
		TableFunction table = new TableFunction(driver);
		List<WebElement> listCheckbox = driver.findElements(By.xpath("//input[@type='checkbox']"));
		int row = table.findRowByString(tableBody, columnn, text, true);
		if (listCheckbox.get(row).isDisplayed()) {
			listCheckbox.get(row).click();
		} else {
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", listCheckbox.get(row + 1));
		}
	}

	public void clickCheckBoxN(int n) {
		List<WebElement> listCheckbox = driver.findElements(By.xpath("//input[@type='checkbox']"));
		if (listCheckbox.size() > 0) {
			if (listCheckbox.get(n).isDisplayed()) {
				listCheckbox.get(n).click();
			}
		}
	}

	public void checkObjSelected(int start, int end) {
		List<WebElement> listCheckbox = driver.findElements(By.xpath("//input[@type='checkbox']"));
		for (int i = start; i < end; i++) {
			listCheckbox.get(i).isSelected();
		}
	}

	public boolean checkObjSelected(int index) {
		boolean isSelected = false;
		List<WebElement> listCheckbox = driver.findElements(By.xpath("//input[@type='checkbox']"));
		isSelected = listCheckbox.get(index).isSelected();
		return isSelected;
	}

	public void clickCheckBoxNInTable(String tableCSS, int n) {
		WebElement table = driver.findElement(By.cssSelector(tableCSS));
		List<WebElement> listCheckbox = table.findElements(By.className("v-grid-selection-checkbox"));
		listCheckbox.get(n).click();
	}

	public void checkAddSuccess(String msg) {

		WebElement flashMessage1 = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By
				.cssSelector(Messages.SUCCESS_MESSAGE)));
		Assert.assertEquals(flashMessage1.getText(), msg);
		flashMessage1.click();
	}

	public void selectComboValue(By by, String value) {
		final Select selectBox = new Select(driver.findElement(by));
		selectBox.selectByValue(value);
	}

	public void selectStatus(String tableCSS, String value) {
		pause(1000);
		waitObjVisible(By.cssSelector(tableCSS));
		WebElement baseTable = driver.findElement(By.cssSelector(tableCSS));
		List<WebElement> tableRows = baseTable.findElements(By.tagName("tr"));
		int sumRow = tableRows.size();
		for (int i = 1; i <= sumRow; i++) {

			WebElement columnValue = driver.findElement(By.xpath("//div[@id='VAADIN_COMBOBOX_OPTIONLIST']//div//div[2]//table//tbody//tr[" + i
					+ "]//td//span"));

			// System.out.println("Status " + columnValue.getText());
			if (columnValue.getText().equals(value)) {
				columnValue.click();
				break;
			}
		}

	}

	public void assertMessge(String msgCSS, String msg) {
		WebElement error = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(msgCSS)));
		if (error.getText().trim().isEmpty()) {
//			if (error.getAttribute("value").trim().isEmpty()){
				Assert.assertTrue(error.getAttribute("innerHTML").trim().contains(msg),String.format("Wrong expected: %s Actual: %s",msg,error.getAttribute("innerHTML")));
//			}else
//			{
//				Assert.assertEquals(error.getAttribute("value").trim(), msg);
//			}
		} else {
			Assert.assertEquals(error.getText().trim(), msg);
		}
//		error.click();
	}

	public boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}


	public void clickVerticalScrollBar(Boolean scrollToTop) {
		WebElement vertical_scroll = driver.findElement(By.className(Home.VERTICAL_SCROLLBAR_CLASS));
		if (vertical_scroll.isDisplayed()) {
			int height = vertical_scroll.getSize().getHeight();
			Actions move = new Actions(driver);
			if (scrollToTop) {
				move.dragAndDropBy(vertical_scroll, 0, (height * -1)).build().perform();
			} else {
				move.dragAndDropBy(vertical_scroll, 0, ((height * 10) / 100)).build().perform();
			}

		}

	}

	public void clickVerticalScrollBar() {
		clickVerticalScrollBar(false);

	}

	public void clickHorizontalScrollBar(Boolean scrollToFirst) {

		WebElement horizontal_scroll = driver.findElement(By.className(Home.HORIZONTAL_SCROLLBAR_CLASS));
		if (horizontal_scroll.isDisplayed()) {
			int width = horizontal_scroll.getSize().getWidth();
			Actions move = new Actions(driver);
			if (scrollToFirst) {
				move.dragAndDropBy(horizontal_scroll, (width * -1), 0).build().perform();
			} else {
				move.dragAndDropBy(horizontal_scroll, ((width * 10) / 100), 0).build().perform();
			}
		}

	}

	public void clickHorizontalScrollBarToElement(WebElement element) {
		for (int i = 0; i < 20; i++) {

			if (!element.isDisplayed()) {
				clickHorizontalScrollBar();
			} else {
				break;
			}
		}

	}

	public void clickHorizontalScrollBar() {
		clickHorizontalScrollBar(false);

	}

	public void scrollToElementWithColumnNo(WebElement element, int column) {
		clickVerticalScrollBar(true);
		WebElement header = driver.findElement(By.xpath(Home.TABLE_HEAD_XPATH + "//tr//th[" + column + "]//div[1]"));
		clickHorizontalScrollBarToElement(header);

		for (int i = 0; i < 20; i++) {

			if (!element.isDisplayed()) {
				clickVerticalScrollBar();
			} else {
				break;
			}
		}
	}

	public void scrollToElement(WebElement element) {
		clickVerticalScrollBar(true);
		clickHorizontalScrollBar(true);

		for (int i = 0; i < 20; i++) {

			if (!element.isDisplayed()) {
				clickHorizontalScrollBar();
				clickVerticalScrollBar();
			} else {
				break;
			}
		}
	}

	public void scrollToElement(String cssObject) {
		scrollToElement(driver.findElement(By.cssSelector(cssObject)));
	}
	public String getTextFieldByCssSelector(String obj) {
		return getTextField(By.cssSelector(obj));
	}
	
	public String getTextFieldByID(String obj) {
		return getTextField(By.id(obj));
	}

	public String getTextField(By by) {
		try {
			WebElement textField = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(by));
			return textField.getText();
		} catch (NoSuchElementException e) {
			return "";
		}

	}

	public void signOut() {
		waitObjVisibleAndClick(By.className(Home.USER_LINK_CLASS));
		pause(3000);
		waitObjVisibleAndClick(By.id(Home.SIGN_OUT));
	}

	public void signIn(String emailAddress, String password) {
		waitObjVisible(By.id(Login.EMAIL_TEXT_FIELD_ID));
		inputEmailField(Login.EMAIL_TEXT_FIELD_ID, emailAddress);
		pause(timeout);
		inputTextFieldByID(Login.PASSWORD_TEXT_FIELD_ID, password);
		clickBtn(By.id(Login.SIGNIN_BUTTON_ID));
	}

	public String getPassword(String passwordMessage) {
		return passwordMessage.substring(31);

	}

	public void pause(int milliseconds) {
		try {
			Thread.sleep(milliseconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void uploadPhoto(By uploadButton, String imageFileName, By buttonSavePhoto, By imageAfterUploaded ) {
	    WebElement uploadPhotoButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(uploadButton));
	    JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",uploadPhotoButton);
	    WebElement campaignChooseImage = driver.findElement(By.xpath(Campaign.CHOOSE_IMAGE_XPATH));
	    campaignChooseImage.clear();
	    Path path = Paths.get(System.getProperty("user.dir"));
	    campaignChooseImage.sendKeys(path.resolve(System.getProperty("user.dir")+File.separator + imageFileName).toAbsolutePath().toString());
	    WebElement saveButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(buttonSavePhoto));
	    js.executeScript("arguments[0].click();",saveButton);
	    WebElement image =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(imageAfterUploaded));
	    image.isDisplayed();
	}
	
	public boolean assertCorrectURL(String expectedURL) {
		boolean result = false;
		String url = driver.getCurrentUrl();
		if(url.equalsIgnoreCase(expectedURL)) {
			result = true;
		}
		return result;	
	}
}