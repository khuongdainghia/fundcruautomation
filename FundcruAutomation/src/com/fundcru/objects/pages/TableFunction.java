package com.fundcru.objects.pages;

import static org.testng.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.fundcru.objects.pagedefinitions.Home;

public class TableFunction {
	WebDriver driver;
	ScreenAction action;
	int timeout = 5;

	public TableFunction(WebDriver driver) {
		this.driver = driver;
		action = new ScreenAction(driver);
	}

	public int findRowByString(String tableCSS, int columnindex, String value) {
		return findRowByString(tableCSS, columnindex, value, false);
	}

	public int findRowByString(int columnindex, String value) {
		return findRowByString(Home.TABLE_BODY_XPATH, columnindex, value, true);
	}

	public int findRowByString(String tableBody, int columnindex, String value, boolean isXpath) {
		int row = -1;
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		WebElement baseTable;
		if (isXpath) {
			baseTable = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(tableBody)));
		} else {
			baseTable = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(tableBody)));
		}
		if (null == baseTable)
			return row;
		List<WebElement> tableRows = baseTable.findElements(By.tagName("tr"));
		int sumRow;
		if (isXpath) {
			sumRow = tableRows.size();
		} else {
			sumRow = tableRows.size() - 1;
		}
		if (sumRow > 0) {
			WebElement columnValue;
			for (int i = 1; i <= sumRow; i++) {
				if (isXpath) {
					columnValue = driver.findElement(By.xpath(String.format("%s//tr[%s]//td[%s]", tableBody, i, columnindex-1)));
				} else {
					columnValue = driver.findElement(By.cssSelector(String.format("%s> table > tbody > tr:nth-child(%s) > td:nth-child(%s)",
							tableBody, i, columnindex-1)));
				}
				if (columnValue.getText().trim().toLowerCase().equals(value.toLowerCase())) {
					row = i;
					break;
				}
			}
		}
		return row;
	}

	public int findRealIndexByCell(int row, int column, String indexString) {
		return findRealIndexByCell(getCellObject(row, column), indexString);
	}

	public int findRealIndexByCell(WebElement cell, String indexString) {
		List<WebElement> idLink = cell.findElements(By.cssSelector(String.format("div[id^=%s]", indexString)));
		int indexOfSupplier = -1;
		if (idLink.size() > 0) {
			String stringIDOfSupplier = idLink.get(0).getAttribute("id");
			stringIDOfSupplier = stringIDOfSupplier.substring(stringIDOfSupplier.indexOf(indexString) + indexString.length(),
					stringIDOfSupplier.length());
			indexOfSupplier = Integer.valueOf(stringIDOfSupplier);
		}
		return indexOfSupplier;
	}

	public boolean isValueExisting(int columnindex, String value) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Home.TABLE_BODY_XPATH + "")));
		// int row = 0;
		WebElement baseTable = driver.findElement(By.xpath(Home.TABLE_BODY_XPATH + ""));
		List<WebElement> tableRows = baseTable.findElements(By.tagName("tr"));
		int sumRow = tableRows.size();
		for (int i = 1; i <= sumRow; i++) {
			WebElement columnValue = driver.findElement(By.xpath(Home.TABLE_BODY_XPATH + "//tr[" + i + "]//td[" + (columnindex-1) + "]"));
			if (columnValue.getText().equals(value)) {

				return true;
			}

		}
		return false;
	}

	// Click User Number in Maintain Customer User (Customer account)
	///Default is column 4
	public void clickDeleteIcon(int row) {
		clickDeleteIcon(4,row);
		
	}
	// Click User Number in Maintain Customer User (Customer account)
	public void clickDeleteIcon(int column,int row) {
		WebElement deleteCell = getCellObject(row, column);
		deleteCell.findElement(By.tagName("button")).click();
		Alert alert = driver.switchTo().alert();
		alert.accept();
		driver.switchTo().defaultContent();
		
	}
	public int countRow() {
		return countRow(Home.TABLE_BODY_XPATH,true);
	}
	public int countRow(String tableCSS) {
		return countRow(tableCSS,false);
	}
	public int countRow(String tableXpath, boolean isXpath) {
		WebElement baseTable;
		if (isXpath) 
		{ 
			baseTable= driver.findElement(By.xpath(tableXpath));
		}else {
			baseTable= driver.findElement(By.cssSelector(tableXpath));
		}
		List<WebElement> tableRows = baseTable.findElements(By.tagName("tr"));
		int sumRow = tableRows.size();
		return sumRow;
	}
	public void selectRow(int rowIndex) {
		WebElement row = driver.findElement(By.xpath(Home.TABLE_BODY_XPATH + "//tr[" + rowIndex + "]"));
		row.click();
	}
	public void assertRowEqual(String obj, String value, int row) {
		WebElement rowFilter = driver.findElement(By.id(obj + row));
		assertEquals(rowFilter.getText(), value, "Title is wrong");
	}

	public void assertRowEqual(By obj, String value) {
		WebElement rowFilter = driver.findElement(obj);
		assertEquals(rowFilter.getText(), value, "Title is wrong");
	}

	public void assertValueRow(int column, int row, String value) {
		assertValueRow(Home.TABLE_BODY_XPATH, column, row, value);
	}

	public void assertValueRow(String tableBodyXpath, int column, int row, String value) {
		WebElement cell = driver.findElement(By.xpath(String.format("%s//tr[%s]//td[%s]", tableBodyXpath, row, column-1)));
		assertEquals(cell.getText(), value);
	}

	public String getValueAllRowchecked(int column, int row) {
		String allValue = "";
		for (int i = 1; i <= row; i++) {
			WebElement cell = driver.findElement(By.xpath(Home.TABLE_BODY_XPATH + "//tr[" + i + "]//td[" + (column-1) + "]"));
			if (i == row) {
				allValue = allValue + cell.getText();
			} else {
				allValue = allValue + cell.getText() + ", ";
			}
		}
		return allValue;
	}
	public WebElement getCellObject(int row, int column) {
		WebElement cell = driver.findElement(By.xpath(String.format("%s//tr[%s]//td[%s]", Home.TABLE_BODY_XPATH, row, column-1)));
		return cell;
	}
	public WebElement getCellObject(String tableXpathToBody, int row, int column) {
		WebElement cell = driver.findElement(By.xpath(String.format("%s/tr[%s]/td[%s]", tableXpathToBody, row, column-1)));
		return cell;
	}

	public String getValueRow(int column, int row) {
		return getValueRow(String.format("%s//tr[%s]//td[%s]", Home.TABLE_BODY_XPATH, row, column-1), column-1, row);
	}

	private String getValueRow(String cellXpath, int column, int row) {
		WebElement cell = driver.findElement(By.xpath(cellXpath));
		action.scrollToElementWithColumnNo(cell, column);
		return cell.getText();
	}
	public WebElement getCellTableHeader(int column) {
		WebElement header = driver.findElement(By.xpath(Home.TABLE_HEAD_XPATH + "//tr//th[" + (column) + "]"));
//		action.clickHorizontalScrollBarToElement(header);
		return header;
	}
	public String getValueTableHeader(int column) {
		WebElement header = driver.findElement(By.xpath(Home.TABLE_HEAD_XPATH + "//tr//th[" + (column) + "]"));
//		action.clickHorizontalScrollBarToElement(header);
		return header.getText();
	}
	public int getHeaderIndex(String columnName) {
		return getHeaderIndex(columnName, Home.TABLE_HEAD_XPATH);
	}
	public int getHeaderIndex(String columnName, String xpath) {
		return getHeaderIndex(columnName, xpath, true);
	}
	public int getHeaderIndex(String columnName, String tablePath, Boolean isXpath) {
		WebElement baseTable;
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		if (isXpath) {
			baseTable = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(tablePath + "//tr")));
		} else {
			baseTable = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(tablePath + " > tr")));
		}
		List<WebElement> tableColumns = baseTable.findElements(By.tagName("th"));
		for (int i = 0; i < tableColumns.size(); i++) {
			action.clickHorizontalScrollBarToElement(tableColumns.get(i));
			if (columnName.equalsIgnoreCase(tableColumns.get(i).getText().trim())) {
				return i + 1;
			}
		}
		return -1;
	}
}
