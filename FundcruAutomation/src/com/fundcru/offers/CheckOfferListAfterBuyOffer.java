package com.fundcru.offers;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Offer;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.objects.pages.TableFunction;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;


@Credentials(user="khuongdainghia@gmail.com", password="loveyou")
public class CheckOfferListAfterBuyOffer extends BaseTestCase {
	int timeout =30,numberDraftFundraiser;
	ScreenAction action;
	TableFunction table;
	JavascriptExecutor executor;
	WebElement fundraiser;
	WebElement parentElement;
	List<WebElement> actionList;
	@Test
	public void AccessToMyOrders() {
		action = new ScreenAction(driver);
		table = new TableFunction(driver);
		executor = (JavascriptExecutor)driver;
		WebElement userLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		userLink.click();
		WebElement myOrder =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.MY_ORDERS_LINK)));
		myOrder.click();
	}
	
	@Test(dependsOnMethods ="AccessToMyOrders",alwaysRun = true )
	public void NavigateToOrdersListScreen() {
		 WebElement fundraiserDonation =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.MY_ORDER_TEXT)));
		 Assert.assertEquals(fundraiserDonation.getText().trim(),"My orders");
		
	}
	@Test(dependsOnMethods ="NavigateToOrdersListScreen",alwaysRun = true )
	public void CheckListOfOrders() {
		 List<WebElement> listOfOrder= driver.findElements(By.className(Offer.MY_ORDER_ITEM_CLASS));
		 action.assertTextEqualCssSelector(Offer.MY_ORDERS_1_NAME, BuyOffer.offerNameStr +" x1");
		 action.assertTextEqualCssSelector(Offer.MY_ORDERS_1_AMOUNT, BuyOffer.orderTotalStr);
//		 Assert.assertEquals(listOfOrder.size(), BuyOffer.noOrders+1);
	}
}
