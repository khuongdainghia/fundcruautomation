package com.fundcru.offers;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;

@Credentials(user="khuongdainghia@gmail.com", password="loveyou")
public class AddOfferToList extends BaseTestCase {
	@Test
	public void addOffer() {
	WebElement userLink =(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Home.LOGINED_USER_LINK)));
	userLink.click();
	
	WebElement myCampaignLink =(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.MY_CAMPAIGN_LINK)));
	myCampaignLink.click();
	
	WebElement createNewCampaign =(new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CREATE_NEW_COMPAIGN)));
	createNewCampaign.click();
	WebElement campaignGoal = (new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.GOAL)));
	campaignGoal.clear();
	campaignGoal.sendKeys("7500");
	WebElement campaignTitle = driver.findElement(By.cssSelector(Campaign.TITLE));
	campaignTitle.clear();
	campaignTitle.sendKeys("Draft Urban Xtreme Adventures");
	WebElement campaignCityCode = driver.findElement(By.cssSelector(Campaign.CITY_CODE));
	campaignCityCode.clear();
	campaignCityCode.sendKeys("Los Angeles, CA");
	WebElement campaignCity_list = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CITY_LIST_1)));
	campaignCityCode.sendKeys(Keys.DOWN);
	campaignCityCode.sendKeys(Keys.ENTER);
	Select campaignPrimaryCategory = new Select( driver.findElement(By.cssSelector(Campaign.PRIMARY_CATERGORY)));
	Select campaignSecondaryCategory = new Select(driver.findElement(By.cssSelector(Campaign.SECONDARY_CATERGORY)));
	campaignPrimaryCategory.selectByVisibleText("Charity");
	campaignSecondaryCategory.selectByVisibleText("Sports");
	WebElement campaignSummary = driver.findElement(By.cssSelector(Campaign.SUMMARY));
	campaignSummary.clear();
	campaignSummary.sendKeys("Draft Summary");
    driver.findElement(By.id(Campaign.UPLOAD_PHOTO)).click();
    WebElement campaignChooseImage = driver.findElement(By.xpath(Campaign.CHOOSE_IMAGE_XPATH));
    campaignChooseImage.clear();
    campaignChooseImage.sendKeys("C:\\Users\\KhuongDai\\Documents\\Untitled.png");
    WebElement uploadButton = (new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.id(Campaign.UPLOAD_BUTTON)));
    JavascriptExecutor js = (JavascriptExecutor) driver;
    js.executeScript("arguments[0].click();",uploadButton);
    WebElement campaignImage =(new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.IMAGE)));
    Assert.assertEquals(campaignImage.getAttribute("class"),"thumbnail");
    WebElement campaignStoryTextArea = driver.findElement(By.cssSelector(Campaign.STORY_TEXTAREA));
    campaignStoryTextArea.clear();
    campaignStoryTextArea.sendKeys("BE PART OF BUILDING SOMETHING GREAT Wouldn???t it be amazing if you could Ski, Snowboard, Bounce and Climb under one roof all year round in Brisbane? With your support we can! We are in the process of building Austrialia???s FIRST Indoor Snow Sport and Adventure Centre! It will be right here in Brisbane within 15km of the CBD, opening mid 2017. In the lead up to our launch, we are now offering DISCOUNTED pre-sale passes for our ski and adventure activities. As a recognition of your support, all pre-sale passes will get limited time discounts as well as other awesome perks???read on to find out more! ");
    driver.findElement(By.cssSelector(Campaign.SAVE_DRAFT)).click();
//    driver.findElement(By.cssSelector(Campaign.SAVE_PUBLISH)).click();    
	}
	@Test(dependsOnMethods ="addOffer" )
	public void CheckCreatedSuccessful() {
			//check result here 
		WebElement campaignEditMode = (new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.SWITCH_TO_EDIT_MODE)));
		String compaignID = driver.getCurrentUrl().substring(driver.getCurrentUrl().lastIndexOf("/")+1,driver.getCurrentUrl().length());
		System.out.println("Campaign ID: " +compaignID);
		Assert.assertEquals(campaignEditMode.getAttribute("href"), String.format("https://user12.fundcru.com/fundraisers/%s/edit",compaignID));
		WebElement campaignTitle = (new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.TITLE_SUCCESS)));
		Assert.assertEquals(campaignTitle.getText(),"Draft Urban Xtreme Adventures");
		WebElement campaignGoal = (new WebDriverWait(driver, 30)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.GOAL_SUCCESS)));
		Assert.assertEquals(campaignGoal.getText(),"$7500.00");
//		WebElement campaignStory = driver.findElement(By.cssSelector(Campaign.STORY_SUCCESS));
//		Assert.assertEquals(campaignStory.getText(),"BE PART OF BUILDING SOMETHING GREAT Wouldn???t it be amazing if you could Ski, Snowboard, Bounce and Climb under one roof all year round in Brisbane? With your support we can! We are in the process of building Austrialia???s FIRST Indoor Snow Sport and Adventure Centre! It will be right here in Brisbane within 15km of the CBD, opening mid 2017. In the lead up to our launch, we are now offering DISCOUNTED pre-sale passes for our ski and adventure activities. As a recognition of your support, all pre-sale passes will get limited time discounts as well as other awesome perks???read on to find out more!");
		WebElement campaignPrimary_Category = driver.findElement(By.cssSelector(Campaign.SECONDARY_CATERGORY_SUCCESS));
		Assert.assertEquals(campaignPrimary_Category.getText(),"Sports");
		WebElement campaignSecondary_Category = driver.findElement(By.cssSelector(Campaign.PRIMARY_CATERGORY_SUCCESS));
		Assert.assertEquals(campaignSecondary_Category.getText(),"Charity");
		WebElement campaignCity = driver.findElement(By.cssSelector(Campaign.CITY_SUCCESS));
		Assert.assertEquals(campaignCity.getText(),"Los Angeles, CA");
	}
}
