package com.fundcru.offers;

import static org.testng.Assert.assertEquals;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.FaceBook;
import com.fundcru.objects.pagedefinitions.Offer;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.utilities.BaseTestCaseNoCredential;

public class ShareOfferToFacebook extends BaseTestCaseNoCredential {
	int timeout =30;
	String email="khuongdainghia@outlook.com",pass="123loveyou", name="Cru Nick",offerNameStr;
	ScreenAction action;
	String mwh;
	@Test
	public void ClickSignInLink() throws Exception {
		action = new ScreenAction(driver);
		driver.navigate().to(getServerURL()+"/offers/7KG6BbMm7CvSensuS");
		WebElement offerTitle = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.OFFER_TITLE)));
		offerNameStr= offerTitle.getText();
	}
	
	@Test(dependsOnMethods ="ClickSignInLink",alwaysRun = true )
	public void CheckButtonDisplayed() {
		WebElement btn = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.SHARE_FACEBOOK)));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", btn);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", btn);
	}
	@Test(dependsOnMethods ="CheckButtonDisplayed",alwaysRun = true )
	public void CheckTheFaceBookLogin() {
		mwh=driver.getWindowHandle();
		Set s=driver.getWindowHandles(); //this method will gives the handles of all opened windows
		Iterator ite=s.iterator();
		while(ite.hasNext())
		{
		    String popupHandle=ite.next().toString();
		    if(!popupHandle.contains(mwh))
		    {
		        driver.switchTo().window(popupHandle);
		        ((JavascriptExecutor) driver).executeScript("window.showModalDialog = window.open;");
		        driver.getCurrentUrl();
		        boolean facebookWindow = false;
		        if(driver.getCurrentUrl().startsWith("https://www.facebook.com")){
		        	facebookWindow = true;
		        }
		        assertEquals(facebookWindow, true);
		        driver.findElement(By.id(FaceBook.FACEBOOK_HOME_LINK_ID));
		        driver.findElement(By.id(FaceBook.FACEBOOK_USERNAME_FIELD_ID)).sendKeys(email);
		        driver.findElement(By.id(FaceBook.FACEBOOK_PASSWORD_FIELD_ID)).sendKeys(pass);
		        driver.findElement(By.id(FaceBook.FACEBOOK_LOGIN_BUTTON_ID)).click();
				
		    }
		    
		}
		
	}
	@Test(dependsOnMethods ="CheckTheFaceBookLogin",alwaysRun = true )
	public void PostToFaceBook() {
		WebElement postToFaceBook =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(FaceBook.POST_TO_FACEBOOK )));
		postToFaceBook.click();
		try {
	    	 driver.switchTo().window(mwh);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	@Test(dependsOnMethods ="PostToFaceBook",alwaysRun = true )
	public void CheckBackToDefaultScreen() {

		WebElement offerTitle = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.OFFER_TITLE)));
		Assert.assertEquals(offerTitle.getText(),offerNameStr);
	}
	
}
