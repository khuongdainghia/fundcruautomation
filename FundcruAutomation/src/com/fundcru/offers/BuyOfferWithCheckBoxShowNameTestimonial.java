package com.fundcru.offers;


import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.Offer;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;

@Credentials(user="khuongdainghia@gmail.com", password="loveyou")
public class BuyOfferWithCheckBoxShowNameTestimonial extends BaseTestCase {
	float campaignRaised=0;
	float donateAmountOffer=0;
	int timeout = 30;
	List<WebElement> numOfTestimonial;
	int numberBeforeDonate,numOfDonors;
	String offerAmountStr,offerPriceStr;
	String campaignName;
	String campaignGoalValue;
	ScreenAction action;
	public static String testImonialName="";
	public static String testImonial="";
	public static String donateAmountStr;
	public static String fundraiserID="YB2oCspPAfsP4xSZt";
	@Test
	public void checkFundraiserBeforeBuyingOffer() {
		action = new ScreenAction(driver);
		driver.get(getServerURL() + "/fundraisers/" + fundraiserID);
		WebElement campaignRaisedElement= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CAMPAIGN_RAISED)));
		campaignRaised= Float.valueOf(campaignRaisedElement.getText().replace("$", "").replace(",", ""));
		WebElement campaignNameElement = driver.findElement(By.cssSelector(Campaign.CAMPAIGN_TITLES));
		campaignName = campaignNameElement.getText();
		WebElement campaignGoal = driver.findElement(By.cssSelector(Campaign.GOAL_SUCCESS));
		campaignGoalValue = campaignGoal.getText();
		numOfTestimonial = driver.findElements(By.xpath(Campaign.TESTIMONIAL_LIST));
	    numberBeforeDonate = numOfTestimonial.size();
	    WebElement campaignDonors= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CAMPAIGN_DONORS)));
		numOfDonors = Integer.valueOf(campaignDonors.getText());
	}
	@Test(dependsOnMethods="checkFundraiserBeforeBuyingOffer")
	public void accessToOffer() 
	{
		driver.get(getServerURL() + "/offers/25AidTuiy4RpuQwJH");
	}
	@Test(dependsOnMethods="accessToOffer")
	public void clickOnBuyOffer() 
	{
		WebElement buyButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Offer.BUY_BUTTON)));
		WebElement offerPrice = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.OFFER_PRICE)));
		offerPriceStr = offerPrice.getText();
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",buyButton);
	}
	@Test(dependsOnMethods="clickOnBuyOffer")
	public void checkDonateAmount() 
	{
		WebElement offerAmount = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.ITEM_PRICE)));
		offerAmountStr = offerAmount.getText();
		WebElement donateAmount = driver.findElement(By.cssSelector(Offer.DONATE_AMOUNT));
		donateAmountStr = donateAmount.getText();
		donateAmountStr = donateAmountStr.substring(donateAmountStr.indexOf("$")+1, donateAmountStr.length());
		donateAmountOffer = Float.valueOf(donateAmountStr);
	}
	@Test(dependsOnMethods="checkDonateAmount")
	public void changeFundraiser() 
	{
		WebElement changeCampaignLink= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated((By.cssSelector(Offer.CHANGE_CAMPAIGN_LINK))));
		changeCampaignLink.click();
		WebElement searchCampaign= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.name(Offer.SEARCH_CAMPAIGN)));
		action.pause(1000);
		searchCampaign.sendKeys("Marley Pickles Medical Bills");
		action.pause(3000);
		(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.SEARCH_AUTOCOMPLETE_RESULT)));
		searchCampaign.sendKeys(Keys.DOWN);
		searchCampaign.sendKeys(Keys.ENTER);
	}
	@Test(dependsOnMethods="changeFundraiser")
	public void clickOnCheckOutButton() 
	{
		WebElement checkoutButton = driver.findElement(By.id(Offer.CHECKOUT_BUTTON));
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",checkoutButton);
	}
	@Test(dependsOnMethods="clickOnCheckOutButton")
	public void inputCardInformation() 
	{
		WebElement fullName = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Offer.DONATE_NAME)));
	    fullName.clear();
	    fullName.sendKeys("Charlie Angles");
	    WebElement cardNumber = driver.findElement(By.id(Offer.CARD_NUMBER));
		cardNumber.clear();
	    cardNumber.sendKeys("4032038120259944");
	    WebElement ccv = driver.findElement(By.id(Offer.CVV));
	    ccv.clear();
	    ccv.sendKeys("123");
	    WebElement expiRation = driver.findElement(By.id(Offer.EXPIRATION));
	    expiRation.clear();
	    expiRation.sendKeys("11/22");
	    WebElement zipCode = driver.findElement(By.id(Offer.ZIPCODE));
	    zipCode.clear();
	    zipCode.sendKeys("12345");
	}
    @Test(dependsOnMethods ="inputCardInformation" )
	public void checkAgainAmountAndCardInformation() { 
    	WebElement amount = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.CONFIRM_OFFER_AMOUNT_CARD_SCREEN)));
 	    Assert.assertEquals(offerAmountStr.replace(".00", ""),amount.getText().replace(".00", ""));
    	WebElement donationAmount = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.CONFIRM_DONATION_AMOUNT_CARD_SCREEN)));
    	Assert.assertEquals(donateAmountStr,donationAmount.getText().replace("$", "").replace(".00", ""));
    	WebElement quantity = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.CONFIRM_QUANTITY_CARD_SCREEN)));
    	Assert.assertEquals("1",quantity.getText());
    	driver.findElement(By.cssSelector(Offer.SUBMIT_BUTTON_CARD_SCREEN)).click();
    }
    @Test(dependsOnMethods ="checkAgainAmountAndCardInformation" )
	public void inputInformationForTestimonial() { 
	    WebElement donateName = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Offer.TESTIMONIAL_NAME)));
	    Random r = new Random();
	    int i = r.nextInt(100);
	    donateName.clear();
	    testImonialName = String.format("Nicholas %s", i);
	    donateName.sendKeys(testImonialName);
	    WebElement testimonialElement= driver.findElement(By.id(Offer.TESTIMONIAL_TEXT));
	    testimonialElement.clear();
	    testImonial = String.format("Testimonial %s",i);
	    testimonialElement.sendKeys(testImonial);
	    WebElement testimonialEmailElement= driver.findElement(By.id(Offer.TESTIMONIAL_EMAIL));
	    testimonialEmailElement.clear();
	    testimonialEmailElement.sendKeys("khuongdainghia02t5@gmail.com");
	    WebElement testimonialShowNameCheckbox= driver.findElement(By.cssSelector(Offer.TESTIMONIAL_SHOWNAME_CHECKBOX));
	    testimonialShowNameCheckbox.click();
	    
	    
	}
	@Test(dependsOnMethods ="inputInformationForTestimonial" )
	public void clickSubmitButton() {
	    driver.findElement(By.cssSelector(Offer.SUBMIT_TESTIMONIAL_BUTTON)).click();
	    
	}
	@Test(dependsOnMethods ="clickSubmitButton",alwaysRun = true )
	public void checkBuyOfferSuccessfully() 
	{
	    (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.VIEW_ORDER)));
	}
	@Test(dependsOnMethods ="checkBuyOfferSuccessfully" )
	public void CheckDonatedByOfferSuccessful() {
			//check result here 
		driver.get(getServerURL() + "/raise/kl9vJoD2M");
		action.pause(30000);
		WebElement campaignTitle = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.TITLE_SUCCESS)));
		Assert.assertEquals(campaignTitle.getText(),campaignName);
		WebElement campaignGoal = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.GOAL_SUCCESS)));
		Assert.assertEquals(campaignGoal.getText(),campaignGoalValue);
		numOfTestimonial = driver.findElements(By.xpath(Campaign.TESTIMONIAL_LIST));
		action.waitObjInvisible(By.cssSelector(Campaign.CAMPAIGN_RAISED_PROGRESS));
		action.pause(30000);
		WebElement campaignDonors= driver.findElement(By.cssSelector(Campaign.CAMPAIGN_DONORS));
		Assert.assertEquals(numberBeforeDonate, numOfTestimonial.size());
		action.waitObjInvisible(By.cssSelector(Campaign.CAMPAIGN_RAISED_PROGRESS));
		WebElement campaignRaisedElement=(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CAMPAIGN_RAISED)));
		Assert.assertEquals(campaignRaisedElement.getText().replace(",", "").replace(".00", "").replace(".0", ""),String.format("$%s",campaignRaised+donateAmountOffer).replace(".00", "").replace(".0", ""));
		WebElement testimonialName = driver.findElement(By.cssSelector(Campaign.TESTIMONIAL_NAME));
		Assert.assertEquals(testimonialName.getText(),"Anonymous");
		Assert.assertEquals(campaignDonors.getText().trim(),String.valueOf(numOfDonors+1));
	}
}
