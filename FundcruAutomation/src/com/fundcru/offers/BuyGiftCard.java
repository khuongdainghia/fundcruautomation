package com.fundcru.offers;

import static org.testng.AssertJUnit.assertEquals;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.objects.pagedefinitions.Offer;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;


@Credentials(user="khuongdainghia@gmail.com", password="loveyou")
public class BuyGiftCard extends BaseTestCase {
	float campaignRaised=0;
	float donateAmountOffer=0;
	List<WebElement> numOfTestimonial;
	int numberBeforeDonate,numOfDonors,noGiftCardLeft=79;
	String testImonialName="";
	String testImonial="";
	String campaignName;
	String campaignGoalValue;
	int timeout =30;
	WebElement offerAmount,orderTotal,offerQuantity,donateAmount,giftCardLeft,giftCardValidation;
	String offerAmountStr,orderTotalStr,donateAmountStr;
	ScreenAction action;
	@Test
	public void buyOfferAndGetTestimonialBeforeBuyingOfffer() {
		action = new ScreenAction(driver);
		//Check CampainRasied before buying offer
		driver.get(getServerURL() + "/fundraisers/SqXRSK9FoZGRipWrv");
		WebElement campaignRaisedElement= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CAMPAIGN_RAISED)));
		campaignRaised= Float.valueOf(campaignRaisedElement.getText().replace("$", "").replace(".00", "").replace(",", ""));
		WebElement campaignNameElement = driver.findElement(By.cssSelector(Campaign.CAMPAIGN_TITLES));
		campaignName = campaignNameElement.getText();
		WebElement campaignGoal = driver.findElement(By.cssSelector(Campaign.GOAL_SUCCESS));
		campaignGoalValue = campaignGoal.getText();
		numOfTestimonial = driver.findElements(By.xpath(Campaign.TESTIMONIAL_LIST));
	    numberBeforeDonate = numOfTestimonial.size();
	    WebElement campaignDonors= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CAMPAIGN_DONORS)));
		numOfDonors = Integer.valueOf(campaignDonors.getText());
	}
	@Test(dependsOnMethods ="buyOfferAndGetTestimonialBeforeBuyingOfffer" )
	public void openOffer() {
		//Navigate to offer
				driver.get(getServerURL() + "/offers/wgQsqujes42Rz7yqE");
	}
	@Test(dependsOnMethods ="openOffer" )
	public void clickBuyButton() {
		
		WebElement buyButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Offer.BUY_BUTTON)));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		
	    js.executeScript("arguments[0].click();",buyButton);
	    WebElement campaigNameLink = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.CAMPAIGN_NAME_LINK)));
		Assert.assertTrue(!campaigNameLink.getText().isEmpty(),"Default Fundraiser is empty");
	}
	@Test(dependsOnMethods ="clickBuyButton" )
	public void checkOfferAmount() {    
	    offerQuantity= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.OFFER_QUANTITY)));
		offerAmount = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.ITEM_PRICE)));
		offerAmountStr = offerAmount.getText();
		orderTotal = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.ORDER_TOTAL)));
		orderTotalStr = orderTotal.getText();
		float orderTotalFloat = Float.valueOf(orderTotalStr.substring(orderTotalStr.indexOf("$")+1, orderTotalStr.length()));
		donateAmount = driver.findElement(By.cssSelector(Offer.DONATE_AMOUNT));
		donateAmountStr = donateAmount.getText();
		donateAmountStr = donateAmountStr.substring(donateAmountStr.indexOf("$")+1, donateAmountStr.length());
		donateAmountOffer = Float.valueOf(donateAmountStr);
		offerQuantity.clear();
		offerQuantity.sendKeys("2");
		action.pause(3000);
		donateAmountStr = donateAmount.getText();
		donateAmountStr = donateAmountStr.substring(donateAmountStr.indexOf("$")+1, donateAmountStr.length());
		Assert.assertEquals(Float.valueOf(donateAmountStr),donateAmountOffer*2);
		Assert.assertEquals(Float.valueOf(orderTotal.getText().replace("$", "").trim()),orderTotalFloat*2);
		
	}
	@Test(dependsOnMethods ="checkOfferAmount" )
	public void getGiftCardleft() { 
		giftCardLeft = driver.findElement(By.cssSelector(Offer.GIFT_CARD_LEFT));
		String cardLeft=giftCardLeft.getText();
		noGiftCardLeft = Integer.valueOf(cardLeft.substring(0, cardLeft.indexOf(" ")));
	}
	@Test(dependsOnMethods ="getGiftCardleft" )
	public void changeQuantityOfferToOne() { 
		offerQuantity.clear();
		offerQuantity.sendKeys("1");
		donateAmountStr = donateAmount.getText();
		donateAmountStr = donateAmountStr.substring(donateAmountStr.indexOf("$")+1, donateAmountStr.length());
	}
	@Test(dependsOnMethods ="changeQuantityOfferToOne" )
	public void changeTheCampaignFromList() { 
		WebElement changeCampaignLink= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.CHANGE_CAMPAIGN_LINK)));
		changeCampaignLink.click();
		WebElement searchCampaign= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.name(Offer.SEARCH_CAMPAIGN)));
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].scrollIntoView(true);",searchCampaign);	
	    action.pause(1000);
	    searchCampaign.sendKeys("ChefsGiving: chefs, restaurants, wineries");
		action.pause(3000);
		(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.SEARCH_AUTOCOMPLETE_RESULT)));
		searchCampaign.sendKeys(Keys.DOWN);
		searchCampaign.sendKeys(Keys.ENTER);
		action.pause(3000);
		WebElement campaigNameLink = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.CAMPAIGN_NAME_LINK)));
		Assert.assertEquals(campaigNameLink.getText(), campaignName);
		
	}
	@Test(dependsOnMethods ="changeTheCampaignFromList" )
	public void clickOnCheckoutButton() { 
		WebElement checkoutButton = driver.findElement(By.id(Offer.CHECKOUT_BUTTON));
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",checkoutButton);
	}
	@Test(dependsOnMethods ="clickOnCheckoutButton" )
	public void inputCardInformatioin() { 
		WebElement fullName = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Offer.DONATE_NAME)));
	    fullName.clear();
	    fullName.sendKeys("Charlie Angles");
	    WebElement cardNumber = driver.findElement(By.id(Offer.CARD_NUMBER));
		cardNumber.clear();
	    cardNumber.sendKeys("4032038120259944");
	    WebElement ccv = driver.findElement(By.id(Offer.CVV));
	    ccv.clear();
	    ccv.sendKeys("123");
	    WebElement expiRation = driver.findElement(By.id(Offer.EXPIRATION));
	    expiRation.clear();
	    expiRation.sendKeys("11/22");
	    WebElement zipCode = driver.findElement(By.id(Offer.ZIPCODE));
	    zipCode.clear();
	    zipCode.sendKeys("12345");
	}
    @Test(dependsOnMethods ="inputCardInformatioin" )
	public void checkAgainAmountAndCardInformation() { 
    	WebElement amount = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.CONFIRM_OFFER_AMOUNT_CARD_SCREEN)));
 	    assertEquals(offerAmountStr.replace("$", "").replace(".00", ""),amount.getText().replace("$", "").replace(".00", ""));
    	WebElement donationAmount = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.CONFIRM_DONATION_AMOUNT_CARD_SCREEN)));
 	    assertEquals(donateAmountStr.replace("$", "").replace(".00", ""),donationAmount.getText().replace("$", "").replace(".00", ""));
    	WebElement quantity = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.CONFIRM_QUANTITY_CARD_SCREEN)));
 	    assertEquals("1",quantity.getText());
    	driver.findElement(By.cssSelector(Offer.SUBMIT_BUTTON_CARD_SCREEN)).click();
    }
    @Test(dependsOnMethods ="checkAgainAmountAndCardInformation" )
	public void inputInformationForTestimonial() { 
	    WebElement donateName = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Offer.TESTIMONIAL_NAME)));
	    Random r = new Random();
	    int i = r.nextInt(100);
	    donateName.clear();
	    testImonialName = String.format("Nicholas %s", i);
	    donateName.sendKeys(testImonialName);
	    WebElement testimonialElement= driver.findElement(By.id(Offer.TESTIMONIAL_TEXT));
	    testimonialElement.clear();
	    testImonial = String.format("Testimonial %s",i);
	    testimonialElement.sendKeys(testImonial);
	    WebElement testimonialEmailElement= driver.findElement(By.id(Offer.TESTIMONIAL_EMAIL));
	    testimonialEmailElement.clear();
	    testimonialEmailElement.sendKeys("khuongdainghia@gmail.com");
	}
	@Test(dependsOnMethods ="inputInformationForTestimonial" )
	public void clickSubmitButton() {
	    driver.findElement(By.cssSelector(Offer.SUBMIT_TESTIMONIAL_BUTTON)).click();
	    (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.VIEW_ORDER)));
	}
	@Test(dependsOnMethods ="clickSubmitButton" )
	public void checkDonatedByOfferSuccessful() {
			//check result here 
		driver.get(getServerURL() + "/fundraisers/SqXRSK9FoZGRipWrv");
		action.pause(60000);
		WebElement campaignTitle = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.TITLE_SUCCESS)));
		Assert.assertEquals(campaignTitle.getText(),campaignName);
		WebElement campaignGoal = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.GOAL_SUCCESS)));
		Assert.assertEquals(campaignGoal.getText(),campaignGoalValue);
		action.waitObjInvisible(By.cssSelector(Campaign.CAMPAIGN_RAISED_PROGRESS));
		action.pause(30000);
		WebElement campaignRaisedElement= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CAMPAIGN_RAISED)));
		Assert.assertEquals(campaignRaisedElement.getText().replace(",", "").replace(".00", ""),String.format("$%s",campaignRaised+donateAmountOffer).replace(".0", ""));
		WebElement campaignDonors= driver.findElement(By.cssSelector(Campaign.CAMPAIGN_DONORS));
		Assert.assertTrue(Integer.valueOf(campaignDonors.getText().trim())==(numOfDonors+1));
		WebElement testimonialName = driver.findElement(By.cssSelector(Campaign.TESTIMONIAL_NAME));
		WebElement testimonialText = driver.findElement(By.cssSelector(Campaign.TESTIMONIAL_TEXT));
		Assert.assertEquals(testimonialName.getText(),testImonialName);
		Assert.assertEquals(testimonialText.getText(),testImonial);
//		Assert.assertEquals(numOfTestimonial.size(),numberBeforeDonate+1);
	}
	@Test(dependsOnMethods ="checkDonatedByOfferSuccessful",alwaysRun =true )
	public void openOfferAgain() {
		action = new ScreenAction(driver);
		//Navigate to offer
		driver.get(getServerURL() + "/offers/wgQsqujes42Rz7yqE");
	}
	@Test(dependsOnMethods ="openOfferAgain" )
	public void clickBuyButtonAgain() {
		
		WebElement buyButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Offer.BUY_BUTTON)));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		
	    js.executeScript("arguments[0].click();",buyButton);
	}
	@Test(dependsOnMethods ="clickBuyButtonAgain" )
	public void checkRemainingOfGiftCard() {
		giftCardLeft = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.GIFT_CARD_LEFT)));
		String cardLeft=giftCardLeft.getText();
		Assert.assertEquals(cardLeft.substring(0, cardLeft.indexOf(" ")).trim(), String.valueOf(noGiftCardLeft-1));
	}
	@Test(dependsOnMethods ="checkRemainingOfGiftCard",alwaysRun =true )
	public void checkValidationOfGiftCardWhenClickDerementButton() {    
		WebElement decrementButton= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Offer.DECREMENT_QUANTITY)));
		decrementButton.click();
		action.pause(1000);
		giftCardValidation = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.GIFT_CARD_VALIDATION)));
		Assert.assertEquals(giftCardValidation.getText(), Messages.MINIMUM_NUMBER_GIFT_CARD);
	}
	@Test(dependsOnMethods ="checkValidationOfGiftCardWhenClickDerementButton" )
	public void checkValidationOfGiftCardWithZero() {    
		offerQuantity= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.OFFER_QUANTITY)));
		offerQuantity.clear();
		offerQuantity.sendKeys("0");
		action.pause(1000);
		giftCardValidation = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.GIFT_CARD_VALIDATION)));
		Assert.assertEquals(giftCardValidation.getText(), Messages.MINIMUM_NUMBER_GIFT_CARD);
	}
	@Test(dependsOnMethods ="checkValidationOfGiftCardWithZero" )
	public void checkValidationOfGiftCardWithGreaterThanTen() {    
		offerQuantity.clear();
		offerQuantity.sendKeys("11");
		action.pause(1000);
		giftCardValidation = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.GIFT_CARD_VALIDATION)));
		Assert.assertEquals(giftCardValidation.getText(), Messages.MAXIMUM_NUMBER_GIFT_CARD);
	}
	@Test(dependsOnMethods ="checkValidationOfGiftCardWithGreaterThanTen" )
	public void checkValidationOfGiftCardWhenClickIncrementButton() {    
		offerQuantity.clear();
		offerQuantity.sendKeys("10");
		WebElement incrementButton= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Offer.INCREMENT_QUANTITY)));
		incrementButton.click();
		action.pause(1000);
		giftCardValidation = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.GIFT_CARD_VALIDATION)));
		Assert.assertEquals(giftCardValidation.getText(), Messages.MAXIMUM_NUMBER_GIFT_CARD);
	}
}
