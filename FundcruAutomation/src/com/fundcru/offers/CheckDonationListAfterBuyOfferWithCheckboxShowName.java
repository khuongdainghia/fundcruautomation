package com.fundcru.offers;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.objects.pages.TableFunction;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;


@Credentials(user="ptvu.it@gmail.com", password="123456789")
public class CheckDonationListAfterBuyOfferWithCheckboxShowName extends BaseTestCase {
	int timeout =30,numberDraftFundraiser;
	ScreenAction action;
	TableFunction table;
	JavascriptExecutor executor;
	WebElement fundraiser;
	WebElement parentElement;
	List<WebElement> actionList;
	@Test
	public void AccessToMyFundraiser() {
		action = new ScreenAction(driver);
		table = new TableFunction(driver);
		executor = (JavascriptExecutor)driver;
		WebElement userLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		userLink.click();
		WebElement myFundraiser =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.MY_CAMPAIGN_LINK)));
		myFundraiser.click();
	}
	
	@Test(dependsOnMethods ="AccessToMyFundraiser",alwaysRun = true )
	public void CheckAction() {
		fundraiser =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(String.format(Campaign.FUNDRAISER_ITEM_ID,BuyOfferWithCheckBoxShowNameTestimonial.fundraiserID))));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", fundraiser);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", fundraiser);
		
		parentElement = (WebElement)executor.executeScript("return arguments[0].parentNode;", fundraiser);
		actionList = parentElement.findElements(By.className(Campaign.ACTION_ITEM_CLASS));
		Assert.assertTrue(actionList.get(0).getText().trim().contains("Edit"),String.format("Wrong expected: Edit but Actual: %s",actionList.get(0).getText().trim()));
		Assert.assertTrue(actionList.get(1).getText().trim().contains("Make private"),String.format("Wrong expected: Publish but Actual: %s",actionList.get(1).getText().trim()));
		Assert.assertTrue(actionList.get(2).getText().trim().contains("Show donations"),String.format("Wrong expected: Show donations but Actual: %s",actionList.get(2).getText().trim()));
	}
	@Test(dependsOnMethods ="CheckAction",alwaysRun = true )
	public void ClickShowDonationsAction() {
		action.pause(30000);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", fundraiser);
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", actionList.get(2));
	}
	@Test(dependsOnMethods ="ClickShowDonationsAction",alwaysRun = true )
	public void NavigateToDonationListScreen() {
		 WebElement fundraiserDonation =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.DONATION_LIST_LABLE)));
		 Assert.assertEquals(fundraiserDonation.getText().trim(),"Donation list");
		 WebElement donationTime = table.getCellTableHeader(6);
		 Assert.assertEquals(donationTime.getText().trim(), "Donated at");
		 ((JavascriptExecutor) driver).executeScript("arguments[0].click();", donationTime.findElement(By.tagName("button")));
		 action.pause(3000);
		 table.assertValueRow(2, 1, BuyOfferWithCheckBoxShowNameTestimonial.testImonialName);
		 table.assertValueRow(3, 1, "khuongdainghia02t5@gmail.com");
		 table.assertValueRow(4, 1, BuyOfferWithCheckBoxShowNameTestimonial.testImonial);
		 Assert.assertEquals(table.getCellObject(1, 5).getText().replace("$", "").replace(".00", "") , BuyOfferWithCheckBoxShowNameTestimonial.donateAmountStr);
	}
	@Test(dependsOnMethods ="NavigateToDonationListScreen",alwaysRun = true  )
	public void checkDonatedByOfferWithOwnerUser() {
			//check result here 
		driver.get(getServerURL() + "/fundraisers/" + BuyOfferWithCheckBoxShowNameTestimonial.fundraiserID);
		(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.TITLE_SUCCESS)));
		action.waitObjInvisible(By.cssSelector(Campaign.CAMPAIGN_RAISED_PROGRESS));
		action.pause(3000);
		WebElement testimonialName = driver.findElement(By.cssSelector(Campaign.TESTIMONIAL_NAME));
		WebElement testimonialText = driver.findElement(By.cssSelector(Campaign.TESTIMONIAL_TEXT));
		Assert.assertEquals(testimonialName.getText(),BuyOfferWithCheckBoxShowNameTestimonial.testImonialName);
		Assert.assertEquals(testimonialText.getText(),BuyOfferWithCheckBoxShowNameTestimonial.testImonial);
	}
}
