package com.fundcru.offers;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Messages;
import com.fundcru.objects.pagedefinitions.Offer;
import com.fundcru.objects.pagedefinitions.TextLabels;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.objects.pages.TableFunction;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;

@Credentials(user="khuongdainghia@gmail.com", password="loveyou")
public class AddOfferToFavorite extends BaseTestCase {
	int timeout = 30;
	TableFunction table;
	String offerNameStr;
	JavascriptExecutor js;
	ScreenAction action;
	
	@Test
	public void clickUserLinkI() {
		table = new TableFunction(driver);
		action = new ScreenAction(driver);
		js = (JavascriptExecutor) driver;
		WebElement userLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		userLink.click();
	}
	
	@Test(dependsOnMethods ="clickUserLinkI",alwaysRun = true)
		public void clickMyfavouriteOfferI() {
		WebElement MY_FAVORITE_ORDERS_LINK =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.MY_FAVORITE_OFFERS_LINK)));
		MY_FAVORITE_ORDERS_LINK.click();
	}
	
	@Test(dependsOnMethods ="clickMyfavouriteOfferI",alwaysRun = true)
		public void checkMyFavouriteOfferScreenDisplaysI() {
		WebElement alertMessageNoOfferInFavoriteList = new WebDriverWait(driver, timeout).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.ALERT_MESSAGE_NO_OFFER_EXIST)));
		Assert.assertEquals(alertMessageNoOfferInFavoriteList.getText(), Messages.ALERT_MESSAGE_NO_OFFER_EXIST);
		driver.findElement(By.cssSelector(Offer.RECOMMENDED_OFFER_LIST)).getText().startsWith(TextLabels.RECOMMENDED_OFFER);
	}
	
	@Test(dependsOnMethods ="checkMyFavouriteOfferScreenDisplaysI",alwaysRun = true)
	public void clickBrowserOffersLink() {
		driver.findElement(By.cssSelector(Home.BROWSER_OFFERS)).click();
	}
	
	@Test(dependsOnMethods ="clickBrowserOffersLink",alwaysRun = true)
	public void checkOffersScreenDisplays() {
		WebElement FOOD_AND_DRINK_BUTTON =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.FOOD_AND_DRINK_BUTTON)));
		FOOD_AND_DRINK_BUTTON.isDisplayed();
		driver.findElement(By.cssSelector(Offer.GOODS_AND_RETAIL_BUTTON)).isDisplayed();
		Assert.assertEquals(driver.findElement(By.cssSelector(Offer.POPULAR_OFFERS_TEXT)).getText(), TextLabels.POPULAR_OFFERS_LABEL);
		Assert.assertEquals(driver.findElement(By.cssSelector(Offer.LOCAL_OFFERS_TEXT)).getText(), TextLabels.LOCAL_OFFERS_TEXT);
	}
	
	@Test(dependsOnMethods ="checkOffersScreenDisplays",alwaysRun = true)
	public void navigateToOffer() {
		//Navigate to offer
		driver.get(getServerURL() + "/offers/25AidTuiy4RpuQwJH");
	}
	
	@Test(dependsOnMethods ="navigateToOffer",alwaysRun = true)
	public void checkOfferTitleI() {
		// Check offer title
		WebElement offerTitle = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.OFFER_TITLE)));
		offerNameStr= offerTitle.getText();
		
	}
	
	@Test(dependsOnMethods ="checkOfferTitleI",alwaysRun = true)
	public void clickAddToFavouriteButton() {
		// Click Add To My Favourite
		WebElement addToFavouriteButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Offer.ADD_TO_FAVORITE_BUTTON)));
	    js.executeScript("arguments[0].click();",addToFavouriteButton);
	    (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Offer.REMOVE_FROM_FAVORITE_BUTTON)));
	}
	
	@Test(dependsOnMethods ="clickAddToFavouriteButton",alwaysRun = true)
	public void checkButtonIsClicked() {
		// Check Remove From Favorite button display
		WebElement removeFromFavouriteButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Offer.REMOVE_FROM_FAVORITE_BUTTON)));
		removeFromFavouriteButton.isDisplayed();
	}
	
	@Test(dependsOnMethods ="checkButtonIsClicked",alwaysRun = true)
	public void clickUserLinkII(){
		WebElement userLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		userLink.click();
	}
	
	@Test(dependsOnMethods ="clickUserLinkII",alwaysRun = true)
	public void clickMyfavouriteOfferII() {
	WebElement MY_FAVORITE_ORDERS_LINK =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.MY_FAVORITE_OFFERS_LINK)));
	MY_FAVORITE_ORDERS_LINK.click();
	}
	
	@Test(dependsOnMethods ="clickMyfavouriteOfferII",alwaysRun = true)
	public void checkMyFavouriteOfferScreenWithOfferAdded() {
		(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(Home.TABLE_BODY_XPATH)));
	 	WebElement cell= table.getCellObject(1, 2);
		Assert.assertTrue(cell.getAttribute("innerHTML").contains(offerNameStr),"Wrong expected Offer! Expected: "+ offerNameStr+ "Actual: " +cell.getAttribute("innerHTML"));
	}
	
	@Test(dependsOnMethods ="checkMyFavouriteOfferScreenWithOfferAdded",alwaysRun = true)
	public void navigateToOfferAgain() {
		driver.get(getServerURL() + "/offers/25AidTuiy4RpuQwJH");
	}
	
	@Test(dependsOnMethods ="navigateToOfferAgain",alwaysRun = true)
	public void checkOfferTitleII() {
		// Check offer title
		WebElement offerTitle = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.OFFER_TITLE)));
		Assert.assertEquals(offerTitle.getText(),"HUNAN-STYLE HOT AND SPICY CHICKEN");
	}
	
	@Test(dependsOnMethods ="checkOfferTitleII",alwaysRun = true)
	public void clickRemoveFromFavoriteButton() {
		// Click Remove From Favourite
		WebElement removeFromFavouriteButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Offer.REMOVE_FROM_FAVORITE_BUTTON)));
		
	    js.executeScript("arguments[0].click();",removeFromFavouriteButton);
	}
	
	@Test(dependsOnMethods ="clickRemoveFromFavoriteButton",alwaysRun = true)
	public void checkAddToFavoriteButtonDisplay() {
		// Check Add to My Favorite button display
		WebElement addToFavouriteButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Offer.ADD_TO_FAVORITE_BUTTON)));
		addToFavouriteButton.isDisplayed();
	}
	
	@Test(dependsOnMethods ="checkAddToFavoriteButtonDisplay",alwaysRun = true)
	public void clickUserLinkIII(){
		WebElement userLink =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		userLink.click();
	}
	
	@Test(dependsOnMethods ="clickUserLinkIII",alwaysRun = true)
		public void clickMyfavouriteOfferIII() {
		WebElement MY_FAVORITE_ORDERS_LINK =(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.MY_FAVORITE_OFFERS_LINK)));
		MY_FAVORITE_ORDERS_LINK.click();
	}
	
	@Test(dependsOnMethods ="clickMyfavouriteOfferIII",alwaysRun = true)
	public void checkMyFavouriteOfferScreenWithOfferRemoved() {
		WebElement alertMessageNoOfferInFavoriteList = new WebDriverWait(driver, timeout).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.ALERT_MESSAGE_NO_OFFER_EXIST)));
		Assert.assertEquals(alertMessageNoOfferInFavoriteList.getText(), Messages.ALERT_MESSAGE_NO_OFFER_EXIST);
		driver.findElement(By.cssSelector(Offer.RECOMMENDED_OFFER_LIST)).getText().startsWith(TextLabels.RECOMMENDED_OFFER);
	}
//----------------------
	@Test(dependsOnMethods ="checkMyFavouriteOfferScreenWithOfferRemoved",alwaysRun = true)
	public void navigateToOfferOneMoreTime() {
		driver.get(getServerURL() + "/offers/25AidTuiy4RpuQwJH");
		WebElement addToFavouriteButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Offer.ADD_TO_FAVORITE_BUTTON)));
		
	    js.executeScript("arguments[0].click();",addToFavouriteButton);
	    (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Offer.REMOVE_FROM_FAVORITE_BUTTON)));
	}
	@Test(dependsOnMethods ="navigateToOfferOneMoreTime",alwaysRun = true)
	public void AccessToFavoriteByURL() {
		driver.get(getServerURL() + "/dashboard/favorite-offers");
	}
		
	@Test(dependsOnMethods ="AccessToFavoriteByURL",alwaysRun = true)
	public void CheckToSeeOfferInTheListAgain() {
		(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(Home.TABLE_BODY_XPATH)));
		WebElement cell= table.getCellObject(1, 2);
		Assert.assertTrue(cell.getAttribute("innerHTML").contains(offerNameStr),"Wrong expected Offer! Expected: "+ offerNameStr+ "Actual: " +cell.getAttribute("innerHTML"));
		Assert.assertEquals(table.countRow(Home.TABLE_BODY_XPATH,true),1,"Row count number is wrong!");
		
	}
	@Test(dependsOnMethods ="CheckToSeeOfferInTheListAgain",alwaysRun = true)
	public void ClickDeleteIconOnGrid() {
		table.clickDeleteIcon(3,1);
	}
	@Test(dependsOnMethods ="ClickDeleteIconOnGrid",alwaysRun = true)
	public void checkMyFavouriteOfferScreenWithOfferRemovedViaIcon() {
		WebElement alertMessageNoOfferInFavoriteList = new WebDriverWait(driver, timeout).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.ALERT_MESSAGE_NO_OFFER_EXIST)));
		Assert.assertEquals(alertMessageNoOfferInFavoriteList.getText(), Messages.ALERT_MESSAGE_NO_OFFER_EXIST);
		driver.findElement(By.cssSelector(Offer.RECOMMENDED_OFFER_LIST)).getText().startsWith(TextLabels.RECOMMENDED_OFFER);
	}
}
