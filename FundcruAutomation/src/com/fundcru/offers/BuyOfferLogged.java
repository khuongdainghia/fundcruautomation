package com.fundcru.offers;

import static org.testng.AssertJUnit.assertEquals;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.Donate;
import com.fundcru.objects.pagedefinitions.Offer;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;


@Credentials(user="khuongdainghia@gmail.com", password="loveyou")
public class BuyOfferLogged extends BaseTestCase {
	float campaignRaised=0;
	float donateAmountOffer=0;
	List<WebElement> numOfTestimonial;
	int numberBeforeDonate;
	String testImonialName="";
	String testImonial="";
	int timeout =30;
	WebElement offerAmount,orderTotal,offerQuantity,donateAmount;
	String offerAmountStr,orderTotalStr,donateAmountStr;
	@Test
	public void buyOfferAndGetTestimonialBeforeBuyingOfffer() {
		//Check CampainRasied before buying offer
		driver.get(getServerURL() + "/fundraisers/Zb2fqPRHLy6T2ipRe");
		WebElement campaignRaisedElement= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CAMPAIGN_RAISED)));
		campaignRaised= Float.valueOf(campaignRaisedElement.getText().replace("$", "").replace(".00", ""));
		numOfTestimonial = driver.findElements(By.xpath(Campaign.TESTIMONIAL_LIST));
	    numberBeforeDonate = numOfTestimonial.size();
	}
	@Test(dependsOnMethods ="buyOfferAndGetTestimonialBeforeBuyingOfffer" )
	public void openOffer() {
		//Navigate to offer
				driver.get(getServerURL() + "/offers/nqYCvH63StdFdPQSM");
	}
	@Test(dependsOnMethods ="openOffer" )
	public void clickBuyButton() {
		
		WebElement buyButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Offer.BUY_BUTTON)));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		
	    js.executeScript("arguments[0].click();",buyButton);
	}
	@Test(dependsOnMethods ="clickBuyButton" )
	public void checkOfferAmount() {    
	    offerQuantity= driver.findElement(By.cssSelector(Offer.OFFER_QUANTITY));
		offerAmount = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.TOTAL_AMOUNT)));
		offerAmountStr = offerAmount.getText();
		orderTotal = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.ORDER_TOTAL)));
		orderTotalStr = orderTotal.getText();
		float orderTotalFloat = Float.valueOf(orderTotalStr.substring(orderTotalStr.indexOf("$")+1, orderTotalStr.length()));
		donateAmount = driver.findElement(By.cssSelector(Offer.DONATE_AMOUNT));
		donateAmountStr = donateAmount.getText();
		donateAmountStr = donateAmountStr.substring(donateAmountStr.indexOf("$")+1, donateAmountStr.length());
		donateAmountOffer = Float.valueOf(donateAmountStr);
		offerQuantity.clear();
		offerQuantity.sendKeys("2");
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		donateAmountStr = donateAmount.getText();
		donateAmountStr = donateAmountStr.substring(donateAmountStr.indexOf("$")+1, donateAmountStr.length());
		Assert.assertEquals(Float.valueOf(donateAmountStr),donateAmountOffer*2);
		Assert.assertEquals(Float.valueOf(orderTotal.getText().replace("$", "").trim()),orderTotalFloat*2);
	}
	@Test(dependsOnMethods ="checkOfferAmount" )
	public void changeQuantityOfferToOne() { 
		offerQuantity.clear();
		offerQuantity.sendKeys("1");
		donateAmountStr = donateAmount.getText();
		donateAmountStr = donateAmountStr.substring(donateAmountStr.indexOf("$")+1, donateAmountStr.length());
	}
	@Test(dependsOnMethods ="changeQuantityOfferToOne" )
	public void changeTheCampaignFromList() { 
		WebElement changeCampaignLink= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.CHANGE_CAMPAIGN_LINK)));
		changeCampaignLink.click();
		WebElement searchCampaign= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.name(Offer.SEARCH_CAMPAIGN)));
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		searchCampaign.sendKeys("kid");
		(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.SEARCH_AUTOCOMPLETE_RESULT)));
		searchCampaign.sendKeys(Keys.DOWN);
		searchCampaign.sendKeys(Keys.ENTER);
		
	}
	@Test(dependsOnMethods ="changeTheCampaignFromList" )
	public void clickOnCheckoutButton() { 
		WebElement checkoutButton = driver.findElement(By.cssSelector(Offer.CHECKOUT_BUTTON));
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click();",checkoutButton);
	}
	@Test(dependsOnMethods ="clickOnCheckoutButton" )
	public void inputCardInformatioin() { 
	    WebElement cardNumber = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.name(Donate.CARD_NUMBER)));
	    cardNumber.clear();
	    cardNumber.sendKeys("4032038120259944");
	    driver.findElement(By.name("ccv")).clear();
	    driver.findElement(By.name("ccv")).sendKeys("123");
	    driver.findElement(By.name("expiration")).clear();
	    driver.findElement(By.name("expiration")).sendKeys("11/22");
	    driver.findElement(By.name("fullname")).clear();
	    driver.findElement(By.name("fullname")).sendKeys("Charlies");
	    driver.findElement(By.name("fullname")).clear();
	    driver.findElement(By.name("fullname")).sendKeys("Charlie");
	    driver.findElement(By.name("fullname")).clear();
	    driver.findElement(By.name("fullname")).sendKeys("Charlie Angles");
	    driver.findElement(By.xpath("//*[@id='wrapper']/div/div/main/div[1]/div[2]/form/div[5]/button[1]")).click();
	}
    @Test(dependsOnMethods ="inputCardInformatioin" )
	public void checkAgainAmountAndCardInformation() { 
	    WebElement amount = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.CONFIRM_OFFER_AMOUNT)));
	    assertEquals(amount.getText().replaceAll("$", "").replaceAll(".00", ""), offerAmountStr.replaceAll("$", "").replaceAll(".00", ""));
	    assertEquals(driver.findElement(By.cssSelector(Offer.CONFIRM_CARD)).getText(), "9944");
	    assertEquals(driver.findElement(By.cssSelector(Offer.CONFIRM_NAME)).getText(), "Charlie Angles");
	    driver.findElement(By.cssSelector(Offer.CONFIRM_BUTTON)).click();
    }
    @Test(dependsOnMethods ="checkAgainAmountAndCardInformation" )
	public void inputInformationForTestimonial() { 
	    WebElement donateName = (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.name(Donate.DONATE_NAME)));
	    Random r = new Random();
	    int i = r.nextInt(100);
	    donateName.clear();
	    testImonialName = String.format("Nicholas %s", i);
	    donateName.sendKeys(testImonialName);
	    driver.findElement(By.id("testimonial")).clear();
	    testImonial = String.format("Testimonial %s",i);
	    driver.findElement(By.id("testimonial")).sendKeys(testImonial);
	}
	@Test(dependsOnMethods ="inputInformationForTestimonial" )
	public void inputTestImonial() {
	    driver.findElement(By.cssSelector(Donate.SHARE_BUTTON)).click();
	}
	@Test(dependsOnMethods ="inputTestImonial" )
	public void checkDonatedByOfferSuccessful() {
			//check result here 
		driver.get(getServerURL() + "/fundraisers/Zb2fqPRHLy6T2ipRe");
		WebElement campaignTitle = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.TITLE_SUCCESS)));
		Assert.assertEquals(campaignTitle.getText(),"Soccer kid");
		WebElement campaignGoal = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.GOAL_SUCCESS)));
		Assert.assertEquals(campaignGoal.getText(),"$5000.00");
		WebElement campaignRaisedElement= driver.findElement(By.cssSelector(Campaign.CAMPAIGN_RAISED));
		numOfTestimonial = driver.findElements(By.xpath(Campaign.TESTIMONIAL_LIST));
		WebElement testimonialName = driver.findElement(By.cssSelector(Campaign.TESTIMONIAL_NAME));
		WebElement testimonialText = driver.findElement(By.cssSelector(Campaign.TESTIMONIAL_TEXT));
		Assert.assertEquals(testimonialName.getText(),testImonialName);
		Assert.assertEquals(testimonialText.getText(),testImonial);
//		Assert.assertEquals(numOfTestimonial.size(),numberBeforeDonate+1);
		Assert.assertEquals(campaignRaisedElement.getText(),String.format("$%s",campaignRaised+donateAmountOffer));
	}
}
