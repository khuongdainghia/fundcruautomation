package com.fundcru.offers;

import static org.testng.AssertJUnit.assertEquals;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.fundcru.objects.pagedefinitions.Campaign;
import com.fundcru.objects.pagedefinitions.Home;
import com.fundcru.objects.pagedefinitions.Offer;
import com.fundcru.objects.pages.ScreenAction;
import com.fundcru.utilities.BaseTestCase;
import com.fundcru.utilities.Credentials;


@Credentials(user="khuongdainghia@gmail.com", password="loveyou")
public class BuyOffer extends BaseTestCase {
	float campaignRaised=0;
	float donateAmountOffer=0;
	List<WebElement> numOfTestimonial;
	int numberBeforeDonate,numOfDonors;
	public static String testImonialName="";
	public static String testImonial="";
	String campaignName;
	String campaignGoalValue;
	int timeout =30;
	WebElement offerAmount,orderTotal,offerQuantity,donateAmount;
	String offerAmountStr;
	ScreenAction action;
	public static int noOrders;
	public static String offerNameStr,orderTotalStr,donateAmountStr,fundraiserID="YB2oCspPAfsP4xSZt";
	JavascriptExecutor executor;
	
	@Test
	public void AccessToMyOrders() {
		action = new ScreenAction(driver);
		executor = (JavascriptExecutor)driver;
		WebElement userLink = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.className(Home.USER_LINK_CLASS)));
		userLink.click();
		WebElement myOrder = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Home.MY_ORDERS_LINK)));
		myOrder.click();
	}
	
	@Test(dependsOnMethods ="AccessToMyOrders")
	public void NavigateToOrdersListScreen() {
		 WebElement fundraiserDonation = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.MY_ORDER_TEXT)));
		 Assert.assertEquals(fundraiserDonation.getText().trim(),"My orders");
		
	}
	@Test(dependsOnMethods ="NavigateToOrdersListScreen")
	public void CheckListOfOrders() {
		 List<WebElement> listOfOrder = driver.findElements(By.className(Offer.MY_ORDER_ITEM_CLASS));
		 noOrders = listOfOrder.size();
	}
	@Test(dependsOnMethods ="CheckListOfOrders",alwaysRun = true )
	public void buyOfferAndGetTestimonialBeforeBuyingOfffer() {
		//Check CampainRasied before buying offer
		driver.get(getServerURL() + "/fundraisers/" + fundraiserID);
		WebElement campaignRaisedElement= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CAMPAIGN_RAISED)));
		campaignRaised= Float.valueOf(campaignRaisedElement.getText().replace("$", "").replace(".00", "").replace(",", ""));
		campaignName = action.getTextFieldByCssSelector(Campaign.CAMPAIGN_TITLES);
		WebElement campaignGoal = driver.findElement(By.cssSelector(Campaign.GOAL_SUCCESS));
		campaignGoalValue = campaignGoal.getText();
		numOfTestimonial = driver.findElements(By.xpath(Campaign.TESTIMONIAL_LIST));
	    numberBeforeDonate = numOfTestimonial.size();
	    WebElement campaignDonors= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CAMPAIGN_DONORS)));
		numOfDonors = Integer.valueOf(campaignDonors.getText());
	}
	@Test(dependsOnMethods ="buyOfferAndGetTestimonialBeforeBuyingOfffer" )
	public void openOffer() {
		//Navigate to offer
				driver.get(getServerURL() + "/offers/25AidTuiy4RpuQwJH");
	}
	@Test(dependsOnMethods ="openOffer" )
	public void clickBuyButton() {
		
		WebElement buyButton = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Offer.BUY_BUTTON)));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		WebElement offerTitle = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.OFFER_TITLE)));
		offerNameStr= offerTitle.getText();
	    executor.executeScript("arguments[0].click();",buyButton);
	}
	@Test(dependsOnMethods ="clickBuyButton" )
	public void checkOfferAmount() {    
	    offerQuantity= (new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.OFFER_QUANTITY)));
		offerAmount = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.ITEM_PRICE)));
		offerAmountStr = offerAmount.getText();
		orderTotal = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.ORDER_TOTAL)));
		orderTotalStr = orderTotal.getText();
		float orderTotalFloat = Float.valueOf(orderTotalStr.substring(orderTotalStr.indexOf("$")+1, orderTotalStr.length()));
		donateAmount = driver.findElement(By.cssSelector(Offer.DONATE_AMOUNT));
		donateAmountStr = donateAmount.getText();
		donateAmountStr = donateAmountStr.substring(donateAmountStr.indexOf("$")+1, donateAmountStr.length());
		donateAmountOffer = Float.valueOf(donateAmountStr);
		offerQuantity.clear();
		offerQuantity.sendKeys("2");
		action.pause(2000);
		donateAmountStr = donateAmount.getText();
		donateAmountStr = donateAmountStr.substring(donateAmountStr.indexOf("$")+1, donateAmountStr.length());
		Assert.assertEquals(Float.valueOf(donateAmountStr),donateAmountOffer*2);
		Assert.assertEquals(Float.valueOf(orderTotal.getText().replace("$", "").trim()),orderTotalFloat*2);
	}
	@Test(dependsOnMethods ="checkOfferAmount" )
	public void changeQuantityOfferToOne() { 
		offerQuantity.clear();
		offerQuantity.sendKeys("1");
		donateAmountStr = donateAmount.getText();
		donateAmountStr = donateAmountStr.substring(donateAmountStr.indexOf("$")+1, donateAmountStr.length());
	}
	@Test(dependsOnMethods ="changeQuantityOfferToOne" )
	public void changeTheCampaignFromList() { 
		WebElement changeCampaignLink= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.CHANGE_CAMPAIGN_LINK)));
		changeCampaignLink.click();
		WebElement searchCampaign= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.name(Offer.SEARCH_CAMPAIGN)));
		action.pause(1000);
		searchCampaign.sendKeys(campaignName);
		action.pause(3000);
		(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.SEARCH_AUTOCOMPLETE_RESULT)));
		searchCampaign.sendKeys(Keys.DOWN);
		searchCampaign.sendKeys(Keys.ENTER);
		WebElement campaigNameLink = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.CAMPAIGN_NAME_LINK)));
		Assert.assertEquals(campaigNameLink.getText(), campaignName);
		
	}
	@Test(dependsOnMethods ="changeTheCampaignFromList" )
	public void clickOnCheckoutButton() { 
		WebElement checkoutButton = driver.findElement(By.id(Offer.CHECKOUT_BUTTON));
	    executor.executeScript("arguments[0].click();",checkoutButton);
	}
	@Test(dependsOnMethods ="clickOnCheckoutButton" )
	public void inputCardInformation() { 
		WebElement fullName = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.id(Offer.DONATE_NAME)));
	    fullName.clear();
	    fullName.sendKeys("Charlie Angles");
	    WebElement cardNumber = driver.findElement(By.id(Offer.CARD_NUMBER));
		cardNumber.clear();
	    cardNumber.sendKeys("4032038120259944");
	    WebElement ccv = driver.findElement(By.id(Offer.CVV));
	    ccv.clear();
	    ccv.sendKeys("123");
	    WebElement expiRation = driver.findElement(By.id(Offer.EXPIRATION));
	    expiRation.clear();
	    expiRation.sendKeys("11/22");
	    WebElement zipCode = driver.findElement(By.id(Offer.ZIPCODE));
	    zipCode.clear();
	    zipCode.sendKeys("12345");
	}
    @Test(dependsOnMethods ="inputCardInformation" )
	public void checkAgainAmountAndCardInformation() { 
    	WebElement amount = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.CONFIRM_OFFER_AMOUNT_CARD_SCREEN)));
 	    assertEquals(offerAmountStr.replaceAll("$", "").replaceAll(".00", ""),amount.getText().replaceAll("$", "").replaceAll(".00", ""));
    	WebElement donationAmount = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.CONFIRM_DONATION_AMOUNT_CARD_SCREEN)));
 	    assertEquals(donateAmountStr,donationAmount.getText().replace("$", "").replace(".00", ""));
    	WebElement quantity = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.CONFIRM_QUANTITY_CARD_SCREEN)));
 	    assertEquals("1",quantity.getText());
    	driver.findElement(By.cssSelector(Offer.SUBMIT_BUTTON_CARD_SCREEN)).click();
    }
    @Test(dependsOnMethods ="checkAgainAmountAndCardInformation" )
	public void inputInformationForTestimonial() { 
	    WebElement donateName = (new WebDriverWait(driver, timeout*2 )).until(ExpectedConditions.presenceOfElementLocated(By.id(Offer.TESTIMONIAL_NAME)));
	    Random r = new Random();
	    int i = r.nextInt(100);
	    donateName.clear();
	    testImonialName = String.format("Nicholas %s", i);
	    donateName.sendKeys(testImonialName);
	    WebElement testimonialElement= driver.findElement(By.id(Offer.TESTIMONIAL_TEXT));
	    testimonialElement.clear();
	    testImonial = String.format("Testimonial %s",i);
	    testimonialElement.sendKeys(testImonial);
	    WebElement testimonialEmailElement= driver.findElement(By.id(Offer.TESTIMONIAL_EMAIL));
	    testimonialEmailElement.clear();
	    testimonialEmailElement.sendKeys("khuongdainghia02t5@gmail.com");
	}
	@Test(dependsOnMethods ="inputInformationForTestimonial" )
	public void clickSubmitButton() {
	    driver.findElement(By.cssSelector(Offer.SUBMIT_TESTIMONIAL_BUTTON)).click();
	    
	}
	@Test(dependsOnMethods ="clickSubmitButton",alwaysRun = true )
	public void CheckToSeeItNavigateToOrdersListScreen() {
		(new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.VIEW_ORDER)));
		 WebElement fundraiserDonation =driver.findElement(By.cssSelector(Offer.MY_ORDER_TEXT));
		 Assert.assertEquals(fundraiserDonation.getText().trim(),"My orders");
		
	}
	@Test(dependsOnMethods ="CheckToSeeItNavigateToOrdersListScreen")
	public void VerifyOrderInfo() {
		 List<WebElement> listOfOrder= driver.findElements(By.className(Offer.MY_ORDER_ITEM_CLASS));
		 action.assertTextEqualCssSelector(Offer.MY_ORDERS_1_NAME, offerNameStr +" x1");
		 action.assertTextEqualCssSelector(Offer.MY_ORDERS_1_AMOUNT, orderTotalStr);
		 action.assertTextEqualCssSelector(Offer.MY_ORDERS_1_STATUS, "VALID");
//		 Assert.assertEquals(listOfOrder.size(), noOrders+1);
		 
		 
	}
	@Test(dependsOnMethods ="VerifyOrderInfo" )
	public void CheckOrderDetail() {
		WebElement viewOrder= driver.findElement(By.cssSelector(Offer.VIEW_ORDER));
		executor.executeScript("arguments[0].click();",viewOrder);
		WebElement offerName = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Offer.ORDETAIL_NAME)));
		WebElement offerTotal = driver.findElement(By.cssSelector(Offer.ORDETAIL_TOTAL));
		Assert.assertEquals(offerName.getText(),offerNameStr +" x1");
		Assert.assertTrue(offerTotal.getText().contains(orderTotalStr.replace(".00", "")), String.format("Wrong expected: %s Actual: %s",orderTotalStr, offerTotal.getText()));
		WebElement offerStatus = driver.findElement(By.cssSelector(Offer.ORDETAIL_STATUS));
		Assert.assertEquals(offerStatus.getText(),"VALID");
		
	}
	@Test(dependsOnMethods ="CheckOrderDetail",alwaysRun = true  )
	public void checkDonatedByOfferSuccessful() {
			//check result here 
		driver.get(getServerURL() + "/raise/kl9vJoD2M");
		WebElement campaignTitle = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.TITLE_SUCCESS)));
		Assert.assertEquals(campaignTitle.getText(),campaignName);
		WebElement campaignGoal = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.GOAL_SUCCESS)));
		Assert.assertEquals(campaignGoal.getText(),campaignGoalValue);
		action.waitObjInvisible(By.cssSelector(Campaign.CAMPAIGN_RAISED_PROGRESS));
		action.pause(30000);
		WebElement campaignRaisedElementAfter= (new WebDriverWait(driver, timeout)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(Campaign.CAMPAIGN_RAISED)));
		Assert.assertEquals(campaignRaisedElementAfter.getText().replace(",", "").replace(".00", ""),String.format("$%s",campaignRaised+donateAmountOffer).replace(".0", ""));
		WebElement campaignDonors= driver.findElement(By.cssSelector(Campaign.CAMPAIGN_DONORS));
		Assert.assertTrue(Integer.valueOf(campaignDonors.getText().trim())==(numOfDonors+1));
		WebElement testimonialName = driver.findElement(By.cssSelector(Campaign.TESTIMONIAL_NAME));
		WebElement testimonialText = driver.findElement(By.cssSelector(Campaign.TESTIMONIAL_TEXT));
		Assert.assertEquals(testimonialName.getText(),testImonialName);
		Assert.assertEquals(testimonialText.getText(),testImonial);
	}
}
